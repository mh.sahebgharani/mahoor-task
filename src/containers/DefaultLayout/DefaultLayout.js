import React, { Component, Suspense } from 'react';
import { Redirect, Route, Switch,BrowserRouter } from 'react-router-dom';
import { Container } from 'reactstrap';
import {
  AppAside,
  AppBreadcrumb,
  AppFooter,
  AppHeader,
  AppSidebar,
  AppSidebarFooter,
  AppSidebarForm,
  AppSidebarHeader,
  AppSidebarMinimizer,
  AppSidebarNav,
} from '@coreui/react';
// sidebar nav config
import navigation from '../../_nav';
import navigation2 from '../../_nav2';
// routes config
import routes from '../../routes';
import AccessManager from "../../classes/AccessManager";
import SweetFetcher from "../../classes/sweet-fetcher";
import Constants from "../../classes/Constants";
import Pusher from "pusher-js";
import {SweetAlert} from 'Libs/one/sweet-react-common-tools';
const DefaultAside = React.lazy(() => import('./DefaultAside'));
const DefaultFooter = React.lazy(() => import('./DefaultFooter'));
const DefaultHeader = React.lazy(() => import('./DefaultHeader'));

class DefaultLayout extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoggedIn: false,
            loginTime:0,
        };
    };

  loading = () => <div className="animated fadeIn pt-1 text-center">در حال بارگذاری...</div>

  signOut(e) {
    e.preventDefault();

      this.setState({isLoggedIn:false,loginTime:0});
      this.props.history.push('/login');
  }

    componentWillUnmount() {
      // try {
      //     if (this.eventSource != null) {
      //         if (typeof this.eventSource.removeAllListeners === "function")
      //             this.eventSource.removeAllListeners();
      //         if (typeof this.eventSource.close === "function")
      //             this.eventSource.close();
      //     }
      // }
      // catch (e) {
      //     console.log(e);
      // }
    }

    startPusherListener(){

        try {
            const pusher = new Pusher(Constants.PusherAppKey, {
                // cluster: 'ap2',
                cluster: Constants.Cluster,
                encrypted: true
            });
            const channel = pusher.subscribe('systemmessage');
            channel.bind('newsystemmessage', data => {
                try {
                    console.log(data);
                    // return;
                    const theData = JSON.parse(data.message);
                    const audioForGood = new Audio(Constants.SiteURL + "/alert.mp3");
                    const audioForBad = new Audio(Constants.SiteURL + "/alert.mp3");
                    if(theData.goodness>50)
                        audioForGood.play();
                    else
                        audioForBad.play();
                    var options = {
                        body: theData.message,
                        icon: require('../../assets/img/SweetLogo1-noborder.png'),
                        dir: "rtl",
                    };
                    var notification = new Notification(theData.title, options);
                    notification.onclick = function (event) {
                        event.preventDefault(); // prevent the browser from focusing the Notification's tab
                        window.open(theData.link, '_blank');
                    }
                }catch (e) {
                    SweetAlert.displayErrorAlert('خطا','خطایی در نمایش پیام های سیستمی به وجود آمد.')
                }

            });
        }catch (e) {

        }
    }
    componentDidMount() {
        if(AccessManager.UserIsLoggedIn()) {
            this.startPusherListener();
            Notification.requestPermission();
        }
    }
  render() {
      // if(!AccessManager.UserIsLoggedIn())
      //     this.props.history.push('/login');
      let {pathname} = this.props.location;
      if(pathname.substring(0,1)==="/")
          pathname=pathname.substring(1);
      pathname=pathname.replace(/\//g,' spath-');
      pathname="spath-"+pathname;
      new SweetFetcher().Fetch('/users/current',SweetFetcher.METHOD_GET,null,()=>{},null,'cmn_dfn',AccessManager.VIEW,this.props.history);
    return (

      <div className={"app "+pathname}>

          {/*<div className={'headerimagediv'} >*/}
              {/*<img src={header} className={'headerimage'} /></div>*/}
        <AppHeader fixed>
          <Suspense  fallback={this.loading()}>
            <DefaultHeader onLogout={e=>this.signOut(e)}/>
          </Suspense>
        </AppHeader>
        <div className="app-body">
          <AppSidebar fixed display="lg">
            <AppSidebarHeader />
            <AppSidebarForm />
            <Suspense>
            <AppSidebarNav navConfig={AccessManager.UserIsLoggedIn()?navigation:navigation2} {...this.props} />
            </Suspense>
            <AppSidebarFooter />
            <AppSidebarMinimizer />
          </AppSidebar>
          <main className="main">
            <AppBreadcrumb appRoutes={routes}/>
            <Container fluid>
              <Suspense fallback={this.loading()}>
                  <Switch>
                  {routes.map((route, idx) => {
                    return route.component ? (
                      <Route
                        key={idx}
                        path={route.path}
                        exact={route.exact}
                        name={route.name}
                        render={props => (
                          <route.component {...props} />
                        )} />
                    ) : (null);
                  })}

                </Switch>
              </Suspense>
            </Container>
          </main>
          <AppAside fixed>
            <Suspense fallback={this.loading()}>
              <DefaultAside />
            </Suspense>
          </AppAside>
        </div>
        <AppFooter>
          <Suspense fallback={this.loading()}>
            <DefaultFooter />
          </Suspense>
        </AppFooter>
      </div>
    );
  }
}

export default DefaultLayout;
