import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import 'font-awesome/css/font-awesome.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import './PersianDatePicker/styles/basic.css';
import 'mdbreact/dist/css/mdb.css';
import Loadable from 'react-loadable';
import './App.scss';
import Constants from "./classes/Constants";
import './index.css';
//
// import Echo from 'laravel-echo'
//
// window.Echo = new Echo({
//   broadcaster: 'pusher',
//   key: '2bd8ed2478cff707c231',
//   cluster: 'eu',
//   forceTLS: true
// });
//
// var channel = Echo.channel('my-channel');
// console.log('sd');
// channel.listen('.my-event', function(data) {
//   alert(JSON.stringify(data));
// });
const loading = () => <div className="animated fadeIn pt-3 text-center">در حال بارگذاری...</div>;
// Containers
const DefaultLayout = Loadable({
  loader: () => import('./containers/DefaultLayout'),
  loading
});

// Pages
const Login = Loadable({
  loader: () => import('./views/Pages/Login'),
  loading
});

const Config=new Constants();
const SweetAppConfig = React.createContext(Config);
global.ServerMode=Constants.ServerMode;
global.DeleteMethodMode=Constants.DeleteMethodMode;
global.SiteURL=Constants.SiteURL;
global.RequestTimeOut=Constants.RequestTimeOut;

class User extends Component {
  render() {
    return (
       <SweetAppConfig.Provider value={Config}>
         <BrowserRouter basename={'/panel'}>
           <Switch>
             <Route exact path="/login" name="Login Page" component={Login} />
             <Route path="/login/:type" name="Login Page" component={Login} />
             <Route path="/" name="Home" component={DefaultLayout} />
           </Switch>
         </BrowserRouter>
       </SweetAppConfig.Provider>
    );
  }
}

export default User;
