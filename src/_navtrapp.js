import AccessManager from "./classes/AccessManager";
let FinalItems=[];
let Items=[
    {
        name: 'ورود به سامانه',
        icon: 'fa fa-sign-in',
        badge: {
            variant: 'info',
        },
        url: '/login',
        access:!AccessManager.UserIsLoggedIn()
    },

    {
        name: 'اطلاعات پایه',
        icon: 'fa fa-database',
        badge: {
            variant: 'info',
        },
        access:AccessManager.UserIsLoggedIn(),
        children:[
            {
                name: 'کاربران',
                url: '/trapp/users',
                icon: 'fa fa-cog',
                badge: {
                    variant: 'info',
                },
                access:AccessManager.UserIsLoggedIn(),
            },
            {
                name: 'همه ویلاها',
                url: '/trapp/villa',
                icon: 'fa fa-cog',
                badge: {
                    variant: 'info',
                },
                access:AccessManager.UserIsLoggedIn(),
            },
            {
                name: 'ویلاهای منتظر تایید',
                url: '/trapp/villa/waiting',
                icon: 'fa fa-cog',
                badge: {
                    variant: 'info',
                },
                access:AccessManager.UserIsLoggedIn(),
            },
            {
                name: 'ویلاهای تایید شده',
                url: '/trapp/villa/accepted',
                icon: 'fa fa-cog',
                badge: {
                    variant: 'info',
                },
                access:AccessManager.UserIsLoggedIn(),
            },
        ]},


    {
        name: 'امور مالی',
        icon: 'fa fa-database',
        badge: {
            variant: 'info',
        },
        access:AccessManager.UserIsLoggedIn(),
        children:[
            {
                name: 'پرداخت ها',
                url: '/trapp/pays',
                icon: 'fa fa-cog',
                badge: {
                    variant: 'info',
                },
                access:AccessManager.UserIsLoggedIn(),
            },
            {
                name: 'درخواست های واریز وجه',
                url: '/trapp/user_withdrawal_requests',
                icon: 'fa fa-cog',
                badge: {
                    variant: 'info',
                },
                access:AccessManager.UserIsLoggedIn(),
            },
        ]},
    // {
    //     name: 'مدیریت',
    //     icon: 'fa fa-cogs',
    //     badge: {
    //         variant: 'info',
    //     },
    //     access:AccessManager.UserIsLoggedIn(),
    //     children:[
    //
    //         {
    //             name: 'تنظیمات',
    //             url: '/common/parameter',
    //             icon: 'fa fa-cog',
    //             badge: {
    //                 variant: 'info',
    //             },
    //             access:AccessManager.UserIsLoggedIn(),
    //         },
    //     ]},
    {
        name: 'تغییر رمز',
        url: '/changepass',
        icon: 'fa fa-lock',
        badge: {
            variant: 'info',
        },
        access:AccessManager.UserIsLoggedIn(),

    },
    {
        name: 'خروج',
        url: '/login/signout',
        icon: 'fa fa-sign-out',
        badge: {
            variant: 'info',
        },
        access:true

    }
];
for(let i=0;i<Items.length;i++)
{
    if(Items[i].access==true)
        FinalItems.push(Items[i]);
}
export default {
    items: FinalItems
};
