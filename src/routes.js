import React from 'react';
import DefaultLayout from './containers/DefaultLayout';
import EcommerceRoutes from "./modules/ecommerce/routes/EcommerceRoutes";
import LocationRoutes from "./modules/location/routes/LocationRoutes";
import CommonRoutes from "./modules/common/routes/CommonRoutes";
import MessagingRoutes from "./modules/messaging/routes/MessagingRoutes";
import FilesRoutes from "./modules/files/routes/FilesRoutes-pathassign";
import FinanceRoutes from "./modules/finance/routes/FinanceRoutes-transactionassign";
import ContentRoutes from "./modules/content/routes/ContentRoutes-pageassign";
import CrmRoutes from "./modules/crm/routes/CrmRoutes";
import Index from "./modules/users/pages/index";
import TrappRoutes from "./modules/trapp/routes/TrappRoutes";
let routes = [
  { path: '/', exact: true, name: 'صفحه اصلی', component: DefaultLayout, },
];

const ChangePass = React.lazy(() => import('./modules/users/pages/password/users_changepass'));
routes.push({ path: '/changepass',exact:true, name: 'تغییر رمز',component:ChangePass});
routes.push({ path: '/index',exact:true, name: 'صفحه اصلی',component:Index});


const common_simplefieldList = React.lazy(() => import('./modules/common/pages/simplefield/common_simplefieldList'));
routes.push({ path: '/common/simplefield',exact:true, name: 'لیست فیلدها',component:common_simplefieldList});
const common_simplefieldManage = React.lazy(() => import('./modules/common/pages/simplefield/Common_SimplefieldManage'));
const common_simplefieldView = React.lazy(() => import('./modules/common/pages/simplefield/common_simplefieldView'));
routes.push({ path: '/common/simplefield/management/:id',exact:false, name: 'ویرایش فیلد',component:common_simplefieldManage});
routes.push({ path: '/common/simplefield/management',exact:false, name: 'تعریف فیلد',component:common_simplefieldManage});
routes.push({ path: '/common/simplefield/view/:id',exact:false, name: 'فیلد',component:common_simplefieldView});

routes = routes.concat(EcommerceRoutes);
routes = routes.concat(LocationRoutes);
routes = routes.concat(CommonRoutes);
routes = routes.concat(MessagingRoutes);
routes = routes.concat(FinanceRoutes);
routes = routes.concat(FilesRoutes);
routes = routes.concat(ContentRoutes);
routes = routes.concat(CrmRoutes);
routes = routes.concat(TrappRoutes);
export default routes;
