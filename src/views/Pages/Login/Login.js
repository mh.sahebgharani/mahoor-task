import React, {Component} from 'react';
import moment from 'moment-jalaali'
import {SweetButton} from 'Libs/one/sweet-react-components';
import {
    Card,
    CardBody,
    CardGroup,
    Col,
    Container,
    Form,
    Input,
    InputGroup,
    InputGroupAddon,
    InputGroupText,
    Row
} from 'reactstrap';
import SweetFetcher from "../../../classes/sweet-fetcher";
import Common from "../../../classes/Common";
import Constants from "../../../classes/Constants";

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {

            username: '',
            password: '',
        };
        localStorage.setItem('redirecting','0');

    }
    componentDidMount() {
        if(this.props.match.params.type==='signout'){
            localStorage.clear();
        }
    }

    render() {
        return (
            <div className="app flex-row align-items-center">
                <Container>
                    <Row className="justify-content-center">
                        <Col md="8">
                            <CardGroup>
                                <Card className="p-4">
                                    <CardBody>
                                        <Form>
                                            <h1>ورود به سامانه
                                            </h1>
                                            <p className="text-muted"></p>
                                            <InputGroup className="mb-3">
                                                <InputGroupAddon addonType="prepend">
                                                    <InputGroupText>
                                                        <i className="icon-user"></i>
                                                    </InputGroupText>
                                                </InputGroupAddon>
                                                <Input type="text" placeholder="نام کاربری" onChange={(e) => {
                                                    this.setState({'username': e.target.value})
                                                }} autoComplete="username"
                                                       value={this.state.username}/>
                                            </InputGroup>
                                            <InputGroup className="mb-4">
                                                <InputGroupAddon addonType="prepend">
                                                    <InputGroupText>
                                                        <i className="icon-lock"></i>
                                                    </InputGroupText>
                                                </InputGroupAddon>
                                                <Input type="password" placeholder="کلمه عبور" onChange={(e) => {
                                                    this.setState({'password': e.target.value})
                                                }} autoComplete="current-password"
                                                value={this.state.password}/>
                                            </InputGroup>
                                            <Row>
                                                <Col xs="6">
                                                    <SweetButton color="primary" value={'ورود'} onButtonPress={(onEnd) => {
                                                        const data = new URLSearchParams();
                                                        data.append('name', this.state.username);
                                                        data.append('password', this.state.password);
                                                        data.append('forceLogin', true);
                                                        data.append('appName', 'Panel');
                                                        localStorage.clear();
                                                        new SweetFetcher().Fetch('/users/login', SweetFetcher.METHOD_POST, data, data => {
                                                            localStorage.setItem('sessionkey', data.Data.sessionkey);
                                                            localStorage.setItem('userdisplayname', data.Data.displayname);
                                                            localStorage.setItem('userroles', data.Data.roles);
                                                            localStorage.setItem('userlogintime', moment().locale('fa').format('HH:mm'));
                                                            let access=Common.convertObjectPropertiesToLowerCase(data.Data.access);
                                                            let key, keys = Object.keys(access);
                                                            let n = keys.length;
                                                            while (n--) {
                                                                key = keys[n];
                                                                localStorage.setItem('access.'+access[key].name,true);
                                                            }
                                                            if (data.Data.sessionkey.length > 2)
                                                            {
                                                                localStorage.setItem('redirecting', '1');
                                                                window.location.href='/panel/index';

                                                            }
                                                            else
                                                                alert("اطلاعات کاربری صحیح نمی باشد.");

                                                            onEnd(true);

                                                        },()=>{
                                                            onEnd(false);},'users','load',this.props.history);
                                                    }}/>
                                                </Col>
                                                <Col xs="6" className="text-right">
                                                </Col>
                                            </Row>
                                        </Form>
                                    </CardBody>
                                </Card>
                                <Card className="text-white bg-primary py-5 d-md-down-none" style={{width: '44%'}}>
                                    <CardBody className="text-center">

                                        <div>
                                            <img src={Constants.LoginLogo} className={'loginlogo'}/>
                                                {/*<h2>سامانه مدیریت آنلاین</h2>*/}
                                            {/*<h2>گروه نرم افزاری Sweet</h2>*/}
                                            {/*<p>ویرایش 99/03</p>*/}
                                        </div>
                                    </CardBody>
                                </Card>
                            </CardGroup>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}

export default Login;
