import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import 'font-awesome/css/font-awesome.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import './modules/common/files/styles/systemmessage.css';
import 'mdbreact/dist/css/mdb.css';
import Loadable from 'react-loadable';
import './App.scss';
import Constants from "./classes/Constants";

import './index.css';

const loading = () => <div className="animated fadeIn pt-3 text-center">در حال بارگذاری...</div>;
// Containers
const DefaultLayout = Loadable({
  loader: () => import('./containers/DefaultLayout'),
  loading
});

// Pages
const Login = Loadable({
  loader: () => import('./views/Pages/Login'),
  loading
});

const Config=new Constants();
const SweetAppConfig = React.createContext(Config);
global.ServerMode=Constants.ServerMode;
global.SiteURL=Constants.SiteURL;
global.Debugging=Constants.Debugging;
global.RequestTimeOut=Constants.RequestTimeOut;
global.onFetchError=Constants.onFetchError;
class Admin extends Component {
  render() {
    return (
       <SweetAppConfig.Provider value={Config}>
      <BrowserRouter >
          <Switch>
            <Route exact path="/login" name="Login Page" component={Login} />
            <Route path="/" name="Home" component={DefaultLayout} />
          </Switch>
      </BrowserRouter>
       </SweetAppConfig.Provider>
    );
  }
}

export default Admin;
