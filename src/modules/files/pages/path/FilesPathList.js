// @flow
import * as React from 'react';
import Constants from '../../../../classes/Constants';
import PathEntity from "../../entity/PathEntity";
import {ListPageContainer,SweetManageListPage} from "Libs/one/sweet-one-react";


export default class FilesPathList extends SweetManageListPage {
    moduleName='files';
    tableName='path';
    entity = new PathEntity();
    componentDidMount() {
        super.componentDidMount();
        this.LoadData(this.state.pageSize,1,null,null);
    }
    render() {
        return <ListPageContainer title={'لیست مسیرها'}>

            {this.getAddButton()}
            {this.getTable()}
        </ListPageContainer>;
    }
    columns =
    [
        {Header: 'مسیر',id: 'path',accessor: 'path'},
        // {Header: 'در نظر گرفتن زیرشاخه ها',id: 'is_recursive',accessor: data => data.isRecursive.displayname},
        // {Header: 'قابلیت مشاهده',id: 'can_read',accessor: data => data.canRead.displayname},
        // {Header: 'قابلیت نوشتن',id: 'can_write',accessor: data => data.canWrite.displayname},
        // {Header: 'امکان حذف',id: 'can_delete',accessor: data => data.canDelete.displayname},
        // {Header: 'امکان ایجاد',id: 'can_create',accessor: data => data.canCreate.displayname},
        {Header: 'الگو',id: 'wildcard',accessor: 'wildcard'},
        {Header: 'نام نمایشی',id: 'name',accessor: 'name'},
        {Header: 'عملیات',accessor: 'id',Cell: props => {return this.getOperationsBox(props.value)}}
    ];
}
