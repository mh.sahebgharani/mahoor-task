// @flow
import * as React from 'react';
import {TextBox,PickerBox,FileSelector,ImageSelector,TimeSelectorBox} from "Libs/one/sweet-react-components";
import {ManagePageFieldsContainer,ManagePageContainer,SweetManagePage} from "Libs/one/sweet-one-react";
import PathEntity from "../../entity/PathEntity";


export default class FileEditor extends SweetManagePage {
    moduleName='files';
    tableName='path';
    constructor(props) {
        super(props);
        this.state = {
            canEdit:this.canEdit(),
            formData:new PathEntity(),
        };
    }

    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.getFileContent(this.getID(), () => {this.notifyFormDataChanged();});

        this.loadSimpleFields();

    }
    render(){
        return <ManagePageContainer title={'ویرایش فایل'}>
            <textarea className={'ltrtext fileeditor'} value={this.state.formData.fileContent}
                onChange={(text)=>{
                    let listener=this.getOnTextChangeListener('fileContent');
                    listener(text.target.value);
                }}/>
        {this.getSaveAndBackButtons()}
        </ManagePageContainer>
    }

    onSave=(OnEnd)=>{this.state.formData.saveFile(this.getID(),res=>{
        console.log(res);
        OnEnd(res);
        this.returnBack();
        return res;
    },OnEnd)};

    returnBack(){
        this.props.history.push('/' + this.moduleName + '/user' + this.tableName);
    }
}
