// @flow
import * as React from 'react';
import {TextViewBox,ImageModal} from "Libs/one/sweet-react-components";
import {ManagePageContainer,ManagePageFieldsContainer,SweetManagePage} from "Libs/one/sweet-one-react";
import Constants from "../../../../classes/Constants";
import PathEntity from "../../entity/PathEntity";

export default class FilesPathView extends SweetManagePage {
    moduleName='files';
    tableName='path';
    constructor(props) {
        super(props);
        this.state = {
            formData:new PathEntity(),

        };
    }

    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

    }
    render(){
        return <ManagePageContainer title={'تعریف مسیر'}>
        <ManagePageFieldsContainer>
            <TextViewBox title={'مسیر'} value={this.state.formData.path}/>
            <TextViewBox title={'در نظر گرفتن زیرشاخه ها'} value={this.state.formData.isRecursive.displayname}/>
            <TextViewBox title={'قابلیت مشاهده'} value={this.state.formData.canRead.displayname}/>
            <TextViewBox title={'قابلیت نوشتن'} value={this.state.formData.canWrite.displayname}/>
            <TextViewBox title={'امکان حذف'} value={this.state.formData.canDelete.displayname}/>
            <TextViewBox title={'امکان ایجاد'} value={this.state.formData.canCreate.displayname}/>
            <TextViewBox title={'الگو'} value={this.state.formData.wildcard}/>
            <TextViewBox title={'نام نمایشی'} value={this.state.formData.name}/>

        </ManagePageFieldsContainer>
        {this.getSaveAndBackButtons()}
        </ManagePageContainer>
    }
}
