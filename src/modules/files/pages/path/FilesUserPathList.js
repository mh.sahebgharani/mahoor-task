// @flow
import * as React from 'react';
import PathEntity from "../../entity/PathEntity";
import {ListPageContainer,SweetManageListPage} from "Libs/one/sweet-one-react";
import {FaEdit} from "react-icons/all";
import {Link} from "react-router-dom";


export default class FilesUserPathList extends SweetManageListPage {
    moduleName='files';
    tableName='path';
    entity = new PathEntity();
    componentDidMount() {
        super.componentDidMount();
        this.LoadData(this.state.pageSize,1,null,null);
    }
    render() {
        return <ListPageContainer title={'لیست مسیرها'}>
            {this.getTable()}
        </ListPageContainer>;
    }
    columns =
    [
        // {Header: 'مسیر',id: 'path',accessor: 'path'},
        // {Header: 'در نظر گرفتن زیرشاخه ها',id: 'is_recursive',accessor: data => data.isRecursive.displayname},
        // {Header: 'قابلیت مشاهده',id: 'can_read',accessor: data => data.canRead.displayname},
        // {Header: 'قابلیت نوشتن',id: 'can_write',accessor: data => data.canWrite.displayname},
        // {Header: 'امکان حذف',id: 'can_delete',accessor: data => data.canDelete.displayname},
        // {Header: 'امکان ایجاد',id: 'can_create',accessor: data => data.canCreate.displayname},
        // {Header: 'الگو',id: 'wildcard',accessor: 'wildcard'},
        {Header: 'نام فایل',id: 'name',accessor: 'name'},
        {Header: 'عملیات',accessor: 'id',Cell: props => {return <div className={'operationsrow'}><Link className={'viewlink'} to={this.getFileEditLink(props.value)}><FaEdit/></Link></div>}}
    ];

    getFileEditLink(id){
        return '/'+this.moduleName+'/'+this.tableName+'/editfile/'+id;
    }
}
