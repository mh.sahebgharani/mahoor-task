// @flow
import * as React from 'react';
import {TextBox,PickerBox,FileSelector,ImageSelector,TimeSelectorBox} from "Libs/one/sweet-react-components";
import {ManagePageFieldsContainer,ManagePageContainer,SweetManagePage} from "Libs/one/sweet-one-react";
import PathEntity from "../../entity/PathEntity";


export default class FilesPathManage extends SweetManagePage {
    moduleName='files';
    tableName='path';
    constructor(props) {
        super(props);
        this.state = {
            canEdit:this.canEdit(),
            formData:new PathEntity(),

        };
    }

    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

        this.loadSimpleFields();

    }
    render(){
        return <ManagePageContainer title={'تعریف مسیر'}>
        <ManagePageFieldsContainer>
            <TextBox title={'مسیر'} id='path' className={'ltrtext'} value={this.state.formData.path}
                onChangeText={this.getOnTextChangeListener('path')}/>
            <PickerBox
                title={'در نظر گرفتن زیرشاخه ها'}
                selectedValue={this.state.formData.isRecursive}
                onValueChange={this.getOnBooleanChangeListener('isRecursive')}
                options={[{id:0,name:'خیر'},{id:1,name:'بله'}]}
            />
            <PickerBox
                title={'قابلیت مشاهده'}
                selectedValue={this.state.formData.canRead}
                onValueChange={this.getOnBooleanChangeListener('canRead')}
                options={[{id:0,name:'خیر'},{id:1,name:'بله'}]}
            />
            <PickerBox
                title={'قابلیت نوشتن'}
                selectedValue={this.state.formData.canWrite}
                onValueChange={this.getOnBooleanChangeListener('canWrite')}
                options={[{id:0,name:'خیر'},{id:1,name:'بله'}]}
            />
            <PickerBox
                title={'امکان حذف'}
                selectedValue={this.state.formData.canDelete}
                onValueChange={this.getOnBooleanChangeListener('canDelete')}
                options={[{id:0,name:'خیر'},{id:1,name:'بله'}]}
            />
            <PickerBox
                title={'امکان ایجاد'}
                selectedValue={this.state.formData.canCreate}
                onValueChange={this.getOnBooleanChangeListener('canCreate')}
                options={[{id:0,name:'خیر'},{id:1,name:'بله'}]}
            />
            <TextBox title={'الگو'} id='wildcard' value={this.state.formData.wildcard}
                onChangeText={this.getOnTextChangeListener('wildcard')}/>
            <TextBox title={'نام نمایشی'} id='name' value={this.state.formData.name}
                onChangeText={this.getOnTextChangeListener('name')}/>


        </ManagePageFieldsContainer>
        {this.getSaveAndBackButtons()}
        </ManagePageContainer>
    }
}
