
import FilesPathList from '../pages/path/FilesPathList';
import FilesPathManage from '../pages/path/FilesPathManage';
import FilesPathView from '../pages/path/FilesPathView';
import FilesUserPathList from "../pages/path/FilesUserPathList";
import FileEditor from "../pages/path/FileEditor";
let FilesPathRoutes=[];
FilesPathRoutes.push({ path: '/files/path',exact:true, name: 'لیست مسیرها',component:FilesPathList});
FilesPathRoutes.push({ path: '/files/userpath',exact:true, name: 'لیست فایلها',component:FilesUserPathList});
FilesPathRoutes.push({ path: '/files/path/management/:id',exact:false, name: 'ویرایش مسیر',component:FilesPathManage});
FilesPathRoutes.push({ path: '/files/path/editfile/:id',exact:false, name: 'ویرایش فایل',component:FileEditor});
FilesPathRoutes.push({ path: '/files/path/management',exact:false, name: 'تعریف مسیر',component:FilesPathManage});
FilesPathRoutes.push({ path: '/files/path/view/:id',exact:false, name: 'مسیر',component:FilesPathView});
export default FilesPathRoutes;
