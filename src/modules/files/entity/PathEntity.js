// @flow
import {SweetEntity,AccessManager} from 'Libs/one/sweet-one-react';

export default class PathEntity extends SweetEntity {
    __moduleName='files';
    __tableName='path';

    path='';
    isRecursive='';
    canRead='';
    canWrite='';
    canDelete='';
    canCreate='';
    wildcard='';
    name='';
    fileContent='';
    fields={id:{field:'id'},path:{field:'path'},isRecursive:{field:'is_recursive'},canRead:{field:'can_read'},canWrite:{field:'can_write'},canDelete:{field:'can_delete'},canCreate:{field:'can_create'},wildcard:{field:'wildcard'},name:{field:'name'},fileContent:{field:'filecontent'}};
    get(id,afterFetch){
        this._get(this.getBaseURL()+'/'+id,this.getServiceName(),AccessManager.VIEW,afterFetch);
    }
    getFileContent(id,afterFetch){
        this._get(this.getBaseURL()+'/filecontent/'+id,this.getServiceName(),AccessManager.VIEW,afterFetch);
    }
    getAll(pageSize,page,sorted,filtered,afterFetch,onError){
        const filterAndSortString=this.getFilterString(pageSize,page,sorted,filtered);
        const url=this.getBaseURL()+filterAndSortString;
        this._getAll(url,this.getServiceName(),AccessManager.LIST,afterFetch,onError);
    }
    save(id,afterFetchListener,onError){
        const formData=this.getFormRequest();
        this._save(this.getServiceName(),this.getBaseURL(),id,formData,afterFetchListener,onError);
    }
    saveFile(id,afterFetchListener,onError){
        const formData=this.getFormRequest();
        this._save(this.getServiceName(),this.getBaseURL()+'/savefile',id,formData,afterFetchListener,onError);
    }
    delete(id,afterFetchListener,onError){
        this._delete(this.getServiceName(),this.getBaseURL()+"/"+id,afterFetchListener,onError);
    }
}
