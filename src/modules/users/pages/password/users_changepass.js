// @flow
import * as React from 'react';
import { MDBContainer, MDBRow, MDBCol, MDBBtn } from 'mdbreact';
import SweetComponent from '../../../../classes/sweet-component';
import {TextBox,SweetButton} from "Libs/one/sweet-react-components";
import {AccessManager,SweetFetcher} from "Libs/one/sweet-one-react";
import {SweetAlert} from "Libs/one/sweet-react-common-tools";
class users_changepass extends SweetComponent {
    constructor(props) {
        super(props);
        this.state = {
            canEdit: true,
            oldpass: '',
            newpass1: '',
            newpass2: '',
        };
    }
    render(){
        return <MDBContainer>
            <MDBRow>
                <MDBCol md='6'>
                    <form>
                        <p className='h5 text-center mb-4'>تغییر رمز</p>

                        <TextBox title={'رمز فعلی'} value={this.state.oldpass} onChangeText={(txt)=>{this.setState({oldpass:txt})}}/>
                            <TextBox title={'رمز جدید'} value={this.state.newpass1} onChangeText={(txt)=>{this.setState({newpass1:txt})}}/>
                            <TextBox title={'تکرار رمز جدید'} value={this.state.newpass2} onChangeText={(txt)=>{this.setState({newpass2:txt})}}/>

                            <div className='text-center'>
                                <SweetButton value={'تغییر رمز'}
                                    onButtonPress={(onEnd) => {
                                        if(this.state.newpass1===this.state.newpass2)
                                        {
                                            let method = SweetFetcher.METHOD_PUT;
                                            let action = AccessManager.EDIT;
                                            const data = new FormData();

                                            data.append('oldpass', this.state.oldpass);
                                            data.append('newpass', this.state.newpass1.trim());
                                            new SweetFetcher().Fetch('/users/changepass', method, data,
                                                res => {
                                                    SweetAlert.displaySimpleAlert('تعییر رمز', res.message);
                                                    onEnd(true);
                                                    //console.log(res);
                                                }, (error) => {
                                                    let status = error.response.status;
                                                    onEnd(true);
                                                    SweetAlert.displaySimpleAlert('خطای پیش بینی نشده', 'خطایی در ذخیره اطلاعات به وجود آمد' + status.toString().trim());

                                                },
                                                'users.changepass', action,
                                                this.props.history);
                                        }
                                        else {
                                            SweetAlert.displaySimpleAlert('تعییر رمز', "رمز جدید با تکرار آن یکسان نیست");
                                            onEnd(true);
                                        }

                                    }
                                }/>

                        </div>
                    </form>
                </MDBCol>
            </MDBRow>
        </MDBContainer>
    }
}

export default users_changepass;
