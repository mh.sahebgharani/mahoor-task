
// @flow
import * as React from 'react';
import { MDBContainer, MDBRow, MDBCol } from 'mdbreact';
import {SweetPage} from "Libs/one/sweet-react-components";
import AryaIndex from "./aryaIndex";
import Constants from "../../../../classes/Constants";
import '../../Files/style/index.scss'
class Index extends SweetPage {
    render(){
        return <MDBContainer className={'main_index_menu'}>
            {Constants.menuName==='arya' &&
            <AryaIndex/>
            }
        </MDBContainer>
    }
}

export default Index;
