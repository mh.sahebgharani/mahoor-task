// @flow
import * as React from 'react';
import { MDBContainer, MDBRow, MDBCol } from 'mdbreact';
import {Link} from 'react-router-dom';
import {SweetPage} from "Libs/one/sweet-react-components";
class AryaIndex extends SweetPage {
    render(){
        return <MDBContainer className={'main_index_menu'}>
            <MDBRow>
                <MDBCol md='6'>
                    <Link to={'ecommerce/order/manual/management'}><div className={'main_index_menu_item'}>
                    <img className={'main_index_menu_item_icon'} src={require('./img/manual.png')}/>
                        <div className={'main_index_menu_item_title'}>
                            ثبت سفارش سالن
                        </div>
                    </div></Link>
                </MDBCol>
                <MDBCol md='6'>
                    <Link to={'ecommerce/order/manualdelivery/management'}><div className={'main_index_menu_item'}>
                        <img className={'main_index_menu_item_icon'} src={require('./img/delivery.png')}/>
                        <div className={'main_index_menu_item_title'}>
                            ثبت سفارش پیک
                        </div>
                    </div></Link>
                </MDBCol>
                <MDBCol md='6'>
                    <Link to={'ecommerce/order/new/0/2'}><div className={'main_index_menu_item'}>
                        <img className={'main_index_menu_item_icon'} src={require('./img/online-order.png')}/>
                        <div className={'main_index_menu_item_title'}>
                            سفارشات جدید
                        </div>
                    </div></Link>
                </MDBCol>
            </MDBRow>
        </MDBContainer>
    }
}

export default AryaIndex;
