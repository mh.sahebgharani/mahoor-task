// @flow
import * as React from 'react';
import Constants from '../../../../classes/Constants';
import ServicestatusEntity from "../../entity/ServicestatusEntity";
import {SweetManageListPage} from "Libs/one/sweet-one-react";

export default class CrmServicestatusListController extends SweetManageListPage {
    moduleName='crm';
    tableName='servicestatus';
    entity = new ServicestatusEntity();
    componentDidMount() {
        super.componentDidMount();
    }
}
