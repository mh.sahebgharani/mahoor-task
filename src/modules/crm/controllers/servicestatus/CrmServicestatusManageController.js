// @flow
import * as React from 'react';
import {SweetManagePage} from "Libs/one/sweet-one-react";
import ServicestatusEntity from "../../entity/ServicestatusEntity";


export default class CrmServicestatusManageController extends SweetManagePage {
    moduleName='crm';
    tableName='servicestatus';
    constructor(props) {
        super(props);
        this.state = {
            canEdit:this.canEdit(),
            formData:new ServicestatusEntity(),

        };
    }

    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

        this.loadSimpleFields();

    }
}
