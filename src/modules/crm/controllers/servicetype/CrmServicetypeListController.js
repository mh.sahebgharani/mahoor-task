// @flow
import * as React from 'react';
import Constants from '../../../../classes/Constants';
import ServicetypeEntity from "../../entity/ServicetypeEntity";
import {SweetManageListPage} from "Libs/one/sweet-one-react";

export default class CrmServicetypeListController extends SweetManageListPage {
    moduleName='crm';
    tableName='servicetype';
    entity = new ServicetypeEntity();
    componentDidMount() {
        super.componentDidMount();
    }
}
