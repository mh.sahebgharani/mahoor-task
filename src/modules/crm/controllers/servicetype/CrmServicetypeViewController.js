// @flow
import * as React from 'react';
import {TextViewBox,ImageModal} from "Libs/one/sweet-react-components";
import {ManagePageContainer,ManagePageFieldsContainer,SweetManagePage} from "Libs/one/sweet-one-react";
import Constants from "../../../../classes/Constants";
import ServicetypeEntity from "../../entity/ServicetypeEntity";


export default class CrmServicetypeView extends SweetManagePage {
    moduleName='crm';
    tableName='servicetype';
    constructor(props) {
        super(props);
        this.state = {
            formData:new ServicetypeEntity(),

        };
    }
    
    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

    }
}
