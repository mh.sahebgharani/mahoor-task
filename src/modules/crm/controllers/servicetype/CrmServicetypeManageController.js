// @flow
import * as React from 'react';
import {SweetManagePage} from "Libs/one/sweet-one-react";
import ServicetypeEntity from "../../entity/ServicetypeEntity";


export default class CrmServicetypeManageController extends SweetManagePage {
    moduleName='crm';
    tableName='servicetype';
    constructor(props) {
        super(props);
        this.state = {
            canEdit:this.canEdit(),
            formData:new ServicetypeEntity(),

        };
    }
    
    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

        this.loadSimpleFields();

    }
}
