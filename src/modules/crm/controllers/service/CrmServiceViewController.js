// @flow
import * as React from 'react';
import {TextViewBox,ImageModal} from "Libs/one/sweet-react-components";
import {ManagePageContainer,ManagePageFieldsContainer,SweetDependentManagePage} from "Libs/one/sweet-one-react";
import Constants from "../../../../classes/Constants";
import ServiceEntity from "../../entity/ServiceEntity";


export default class CrmServiceView extends SweetDependentManagePage {
    moduleName='crm';
    tableName='service';
    __ownerTable='project';
    constructor(props) {
        super(props);
        this.state = {
            formData:new ServiceEntity(this.getOwnerID()),

        };
    }

    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

    }
}
