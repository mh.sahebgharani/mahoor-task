// @flow
import * as React from 'react';
import {SweetDependentManagePage} from "Libs/one/sweet-one-react";
import ServiceEntity from "../../entity/ServiceEntity";
import HandlingpriorityEntity from '../../entity/HandlingpriorityEntity';import ServicerequesttypeEntity from '../../entity/ServicerequesttypeEntity';import ServicevalueEntity from '../../entity/ServicevalueEntity';import PaymenttypeEntity from '../../entity/PaymenttypeEntity';import ServicetypeEntity from '../../entity/ServicetypeEntity';

export default class CrmServiceManageController extends SweetDependentManagePage {
    moduleName='crm';
    tableName='service';
    __ownerTable='project';
    constructor(props) {
        super(props);
        this.state = {
            canEdit:this.canEdit(),
            formData:new ServiceEntity(this.getOwnerID()),

			handlingpriorityOptions:new HandlingpriorityEntity(),
			serviceRequestTypeOptions:new ServicerequesttypeEntity(),
			serviceValueOptions:new ServicevalueEntity(),
			paymentTypeOptions:new PaymenttypeEntity(),
			serviceTypeOptions:new ServicetypeEntity(),
        };
    }
    
    loadHandlingprioritys = () => {
        this.state.handlingpriorityOptions.getAll(null,null,null,null,(data)=>{
                this.setState({handlingpriorityOptions:data});
            },null);
    };
                
    loadServiceRequestTypes = () => {
        this.state.serviceRequestTypeOptions.getAll(null,null,null,null,(data)=>{
                this.setState({serviceRequestTypeOptions:data});
            },null);
    };
                
    loadServiceValues = () => {
        this.state.serviceValueOptions.getAll(null,null,null,null,(data)=>{
                this.setState({serviceValueOptions:data});
            },null);
    };
                
    loadPaymentTypes = () => {
        this.state.paymentTypeOptions.getAll(null,null,null,null,(data)=>{
                this.setState({paymentTypeOptions:data});
            },null);
    };
                
    loadServiceTypes = () => {
        this.state.serviceTypeOptions.getAll(null,null,null,null,(data)=>{
                this.setState({serviceTypeOptions:data});
            },null);
    };
                
    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

        this.loadHandlingprioritys();
        this.loadServiceRequestTypes();
        this.loadServiceValues();
        this.loadPaymentTypes();
        this.loadServiceTypes();
        this.loadSimpleFields();

    }
}
