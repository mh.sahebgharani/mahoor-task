// @flow
import * as React from 'react';
import Constants from '../../../../classes/Constants';
import ServiceEntity from "../../entity/ServiceEntity";
import {SweetManageDepenedentListPage} from "Libs/one/sweet-one-react";

export default class CrmServiceListController extends SweetManageDepenedentListPage {
    moduleName='crm';
    tableName='service';__defaultFilters=[{id:'project_fid',value:this.getOwnerID()}]
    entity = new ServiceEntity();
    componentDidMount() {
        super.componentDidMount();
    }
}
