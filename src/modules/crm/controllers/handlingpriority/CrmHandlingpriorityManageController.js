// @flow
import * as React from 'react';
import {SweetManagePage} from "Libs/one/sweet-one-react";
import HandlingpriorityEntity from "../../entity/HandlingpriorityEntity";


export default class CrmHandlingpriorityManageController extends SweetManagePage {
    moduleName='crm';
    tableName='handlingpriority';
    constructor(props) {
        super(props);
        this.state = {
            canEdit:this.canEdit(),
            formData:new HandlingpriorityEntity(),

        };
    }

    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

        this.loadSimpleFields();

    }
}
