// @flow
import * as React from 'react';
import Constants from '../../../../classes/Constants';
import HandlingpriorityEntity from "../../entity/HandlingpriorityEntity";
import {SweetManageListPage} from "Libs/one/sweet-one-react";

export default class CrmHandlingpriorityListController extends SweetManageListPage {
    moduleName='crm';
    tableName='handlingpriority';
    entity = new HandlingpriorityEntity();
    componentDidMount() {
        super.componentDidMount();
    }
}
