// @flow
import * as React from 'react';
import {TextViewBox,ImageModal} from "Libs/one/sweet-react-components";
import {ManagePageContainer,ManagePageFieldsContainer,SweetManagePage} from "Libs/one/sweet-one-react";
import Constants from "../../../../classes/Constants";
import HandlingpriorityEntity from "../../entity/HandlingpriorityEntity";


export default class CrmHandlingpriorityView extends SweetManagePage {
    moduleName='crm';
    tableName='handlingpriority';
    constructor(props) {
        super(props);
        this.state = {
            formData:new HandlingpriorityEntity(),

        };
    }

    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

    }
}
