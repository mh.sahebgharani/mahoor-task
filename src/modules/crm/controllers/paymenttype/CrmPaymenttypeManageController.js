// @flow
import * as React from 'react';
import {SweetManagePage} from "Libs/one/sweet-one-react";
import PaymenttypeEntity from "../../entity/PaymenttypeEntity";


export default class CrmPaymenttypeManageController extends SweetManagePage {
    moduleName='crm';
    tableName='paymenttype';
    constructor(props) {
        super(props);
        this.state = {
            canEdit:this.canEdit(),
            formData:new PaymenttypeEntity(),

        };
    }

    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

        this.loadSimpleFields();

    }
}
