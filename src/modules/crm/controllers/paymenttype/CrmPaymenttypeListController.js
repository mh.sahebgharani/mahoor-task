// @flow
import * as React from 'react';
import Constants from '../../../../classes/Constants';
import PaymenttypeEntity from "../../entity/PaymenttypeEntity";
import {SweetManageListPage} from "Libs/one/sweet-one-react";

export default class CrmPaymenttypeListController extends SweetManageListPage {
    moduleName='crm';
    tableName='paymenttype';
    entity = new PaymenttypeEntity();
    componentDidMount() {
        super.componentDidMount();
    }
}
