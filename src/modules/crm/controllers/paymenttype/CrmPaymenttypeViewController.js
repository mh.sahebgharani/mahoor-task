// @flow
import * as React from 'react';
import {TextViewBox,ImageModal} from "Libs/one/sweet-react-components";
import {ManagePageContainer,ManagePageFieldsContainer,SweetManagePage} from "Libs/one/sweet-one-react";
import Constants from "../../../../classes/Constants";
import PaymenttypeEntity from "../../entity/PaymenttypeEntity";


export default class CrmPaymenttypeView extends SweetManagePage {
    moduleName='crm';
    tableName='paymenttype';
    constructor(props) {
        super(props);
        this.state = {
            formData:new PaymenttypeEntity(),

        };
    }

    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

    }
}
