// @flow
import * as React from 'react';
import Constants from '../../../../classes/Constants';
import ServicerequesttypeEntity from "../../entity/ServicerequesttypeEntity";
import {SweetManageListPage} from "Libs/one/sweet-one-react";

export default class CrmServicerequesttypeListController extends SweetManageListPage {
    moduleName='crm';
    tableName='servicerequesttype';
    entity = new ServicerequesttypeEntity();
    componentDidMount() {
        super.componentDidMount();
    }
}
