// @flow
import * as React from 'react';
import {TextViewBox,ImageModal} from "Libs/one/sweet-react-components";
import {ManagePageContainer,ManagePageFieldsContainer,SweetManagePage} from "Libs/one/sweet-one-react";
import Constants from "../../../../classes/Constants";
import ServicerequesttypeEntity from "../../entity/ServicerequesttypeEntity";


export default class CrmServicerequesttypeView extends SweetManagePage {
    moduleName='crm';
    tableName='servicerequesttype';
    constructor(props) {
        super(props);
        this.state = {
            formData:new ServicerequesttypeEntity(),

        };
    }

    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

    }
}
