// @flow
import * as React from 'react';
import {SweetManagePage} from "Libs/one/sweet-one-react";
import ServicerequesttypeEntity from "../../entity/ServicerequesttypeEntity";


export default class CrmServicerequesttypeManageController extends SweetManagePage {
    moduleName='crm';
    tableName='servicerequesttype';
    constructor(props) {
        super(props);
        this.state = {
            canEdit:this.canEdit(),
            formData:new ServicerequesttypeEntity(),

        };
    }

    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

        this.loadSimpleFields();

    }
}
