// @flow
import * as React from 'react';
import {SweetManagePage} from "Libs/one/sweet-one-react";
import CustomerEntity from "../../entity/CustomerEntity";
import CompanyEntity from '../../entity/CompanyEntity';

export default class CrmCustomerManageController extends SweetManagePage {
    moduleName='crm';
    tableName='customer';
    constructor(props) {
        super(props);
        this.state = {
            canEdit:this.canEdit(),
            formData:new CustomerEntity(),

			companyOptions:new CompanyEntity(),
        };
    }

    loadCompanys = () => {
        this.state.companyOptions.getAll(null,null,null,null,(data)=>{
                this.setState({companyOptions:data});
            },null);
    };

    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

        this.loadCompanys();
        this.loadSimpleFields();

    }
}
