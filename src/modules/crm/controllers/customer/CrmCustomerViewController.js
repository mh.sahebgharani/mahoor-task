// @flow
import * as React from 'react';
import {TextViewBox,ImageModal} from "Libs/one/sweet-react-components";
import {ManagePageContainer,ManagePageFieldsContainer,SweetManagePage} from "Libs/one/sweet-one-react";
import Constants from "../../../../classes/Constants";
import CustomerEntity from "../../entity/CustomerEntity";


export default class CrmCustomerView extends SweetManagePage {
    moduleName='crm';
    tableName='customer';
    constructor(props) {
        super(props);
        this.state = {
            formData:new CustomerEntity(),

        };
    }

    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

    }
}
