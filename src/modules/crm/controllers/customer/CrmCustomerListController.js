// @flow
import * as React from 'react';
import Constants from '../../../../classes/Constants';
import CustomerEntity from "../../entity/CustomerEntity";
import {SweetManageListPage} from "Libs/one/sweet-one-react";

export default class CrmCustomerListController extends SweetManageListPage {
    moduleName='crm';
    tableName='customer';
    entity = new CustomerEntity();
    componentDidMount() {
        super.componentDidMount();
    }
}
