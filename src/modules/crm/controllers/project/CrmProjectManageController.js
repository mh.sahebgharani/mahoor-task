// @flow
import * as React from 'react';
import {SweetManagePage} from "Libs/one/sweet-one-react";
import ProjectEntity from "../../entity/ProjectEntity";
export default class CrmProjectManageController extends SweetManagePage {
    moduleName='crm';
    tableName='project';
    constructor(props) {
        super(props);
        this.state = {
            canEdit:this.canEdit(),
            formData:new ProjectEntity(),

        };
    }

    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

        this.loadSimpleFields();

    }
}
