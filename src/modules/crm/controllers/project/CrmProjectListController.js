// @flow
import * as React from 'react';
import Constants from '../../../../classes/Constants';
import ProjectEntity from "../../entity/ProjectEntity";
import {SweetManageListPage} from "Libs/one/sweet-one-react";

export default class CrmProjectListController extends SweetManageListPage {
    moduleName='crm';
    tableName='project';
    entity = new ProjectEntity();
    componentDidMount() {
        super.componentDidMount();
    }
}
