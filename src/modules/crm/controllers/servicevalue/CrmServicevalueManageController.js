// @flow
import * as React from 'react';
import {SweetManagePage} from "Libs/one/sweet-one-react";
import ServicevalueEntity from "../../entity/ServicevalueEntity";


export default class CrmServicevalueManageController extends SweetManagePage {
    moduleName='crm';
    tableName='servicevalue';
    constructor(props) {
        super(props);
        this.state = {
            canEdit:this.canEdit(),
            formData:new ServicevalueEntity(),

        };
    }

    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

        this.loadSimpleFields();

    }
}
