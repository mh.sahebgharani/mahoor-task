// @flow
import * as React from 'react';
import {TextViewBox,ImageModal} from "Libs/one/sweet-react-components";
import {ManagePageContainer,ManagePageFieldsContainer,SweetManagePage} from "Libs/one/sweet-one-react";
import Constants from "../../../../classes/Constants";
import ServicevalueEntity from "../../entity/ServicevalueEntity";


export default class CrmServicevalueView extends SweetManagePage {
    moduleName='crm';
    tableName='servicevalue';
    constructor(props) {
        super(props);
        this.state = {
            formData:new ServicevalueEntity(),

        };
    }

    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

    }
}
