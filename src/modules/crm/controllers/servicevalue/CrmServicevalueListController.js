// @flow
import * as React from 'react';
import Constants from '../../../../classes/Constants';
import ServicevalueEntity from "../../entity/ServicevalueEntity";
import {SweetManageListPage} from "Libs/one/sweet-one-react";

export default class CrmServicevalueListController extends SweetManageListPage {
    moduleName='crm';
    tableName='servicevalue';
    entity = new ServicevalueEntity();
    componentDidMount() {
        super.componentDidMount();
    }
}
