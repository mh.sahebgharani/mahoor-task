// @flow
import * as React from 'react';
import Constants from '../../../../classes/Constants';
import CompanyEntity from "../../entity/CompanyEntity";
import {SweetManageListPage} from "Libs/one/sweet-one-react";

export default class CrmCompanyListController extends SweetManageListPage {
    moduleName='crm';
    tableName='company';
    entity = new CompanyEntity();
    componentDidMount() {
        super.componentDidMount();
    }
}
