// @flow
import * as React from 'react';
import {TextViewBox,ImageModal} from "Libs/one/sweet-react-components";
import {ManagePageContainer,ManagePageFieldsContainer,SweetManagePage} from "Libs/one/sweet-one-react";
import Constants from "../../../../classes/Constants";
import CompanyEntity from "../../entity/CompanyEntity";


export default class CrmCompanyView extends SweetManagePage {
    moduleName='crm';
    tableName='company';
    constructor(props) {
        super(props);
        this.state = {
            formData:new CompanyEntity(),

        };
    }

    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

    }
}
