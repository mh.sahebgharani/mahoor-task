// @flow
import * as React from 'react';
import {SweetManagePage} from "Libs/one/sweet-one-react";
import CompanyEntity from "../../entity/CompanyEntity";


export default class CrmCompanyManageController extends SweetManagePage {
    moduleName='crm';
    tableName='company';
    constructor(props) {
        super(props);
        this.state = {
            canEdit:this.canEdit(),
            formData:new CompanyEntity(),

        };
    }
    
    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

        this.loadSimpleFields();

    }
}
