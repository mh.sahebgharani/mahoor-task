// @flow
import * as React from 'react';
import {TextBox,PickerBox,FileSelector,ImageSelector,TimeSelectorBox} from "Libs/one/sweet-react-components";
import {ManagePageFieldsContainer,ManagePageContainer} from "Libs/one/sweet-one-react";
import CrmServiceManageController from "../../controllers/service/CrmServiceManageController";
import HandlingpriorityEntity from '../../entity/HandlingpriorityEntity';import ServicerequesttypeEntity from '../../entity/ServicerequesttypeEntity';import ServicevalueEntity from '../../entity/ServicevalueEntity';import PaymenttypeEntity from '../../entity/PaymenttypeEntity';import ServicetypeEntity from '../../entity/ServicetypeEntity';

export default class CrmServiceManage extends CrmServiceManageController {
    render(){
        return <ManagePageContainer title={'تعریف خدمت'}>
        <ManagePageFieldsContainer>
            <PickerBox
                title={'اولویت رسیدگی'}
                selectedValue={this.state.formData.handlingpriority.id}
                onValueChange={this.getOnFidValueChangeListener('handlingpriority')}
                options={this.state.handlingpriorityOptions}
            />
            <TextBox multiline={true} colweight={2} title={'شرح خدمات ارائه شده'} id='descriptionAfterAccomplish' value={this.state.formData.descriptionAfterAccomplish}
                onChangeText={this.getOnTextChangeListener('descriptionAfterAccomplish')}/>
            <PickerBox
                title={'نوع درخواست خدمت'}
                selectedValue={this.state.formData.serviceRequestType.id}
                onValueChange={this.getOnFidValueChangeListener('serviceRequestType')}
                options={this.state.serviceRequestTypeOptions}
            />
            <PickerBox
                title={'میزان پیچیدگی سرویس'}
                selectedValue={this.state.formData.serviceValue.id}
                onValueChange={this.getOnFidValueChangeListener('serviceValue')}
                options={this.state.serviceValueOptions}
            />
            <PickerBox
                title={'نوع تسویه حساب'}
                selectedValue={this.state.formData.paymentType.id}
                onValueChange={this.getOnFidValueChangeListener('paymentType')}
                options={this.state.paymentTypeOptions}
            />
            <PickerBox
                title={'نوع خدمت'}
                selectedValue={this.state.formData.serviceType.id}
                onValueChange={this.getOnFidValueChangeListener('serviceType')}
                options={this.state.serviceTypeOptions}
            />
            <TextBox multiline={true} colweight={2} title={'شرح'} id='text' value={this.state.formData.text}
                onChangeText={this.getOnTextChangeListener('text')}/>
            <TextBox title={'عنوان'} id='title' value={this.state.formData.title}
                onChangeText={this.getOnTextChangeListener('title')}/>
        <TextBox title={'دقایق صرف شده'} id='spentMinutes' value={this.state.formData.spentMinutes}
         onChangeText={this.getOnTextChangeListener('spentMinutes')} keyboardType='numeric'/>


        </ManagePageFieldsContainer>
        {this.getSaveAndBackButtons()}
        </ManagePageContainer>
    }
}
