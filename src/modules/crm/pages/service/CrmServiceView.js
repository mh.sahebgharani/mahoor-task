// @flow
import * as React from 'react';
import {TextViewBox,ImageModal} from "Libs/one/sweet-react-components";
import {ManagePageContainer,ManagePageFieldsContainer,SweetDependentManagePage} from "Libs/one/sweet-one-react";
import Constants from "../../../../classes/Constants";
import ServiceEntity from "../../entity/ServiceEntity";
import CrmServiceViewController from "../../controllers/service/CrmServiceViewController";


export default class CrmServiceView extends CrmServiceViewController {
    render(){
        return <ManagePageContainer title={'تعریف خدمت'}>
        <ManagePageFieldsContainer>
            <TextViewBox title={'اولویت رسیدگی'} value={this.state.formData.handlingpriority.displayname}/>
            <TextViewBox title={'شرح خدمات ارائه شده'} value={this.state.formData.descriptionAfterAccomplish}/>
            <TextViewBox title={'نوع درخواست خدمت'} value={this.state.formData.serviceRequestType.displayname}/>
            <TextViewBox title={'میزان پیچیدگی سرویس'} value={this.state.formData.serviceValue.displayname}/>
            <TextViewBox title={'هزینه'} value={this.state.formData.price}/>
            <TextViewBox title={'نوع تسویه حساب'} value={this.state.formData.paymentType.displayname}/>
            <TextViewBox title={'نوع خدمت'} value={this.state.formData.serviceType.displayname}/>
            <TextViewBox title={'شرح'} value={this.state.formData.text}/>
            <TextViewBox title={'عنوان'} value={this.state.formData.title}/>
            <TextViewBox title={'پروژه'} value={this.state.formData.project.displayname}/>

        </ManagePageFieldsContainer>
        {this.getSaveAndBackButtons()}
        </ManagePageContainer>
    }
}
