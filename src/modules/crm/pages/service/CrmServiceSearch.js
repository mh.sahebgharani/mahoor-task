// @flow
import * as React from 'react';
import {TextBox,PickerBox} from "Libs/one/sweet-react-components";
import {SearchBoxContainer,SweetSearchBox} from "Libs/one/sweet-one-react";
import ServiceEntity from "../../entity/ServiceEntity";
import HandlingpriorityEntity from '../../entity/HandlingpriorityEntity';import ServicerequesttypeEntity from '../../entity/ServicerequesttypeEntity';import ServicevalueEntity from '../../entity/ServicevalueEntity';import PaymenttypeEntity from '../../entity/PaymenttypeEntity';import ServicetypeEntity from '../../entity/ServicetypeEntity';
export default class CrmServiceSearch extends SweetSearchBox {
    moduleName='crm';
    tableName='service';
    constructor(props) {
        super(props);
        this.state = {
            searchParams:{},
            formData:new ServiceEntity(),

			handlingpriorityOptions:new HandlingpriorityEntity(),
			serviceRequestTypeOptions:new ServicerequesttypeEntity(),
			serviceValueOptions:new ServicevalueEntity(),
			paymentTypeOptions:new PaymenttypeEntity(),
			serviceTypeOptions:new ServicetypeEntity(),
        };
    }
    
    loadHandlingprioritys = () => {
        this.state.handlingpriorityOptions.getAll(null,null,null,null,(data)=>{
                this.setState({handlingpriorityOptions:data});
            },null);
    };
                
    loadServiceRequestTypes = () => {
        this.state.serviceRequestTypeOptions.getAll(null,null,null,null,(data)=>{
                this.setState({serviceRequestTypeOptions:data});
            },null);
    };
                
    loadServiceValues = () => {
        this.state.serviceValueOptions.getAll(null,null,null,null,(data)=>{
                this.setState({serviceValueOptions:data});
            },null);
    };
                
    loadPaymentTypes = () => {
        this.state.paymentTypeOptions.getAll(null,null,null,null,(data)=>{
                this.setState({paymentTypeOptions:data});
            },null);
    };
                
    loadServiceTypes = () => {
        this.state.serviceTypeOptions.getAll(null,null,null,null,(data)=>{
                this.setState({serviceTypeOptions:data});
            },null);
    };
                
    componentDidMount() {
        super.componentDidMount();

        this.loadHandlingprioritys();
        this.loadServiceRequestTypes();
        this.loadServiceValues();
        this.loadPaymentTypes();
        this.loadServiceTypes();
        this.loadSimpleFields();

    }
    render(){
        return <SearchBoxContainer title={'جستجوی خدمات'} onConfirm={this.getOnConfirm()}>

            <PickerBox
                title={'اولویت رسیدگی'}
                selectedValue={this.state.searchParams.handlingpriority_fid}
                onValueChange={this.getOnFidValueChangeListener('handlingpriority_fid')}
                options={this.state.handlingpriorityOptions}
            />

            <PickerBox
                title={'نوع درخواست خدمت'}
                selectedValue={this.state.searchParams.servicerequesttype_fid}
                onValueChange={this.getOnFidValueChangeListener('servicerequesttype_fid')}
                options={this.state.serviceRequestTypeOptions}
            />

            <PickerBox
                title={'میزان پیچیدگی سرویس'}
                selectedValue={this.state.searchParams.servicevalue_fid}
                onValueChange={this.getOnFidValueChangeListener('servicevalue_fid')}
                options={this.state.serviceValueOptions}
            />

            <PickerBox
                title={'نوع تسویه حساب'}
                selectedValue={this.state.searchParams.paymenttype_fid}
                onValueChange={this.getOnFidValueChangeListener('paymenttype_fid')}
                options={this.state.paymentTypeOptions}
            />

            <PickerBox
                title={'نوع خدمت'}
                selectedValue={this.state.searchParams.servicetype_fid}
                onValueChange={this.getOnFidValueChangeListener('servicetype_fid')}
                options={this.state.serviceTypeOptions}
            />

         <TextBox title={'عنوان'} id='title'
                value={this.state.searchParams.title}
                onChangeText={this.getOnTextChangeListener('title')}/>

        </SearchBoxContainer>
    }
}
