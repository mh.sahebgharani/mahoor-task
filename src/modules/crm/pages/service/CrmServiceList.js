// @flow
import * as React from 'react';
import {DependentListPageContainer} from "Libs/one/sweet-one-react";
import CrmServiceListController from "../../controllers/service/CrmServiceListController";
import CrmServiceSearch from './CrmServiceSearch';

export default class CrmServiceList extends CrmServiceListController {
    render() {
        return <DependentListPageContainer title={'لیست خدمات'}>
            <CrmServiceSearch onConfirm={this.searchData}/>
            {this.getAddButton()}
            {this.getTable()}
        </DependentListPageContainer>;
    }
    columns =
    [
        {Header: 'نوع خدمت',id: 'servicetype_fid',accessor: data => data.serviceType.displayname},
        {Header: 'شرح',id: 'text',accessor: 'text'},
        {Header: 'عنوان',id: 'title',accessor: 'title'},
        {Header: 'عملیات',accessor: 'id',Cell: props => {return this.getOperationsBox(props.value)}}
    ];
}
