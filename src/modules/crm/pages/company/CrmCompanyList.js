// @flow
import * as React from 'react';
import {ListPageContainer} from "Libs/one/sweet-one-react";
import CrmCompanyListController from "../../controllers/company/CrmCompanyListController";
import CrmCompanySearch from './CrmCompanySearch';

export default class CrmCompanyList extends CrmCompanyListController {
    render() {
        return <ListPageContainer title={'لیست شرکت ها'}>
            <CrmCompanySearch onConfirm={this.searchData}/>
            {this.getAddButton()}
            {this.getTable()}
        </ListPageContainer>;
    }
    columns =
    [
        {Header: 'عنوان فارسی',id: 'displayname',accessor: 'displayName'},
        {Header: 'نام انگلیسی',id: 'name',accessor: 'name'},
        {Header: 'عملیات',accessor: 'id',Cell: props => {return this.getOperationsBox(props.value)}}
    ];
}
