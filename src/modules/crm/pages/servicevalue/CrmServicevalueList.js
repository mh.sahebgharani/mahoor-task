// @flow
import * as React from 'react';
import {ListPageContainer} from "Libs/one/sweet-one-react";
import CrmServicevalueListController from "../../controllers/servicevalue/CrmServicevalueListController";
import CrmServicevalueSearch from './CrmServicevalueSearch';

export default class CrmServicevalueList extends CrmServicevalueListController {
    render() {
        return <ListPageContainer title={'لیست ارزش های خدمت'}>
            <CrmServicevalueSearch onConfirm={this.searchData}/>
            {this.getAddButton()}
            {this.getTable()}
        </ListPageContainer>;
    }
    columns =
    [
        {Header: 'ارزش',id: 'value_num',accessor: 'value'},
        {Header: 'عنوان فارسی',id: 'displayname',accessor: 'displayName'},
        {Header: 'نام انگلیسی',id: 'name',accessor: 'name'},
        {Header: 'عملیات',accessor: 'id',Cell: props => {return this.getOperationsBox(props.value)}}
    ];
}
