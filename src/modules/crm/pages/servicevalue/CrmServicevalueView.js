// @flow
import * as React from 'react';
import {TextViewBox,ImageModal} from "Libs/one/sweet-react-components";
import {ManagePageContainer,ManagePageFieldsContainer,SweetManagePage} from "Libs/one/sweet-one-react";
import Constants from "../../../../classes/Constants";
import ServicevalueEntity from "../../entity/ServicevalueEntity";
import CrmServicevalueViewController from "../../controllers/servicevalue/CrmServicevalueViewController";


export default class CrmServicevalueView extends CrmServicevalueViewController {
    render(){
        return <ManagePageContainer title={'تعریف ارزش خدمت'}>
        <ManagePageFieldsContainer>
            <TextViewBox title={'ارزش'} value={this.state.formData.value}/>
            <TextViewBox title={'عنوان فارسی'} value={this.state.formData.displayName}/>
            <TextViewBox title={'نام انگلیسی'} value={this.state.formData.name}/>

        </ManagePageFieldsContainer>
        {this.getSaveAndBackButtons()}
        </ManagePageContainer>
    }
}
