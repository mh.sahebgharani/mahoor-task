// @flow
import * as React from 'react';
import {TextBox,PickerBox,FileSelector,ImageSelector,TimeSelectorBox} from "Libs/one/sweet-react-components";
import {ManagePageFieldsContainer,ManagePageContainer} from "Libs/one/sweet-one-react";
import CrmServicevalueManageController from "../../controllers/servicevalue/CrmServicevalueManageController";


export default class CrmServicevalueManage extends CrmServicevalueManageController {
    render(){
        return <ManagePageContainer title={'تعریف ارزش خدمت'}>
        <ManagePageFieldsContainer>
        <TextBox title={'ارزش'} id='value' value={this.state.formData.value}
         onChangeText={this.getOnTextChangeListener('value')} keyboardType='numeric'/>
            <TextBox title={'عنوان فارسی'} id='displayName' value={this.state.formData.displayName}
                onChangeText={this.getOnTextChangeListener('displayName')}/>
            <TextBox title={'نام انگلیسی'} id='name' value={this.state.formData.name}
                onChangeText={this.getOnTextChangeListener('name')}/>


        </ManagePageFieldsContainer>
        {this.getSaveAndBackButtons()}
        </ManagePageContainer>
    }
}
