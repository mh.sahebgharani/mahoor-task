// @flow
import * as React from 'react';
import {ListPageContainer} from "Libs/one/sweet-one-react";
import CrmServicerequesttypeListController from "../../controllers/servicerequesttype/CrmServicerequesttypeListController";
import CrmServicerequesttypeSearch from './CrmServicerequesttypeSearch';

export default class CrmServicerequesttypeList extends CrmServicerequesttypeListController {
    render() {
        return <ListPageContainer title={'لیست انواع درخواست سرویس'}>
            <CrmServicerequesttypeSearch onConfirm={this.searchData}/>
            {this.getAddButton()}
            {this.getTable()}
        </ListPageContainer>;
    }
    columns =
    [
        {Header: 'عنوان فارسی',id: 'displayname',accessor: 'displayName'},
        {Header: 'نام انگلیسی',id: 'name',accessor: 'name'},
        {Header: 'عملیات',accessor: 'id',Cell: props => {return this.getOperationsBox(props.value)}}
    ];
}
