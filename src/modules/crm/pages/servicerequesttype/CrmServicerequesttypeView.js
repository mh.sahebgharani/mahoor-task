// @flow
import * as React from 'react';
import {TextViewBox,ImageModal} from "Libs/one/sweet-react-components";
import {ManagePageContainer,ManagePageFieldsContainer,SweetManagePage} from "Libs/one/sweet-one-react";
import Constants from "../../../../classes/Constants";
import ServicerequesttypeEntity from "../../entity/ServicerequesttypeEntity";
import CrmServicerequesttypeViewController from "../../controllers/servicerequesttype/CrmServicerequesttypeViewController";


export default class CrmServicerequesttypeView extends CrmServicerequesttypeViewController {
    render(){
        return <ManagePageContainer title={'تعریف نوع درخواست سرویس'}>
        <ManagePageFieldsContainer>
            <TextViewBox title={'عنوان فارسی'} value={this.state.formData.displayName}/>
            <TextViewBox title={'نام انگلیسی'} value={this.state.formData.name}/>

        </ManagePageFieldsContainer>
        {this.getSaveAndBackButtons()}
        </ManagePageContainer>
    }
}
