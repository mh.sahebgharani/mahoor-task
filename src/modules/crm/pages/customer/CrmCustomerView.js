// @flow
import * as React from 'react';
import {TextViewBox,ImageModal} from "Libs/one/sweet-react-components";
import {ManagePageContainer,ManagePageFieldsContainer,SweetManagePage} from "Libs/one/sweet-one-react";
import Constants from "../../../../classes/Constants";
import CustomerEntity from "../../entity/CustomerEntity";
import CrmCustomerViewController from "../../controllers/customer/CrmCustomerViewController";


export default class CrmCustomerView extends CrmCustomerViewController {
    render(){
        return <ManagePageContainer title={'تعریف مشتری'}>
        <ManagePageFieldsContainer>
            <TextViewBox title={'شرکت'} value={this.state.formData.company.displayname}/>

        </ManagePageFieldsContainer>
        {this.getSaveAndBackButtons()}
        </ManagePageContainer>
    }
}
