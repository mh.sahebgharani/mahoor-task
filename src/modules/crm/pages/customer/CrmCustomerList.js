// @flow
import * as React from 'react';
import {ListPageContainer} from "Libs/one/sweet-one-react";
import CrmCustomerListController from "../../controllers/customer/CrmCustomerListController";
import CrmCustomerSearch from './CrmCustomerSearch';

export default class CrmCustomerList extends CrmCustomerListController {
    render() {
        return <ListPageContainer title={'لیست مشتریان'}>
            <CrmCustomerSearch onConfirm={this.searchData}/>
            {this.getAddButton()}
            {this.getTable()}
        </ListPageContainer>;
    }
    columns =
    [
        {Header: 'شرکت',id: 'company_fid',accessor: data => data.company.displayname},
        {Header: 'عملیات',accessor: 'id',Cell: props => {return this.getOperationsBox(props.value)}}
    ];
}
