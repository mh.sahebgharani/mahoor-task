// @flow
import * as React from 'react';
import {TextBox,PickerBox} from "Libs/one/sweet-react-components";
import {SearchBoxContainer,SweetSearchBox} from "Libs/one/sweet-one-react";
import CustomerEntity from "../../entity/CustomerEntity";
import CompanyEntity from '../../entity/CompanyEntity';
export default class CrmCustomerSearch extends SweetSearchBox {
    moduleName='crm';
    tableName='customer';
    constructor(props) {
        super(props);
        this.state = {
            searchParams:{},
            formData:new CustomerEntity(),

			companyOptions:new CompanyEntity(),
        };
    }
    
    loadCompanys = () => {
        this.state.companyOptions.getAll(null,null,null,null,(data)=>{
                this.setState({companyOptions:data});
            },null);
    };
                
    componentDidMount() {
        super.componentDidMount();

        this.loadCompanys();
        this.loadSimpleFields();

    }
    render(){
        return <SearchBoxContainer title={'جستجوی مشتریان'} onConfirm={this.getOnConfirm()}>

            <PickerBox
                title={'شرکت'}
                selectedValue={this.state.searchParams.company_fid}
                onValueChange={this.getOnFidValueChangeListener('company_fid')}
                options={this.state.companyOptions}
            />

        </SearchBoxContainer>
    }
}
