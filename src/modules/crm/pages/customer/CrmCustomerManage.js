// @flow
import * as React from 'react';
import {TextBox,PickerBox,FileSelector,ImageSelector,TimeSelectorBox} from "Libs/one/sweet-react-components";
import {ManagePageFieldsContainer,ManagePageContainer} from "Libs/one/sweet-one-react";
import CrmCustomerManageController from "../../controllers/customer/CrmCustomerManageController";
import CompanyEntity from '../../entity/CompanyEntity';

export default class CrmCustomerManage extends CrmCustomerManageController {
    render(){
        return <ManagePageContainer title={'تعریف مشتری'}>
        <ManagePageFieldsContainer>
            <PickerBox
                title={'شرکت'}
                selectedValue={this.state.formData.company.id}
                onValueChange={this.getOnFidValueChangeListener('company')}
                options={this.state.companyOptions}
            />


        </ManagePageFieldsContainer>
        {this.getSaveAndBackButtons()}
        </ManagePageContainer>
    }
}
