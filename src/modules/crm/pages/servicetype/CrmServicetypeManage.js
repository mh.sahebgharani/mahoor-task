// @flow
import * as React from 'react';
import {TextBox,PickerBox,FileSelector,ImageSelector,TimeSelectorBox} from "Libs/one/sweet-react-components";
import {ManagePageFieldsContainer,ManagePageContainer} from "Libs/one/sweet-one-react";
import CrmServicetypeManageController from "../../controllers/servicetype/CrmServicetypeManageController";


export default class CrmServicetypeManage extends CrmServicetypeManageController {
    render(){
        return <ManagePageContainer title={'تعریف نوع خدمت'}>
        <ManagePageFieldsContainer>
            <TextBox title={'عنوان فارسی'} id='displayName' value={this.state.formData.displayName}
                onChangeText={this.getOnTextChangeListener('displayName')}/>
            <TextBox title={'نام انگلیسی'} id='name' value={this.state.formData.name}
                onChangeText={this.getOnTextChangeListener('name')}/>


        </ManagePageFieldsContainer>
        {this.getSaveAndBackButtons()}
        </ManagePageContainer>
    }
}
