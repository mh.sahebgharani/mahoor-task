// @flow
import * as React from 'react';
import {TextViewBox,ImageModal} from "Libs/one/sweet-react-components";
import {ManagePageContainer,ManagePageFieldsContainer,SweetManagePage} from "Libs/one/sweet-one-react";
import Constants from "../../../../classes/Constants";
import ServicetypeEntity from "../../entity/ServicetypeEntity";
import CrmServicetypeViewController from "../../controllers/servicetype/CrmServicetypeViewController";


export default class CrmServicetypeView extends CrmServicetypeViewController {
    render(){
        return <ManagePageContainer title={'تعریف نوع خدمت'}>
        <ManagePageFieldsContainer>
            <TextViewBox title={'عنوان فارسی'} value={this.state.formData.displayName}/>
            <TextViewBox title={'نام انگلیسی'} value={this.state.formData.name}/>

        </ManagePageFieldsContainer>
        {this.getSaveAndBackButtons()}
        </ManagePageContainer>
    }
}
