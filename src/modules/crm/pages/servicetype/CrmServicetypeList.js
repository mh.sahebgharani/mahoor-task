// @flow
import * as React from 'react';
import {ListPageContainer} from "Libs/one/sweet-one-react";
import CrmServicetypeListController from "../../controllers/servicetype/CrmServicetypeListController";
import CrmServicetypeSearch from './CrmServicetypeSearch';

export default class CrmServicetypeList extends CrmServicetypeListController {
    render() {
        return <ListPageContainer title={'لیست انواع خدمت'}>
            <CrmServicetypeSearch onConfirm={this.searchData}/>
            {this.getAddButton()}
            {this.getTable()}
        </ListPageContainer>;
    }
    columns =
    [
        {Header: 'عنوان فارسی',id: 'displayname',accessor: 'displayName'},
        {Header: 'نام انگلیسی',id: 'name',accessor: 'name'},
        {Header: 'عملیات',accessor: 'id',Cell: props => {return this.getOperationsBox(props.value)}}
    ];
}
