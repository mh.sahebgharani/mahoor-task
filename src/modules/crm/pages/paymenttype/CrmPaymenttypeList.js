// @flow
import * as React from 'react';
import {ListPageContainer} from "Libs/one/sweet-one-react";
import CrmPaymenttypeListController from "../../controllers/paymenttype/CrmPaymenttypeListController";
import CrmPaymenttypeSearch from './CrmPaymenttypeSearch';

export default class CrmPaymenttypeList extends CrmPaymenttypeListController {
    render() {
        return <ListPageContainer title={'لیست انواع پرداخت'}>
            <CrmPaymenttypeSearch onConfirm={this.searchData}/>
            {this.getAddButton()}
            {this.getTable()}
        </ListPageContainer>;
    }
    columns =
    [
        {Header: 'عنوان فارسی',id: 'displayname',accessor: 'displayName'},
        {Header: 'نام انگلیسی',id: 'name',accessor: 'name'},
        {Header: 'عملیات',accessor: 'id',Cell: props => {return this.getOperationsBox(props.value)}}
    ];
}
