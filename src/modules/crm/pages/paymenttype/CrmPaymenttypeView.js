// @flow
import * as React from 'react';
import {TextViewBox,ImageModal} from "Libs/one/sweet-react-components";
import {ManagePageContainer,ManagePageFieldsContainer,SweetManagePage} from "Libs/one/sweet-one-react";
import Constants from "../../../../classes/Constants";
import PaymenttypeEntity from "../../entity/PaymenttypeEntity";
import CrmPaymenttypeViewController from "../../controllers/paymenttype/CrmPaymenttypeViewController";


export default class CrmPaymenttypeView extends CrmPaymenttypeViewController {
    render(){
        return <ManagePageContainer title={'تعریف نوع پرداخت'}>
        <ManagePageFieldsContainer>
            <TextViewBox title={'عنوان فارسی'} value={this.state.formData.displayName}/>
            <TextViewBox title={'نام انگلیسی'} value={this.state.formData.name}/>

        </ManagePageFieldsContainer>
        {this.getSaveAndBackButtons()}
        </ManagePageContainer>
    }
}
