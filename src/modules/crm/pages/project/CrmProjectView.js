// @flow
import * as React from 'react';
import {TextViewBox,ImageModal} from "Libs/one/sweet-react-components";
import {ManagePageContainer,ManagePageFieldsContainer,SweetManagePage} from "Libs/one/sweet-one-react";
import Constants from "../../../../classes/Constants";
import ProjectEntity from "../../entity/ProjectEntity";
import CrmProjectViewController from "../../controllers/project/CrmProjectViewController";


export default class CrmProjectView extends CrmProjectViewController {
    render(){
        return <ManagePageContainer title={'تعریف پروژه'}>
        <ManagePageFieldsContainer>

                    <ImageModal title='لوگو'
                        previewImage={this.state.formData.logo.url}
                    />            <TextViewBox title={'کد'} value={this.state.formData.code}/>
            <TextViewBox title={'عنوان فارسی'} value={this.state.formData.displayName}/>
            <TextViewBox title={'نام انگلیسی'} value={this.state.formData.name}/>

        </ManagePageFieldsContainer>
        {this.getSaveAndBackButtons()}
        </ManagePageContainer>
    }
}
