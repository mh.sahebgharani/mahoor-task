// @flow
import * as React from 'react';
import {TextBox,PickerBox,FileSelector,ImageSelector,TimeSelectorBox} from "Libs/one/sweet-react-components";
import {ManagePageFieldsContainer,ManagePageContainer} from "Libs/one/sweet-one-react";
import CrmProjectManageController from "../../controllers/project/CrmProjectManageController";


import CrmServiceList from '../service/CrmServiceList';
export default class CrmProjectManage extends CrmProjectManageController {
    render(){
        return <ManagePageContainer title={'تعریف پروژه'}>
        <ManagePageFieldsContainer>

                    <ImageSelector title='لوگو'
                        onConfirm={this.getOnFilePathChanged('logo')}
                        previewImage={this.state.formData.logo.url}
                        onImagePreviewLoaded={this.getOnImagePreviewLoaded('logo')}
                    />            <TextBox title={'کد'} id='code' value={this.state.formData.code}
                onChangeText={this.getOnTextChangeListener('code')}/>
            <TextBox title={'عنوان فارسی'} id='displayName' value={this.state.formData.displayName}
                onChangeText={this.getOnTextChangeListener('displayName')}/>
            <TextBox title={'نام انگلیسی'} id='name' value={this.state.formData.name}
                onChangeText={this.getOnTextChangeListener('name')}/>


        </ManagePageFieldsContainer>
        {this.getSaveAndBackButtons()}
        <CrmServiceList ownerID={this.getID()}/>
        </ManagePageContainer>
    }
}
