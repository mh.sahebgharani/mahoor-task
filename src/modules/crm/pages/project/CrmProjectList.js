// @flow
import * as React from 'react';
import {ListPageContainer} from "Libs/one/sweet-one-react";
import CrmProjectListController from "../../controllers/project/CrmProjectListController";
import CrmProjectSearch from './CrmProjectSearch';

export default class CrmProjectList extends CrmProjectListController {
    render() {
        return <ListPageContainer title={'لیست پروژه ها'}>
            <CrmProjectSearch onConfirm={this.searchData}/>
            {this.getAddButton()}
            {this.getTable()}
        </ListPageContainer>;
    }
    columns =
    [
        {Header: 'کد',id: 'code',accessor: 'code'},
        {Header: 'عنوان فارسی',id: 'displayname',accessor: 'displayName'},
        {Header: 'نام انگلیسی',id: 'name',accessor: 'name'},
        {Header: 'عملیات',accessor: 'id',Cell: props => {return this.getOperationsBox(props.value)}}
    ];
}
