// @flow
import * as React from 'react';
import {TextBox,PickerBox} from "Libs/one/sweet-react-components";
import {SearchBoxContainer,SweetSearchBox} from "Libs/one/sweet-one-react";
import ProjectEntity from "../../entity/ProjectEntity";

export default class CrmProjectSearch extends SweetSearchBox {
    moduleName='crm';
    tableName='project';
    constructor(props) {
        super(props);
        this.state = {
            searchParams:{},
            formData:new ProjectEntity(),

        };
    }

    componentDidMount() {
        super.componentDidMount();

        this.loadSimpleFields();

    }
    render(){
        return <SearchBoxContainer title={'جستجوی پروژه ها'} onConfirm={this.getOnConfirm()}>

         <TextBox title={'کد'} id='code'
                value={this.state.searchParams.code}
                onChangeText={this.getOnTextChangeListener('code')}/>

         <TextBox title={'عنوان فارسی'} id='displayName'
                value={this.state.searchParams.displayname}
                onChangeText={this.getOnTextChangeListener('displayname')}/>

         <TextBox title={'نام انگلیسی'} id='name'
                value={this.state.searchParams.name}
                onChangeText={this.getOnTextChangeListener('name')}/>

        </SearchBoxContainer>
    }
}
