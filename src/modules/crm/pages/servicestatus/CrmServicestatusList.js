// @flow
import * as React from 'react';
import {ListPageContainer} from "Libs/one/sweet-one-react";
import CrmServicestatusListController from "../../controllers/servicestatus/CrmServicestatusListController";
import CrmServicestatusSearch from './CrmServicestatusSearch';

export default class CrmServicestatusList extends CrmServicestatusListController {
    render() {
        return <ListPageContainer title={'لیست وضعیت های خدمت'}>
            <CrmServicestatusSearch onConfirm={this.searchData}/>
            {this.getAddButton()}
            {this.getTable()}
        </ListPageContainer>;
    }
    columns =
    [
        {Header: 'عنوان فارسی',id: 'displayname',accessor: 'displayName'},
        {Header: 'نام انگلیسی',id: 'name',accessor: 'name'},
        {Header: 'عملیات',accessor: 'id',Cell: props => {return this.getOperationsBox(props.value)}}
    ];
}
