// @flow
import * as React from 'react';
import {TextBox,PickerBox} from "Libs/one/sweet-react-components";
import {SearchBoxContainer,SweetSearchBox} from "Libs/one/sweet-one-react";
import ServicestatusEntity from "../../entity/ServicestatusEntity";

export default class CrmServicestatusSearch extends SweetSearchBox {
    moduleName='crm';
    tableName='servicestatus';
    constructor(props) {
        super(props);
        this.state = {
            searchParams:{},
            formData:new ServicestatusEntity(),

        };
    }
    
    componentDidMount() {
        super.componentDidMount();

        this.loadSimpleFields();

    }
    render(){
        return <SearchBoxContainer title={'جستجوی وضعیت های خدمت'} onConfirm={this.getOnConfirm()}>

         <TextBox title={'عنوان فارسی'} id='displayName'
                value={this.state.searchParams.displayname}
                onChangeText={this.getOnTextChangeListener('displayname')}/>

         <TextBox title={'نام انگلیسی'} id='name'
                value={this.state.searchParams.name}
                onChangeText={this.getOnTextChangeListener('name')}/>

        </SearchBoxContainer>
    }
}
