// @flow
import * as React from 'react';
import {TextViewBox,ImageModal} from "Libs/one/sweet-react-components";
import {ManagePageContainer,ManagePageFieldsContainer,SweetManagePage} from "Libs/one/sweet-one-react";
import Constants from "../../../../classes/Constants";
import ServicestatusEntity from "../../entity/ServicestatusEntity";
import CrmServicestatusViewController from "../../controllers/servicestatus/CrmServicestatusViewController";


export default class CrmServicestatusView extends CrmServicestatusViewController {
    render(){
        return <ManagePageContainer title={'تعریف وضعیت خدمت'}>
        <ManagePageFieldsContainer>
            <TextViewBox title={'عنوان فارسی'} value={this.state.formData.displayName}/>
            <TextViewBox title={'نام انگلیسی'} value={this.state.formData.name}/>

        </ManagePageFieldsContainer>
        {this.getSaveAndBackButtons()}
        </ManagePageContainer>
    }
}
