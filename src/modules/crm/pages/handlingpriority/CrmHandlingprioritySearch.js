// @flow
import * as React from 'react';
import {TextBox,PickerBox} from "Libs/one/sweet-react-components";
import {SearchBoxContainer,SweetSearchBox} from "Libs/one/sweet-one-react";
import HandlingpriorityEntity from "../../entity/HandlingpriorityEntity";

export default class CrmHandlingprioritySearch extends SweetSearchBox {
    moduleName='crm';
    tableName='handlingpriority';
    constructor(props) {
        super(props);
        this.state = {
            searchParams:{},
            formData:new HandlingpriorityEntity(),

        };
    }
    
    componentDidMount() {
        super.componentDidMount();

        this.loadSimpleFields();

    }
    render(){
        return <SearchBoxContainer title={'جستجوی اولویت های رسیدگی'} onConfirm={this.getOnConfirm()}>

         <TextBox title={'عنوان فارسی'} id='displayName'
                value={this.state.searchParams.displayname}
                onChangeText={this.getOnTextChangeListener('displayname')}/>

         <TextBox title={'نام انگلیسی'} id='name'
                value={this.state.searchParams.name}
                onChangeText={this.getOnTextChangeListener('name')}/>

        </SearchBoxContainer>
    }
}
