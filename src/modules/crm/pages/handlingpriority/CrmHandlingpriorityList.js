// @flow
import * as React from 'react';
import {ListPageContainer} from "Libs/one/sweet-one-react";
import CrmHandlingpriorityListController from "../../controllers/handlingpriority/CrmHandlingpriorityListController";
import CrmHandlingprioritySearch from './CrmHandlingprioritySearch';

export default class CrmHandlingpriorityList extends CrmHandlingpriorityListController {
    render() {
        return <ListPageContainer title={'لیست اولویت های رسیدگی'}>
            <CrmHandlingprioritySearch onConfirm={this.searchData}/>
            {this.getAddButton()}
            {this.getTable()}
        </ListPageContainer>;
    }
    columns =
    [
        {Header: 'عنوان فارسی',id: 'displayname',accessor: 'displayName'},
        {Header: 'نام انگلیسی',id: 'name',accessor: 'name'},
        {Header: 'عملیات',accessor: 'id',Cell: props => {return this.getOperationsBox(props.value)}}
    ];
}
