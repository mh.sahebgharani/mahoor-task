// @flow
import * as React from 'react';
import {TextViewBox,ImageModal} from "Libs/one/sweet-react-components";
import {ManagePageContainer,ManagePageFieldsContainer,SweetManagePage} from "Libs/one/sweet-one-react";
import Constants from "../../../../classes/Constants";
import HandlingpriorityEntity from "../../entity/HandlingpriorityEntity";
import CrmHandlingpriorityViewController from "../../controllers/handlingpriority/CrmHandlingpriorityViewController";


export default class CrmHandlingpriorityView extends CrmHandlingpriorityViewController {
    render(){
        return <ManagePageContainer title={'تعریف اولویت رسیدگی'}>
        <ManagePageFieldsContainer>
            <TextViewBox title={'عنوان فارسی'} value={this.state.formData.displayName}/>
            <TextViewBox title={'نام انگلیسی'} value={this.state.formData.name}/>

        </ManagePageFieldsContainer>
        {this.getSaveAndBackButtons()}
        </ManagePageContainer>
    }
}
