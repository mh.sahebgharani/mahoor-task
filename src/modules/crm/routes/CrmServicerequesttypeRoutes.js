
import CrmServicerequesttypeList from '../pages/servicerequesttype/CrmServicerequesttypeList';
import CrmServicerequesttypeManage from '../pages/servicerequesttype/CrmServicerequesttypeManage';
import CrmServicerequesttypeView from '../pages/servicerequesttype/CrmServicerequesttypeView';
let CrmServicerequesttypeRoutes=[];
CrmServicerequesttypeRoutes.push({ path: '/crm/servicerequesttype',exact:true, name: 'لیست انواع درخواست سرویس',component:CrmServicerequesttypeList});
CrmServicerequesttypeRoutes.push({ path: '/crm/servicerequesttype/management/:id',exact:false, name: 'ویرایش نوع درخواست سرویس',component:CrmServicerequesttypeManage});
CrmServicerequesttypeRoutes.push({ path: '/crm/servicerequesttype/management',exact:false, name: 'تعریف نوع درخواست سرویس',component:CrmServicerequesttypeManage});
CrmServicerequesttypeRoutes.push({ path: '/crm/servicerequesttype/view/:id',exact:false, name: 'نوع درخواست سرویس',component:CrmServicerequesttypeView});
export default CrmServicerequesttypeRoutes;
