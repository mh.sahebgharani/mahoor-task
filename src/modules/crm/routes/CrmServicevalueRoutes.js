
import CrmServicevalueList from '../pages/servicevalue/CrmServicevalueList';
import CrmServicevalueManage from '../pages/servicevalue/CrmServicevalueManage';
import CrmServicevalueView from '../pages/servicevalue/CrmServicevalueView';
let CrmServicevalueRoutes=[];
CrmServicevalueRoutes.push({ path: '/crm/servicevalue',exact:true, name: 'لیست ارزش های خدمت',component:CrmServicevalueList});
CrmServicevalueRoutes.push({ path: '/crm/servicevalue/management/:id',exact:false, name: 'ویرایش ارزش خدمت',component:CrmServicevalueManage});
CrmServicevalueRoutes.push({ path: '/crm/servicevalue/management',exact:false, name: 'تعریف ارزش خدمت',component:CrmServicevalueManage});
CrmServicevalueRoutes.push({ path: '/crm/servicevalue/view/:id',exact:false, name: 'ارزش خدمت',component:CrmServicevalueView});
export default CrmServicevalueRoutes;
