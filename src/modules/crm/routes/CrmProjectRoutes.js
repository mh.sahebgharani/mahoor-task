
import CrmProjectList from '../pages/project/CrmProjectList';
import CrmProjectManage from '../pages/project/CrmProjectManage';
import CrmProjectView from '../pages/project/CrmProjectView';
let CrmProjectRoutes=[];
CrmProjectRoutes.push({ path: '/crm/project',exact:true, name: 'لیست پروژه ها',component:CrmProjectList});
CrmProjectRoutes.push({ path: '/crm/project/management/:id',exact:false, name: 'ویرایش پروژه',component:CrmProjectManage});
CrmProjectRoutes.push({ path: '/crm/project/management',exact:false, name: 'تعریف پروژه',component:CrmProjectManage});
CrmProjectRoutes.push({ path: '/crm/project/view/:id',exact:false, name: 'پروژه',component:CrmProjectView});
export default CrmProjectRoutes;
