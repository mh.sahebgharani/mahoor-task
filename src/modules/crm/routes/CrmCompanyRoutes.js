
import CrmCompanyList from '../pages/company/CrmCompanyList';
import CrmCompanyManage from '../pages/company/CrmCompanyManage';
import CrmCompanyView from '../pages/company/CrmCompanyView';
let CrmCompanyRoutes=[];
CrmCompanyRoutes.push({ path: '/crm/company',exact:true, name: 'لیست شرکت ها',component:CrmCompanyList});
CrmCompanyRoutes.push({ path: '/crm/company/management/:id',exact:false, name: 'ویرایش شرکت',component:CrmCompanyManage});
CrmCompanyRoutes.push({ path: '/crm/company/management',exact:false, name: 'تعریف شرکت',component:CrmCompanyManage});
CrmCompanyRoutes.push({ path: '/crm/company/view/:id',exact:false, name: 'شرکت',component:CrmCompanyView});
export default CrmCompanyRoutes;
