
import CrmServicetypeList from '../pages/servicetype/CrmServicetypeList';
import CrmServicetypeManage from '../pages/servicetype/CrmServicetypeManage';
import CrmServicetypeView from '../pages/servicetype/CrmServicetypeView';
let CrmServicetypeRoutes=[];
CrmServicetypeRoutes.push({ path: '/crm/servicetype',exact:true, name: 'لیست انواع خدمت',component:CrmServicetypeList});
CrmServicetypeRoutes.push({ path: '/crm/servicetype/management/:id',exact:false, name: 'ویرایش نوع خدمت',component:CrmServicetypeManage});
CrmServicetypeRoutes.push({ path: '/crm/servicetype/management',exact:false, name: 'تعریف نوع خدمت',component:CrmServicetypeManage});
CrmServicetypeRoutes.push({ path: '/crm/servicetype/view/:id',exact:false, name: 'نوع خدمت',component:CrmServicetypeView});
export default CrmServicetypeRoutes;
