
import CrmServiceList from '../pages/service/CrmServiceList';
import CrmServiceManage from '../pages/service/CrmServiceManage';
import CrmServiceView from '../pages/service/CrmServiceView';
let CrmServiceRoutes=[];
CrmServiceRoutes.push({ path: '/crm/service/management/:ownerid/:id',exact:false, name: 'ویرایش خدمت',component:CrmServiceManage});
CrmServiceRoutes.push({ path: '/crm/service/management/:ownerid',exact:false, name: 'تعریف خدمت',component:CrmServiceManage});
CrmServiceRoutes.push({ path: '/crm/service/view/:ownerid/:id',exact:false, name: 'خدمت',component:CrmServiceView});
export default CrmServiceRoutes;
