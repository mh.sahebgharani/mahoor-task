
import CrmServicestatusList from '../pages/servicestatus/CrmServicestatusList';
import CrmServicestatusManage from '../pages/servicestatus/CrmServicestatusManage';
import CrmServicestatusView from '../pages/servicestatus/CrmServicestatusView';
let CrmServicestatusRoutes=[];
CrmServicestatusRoutes.push({ path: '/crm/servicestatus',exact:true, name: 'لیست وضعیت های خدمت',component:CrmServicestatusList});
CrmServicestatusRoutes.push({ path: '/crm/servicestatus/management/:id',exact:false, name: 'ویرایش وضعیت خدمت',component:CrmServicestatusManage});
CrmServicestatusRoutes.push({ path: '/crm/servicestatus/management',exact:false, name: 'تعریف وضعیت خدمت',component:CrmServicestatusManage});
CrmServicestatusRoutes.push({ path: '/crm/servicestatus/view/:id',exact:false, name: 'وضعیت خدمت',component:CrmServicestatusView});
export default CrmServicestatusRoutes;
