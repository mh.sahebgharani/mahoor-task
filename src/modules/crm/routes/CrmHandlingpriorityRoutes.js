
import CrmHandlingpriorityList from '../pages/handlingpriority/CrmHandlingpriorityList';
import CrmHandlingpriorityManage from '../pages/handlingpriority/CrmHandlingpriorityManage';
import CrmHandlingpriorityView from '../pages/handlingpriority/CrmHandlingpriorityView';
let CrmHandlingpriorityRoutes=[];
CrmHandlingpriorityRoutes.push({ path: '/crm/handlingpriority',exact:true, name: 'لیست اولویت های رسیدگی',component:CrmHandlingpriorityList});
CrmHandlingpriorityRoutes.push({ path: '/crm/handlingpriority/management/:id',exact:false, name: 'ویرایش اولویت رسیدگی',component:CrmHandlingpriorityManage});
CrmHandlingpriorityRoutes.push({ path: '/crm/handlingpriority/management',exact:false, name: 'تعریف اولویت رسیدگی',component:CrmHandlingpriorityManage});
CrmHandlingpriorityRoutes.push({ path: '/crm/handlingpriority/view/:id',exact:false, name: 'اولویت رسیدگی',component:CrmHandlingpriorityView});
export default CrmHandlingpriorityRoutes;
