
import CrmCustomerList from '../pages/customer/CrmCustomerList';
import CrmCustomerManage from '../pages/customer/CrmCustomerManage';
import CrmCustomerView from '../pages/customer/CrmCustomerView';
let CrmCustomerRoutes=[];
CrmCustomerRoutes.push({ path: '/crm/customer',exact:true, name: 'لیست مشتریان',component:CrmCustomerList});
CrmCustomerRoutes.push({ path: '/crm/customer/management/:id',exact:false, name: 'ویرایش مشتری',component:CrmCustomerManage});
CrmCustomerRoutes.push({ path: '/crm/customer/management',exact:false, name: 'تعریف مشتری',component:CrmCustomerManage});
CrmCustomerRoutes.push({ path: '/crm/customer/view/:id',exact:false, name: 'مشتری',component:CrmCustomerView});
export default CrmCustomerRoutes;
