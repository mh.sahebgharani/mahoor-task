
import CrmPaymenttypeList from '../pages/paymenttype/CrmPaymenttypeList';
import CrmPaymenttypeManage from '../pages/paymenttype/CrmPaymenttypeManage';
import CrmPaymenttypeView from '../pages/paymenttype/CrmPaymenttypeView';
let CrmPaymenttypeRoutes=[];
CrmPaymenttypeRoutes.push({ path: '/crm/paymenttype',exact:true, name: 'لیست انواع پرداخت',component:CrmPaymenttypeList});
CrmPaymenttypeRoutes.push({ path: '/crm/paymenttype/management/:id',exact:false, name: 'ویرایش نوع پرداخت',component:CrmPaymenttypeManage});
CrmPaymenttypeRoutes.push({ path: '/crm/paymenttype/management',exact:false, name: 'تعریف نوع پرداخت',component:CrmPaymenttypeManage});
CrmPaymenttypeRoutes.push({ path: '/crm/paymenttype/view/:id',exact:false, name: 'نوع پرداخت',component:CrmPaymenttypeView});
export default CrmPaymenttypeRoutes;
