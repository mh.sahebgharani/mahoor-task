// @flow
import {SweetEntity,AccessManager} from 'Libs/one/sweet-one-react';

export default class ServiceEntity extends SweetEntity {
    __moduleName='crm';
    __tableName='service';

    handlingpriority={};
    descriptionAfterAccomplish='';
    serviceRequestType={};
    serviceValue={};
    price='';
    paymentType={};
    serviceType={};
    priority='';
    text='';
    title='';
    spentMinutes='';
    project={};
    fields={id:{field:'id'},handlingpriority:{field:'handlingpriority_fid',type:'fid'},descriptionAfterAccomplish:{field:'descriptionafteraccomplish'},serviceRequestType:{field:'servicerequesttype_fid',type:'fid'},serviceValue:{field:'servicevalue_fid',type:'fid'},price:{field:'price'},paymentType:{field:'paymenttype_fid',type:'fid'},serviceType:{field:'servicetype_fid',type:'fid'},priority:{field:'priority'},text:{field:'text'},title:{field:'title'},spentMinutes:{field:'spendminutes_num'},project:{field:'project_fid',type:'fid'}};
    constructor(ownerID){
        super();
        this.project={id:ownerID};
    }
    get(id,afterFetch){
        this._get(this.getBaseURL()+'/'+id,this.getServiceName(),AccessManager.VIEW,afterFetch);
    }
    getAll(pageSize,page,sorted,filtered,afterFetch,onError){
        const filterAndSortString=this.getFilterString(pageSize,page,sorted,filtered);
        const url=this.getBaseURL()+filterAndSortString;
        this._getAll(url,this.getServiceName(),AccessManager.LIST,afterFetch,onError);
    }
    save(id,afterFetchListener,onError){
        const formData=this.getFormRequest();
        this._save(this.getServiceName(),this.getBaseURL(),id,formData,afterFetchListener,onError);
    }
    delete(id,afterFetchListener,onError){
        this._delete(this.getServiceName(),this.getBaseURL()+"/"+id,afterFetchListener,onError);
    }
}
