// @flow
import {SweetEntity,AccessManager} from 'Libs/one/sweet-one-react';
import CustomerEntity from "./CustomerEntity";

export default class ProjectEntity extends SweetEntity {
    __moduleName='crm';
    __tableName='project';

    logo={};
    code='';
    displayName='';
    name='';
    customers=[];
    fields={id:{field:'id'},logo:{field:'logo_igu',type:'file'},code:{field:'code'},displayName:{field:'displayname'},name:{field:'name'},customers:{field:'customers',type:'entityarray',object:new CustomerEntity()}};
    get(id,afterFetch){
        this._get(this.getBaseURL()+'/'+id,this.getServiceName(),AccessManager.VIEW,afterFetch);
    }
    getAll(pageSize,page,sorted,filtered,afterFetch,onError){
        const filterAndSortString=this.getFilterString(pageSize,page,sorted,filtered);
        const url=this.getBaseURL()+filterAndSortString;
        this._getAll(url,this.getServiceName(),AccessManager.LIST,afterFetch,onError);
    }
    save(id,afterFetchListener,onError){
        const formData=this.getFormRequest();
        this._save(this.getServiceName(),this.getBaseURL(),id,formData,afterFetchListener,onError);
    }
    delete(id,afterFetchListener,onError){
        this._delete(this.getServiceName(),this.getBaseURL()+"/"+id,afterFetchListener,onError);
    }
}
