// @flow
import {SweetEntity,AccessManager} from 'Libs/one/sweet-one-react';

export default class FieldvalueEntity extends SweetEntity {
    __moduleName='common';
    __tableName='fieldvalue';

    simpleFieldID='';
    displayName='';
    name='';
    logo={};
    fields={id:{field:'id'},simpleFieldID:{field:'simplefield_fid'},displayName:{field:'displayname'},name:{field:'name'},logo:{field:'logo',type:'file'}};
    get(id,afterFetch){
        this._get(this.getBaseURL()+'/'+id,this.getServiceName(),AccessManager.VIEW,afterFetch);
    }
    getAll(pageSize,page,sorted,filtered,afterFetch,onError){
        let filterAndSortString=this.getFilterString(pageSize,page,sorted,filtered);
        let url=this.getBaseURL()+filterAndSortString;
        this._getAll(url,this.getServiceName(),AccessManager.LIST,afterFetch,onError);
    }
    save(id,afterFetchListener,onError){
        let formData=this.getFormRequest();
        this._save(this.getServiceName(),this.getBaseURL(),id,formData,afterFetchListener,onError);
    }

}
