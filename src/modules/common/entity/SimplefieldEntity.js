// @flow
import {SweetEntity,AccessManager} from 'Libs/one/sweet-one-react';

export default class SimplefieldEntity extends SweetEntity {
    __moduleName='common';
    __tableName='simplefield';

    name='';
    displayName='';
    isEditable='';
    fields={id:{field:'id'},name:{field:'name'},displayName:{field:'displayname'},isEditable:{field:'is_editable'}};
    get(id,afterFetch){
        this._get(this.getBaseURL()+'/'+id,this.getServiceName(),AccessManager.VIEW,afterFetch);
    }
    getAll(pageSize,page,sorted,filtered,afterFetch){
        let filterAndSortString=this.getFilterString(pageSize,page,sorted,filtered);
        let url=this.getBaseURL()+filterAndSortString;
        this._getAll(url,this.getServiceName(),AccessManager.LIST,afterFetch);
    }
    save(id,afterFetchListener){
        let formData=this.getFormRequest();
        this._save(this.getServiceName(),this.getBaseURL(),id,formData,afterFetchListener,null);
    }
}
