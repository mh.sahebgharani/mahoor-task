// @flow
import {SweetEntity,AccessManager} from 'Libs/one/sweet-one-react';

export default class SystemmessageEntity extends SweetEntity {
    __moduleName='common';
    __tableName='systemmessage';

    title='';
    message='';
    group={};
    isAlert='';
    goodness='';
    fields={id:{field:'id'},title:{field:'title'},message:{field:'message'},group:{field:'group_fid',type:'simplefield',simplefieldname:'systemmessagegroup'},isAlert:{field:'is_alert'},goodness:{field:'goodness'}};
    get(id,afterFetch){
        this._get(this.getBaseURL()+'/'+id,this.getServiceName(),AccessManager.VIEW,afterFetch);
    }
    getAll(pageSize,page,sorted,filtered,afterFetch,onError){
        const filterAndSortString=this.getFilterString(pageSize,page,sorted,filtered);
        const url=this.getBaseURL()+filterAndSortString;
        this._getAll(url,this.getServiceName(),AccessManager.LIST,afterFetch,onError);
    }
    save(id,afterFetchListener,onError){
        const formData=this.getFormRequest();
        this._save(this.getServiceName(),this.getBaseURL(),id,formData,afterFetchListener,onError);
    }
    delete(id,afterFetchListener,onError){
        this._delete(this.getServiceName(),this.getBaseURL()+"/"+id,afterFetchListener,onError);
    }
}
