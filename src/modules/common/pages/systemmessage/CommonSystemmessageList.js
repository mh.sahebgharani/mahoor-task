// @flow
import * as React from 'react';
import Constants from '../../../../classes/Constants';
import SystemmessageEntity from "../../entity/SystemmessageEntity";
import {ListPageContainer,SweetManageListPage} from "Libs/one/sweet-one-react";

import '../../files/styles/systemmessage.css';

export default class CommonSystemmessageList extends SweetManageListPage {
    moduleName='common';
    tableName='systemmessage';
    entity = new SystemmessageEntity();
    componentDidMount() {
        super.componentDidMount();
    }
    componentWillUnmount() {
        clearInterval();
    }

    // reloadData(pageSize){
    //     console.log('reloading');
    //     this.LoadData(pageSize,1,null,null);
    // }
    render() {
        return <ListPageContainer title={'لیست پیامهای سیستمی'}>
            {this.getTable()}
        </ListPageContainer>;
    }
    columns =
    [
        {Header: 'عنوان',id: 'title',accessor: (a)=>{return a}
        ,Cell:(a)=>{return <div className={'systemmessage g'+a.value.goodness}>
                <div className={'systemmessagetitle'}>
                {a.value.title}
                </div>
                <div className={'systemmessagemessage'}>
                    {a.value.message}
                </div>
        </div>}},
    ];
    getRowProps=(row) => ({
        style: {
            background: row.goodness>0 ? '#0000ee' : 'white',
        },
    });
}
