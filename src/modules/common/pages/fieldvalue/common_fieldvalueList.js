// @flow
import * as React from 'react';
import { Link} from 'react-router-dom';
import { FaEdit } from 'react-icons/fa';
import { IoMdAddCircle } from 'react-icons/io';
import { MdDeleteForever } from 'react-icons/md';
import Constants from '../../../../classes/Constants';
import {SweetTable} from 'Libs/one/sweet-react-components';
import {SweetAlert,Common} from 'Libs/one/sweet-react-common-tools';
import {AccessManager,SweetFetcher,SweetHttpRequest,ManagePageContainer,SweetManageListPage} from 'Libs/one/sweet-one-react';
class common_fieldvalueList extends SweetManageListPage {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            pages:1,
            page:0,
            canEdit:AccessManager.UserCan('common','fieldvalue',AccessManager.EDIT),
            canInsert:AccessManager.UserCan('common','fieldvalue',AccessManager.INSERT),
            canDelete:AccessManager.UserCan('common','fieldvalue',AccessManager.DELETE),
            displaySearchWindow:false,

            simpleFieldIDOptions:[],
        };
    };
    searchParams={};
    getSimpleFieldID(){
        return this.props.match.params.simplefieldid;
    }
    componentDidMount() {
        this.LoadData(Constants.DefaultPageSize,1,null,null);
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.match.params.simplefieldid !== this.getSimpleFieldID())
        {
            this.props.match.params.simplefieldid=nextProps.match.params.simplefieldid;
            this.LoadData(Constants.DefaultPageSize,1,null,null);
        }
    }
    LoadData(pageSize,page,sorted,filtered)
    {
        let RecordStart=((page-1)*pageSize);
        let Request=new SweetHttpRequest();
        Request.appendVariables(filtered,'id','value');
        Request.appendVariablesWithPostFix(sorted,'id','desc','__sort');
        Request.appendVariable('__pagesize',pageSize);
        Request.appendVariable('__startrow',RecordStart);
        let filterAndSortString=Request.getParamsString();
        if(filterAndSortString!='') filterAndSortString='&'+filterAndSortString;
        let url='/common/fieldvalue/byfield?fields='+this.getSimpleFieldID()+filterAndSortString;
        new SweetFetcher().Fetch(url, SweetFetcher.METHOD_GET, null,
        data => {
            let dt=data.Data[this.getSimpleFieldID()].Data;
            let count=data.Data[this.getSimpleFieldID()].RecordCount;
            let Pages=Math.ceil(count/Constants.DefaultPageSize);
            for(let i=0;i<dt.length;i++)
                dt[i]=Common.convertNullKeysToEmpty(dt[i]);
            this.setState({data: dt,pages:Pages})
        },
        null,'common.fieldvalue',AccessManager.LIST,
        this.props.history);
    };
    searchData=()=>
    {
        this.toggleSearchWindow();
        if(this.searchParams!=null)
            this.LoadData(Constants.DefaultPageSize,1,null,Common.ObjectToIdValueArray(this.searchParams));
    };
    render(){
        return <ManagePageContainer title={'مقادیر فیلد'}><div className={'pagecontent'}>
            <div className={'topoperationsrow'}>{this.state.canInsert && <Link className={'addlink'}  to={'/common/fieldvalue/management/'+this.getSimpleFieldID()}><IoMdAddCircle/></Link>}</div>
        <SweetTable
            filterable={false}
            className='-striped -highlight'
            defaultPageSize={Constants.DefaultPageSize}
            data={this.state.data}
            pages={this.state.pages}
            columns={this.columns}
            excludedExportColumns={'id'}
            manual
            onFetchData={(state, instance) => {
                this.setState({loading: true,page:state.page});
                this.LoadData(state.pageSize,state.page+1,state.sorted,state.filtered);
            }}
        />
        </div></ManagePageContainer>;
    }

columns = [
{
    Header: 'نام نمایشی',
    accessor: 'displayname'
},
{
    Header: 'عملیات',
    accessor: 'id',
    Cell: props => <div className={'operationsrow'}>
                   {this.state.canEdit &&
                    <Link className={'editlink'}  to={'/common/fieldvalue/management/'+this.getSimpleFieldID()+'/'+props.value}><FaEdit/></Link>
                   }
                   {this.state.canDelete &&
                       <MdDeleteForever onClick={
                       () =>{
                         SweetAlert.displayDeleteAlert(()=>{
                            new SweetFetcher().Fetch('/common/fieldvalue/' + props.value, SweetFetcher.METHOD_DELETE, null,
                                data => {
                                    this.LoadData(Constants.DefaultPageSize,this.state.page+1,null,null);
                                },(error)=>{
                                            let status=error.response.status;
                                            SweetAlert.displaySimpleAlert('خطای پیش بینی نشده ','خطایی در حذف اطلاعات به وجود آمد'+status.toString().trim());
                                        },'common.fieldvalue',AccessManager.DELETE,this.props.history);
                         });
                        }
                        }/>
                 }
                </div>,
},];
        }
export default common_fieldvalueList;
