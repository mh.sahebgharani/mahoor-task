// @flow
import * as React from 'react';
import FieldvalueEntity from "../../entity/FieldvalueEntity";
import {TextBox} from 'Libs/one/sweet-react-components';
import {SweetManagePage,ManagePageContainer} from 'Libs/one/sweet-one-react';
export default class Common_FieldvalueManage extends SweetManagePage {
    moduleName='common';
    tableName='fieldvalue';
    constructor(props) {
        super(props);
        this.state = {
            canEdit:this.canEdit(),
            formData:new FieldvalueEntity(),
        };
    }

    returnBack(){
        if(this.props!=null)
            this.props.history.push('/'+this.moduleName+'/'+this.tableName+'/'+this.getSimpleFieldID());
    }
    getSimpleFieldID(){
        return this.props.match.params.simplefieldid;
    }
    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});
        else{
            const fd=this.state.formData;
            fd.simpleFieldID=this.getSimpleFieldID();
            this.setState({formData:fd});
        }

    }
    render(){
        console.log(this.state.formData);
        return <ManagePageContainer title={'تعریف مقدار فیلد'}>
            <TextBox title={'نام انگلیسی'} id='name' value={this.state.formData.name}
                     onChangeText={this.getOnTextChangeListener('name')}/>
            <TextBox title={'نام نمایشی'} id='displayName' value={this.state.formData.displayName}
                onChangeText={this.getOnTextChangeListener('displayName')}/>

        {this.getSaveAndBackButtons()}
        </ManagePageContainer>
    }
}
