// @flow
import * as React from 'react';
import { MDBContainer, MDBRow, MDBCol, MDBBtn } from 'mdbreact';
import Constants from '../../../../classes/Constants';
import SweetComponent from '../../../../classes/sweet-component';
import ModalImage from 'react-modal-image'
import {Common} from 'Libs/one/sweet-react-common-tools';
import {AccessManager,SweetFetcher} from 'Libs/one/sweet-one-react';
class common_fieldvalueView extends SweetComponent {
    constructor(props) {
        super(props);
        this.state = {
                canEdit:AccessManager.UserCan('common','fieldvalue',AccessManager.EDIT),

			name:'',
			displayName:'',
			simpleFieldID:'',
			simpleFieldIDOptions:[],
			logo:'',
			logoPreview:'',
        };
        if(this.props.match.params.id>0){
        new SweetFetcher().Fetch('/common/fieldvalue/'+this.props.match.params.id, SweetFetcher.METHOD_GET,null,
        data => {
            data.Data=Common.convertNullKeysToEmpty(data.Data);

                 this.setState({
name:data.Data.name,
displayName:data.Data.displayname,
simpleFieldID:data.Data.simplefield_fidinfo.name,
logoPreview:Constants.SiteURL+'/'+data.Data.logo,});

            },
            null,'common.fieldvalue',AccessManager.VIEW,
            this.props.history);
        }//IF

    }
    render(){
        return <MDBContainer>
            <MDBRow>
                <MDBCol md='6'>
                    <form>
                        <p className='h5 text-center mb-4'> fieldvalue</p>

                        <div className='form-group'>
                            <label>نام</label>
                            <label
                                className='valuelabel'>
                                {this.state.name}
                            </label>
                        </div>
                        <div className='form-group'>
                            <label>نام نمایشی</label>
                            <label
                                className='valuelabel'>
                                {this.state.displayName}
                            </label>
                        </div>
                        <div className='form-group'>
                            <label>فیلد</label>
                            <label
                                className='valuelabel'>
                                {this.state.simpleFieldID}
                            </label>
                        </div><div className='form-group'>
                            <label htmlFor='logo'>logo</label>

                        <ModalImage
                            small={this.state.logoPreview}
                            large={this.state.logoPreview}
                            className={'imageuploadpreview'} />
                        </div>

                            <div className='text-center'>
                            <MDBBtn onClick={() =>
                             {
                                this.props.history.push('/common/fieldvalues');
                             }
                            }>برگشت</MDBBtn>
                        </div>
                    </form>
                </MDBCol>
            </MDBRow>
        </MDBContainer>
    }
}

export default common_fieldvalueView;
