// @flow
import * as React from 'react';
import {TextBox} from "Libs/one/sweet-react-components";
import {ManagePageFieldsContainer,ManagePageContainer,SweetManagePage} from "Libs/one/sweet-one-react";

import ParameterEntity from "../../entity/ParameterEntity";


export default class CommonParameterManage extends SweetManagePage {
    moduleName='common';
    tableName='parameter';
    constructor(props) {
        super(props);
        this.state = {
            canEdit:this.canEdit(),
            formData:new ParameterEntity(),

        };
    }

    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

        this.loadSimpleFields();

    }
    render(){
        return <ManagePageContainer title={'تعریف پارامتر'}>
        <ManagePageFieldsContainer>
            <TextBox title={'نام'} id='name' value={this.state.formData.name}
                onChangeText={this.getOnTextChangeListener('name')}/>
            <TextBox title={'نام نمایشی'} id='displayName' value={this.state.formData.displayName}
                onChangeText={this.getOnTextChangeListener('displayName')}/>
            <TextBox title={'مقدار'} id='value' value={this.state.formData.value}
                onChangeText={this.getOnTextChangeListener('value')}/>
            <TextBox title={'توضیحات'} id='description' value={this.state.formData.description}
                onChangeText={this.getOnTextChangeListener('description')}/>


        </ManagePageFieldsContainer>
        {this.getSaveAndBackButtons()}
        </ManagePageContainer>
    }
}
