// @flow
import * as React from 'react';
import {SweetManageListPage,ListPageContainer} from "Libs/one/sweet-one-react";
import ParameterEntity from "../../entity/ParameterEntity";


export default class CommonParameterList extends SweetManageListPage {
    moduleName='common';
    tableName='parameter';
    entity = new ParameterEntity();
    componentDidMount() {
        super.componentDidMount();
        this.LoadData(this.state.pageSize,1,null,null);
    }
    render() {
        return <ListPageContainer title={'لیست پارامترها'}>

            {this.getAddButton()}
            {this.getTable()}
        </ListPageContainer>;
    }
    columns =
    [
        {Header: 'نام',id: 'displayname',accessor: 'displayName'},
        {Header: 'مقدار',id: 'value',accessor: 'value'},
        {Header: 'عملیات',accessor: 'id',Cell: props => {return this.getOperationsBox(props.value)}}
    ];

    getOperationsBox(id){
        return <div className={'operationsrow'}>
            {this.getEditButton(id)}
        </div>
    }
}
