// @flow
import * as React from 'react';
import {TextViewBox} from "Libs/one/sweet-react-components";
import {ManagePageFieldsContainer,ManagePageContainer,SweetManagePage} from "Libs/one/sweet-one-react";
import ParameterEntity from "../../entity/ParameterEntity";

export default class CommonParameterView extends SweetManagePage {
    moduleName='common';
    tableName='parameter';
    constructor(props) {
        super(props);
        this.state = {
            formData:new ParameterEntity(),

        };
    }

    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

    }
    render(){
        return <ManagePageContainer title={'تعریف پارامتر'}>
        <ManagePageFieldsContainer>
            <TextViewBox title={'نام نمایشی'} value={this.state.formData.displayName}/>
            <TextViewBox title={'مقدار'} value={this.state.formData.value}/>
            <TextViewBox title={'توضیحات'} value={this.state.formData.description}/>

        </ManagePageFieldsContainer>
        {this.getSaveAndBackButtons()}
        </ManagePageContainer>
    }
}
