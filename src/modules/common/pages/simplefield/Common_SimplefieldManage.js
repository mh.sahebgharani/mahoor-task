// @flow
import * as React from 'react';
import {SweetButton,TextBox,PickerBox} from 'Libs/one/sweet-react-components';
import SimplefieldEntity from "../../entity/SimplefieldEntity";
import {SweetManagePage,ManagePageContainer} from "Libs/one/sweet-one-react";
export default class Common_SimplefieldManage extends SweetManagePage {
    moduleName='common';
    tableName='simplefield';
    constructor(props) {
        super(props);
        this.state = {
            canEdit:this.canEdit(),
            formData:new SimplefieldEntity(),

        };
    }

    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0){
            this.state.formData.get(this.getID(),()=>{
                this.setState({formData:this.state.formData});
            });
        }

    }
    render(){
        return <ManagePageContainer title={'تعریف فیلد'}>
            <TextBox title={'نام'} id='name' value={this.state.formData.name}
                onChangeText={this.getOnTextChangeListener('name')}/>
            <TextBox title={'نام نمایشی'} id='displayName' value={this.state.formData.displayName}
                onChangeText={this.getOnTextChangeListener('displayName')}/>
            <PickerBox
                title={'قابلیت ویرایش'}
                selectedValue={this.state.formData.isEditable}
                onValueChange={this.getOnValueChangeListener('isEditable')}
                options={[{id:0,name:'خیر'},{id:1,name:'بله'}]}
            />

            <div className='text-center'>
                {this.state.canEdit &&
                <SweetButton value={'ذخیره'}
                             onButtonPress={(afterFetchListener) => {
                                 this.state.formData.save(this.getID(),res=>{
                                     return this.returnBack();
                                 });
                             }
                             }
                />
                }
                <SweetButton value={'برگشت'} onButtonPress={this.returnBack} />
            </div>
        </ManagePageContainer>
    }
}
