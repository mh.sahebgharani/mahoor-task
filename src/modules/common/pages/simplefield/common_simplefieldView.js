// @flow
import * as React from 'react';
import { MDBContainer, MDBRow, MDBCol, MDBBtn,FormInline, Input } from 'mdbreact';
import {AccessManager,SweetFetcher} from 'Libs/one/sweet-one-react';
import {Common} from 'Libs/one/sweet-react-common-tools';
import SweetComponent from '../../../../classes/sweet-component';

class common_simplefieldView extends SweetComponent {
    constructor(props) {
        super(props);
        this.state = {
                canEdit:AccessManager.UserCan('common','simplefield',AccessManager.EDIT),

			name:'',
			displayName:'',
			isEditable:'',
        };
        if(this.props.match.params.id>0){
        new SweetFetcher().Fetch('/common/simplefield/'+this.props.match.params.id, SweetFetcher.METHOD_GET,null,
        data => {
            data.Data=Common.convertNullKeysToEmpty(data.Data);

                 this.setState({
name:data.Data.name,
displayName:data.Data.displayname,
isEditable:data.Data.is_editable,});

            },
            null,'common.simplefield',AccessManager.VIEW,
            this.props.history);
        }//IF

    }
    render(){
        return <MDBContainer>
            <MDBRow>
                <MDBCol md='6'>
                    <form>
                        <p className='h5 text-center mb-4'> simplefield</p>

                        <div className='form-group'>
                            <label>نام</label>
                            <label
                                className='valuelabel'>
                                {this.state.name}
                            </label>
                        </div>
                        <div className='form-group'>
                            <label>نام نمایشی</label>
                            <label
                                className='valuelabel'>
                                {this.state.displayName}
                            </label>
                        </div>
                         <div className='form-group'>
                            <FormInline>

                           <label>قابلیت ویرایش</label>
                                    <Input
                                        onClick={() => this.setState({isEditable : 0})}
                                        checked={this.state.is_editable== 0}
                                        label='قابلیت ویرایش ندارد'
                                        type='radio'
                                        id='radioisEditable1'
                                readOnly={!this.state.canEdit}
                                    />
                                    <Input
                                        onClick={() => this.setState({isEditable : 1})}
                                        checked={this.state.isEditable == 1}
                                        label='قابلیت ویرایش دارد'
                                        type='radio'
                                        id='radioisEditable2'
                                readOnly={!this.state.canEdit}
                                    />
                                </FormInline>
                        </div>
                            <div className='text-center'>
                            <MDBBtn onClick={() =>
                             {
                                this.props.history.push('/common/simplefields');
                             }
                            }>برگشت</MDBBtn>
                        </div>
                    </form>
                </MDBCol>
            </MDBRow>
        </MDBContainer>
    }
}

export default common_simplefieldView;
