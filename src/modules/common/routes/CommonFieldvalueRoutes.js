
import common_fieldvalueList from '../pages/fieldvalue/common_fieldvalueList';
import Common_FieldvalueManage from '../pages/fieldvalue/Common_FieldvalueManage';
let CommonFieldvalueRoutes=[];
CommonFieldvalueRoutes.push({ path: '/common/fieldvalue/management/:simplefieldid/:id',exact:false, name: 'ویرایش مقدار فیلد',component:Common_FieldvalueManage});
CommonFieldvalueRoutes.push({ path: '/common/fieldvalue/management/:simplefieldid',exact:false, name: 'تعریف مقدار فیلد',component:Common_FieldvalueManage});
CommonFieldvalueRoutes.push({ path: '/common/fieldvalue/:simplefieldid',exact:false, name: 'لیست مقادیر فیلد',component:common_fieldvalueList});
export default CommonFieldvalueRoutes;
