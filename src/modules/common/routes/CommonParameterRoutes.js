
import CommonParameterList from '../pages/parameter/CommonParameterList';
import CommonParameterManage from '../pages/parameter/CommonParameterManage';
import CommonParameterView from '../pages/parameter/CommonParameterView';
let CommonParameterRoutes=[];
CommonParameterRoutes.push({ path: '/common/parameter',exact:true, name: 'لیست پارامترها',component:CommonParameterList});
CommonParameterRoutes.push({ path: '/common/parameter/management/:id',exact:false, name: 'ویرایش پارامتر',component:CommonParameterManage});
CommonParameterRoutes.push({ path: '/common/parameter/management',exact:false, name: 'تعریف پارامتر',component:CommonParameterManage});CommonParameterRoutes.push({ path: '/common/parameter/view/:id',exact:false, name: 'پارامتر',component:CommonParameterView});
export default CommonParameterRoutes;
