
import CommonParameterRoutes from './CommonParameterRoutes';
import CommonFieldvalueRoutes from "./CommonFieldvalueRoutes";
import CommonSystemmessageRoutes from "./CommonSystemmessageRoutes";
let CommonRoutes=[];
CommonRoutes = CommonRoutes.concat(CommonParameterRoutes);
CommonRoutes = CommonRoutes.concat(CommonFieldvalueRoutes);
CommonRoutes = CommonRoutes.concat(CommonSystemmessageRoutes);
export default CommonRoutes;
