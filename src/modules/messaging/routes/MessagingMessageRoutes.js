
import MessagingMessageList from '../pages/message/MessagingMessageList';
import MessagingMessageManage from '../pages/message/MessagingMessageManage';
let MessagingMessageRoutes=[];
MessagingMessageRoutes.push({ path: '/messaging/message/:userid',exact:true, name: 'لیست پیام ها',component:MessagingMessageList});
MessagingMessageRoutes.push({ path: '/messaging/message/management/:userid',exact:false, name: 'تعریف پیام',component:MessagingMessageManage});
export default MessagingMessageRoutes;
