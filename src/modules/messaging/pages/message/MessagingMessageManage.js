// @flow
import * as React from 'react';
import {TextBox,PickerBox} from "Libs/one/sweet-react-components";
import {ManagePageFieldsContainer,ManagePageContainer,SweetManagePage} from "Libs/one/sweet-one-react";
import MessageEntity from "../../entity/MessageEntity";


export default class MessagingMessageManage extends SweetManagePage {
    moduleName='messaging';
    tableName='message';
    constructor(props) {
        super(props);
        let Ent=new MessageEntity();
        Ent.receiverUser={id:this.getUserID()};
        Ent.notifyByApp=0;
        Ent.isSeen=0;
        this.state = {
            canEdit:true,
            formData:Ent,
        };
    }

    getUserID(){
        return this.props.match.params.userid;
    }
    componentDidMount() {
        super.componentDidMount();
    }
    render(){
        return <ManagePageContainer title={'تعریف پیام'}>
        <ManagePageFieldsContainer>
            <TextBox title={'عنوان'} id='title' value={this.state.formData.title}
                onChangeText={this.getOnTextChangeListener('title')}/>
            <TextBox title={'متن پیام'} id='text' value={this.state.formData.text}
                onChangeText={this.getOnTextChangeListener('text')}/>
            <PickerBox
                title={'اطلاع رسانی از طریق پیامک'}
                selectedValue={this.state.formData.notifyBySMS.id}
                onValueChange={this.getOnValueChangeListener('notifyBySMS')}
                options={[{id:0,name:'خیر'},{id:1,name:'بله'}]}
            />

        </ManagePageFieldsContainer>
        {this.getSaveAndBackButtons()}
        </ManagePageContainer>
    }
    returnBack() {
        this.props.history.push('/' + this.moduleName + '/' + this.tableName+'/'+this.getUserID());
    }
}
