// @flow
import * as React from 'react';
import MessageEntity from "../../entity/MessageEntity";
import {ListPageContainer,SweetManageListPage} from "Libs/one/sweet-one-react";


export default class MessagingMessageList extends SweetManageListPage {
    moduleName='messaging';
    tableName='message';
    entity = new MessageEntity();
    constructor(props){
        super(props);
        this.LoadData(this.state.pageSize,1,null,null);
    }
    LoadData(pageSize,page,sorted,filtered) {
        this.entity.getAll(this.getUserID(),pageSize, page, sorted, [], (data,RecordCount) => {
            let Pages = this.getPageCount(RecordCount, pageSize);
            this.setState({data: this.normalizeAllListItems(data), pages: Pages})
        });
    }
    getUserID(){
        return this.props.match.params.userid;
    }
    render() {
        return <ListPageContainer title={'لیست پیام ها'}>

            {this.getAddButton()}
            {this.getTable()}
        </ListPageContainer>;
    }

    getAddLink(){
        return '/'+this.moduleName+'/'+this.tableName+'/management/'+this.getUserID();
    }
    columns =
    [
        {Header: 'عنوان',id: 'title',accessor: 'title'},
        {Header: 'متن پیام',id: 'text',accessor: 'text'},
    ];
}
