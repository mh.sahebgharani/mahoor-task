// @flow
import {SweetEntity,AccessManager} from 'Libs/one/sweet-one-react';

export default class MessageEntity extends SweetEntity {
    __moduleName='messaging';
    __tableName='message';

    title='';
    text='';
    senderUser={};
    receiverUser={};
    notifyBySMS='';
    isSeen='';
    notifyByApp='';
    fields={id:{field:'id'},title:{field:'title'},text:{field:'text'},
        senderUser:{field:'sender__user_fid',type:'fid'},
        receiverUser:{field:'receiver__user_fid',type:'fid'},
        notifyBySMS:{field:'notifybysms'},isSeen:{field:'is_seen'},
        notifyByApp:{field:'notifybyapp'}};
    get(id,afterFetch){
        this._get(this.getBaseURL()+'/'+id,this.getServiceName(),AccessManager.VIEW,afterFetch);
    }
    getAll(userID,pageSize,page,sorted,filtered,afterFetch,onError){
        const filterAndSortString=this.getFilterString(pageSize,page,sorted,filtered);
        const url=this.getBaseURL()+'/usermessages/'+userID+filterAndSortString;
        this._getAll(url,this.getServiceName(),AccessManager.LIST,afterFetch,onError);
    }
    save(id,afterFetchListener,onError){
        console.log(this);
        const formData=this.getFormRequest();
        this._save(this.getServiceName(),this.getBaseURL(),id,formData,afterFetchListener,onError);
    }
    delete(id,afterFetchListener,onError){
        this._delete(this.getServiceName(),this.getBaseURL()+"/"+id,afterFetchListener,onError);
    }
}
