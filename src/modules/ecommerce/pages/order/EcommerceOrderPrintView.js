// @flow
import * as React from 'react';
import {TextViewBox, ImageModal, SweetButton, SweetModal, PickerBox} from "Libs/one/sweet-react-components";
import {ManagePageContainer,ManagePageFieldsContainer,SweetManagePage} from "Libs/one/sweet-one-react";
import Constants from "../../../../classes/Constants";
import OrderEntity from "../../entity/OrderEntity";
import {MDBCol, MDBContainer, MDBRow} from "mdbreact";
import '../../files/styles/ecommerce_order.css';
export default class EcommerceOrderView extends SweetManagePage {
    moduleName='ecommerce';
    tableName='order';
    constructor(props) {
        super(props);
        this.state = {
            formData:new OrderEntity(),
            isStatusChangeModalOpen:false,
        };
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

        this.loadSimpleFields();
    }
    userID(){
        return this.props.match.params.userid||-1;
    }
    returnBack() {
        this.props.history.push('/' + this.moduleName + '/' + this.tableName+'/'+this.userID());
    }

    componentDidMount() {
        super.componentDidMount();
    }
    render(){
        return <ManagePageContainer title={'اطلاعات سفارش'}>
            <SweetModal centered visible={this.state.isStatusChangeModalOpen} onHideRequest={()=>{
                this.setState({isStatusChangeModalOpen:false});
            }}>
                <PickerBox
                    title={'وضعیت جدید'}
                    selectedValue={this.state.formData.orderStatus.id}
                    onValueChange={this.getOnFidValueChangeListener('orderStatus')}
                    options={this.state.formData.simpleFields.orderstatus}
                />

                <SweetButton value={'ثبت'} onButtonPress={(onEnd)=>{
                    this.state.formData.save(this.getID(),(res)=>{
                        this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});
                        this.setState({isStatusChangeModalOpen:false});
                        onEnd(true);
                    });
                }}/>
            </SweetModal>
        <ManagePageFieldsContainer cols={3}>
            <TextViewBox title={'مبلغ قابل پرداخت'} value={this.state.formData.toPayPrice}/>
            <TextViewBox title={'نام کاربر'} value={this.state.formData.name}/>
            <TextViewBox title={'تلفن همراه کاربر'} value={this.state.formData.mobile}/>
            <TextViewBox title={'روش پرداخت'} value={this.state.formData.paymenttype.displayname}/>
            <TextViewBox title={'بازه زمانی ارسال سفارش'} value={this.state.formData.sendTime.displayname}/>
            <TextViewBox onClick={()=>{
                this.setState({isStatusChangeModalOpen:true});
            }} title={'وضعیت سفارش'} value={this.state.formData.orderStatus.displayname}/>
            <TextViewBox title={'تخفیف'} value={this.state.formData.discount}/>
            <TextViewBox title={'تلفن تحویل گیرنده'} value={this.state.formData.address.tel}/>
            <TextViewBox title={'امتیاز مشتری'} value={this.state.formData.userrate}/>
            <TextViewBox title={'نظر مشتری'} colweight={3} value={this.state.formData.usercomment}/>
            <TextViewBox colweight={2} title={'آدرس'} value={this.state.formData.address.address}/>
            <a className={'fonticonlink'} href={'https://maps.google.com/?q='+this.state.formData.address.latitude+','+this.state.formData.address.longitude}><i
                className="fa fa-map" aria-hidden="true"></i>
            </a>

            <div colweight={3}>
                {this.productKinds(this.state.formData.productkinds)}
            </div>

        </ManagePageFieldsContainer>
        {this.getSaveAndBackButtons()}
        </ManagePageContainer>
    }
    productKinds(orderProductKinds){
        // console.log(orderProductKinds);
        const result=orderProductKinds.map((pk,index)=>{
            return <MDBRow>
                <MDBCol>{index+1}</MDBCol>
                <MDBCol>{pk.product.displayname}</MDBCol>
                <MDBCol>{pk.quality.displayname}</MDBCol>
                <MDBCol>{pk.count}</MDBCol>
                <MDBCol>{pk.price}</MDBCol>
                <MDBCol>{pk.count*pk.price}</MDBCol>
            </MDBRow>
        });
        return <MDBContainer className={'orderproductkinds'}>
            <MDBRow>

                <MDBCol>ردیف</MDBCol>
                <MDBCol>محصول</MDBCol>
                <MDBCol>کیفیت</MDBCol>
                <MDBCol>تعداد</MDBCol>
                <MDBCol>قیمت واحد</MDBCol>
                <MDBCol>قیمت کل</MDBCol>
            </MDBRow>
            {result}
        </MDBContainer>;

    }
}
