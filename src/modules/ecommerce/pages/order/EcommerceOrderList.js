// @flow
import * as React from 'react';
import Constants from '../../../../classes/Constants';
import OrderEntity from "../../entity/OrderEntity";
import {ListPageContainer,SweetManageListPage} from "Libs/one/sweet-one-react";
import {PickerBox, SweetButton, SweetModal} from "Libs/one/sweet-react-components";
import {Link} from "react-router-dom";
import {IoMdAddCircle} from "react-icons/all";
import {MDBContainer} from "mdbreact";


export default class EcommerceOrderList extends SweetManageListPage {
    moduleName='ecommerce';
    tableName='order';
    entity = new OrderEntity();
    __defaultFilters=[{id:'user_fid',value:this.userID()},
        {id:'orderstatus',value:this.status()}];
    __defaultSorts=[{id:'id',desc:'1'}];
    componentDidMount() {
        super.componentDidMount();
    }
    years=[];
    months=[];
    days=[];
    constructor(props){
        super(props);
        this.entity.RequestedFields=['paymenttype','sendtime','orderstatus'];
        this.years=[1399,1400,1401,1402,1403,1404,1405,1406,1407,1408,1409,1410].map(a=>{return {id:a,displayName:a}});
        this.days=[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31].map(a=>{return {id:a,displayName:a}});
        this.months=[1,2,3,4,5,6,7,8,9,10,11,12].map(a=>{return {id:a,displayName:a}});
        // console.log('loading in constructor');
        // this.LoadData(this.state.pageSize,1,null,null);
        this.state={
            ...super.state,
            year:'1399',
            month:'12',
            day:'1',
            endyear:'1399',
            endmonth:'12',
            endday:'22',

        }
    }

    status(){
        return this.props.match.params.status||-1;
    }
    returnBack() {
        this.props.history.goBack();
    }
    userID(){
        return this.props.match.params.userid||-1;
    }
    render() {
        return <ListPageContainer title={'لیست سفارشات'}>
            <SweetModal centered visible={this.state.isDateSelectVisible} onHideRequest={()=>{
                this.setState({isDateSelectVisible:false});
            }}>
                <MDBContainer>

                    <div>از</div>
                    <PickerBox
                        title={'سال'}
                        selectedValue={this.state.year}
                        onValueChange={val=>{this.setState({year:val})}}
                        options={this.years}
                    />
                    <PickerBox
                        title={'ماه'}
                        selectedValue={this.state.month}
                        onValueChange={val=>{this.setState({month:val})}}
                        options={this.months}
                    />
                    <PickerBox
                        title={'روز'}
                        selectedValue={this.state.day}
                        onValueChange={val=>{this.setState({day:val})}}
                        options={this.days}
                    />

                    <div>تا</div>
                    <PickerBox
                        title={'سال'}
                        selectedValue={this.state.endyear}
                        onValueChange={val=>{this.setState({endyear:val})}}
                        options={this.years}
                    />
                    <PickerBox
                        title={'ماه'}
                        selectedValue={this.state.endmonth}
                        onValueChange={val=>{this.setState({endmonth:val})}}
                        options={this.months}
                    />
                    <PickerBox
                        title={'روز'}
                        selectedValue={this.state.endday}
                        onValueChange={val=>{this.setState({endday:val})}}
                        options={this.days}
                    />
                <SweetButton value={'دریافت گزارش'} onButtonPress={(onEnd)=>{
                    OrderEntity.getDateIntervalReport(this.state.year,this.state.month,this.state.day,this.state.endyear,this.state.endmonth,this.state.endday,(res)=>{
                        console.log(res);
                        this.setState({isDateSelectVisible:false});
                        onEnd(true);
                        window.location.href =Constants.SiteURL+'/'+res.Data.path;
                    },()=>{ this.setState({isDateSelectVisible:false});onEnd(false)});
                }}/>
                </MDBContainer>
            </SweetModal>
            <div className={'topoperationsrow'}>
                {this.userID()<= 0 &&
                <div><Link className={'link'} to={'/ecommerce/factorexport'}>
                    <div>
                        <i className="fa fa-file-excel-o" aria-hidden="true"></i>
                        <div className={'topoperationsrowtitle'}>دریافت خروجی Excel سفارشات</div>
                    </div>
                </Link>
                    <a className={'link'} href={'#'} onClick={()=>{
                        this.setState({isDateSelectVisible:true});
                    }}>
                        <div>
                            <i className="fa fa-file-zip-o" aria-hidden="true"></i>
                            <div className={'topoperationsrowtitle'}>دریافت گزارشات روزانه</div>
                        </div>
                    </a>
                </div>

                }
            </div>
            {this.getTable()}
            {this.userID()>0 &&
            <SweetButton value={'برگشت'} onButtonPress={(onEnd)=>{
                this.returnBack();
                onEnd(true);
            }}/>
            }
        </ListPageContainer>;
    }

    getOperationsBox(id){
        return <div className={'operationsrow'}>
            {this.getViewButton(id)}
        </div>
    }

    getViewLink(id){
        return '/'+this.moduleName+'/'+this.tableName+'/view/'+this.userID()+'/'+id;
    }
    columns =
    [
        {Header: 'کد',id: 'orderid',accessor: 'id'},
        {Header: 'مبلغ سفارش',id: 'topayprice',accessor: 'toPayPrice'},
        {Header: 'نام ',id: 'name',accessor: 'name'},
        // {Header: 'تلفن همراه ',id: 'mobile',accessor: 'mobile'},
        {Header: 'روش پرداخت',id: 'paymenttype',accessor: data => data.paymenttype.displayname},
        {Header: 'زمان تحویل',id: 'sendtime',accessor: data => data.sendTime.displayname},
        {Header: 'وضعیت سفارش',id: 'orderstatus',accessor: data => data.orderStatus.displayname},
        {Header: 'عملیات',accessor: 'id',Cell: props => {return this.getOperationsBox(props.value)}}
    ];
}
