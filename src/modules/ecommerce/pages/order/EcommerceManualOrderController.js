// @flow
import * as React from 'react';
import { SweetManagePage } from "Libs/one/sweet-one-react";
import EcommerceuserEntity from "../../entity/EcommerceuserEntity";
import { Ecommerce } from 'Libs/one/sweet-one-app-react';
import RegionEntity from "../../../location/entity/RegionEntity";
import SendtimeEntity from "../../entity/SendtimeEntity";
import PlaceEntity from "../../../location/entity/PlaceEntity";
import OrderEntity from "../../entity/OrderEntity";
export default class EcommerceManualOrderController extends SweetManagePage {
    constructor(props) {
        super(props);
        this.state = {
            groups: [],
            group: {},
            products: [],
            productKinds: [],
            deliveries: [],
            customers: [],
            orderItems: [],
            delivery: { id: -1 },
            product: {},
            discount: 0,
            search: {
                code: '',
                tel: '',
                name: '',
                address: '',
            },
            newUser: {
                code: '',
                tel: '',
                name: '',
                address: '',
                region: 1,
            },
            receiverInfo: {
                name: '',
                tel: '',
                code: 0,
                address: {},
            },
            addNewModalVisible: false,
            regions: [],
            sendTimes: [],
        };
        this.loadProducts();
        this.loadCustomers();
        this.loadDeliveries();
        this.loadRegions();
    }
    clearState(){
        const newState = {
            orderItems: [],
            delivery: { id: -1 },
            product: {},
            discount: 0,
            search: {
                code: '',
                tel: '',
                name: '',
                address: '',
            },
            newUser: {
                code: '',
                tel: '',
                name: '',
                address: '',
                region: 1,
            },
            receiverInfo: {
                name: '',
                tel: '',
                code: 0,
                address: {},
            },
            addNewModalVisible: false,
        };
        this.setState(newState);

    }
    getOrderType(){
        return Ecommerce.Entity.OrderType.Manual;
    }
    loadSendTimes() {
        if (this.state.user.defaultAddress) {
            const stent = new SendtimeEntity();
            stent.getAll(null, null, [{ id: 'priority', desc: true }, { id: 'starttime', desc: true }],
                [{ 'id': 'placeid', value: this.state.user.defaultAddress.id }], (res) => {
                    // let visibles=[];
                    // if(res!=null && res.length>0)
                    //     visibles=res.filter(st=>{return st.isEnabled});
                    // console.log(visibles);
                    this.setState({ sendTimes: res });
                });
        }
    }
    getSendTimeFromID(id) {
        console.log(this.state.sendTimes);
        return this.findById(id, this.state.sendTimes);
    }
    findById(theID, theArray) {
        for (let item of theArray) {
            if (item.id == theID)
                return item;
        }
        return null;
    }
    getSendPriceFromSendTime(sendtime, sum) {
        if (sendtime) {
            let price = 0;
            if (sendtime.tariff.minFreeSendPrice > sum) {
                price = sendtime.tariff.priceStart;
            }
            return price;
        }
        return 0;
    }
    loadProducts = () => {
        new Ecommerce.Entity.ProductEntity().getAllByGroups(10000, 1, null, [{ id: 'groupnames', value: JSON.stringify(['jointgroups','iranian_food','fast_food','restaurant','cafe']) }, { id: 'removeinvisibles', value: '1' }]
            , (data) => {
                // let data=[];
                let firstFull = data.findIndex(a => a.products != null && a.products.length > 0);
                // console.log(firstFull);
                const products = data[firstFull].products;
                const product = products[0];
                console.log(products);
                const productKinds = product.productKinds;
                const productKind = productKinds[0];
                // this.setState({groups:data});
                this.setState({ groups: data, products: products, productKinds: productKinds, product: product, productKind: productKind });
            }, null);
    };
    loadCustomers = () => {
        new EcommerceuserEntity().getAll(10000, 1, null, null
            , (data) => {
                this.setState({ customers: data });
            }, null);
    };
    loadRegions = () => {
        new RegionEntity().getAll(1000, 1, null, null
            , (data) => {
                // console.log(data);
                this.setState({ regions: data });
            }, null);
    };
    loadDeliveries = () => {
        new EcommerceuserEntity().getAllDeliveries((data) => {
            this.setState({ deliveries: data });
        }, null);
    };
    findInCustomers() {
        let { search } = this.state;

        return this.state.customers.filter(c => {
            let found = true;
            if (search.name.trim())
                found = found && c.user.name.includes(search.name.trim());
            if (search.tel.trim())
                found = found && (c.user.phone + "").includes(search.tel.trim());
            if (search.code.trim())
                found = found && (c.code + "").includes(search.code.trim());
            if (search.address.trim())
                found = found && c.defaultAddress && c.defaultAddress.address.includes(search.address.trim());
            return found;
        });
    }
    getOrderSum() {
        const { orderItems } = this.state;
        let sum = 0;
        for (let i = 0; i < orderItems.length; i++)
            sum += orderItems[i].count * orderItems[i].productKind.price;
        return sum;
    }
    addNewUser(newUser, onEnd) {
        let Location = new PlaceEntity();
        Location.address = newUser.address;
        Location.region = newUser.region;
        Location.code = newUser.code;
        Location.tel = newUser.tel;
        Location.title = newUser.name;
        // console.log(Location);
        const theUser = new EcommerceuserEntity();
        theUser.defaultAddress = Location;
        theUser.user.name = newUser.name;
        theUser.code = newUser.code;
        theUser.saveManual(undefined, (newCustomer) => {
            const newCustomers = this.state.customers;
            newUser.defaultAddress = Location;
            newUser.id = newCustomer.Data.id;
            newUser.code = newCustomer.Data.code;
            newUser.user = { name: newUser.name, phone: newUser.tel, id: newCustomer.Data.user_fid };
            newCustomers.push(newUser);
            this.setState({ customers: newCustomers, user: newUser, addNewModalVisible: false }, () => { this.loadSendTimes() });
            onEnd(true);

        });
    }
    editUser(newUser, onEnd) {
        let Location = new PlaceEntity();
        Location.address = newUser.address;
        Location.region = newUser.region;
        Location.code = newUser.code;
        Location.tel = newUser.tel;
        Location.title = newUser.name;
        // console.log(Location);
        const theUser = new EcommerceuserEntity();
        theUser.defaultAddress = Location;
        theUser.user.name = newUser.name;
        theUser.code = newUser.code;
        theUser.saveManual(newUser.ecommerce_user_id, (newCustomer) => {
            const newCustomers = this.state.customers;
            newUser.defaultAddress = Location;
            newUser.id = newCustomer.Data.id;
            newUser.code = newCustomer.Data.code;
            newUser.user = { name: newUser.name, phone: newUser.tel, id: newCustomer.Data.user_fid };
            newCustomers.push(newUser);
            this.setState({ customers: newCustomers, user: newUser, addNewModalVisible: false }, () => { this.loadSendTimes() });
            onEnd(true);

        });
    }
    addOrder(orderItems, address, onEnd) {
        const { receiverInfo } = this.state;
        console.log(receiverInfo);

        let o = new OrderEntity();
        o.region = { id: 1 };
        o.manualAddress = address;
        o.orderType=this.getOrderType();
        o.manualAddress.tel = receiverInfo.tel;
        o.name = receiverInfo.name;
        o.mobile = receiverInfo.tel;
        o.delivery = this.state.delivery;
        let items = {};
        orderItems.forEach(item => {
            let itemObject = {};
            itemObject.count = item.count;
            items[item.productKind.id] = itemObject;
        });
        o.productkinds = items;
        // this.props.history.push('/index');
        // this.clearState();
        o.saveManualOrder(undefined, () => {
            onEnd(true);
            this.clearState();
            // this.props.history.push('/index');
        });
    }
}
