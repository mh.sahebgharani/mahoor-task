// @flow
import * as React from 'react';
import OrderEntity from "../../entity/OrderEntity";
import {ListPageContainer,SweetManageListPage} from "Libs/one/sweet-one-react";
import {PickerBox, SweetButton, SweetModal} from "Libs/one/sweet-react-components";
import Constants from "../../../../classes/Constants";
export default class EcommerceDeliveryOrderList extends SweetManageListPage {
    moduleName='ecommerce';
    tableName='order';
    entity = new OrderEntity();
    __defaultFilters=[
        {id:'delivery__ecommerceuser_fid',value:this.deliveryID()},
        {id:'orderstatus',value:this.status()},
    ];
    __defaultSorts=[{id:'id',desc:'1'}];
    componentDidMount() {
        super.componentDidMount();
    }
    constructor(props){
        super(props);
        // this.LoadData(this.state.pageSize,1,null,null);
    }
    returnBack() {
        this.props.history.goBack();
    }
    deliveryID(){
        return this.props.match.params.userid||-1;
    }
    status(){
        return this.props.match.params.status||-1;
    }

    getListPDF(pageSize,page,sorted,filtered) {
        let finalFilter=this.getFilterString(filtered);
        let finalSort=this.getSortString(sorted);
        this.entity.getAllPDF(pageSize, page, finalSort, finalFilter, (data,RecordCount) => {
            window.location.href=Constants.SiteURL+data.url;
            // console.log(data);
        },(err)=>{console.log(err)});
    }
    render() {
        return <ListPageContainer title={'لیست سفارشات'}>
            <div className={'topoperationsrow'}>
                <div>

                    <a className={'link'} href={'#'} onClick={()=>{
                        this.getListPDF(this.state.pageSize,1,null,null);
                    }}>
                        <div>
                            <i className="fa fa-print" aria-hidden="true"></i>
                            <div className={'topoperationsrowtitle'}>چاپ فهرست سفارشات</div>
                        </div>
                    </a>
                </div>
            </div>
            {this.getTable()}
            {this.deliveryID()>0 &&
            <SweetButton value={'برگشت'} onButtonPress={(onEnd)=>{
                this.returnBack();
                onEnd(true);
            }}/>
            }
        </ListPageContainer>;
    }

    getOperationsBox(id){
        return <div className={'operationsrow'}>
            {this.getViewButton(id)}
        </div>
    }

    getViewLink(id){
        return '/'+this.moduleName+'/'+this.tableName+'/view/'+this.deliveryID()+'/'+id;
    }
    columns =
    [
        {Header: 'کد',id: 'orderid',accessor: 'id'},
        {Header: 'مبلغ سفارش',id: 'topayprice',accessor: 'toPayPrice'},
        {Header: 'نام ',id: 'name',accessor: 'name'},
        // {Header: 'تلفن همراه ',id: 'mobile',accessor: 'mobile'},
        {Header: 'روش پرداخت',id: 'paymenttype',accessor: data => data.paymenttype.displayname},
        {Header: 'زمان تحویل',id: 'sendtime',accessor: data => data.sendTime.displayname},
        {Header: 'وضعیت سفارش',id: 'orderstatus',accessor: data => data.orderStatus.displayname},
        {Header: 'عملیات',accessor: 'id',Cell: props => {return this.getOperationsBox(props.value)}}
    ];
}
