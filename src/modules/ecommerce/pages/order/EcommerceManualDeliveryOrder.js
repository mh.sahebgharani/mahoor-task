// @flow
import * as React from 'react';
import { MDBCol, MDBContainer, MDBRow } from "mdbreact";
import '../../files/styles/ecommerce_manualorder.scss';
import { PickerBox, SweetButton, SweetModal, TextBox } from "Libs/one/sweet-react-components";
import EcommerceBaseManualOrder from "./EcommerceBaseManualOrder";
import {Ecommerce} from "Libs/one/sweet-one-app-react";
export default class EcommerceManualDeliveryOrder extends EcommerceBaseManualOrder {

    getOrderType(){
        return Ecommerce.Entity.OrderType.Delivery;
    }
    getDeliveryViews(deliveries) {
        const deliveryColCount = 3;
        const deliveryViews = deliveries.flatMap((tempDelivery, index) => {
            if (index % deliveryColCount === 0) {
                return [<tr>
                    {
                        deliveries.slice(index, index + deliveryColCount).map((delivery, index2) => {
                            return <td onClick={() => {
                                this.setState({ delivery: delivery });
                            }} className={this.state.delivery === delivery ? 'selected' : ''}>{delivery.user.name}</td>;
                        })
                    }
                    {index + deliveryColCount >= deliveries.length &&
                        <td colspan={(deliveries.length - index) - 1}>&nbsp;</td>
                    }
                </tr>]
            }
            return null;
        });
        return deliveryViews;
    }
    render() {
        const { deliveries, regions, addNewModalVisible } = this.state;
        let { search, newUser, user, receiverInfo } = this.state;
        const customers = this.findInCustomers();
        let address = {};
        if (user && user.defaultAddress)
            address = user.defaultAddress;
        let sum = this.getOrderSum();
        let sendPrice = this.getSendPriceFromSendTime(this.getSendTimeFromID(1), sum);
        const deliveryViews = this.getDeliveryViews(deliveries);
        const addNewModal = <SweetModal visible={addNewModalVisible} onHideRequest={() => { this.setState({ addNewModalVisible: false }) }}>
            <MDBContainer>
                <MDBRow>
                    <MDBCol md={6} className={'manualorderpage_customeraddress'}>
                        <TextBox value={newUser.code} title={'کد'} onChangeText={(a) => {
                            newUser.code = a;
                            this.setState({ newUser: newUser });
                        }} />
                    </MDBCol>
                    <MDBCol md={6} className={'manualorderpage_customeraddress'}>
                        <TextBox value={newUser.tel} title={'تلفن'} onChangeText={(a) => {
                            newUser.tel = a;
                            this.setState({ newUser: newUser });
                        }} />
                    </MDBCol>
                    <MDBCol md={6} className={'manualorderpage_customeraddress'}>
                        <TextBox value={newUser.name} title={'نام'} onChangeText={(a) => {
                            newUser.name = a;
                            this.setState({ newUser: newUser });
                        }} />
                    </MDBCol>
                    {!newUser.address &&
                        <MDBCol md={6} className={'manualorderpage_customeraddress'}>
                            <PickerBox selectedValue={newUser.region} options={regions} title={'منطقه'}
                                       onValueChange={(a) => {
                                           newUser.region = a;
                                           this.setState({newUser: newUser});
                                       }}/>
                        </MDBCol>
                    }
                    <MDBCol md={12} className={'manualorderpage_customeraddress'}>
                        <TextBox value={newUser.address} title={'آدرس'} onChangeText={(a) => {
                            newUser.address = a;
                            this.setState({ newUser: newUser });
                        }} />
                    </MDBCol>
                    <MDBCol md={12} className={'manualorderpage_customeraddress'}>
                        <SweetButton value={'ثبت'} onButtonPress={(onEnd) => {
                            if(!this.state.editingUser)
                                this.addNewUser(newUser, onEnd);
                            else
                                this.editUser(newUser, onEnd);
                        }} />
                    </MDBCol>
                </MDBRow>
            </MDBContainer>
        </SweetModal>;
        const customerSearchBar = <MDBRow className={'manualorderpage_customeraddresses_search card'}>
            <MDBContainer>
                <MDBRow>
                    {<MDBCol md={12}><MDBRow>
                        <MDBCol md={3} className={'manualorderpage_customeraddress'}>
                            <TextBox value={search.code} title={'کد'} onChangeText={(a) => {
                                search.code = a;
                                this.setState({ search: search });
                            }} />
                        </MDBCol>
                        <MDBCol md={3} className={'manualorderpage_customeraddress'}>
                            <TextBox value={search.tel} title={'تلفن'} onChangeText={(a) => {
                                search.tel = a;
                                this.setState({ search: search });
                            }} />
                        </MDBCol>
                        <MDBCol md={3} className={'manualorderpage_customeraddress'}>
                            <TextBox value={search.name} title={'نام'} onChangeText={(a) => {
                                search.name = a;
                                this.setState({ search: search });
                            }} />
                        </MDBCol>
                        <MDBCol md={3} className={'manualorderpage_customeraddress'}>
                            <TextBox value={search.address} title={'آدرس'} onChangeText={(a) => {
                                search.address = a;
                                this.setState({ search: search });
                            }} />
                        </MDBCol>
                    </MDBRow></MDBCol>
                    }
                </MDBRow>
            </MDBContainer>
        </MDBRow>;
        const customerList = <MDBRow className={'manualorderpage_customers  card'}>
            <MDBContainer>

                <table>
                    {customers.slice(0, 10000).map((u, index) => {
                        const addressOfUser = u.defaultAddress ? u.defaultAddress.address : '';
                        return <tr className={'manualorderpage_customer ' + (this.state.user === u ? 'selected' : '')} onClick={() => {
                            receiverInfo.name = u.user.name;
                            receiverInfo.tel = u.user.phone;
                            receiverInfo.code = u.user.id;
                            receiverInfo.address = { ...u.defaultAddress };
                            this.setState({ user: u, receiverInfo: receiverInfo }, () => { this.loadSendTimes() })
                        }}>
                            <td>
                                {u.user.name}
                            </td>
                            <td>
                                {u.user.phone}
                            </td>
                            <td>
                                {u.code}
                            </td>
                            <td>
                                {addressOfUser}
                            </td>
                            <td >
                                <a  onClick={() => {
                                    this.setState({ addNewModalVisible: true,newUser:{name:u.user.name,tel:u.user.phone,address:addressOfUser,code:u.code,ecommerce_user_id:u.id},editingUser:true })
                                }}><i className={'fa fa-edit'}></i></a>
                            </td>
                        </tr>
                    })
                    }
                </table>
            </MDBContainer>
        </MDBRow>;
        const customerInfo = <MDBRow className={'manualorderpage_customeraddresses card'}>
            <MDBContainer>
                <MDBRow>
                    {<MDBCol md={12}>
                        {false &&
                            <MDBRow>
                                <MDBCol md={3} className={'manualorderpage_customeraddress'}>
                                    <TextBox value={receiverInfo.code} title={'کد'} onChangeText={(text) => {
                                        receiverInfo.code = text;
                                        this.setState({ receiverInfo: receiverInfo });
                                    }} />
                                </MDBCol>
                                <MDBCol md={3} className={'manualorderpage_customeraddress'}>
                                    <TextBox value={receiverInfo.tel} title={'تلفن'} onChangeText={(text) => {
                                        receiverInfo.tel = text;
                                        this.setState({ receiverInfo: receiverInfo });
                                    }} />
                                </MDBCol>
                                <MDBCol md={3} className={'manualorderpage_customeraddress'}>
                                    <TextBox value={receiverInfo.name} title={'نام'} onChangeText={(text) => {
                                        receiverInfo.name = text;
                                        this.setState({ receiverInfo: receiverInfo });
                                    }} />
                                </MDBCol>
                                <MDBCol md={3} className={'manualorderpage_customeraddress'}>

                                    <PickerBox selectedValue={receiverInfo.region} options={regions} title={'منطقه'} onValueChange={(text) => {
                                        receiverInfo.region = text;
                                        this.setState({ receiverInfo: receiverInfo });
                                    }} />
                                </MDBCol>
                            </MDBRow>
                        }
                        <MDBRow>

                            <MDBCol md={8} className={'manualorderpage_customeraddress'}>
                                <TextBox value={receiverInfo.address ? receiverInfo.address.address : ''} title={'آدرس'} onChangeText={(text) => {
                                    receiverInfo.address.address = text;
                                    this.setState({ receiverInfo: receiverInfo });
                                }} />
                            </MDBCol>
                            <MDBCol md={4} className={'manualorderpage_addcustomer'}>
                                <a onClick={() => {
                                    const emptyUser= {
                                            code: '',
                                            tel: '',
                                            name: '',
                                            address: '',
                                            region: 1,
                                    };
                                    this.setState({ addNewModalVisible: true,newUser: emptyUser,editingUser:false})
                                }}><div><i className={'fa fa-plus'}></i>تعریف مشتری جدید</div></a>
                            </MDBCol>
                        </MDBRow>

                    </MDBCol>
                    }
                </MDBRow>
            </MDBContainer>
        </MDBRow>;
        const deliveryList = <MDBCol size={8} className={'manualorderpage_bottom'}>
            <table border={1} className={'manualorderpage_deliveries card'}>
                <tbody>
                    {deliveryViews}
                </tbody>
            </table>

        </MDBCol>;
        return this.renderData(customerSearchBar, customerList, customerInfo, deliveryList, addNewModal, undefined);
    }
}
