// @flow
import * as React from 'react';
import {TextViewBox, SweetButton, SweetModal, PickerBox} from "Libs/one/sweet-react-components";
import {ManagePageContainer,ManagePageFieldsContainer} from "Libs/one/sweet-one-react";
import {Ecommerce} from "Libs/one/sweet-one-app-react";
import { MDBTable} from "mdbreact";
import '../../files/styles/ecommerce_order.css';
import EcommerceOrderViewController from "../../controllers/order/EcommerceOrderViewController";
export default class EcommerceOrderView extends EcommerceOrderViewController {
    render(){

        const formData=this.state.formData;
        const hasAddress=formData.deliveryType==Ecommerce.Entity.DeliveryType.ByDelivery;
        const isUseAtPlaceOrder=formData.deliveryType==Ecommerce.Entity.DeliveryType.UseAtPlace;
        const hasSendTime=formData.deliveryType==Ecommerce.Entity.DeliveryType.ByDelivery || formData.deliveryType==Ecommerce.Entity.DeliveryType.GetAtPlace;
        const pks=formData.productkinds;
        const userID=formData.address.user_fid;
        const stockFactors=this.getReceiverFactorLinks(pks,this.getID());
        let moment = require('moment-jalaali');
        let date=moment(formData.updatedAt, 'jYYYY/jM/jD HH:mm:ss').format('jYYYY/jM/jD ساعت HH:mm');
        let startDate=moment(formData.createdAt, 'jYYYY/jM/jD HH:mm:ss').format('jYYYY/jM/jD ساعت HH:mm');
        return <ManagePageContainer title={'اطلاعات سفارش'}>
            <SweetModal centered visible={this.state.isStatusChangeModalOpen} onHideRequest={()=>{
                this.setState({isStatusChangeModalOpen:false});
            }}>
                <PickerBox
                    title={'مامور تحویل'}
                    selectedValue={formData.delivery.id}
                    onValueChange={this.getOnFidValueChangeListener('delivery')}
                    options={this.state.deliveryOptions}
                    getTitle={(row)=>{return row.user.name}}
                />

                <SweetButton value={'ثبت'} onButtonPress={(onEnd)=>{
                    formData.setDelivery(this.getID(),(res)=>{
                        formData.get(this.getID(), () => {this.notifyFormDataChanged();});
                        this.setState({isStatusChangeModalOpen:false});
                        onEnd(true);
                    },()=>{ this.setState({isStatusChangeModalOpen:false});onEnd(false)});
                }}/>
            </SweetModal>
            <ManagePageFieldsContainer cols={3}>
                <TextViewBox title={'مبلغ قابل پرداخت'} value={formData.toPayPrice}/>
                <TextViewBox onClick={()=>{
                    this.props.history.push('/ecommerce/order/all/'+userID+'/0')
                }} title={'نام کاربر'} value={formData.name}/>
                <TextViewBox title={'تلفن همراه کاربر'} value={formData.mobile}/>
                <TextViewBox title={'روش پرداخت'} value={formData.paymenttype.displayname}/>
                {console.log("*9*", formData.paymenttype.displayname)};
                <TextViewBox title={'وضعیت سفارش'} value={formData.orderStatus.displayname}/>
                <TextViewBox title={'نوع سفارش'} value={Ecommerce.Entity.DeliveryType.getDisplayName(formData.deliveryType)}/>


                <TextViewBox title={'تخفیف'} value={formData.discount}/>
                <TextViewBox title={'تلفن تحویل گیرنده'} value={formData.mobile}/>
                <TextViewBox title={'زمان شروع سفارش'} value={startDate}/>


                {isUseAtPlaceOrder && <TextViewBox title={'شماره میز'} value={formData.atPlaceOrder.internalPlace}/>}
                {isUseAtPlaceOrder && <TextViewBox title={'تعداد نفرات'} value={formData.atPlaceOrder.personCount}/>}
                {hasSendTime && <TextViewBox title={'بازه زمانی ارسال سفارش'} value={formData.sendTime.displayname}/>}
                {hasAddress && <TextViewBox onClick={()=>{
                    this.setState({isStatusChangeModalOpen:true});
                }} title={'مامور ارسال'} value={formData.delivery.user!=null?formData.delivery.user.name:'تعیین نشده'}/>}
                {hasAddress && <TextViewBox colweight={2} title={'آدرس'} value={formData.address.address}/>}

                <div colweight={3} className={'stockfactors'}>
                    {
                        hasAddress && this.getIconLink('مسیریابی',"fa fa-map-marker",
                        'https://maps.google.com/?q='+formData.address.latitude+','+formData.address.longitude
                    ,false)
                    }
                    {this.getIconLink('فاکتور مشتری',"fa fa-print",
                        global.SiteURL+formData.pdfPath,false)}
                    {formData.delivery != null && formData.delivery.id > 0 &&
                    this.getIconLink('لیست تحویل راننده',"fa fa-truck",
                        '/ecommerce/order/delivery/' + formData.delivery.id + '/3',true)
                    }
                    {stockFactors}
                </div>
                <TextViewBox colweight={3} title={'توضیحات مشتری'} value={formData.description}/>

                <div colweight={3} className={'order-productkinds'}>
                    {this.productKinds(pks)}
                </div>
                <TextViewBox title={'آخرین تغییر وضعیت'} value={date}/>
                <TextViewBox title={'امتیاز کلی'} value={formData.orderPoll.overallRate}/>
                <TextViewBox title={'امتیاز تحویل'} value={formData.orderPoll.deliveryRate}/>
                <TextViewBox colweight={3} title={'نظر مشتری'} value={formData.orderPoll.comment}/>

            </ManagePageFieldsContainer>
            <div className='text-center'>
               <SweetButton value={'لغو سفارش'} onButtonPress={(onEnd)=>{this.reject(formData,onEnd)}}/>
               <SweetButton value={'ثبت تحویل به مشتری'} onButtonPress={(onEnd)=>{this.approve(formData,onEnd)}}/>
                <SweetButton value={'برگشت'} onButtonPress={()=>{this.returnBack()}} />
            </div>
        </ManagePageContainer>
    }
    productKinds(orderProductKinds){
        // console.log(orderProductKinds);
        const result=orderProductKinds.map((pk,index)=>{
            return <tr>
                <td>{index+1}</td>
                <td>{pk.product.displayname}</td>
                <td>{pk.quality.displayname}</td>
                <td>{pk.count}</td>
                <td>{pk.price}</td>
                <td>{pk.count*pk.price}</td>
            </tr>
        });
        return <MDBTable className={'orderproductkinds'}>
            <tr>

                <th>ردیف</th>
                <th>محصول</th>
                <th>کیفیت</th>
                <th>تعداد</th>
                <th>قیمت واحد</th>
                <th>قیمت کل</th>
            </tr>
            {result}
        </MDBTable>;

    }
}
