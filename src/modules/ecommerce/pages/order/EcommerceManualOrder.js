// @flow
import * as React from 'react';
import EcommerceBaseManualOrder from "./EcommerceBaseManualOrder";
import { MDBCol, MDBContainer, MDBRow } from "mdbreact";
import PlaceEntity from '../../../location/entity/PlaceEntity';
export default class EcommerceManualOrder extends EcommerceBaseManualOrder {

    getAddresses() {
        let { receiverInfo } = this.state;
        let deliveries = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
        const deliveryColCount = 4;
        const deliveryViews = deliveries.flatMap((tempDelivery, index) => {
            if (index % deliveryColCount === 0) {
                return [<tr>
                    {
                        deliveries.slice(index, index + deliveryColCount).map((delivery, index2) => {
                            return <td onClick={() => {
                                const nameOfAddress = "میز " + (index + index2 + 1);
                                receiverInfo.tel = 0;
                                receiverInfo.name = nameOfAddress;
                                receiverInfo.code = index + index2;
                                const p = new PlaceEntity();
                                p.address = nameOfAddress;
                                p.tel = 0;
                                p.latitude = -1;
                                p.longitude = -1;
                                p.title = nameOfAddress;
                                p.user = {id:-1};
                                p.visible = false;
                                p.area_fid ={id:1};
                                p.id = 0;
                                p.region={id:1};
                                receiverInfo.address = p;
                                this.setState({ receiverInfo: receiverInfo });
                            }} className={this.state.receiverInfo.code === (index + index2) ? 'selected' : ''}>{"میز " + (index + index2 + 1)}</td>;
                        })
                    }
                    {index + deliveryColCount > deliveries.length &&
                        <td colspan={(deliveries.length - index) - 1}>&nbsp;</td>
                    }
                </tr>]
            }
            return null;
        });
        return deliveryViews;
    }
    render() {
        const addressesViews = this.getAddresses();
        const definedAddressesList = <MDBCol size={8} className={'manualorderpage_bottom'}>
            <table border={1} className={'manualorderpage_deliveries card'}>
                <tbody>
                    {addressesViews}
                </tbody>
            </table>

        </MDBCol>;
        return this.renderData(undefined, undefined, undefined, undefined, undefined, definedAddressesList);
    }
}
