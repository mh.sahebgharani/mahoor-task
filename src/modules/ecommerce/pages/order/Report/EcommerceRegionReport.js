// @flow
import EcommerceReport from "./EcommerceReport";

export default class EcommerceRegionReport extends EcommerceReport {

    isReportFieldVisible($fieldName){
        return ($fieldName===EcommerceRegionReport.REPORTFIELD_REGION);
    }
}
