// @flow
import EcommerceReport from "./EcommerceReport";

export default class EcommercePaymentTypeReport extends EcommerceReport {

    isReportFieldVisible($fieldName){
        return ($fieldName===EcommercePaymentTypeReport.REPORTFIELD_PAYMENTTYPE);
    }
}
