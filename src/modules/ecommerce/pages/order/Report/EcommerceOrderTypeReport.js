// @flow
import EcommerceReport from "./EcommerceReport";

export default class EcommerceOrderTypeReport extends EcommerceReport {

    isReportFieldVisible($fieldName){
        return ($fieldName===EcommerceOrderTypeReport.REPORTFIELD_ORDERTYPE);
    }
}
