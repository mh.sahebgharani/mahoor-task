// @flow
import * as React from 'react';
import Constants from '../../../../../classes/Constants';
import OrderEntity from "../../../entity/OrderEntity";
import {ManagePageContainer, ManagePageFieldsContainer, SweetManagePage} from "Libs/one/sweet-one-react";
import { PickerBox, SweetButton} from "Libs/one/sweet-react-components";
import EcommerceuserEntity from "../../../entity/EcommerceuserEntity";
import OrderstatusEntity from "../../../entity/OrderstatusEntity";
import {Ecommerce} from "Libs/one/sweet-one-app-react";
import RegionEntity from "../../../../location/entity/RegionEntity";


export default class EcommerceReport extends SweetManagePage {
    moduleName='ecommerce';
    tableName='order';
    entity = new OrderEntity();
    __defaultFilters=[{id:'user_fid',value:this.userID()}];
    static REPORTFIELD_DELIVERY=1;
    static REPORTFIELD_ORDERTYPE=2;
    static REPORTFIELD_REGION=3;
    static REPORTFIELD_PAYMENTTYPE=4;
    componentDidMount() {
        super.componentDidMount();
    }
    isReportFieldVisible($fieldName){
        return true;
    }
    years=[];
    months=[];
    days=[];
    constructor(props){
        super(props);
        this.years=[1399,1400,1401,1402,1403,1404,1405,1406,1407,1408,1409,1410].map(a=>{return {id:a,displayName:a}});
        this.days=[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31].map(a=>{return {id:a,displayName:a}});
        this.months=[1,2,3,4,5,6,7,8,9,10,11,12].map(a=>{return {id:a,displayName:a}});
        this.state={
            ...super.state,
            year:'1399',
            month:'12',
            day:'1',
            endyear:'1399',
            endmonth:'12',
            endday:'22',
            status:'',
            delivery:'',
            statuses:[],
            deliveries:[],
            regions: [],
        };
        this.loadStatuses();
        this.loadDeliveries();
        this.loadRegions();
    }
    loadStatuses(){
            new OrderstatusEntity().getAll(null,null,null,null,(data) => {
                this.setState({ statuses: data });
            }, null);
    }
    loadDeliveries = () => {
        new EcommerceuserEntity().getAllDeliveries((data) => {
            this.setState({ deliveries: data });
        }, null);
    };
    loadRegions = () => {
        new RegionEntity().getAll(1000, 1, null, null
            , (data) => {
                // console.log(data);
                this.setState({ regions: data });
            }, null);
    };
    returnBack() {
        this.props.history.goBack();
    }
    userID(){
        return this.props.match.params.userid||-1;
    }
    render() {
        return <ManagePageContainer title={'دریافت گزارش'}>

            <ManagePageFieldsContainer cols={4}>
                <div>از</div>
                    <PickerBox
                        title={'سال'}
                        selectedValue={this.state.year}
                        onValueChange={val=>{this.setState({year:val})}}
                        options={this.years}
                    />
                    <PickerBox
                        title={'ماه'}
                        selectedValue={this.state.month}
                        onValueChange={val=>{this.setState({month:val})}}
                        options={this.months}
                    />
                    <PickerBox
                        title={'روز'}
                        selectedValue={this.state.day}
                        onValueChange={val=>{this.setState({day:val})}}
                        options={this.days}
                    />

            </ManagePageFieldsContainer>

            <ManagePageFieldsContainer cols={4}>
                <div>تا</div>
                    <PickerBox
                        title={'سال'}
                        selectedValue={this.state.endyear}
                        onValueChange={val=>{this.setState({endyear:val})}}
                        options={this.years}
                    />
                    <PickerBox
                        title={'ماه'}
                        selectedValue={this.state.endmonth}
                        onValueChange={val=>{this.setState({endmonth:val})}}
                        options={this.months}
                    />
                    <PickerBox
                        title={'روز'}
                        selectedValue={this.state.endday}
                        onValueChange={val=>{this.setState({endday:val})}}
                        options={this.days}

                    />
            </ManagePageFieldsContainer>
            <ManagePageFieldsContainer cols={2}>
                <PickerBox
                    title={'وضعیت'}
                    selectedValue={this.state.status}
                    onValueChange={val=>{this.setState({status:val})}}
                    options={this.state.statuses}
                />
                {this.isReportFieldVisible(EcommerceReport.REPORTFIELD_DELIVERY) &&
                <PickerBox
                    title={'راننده'}
                    selectedValue={this.state.delivery}
                    onValueChange={val => {
                        this.setState({delivery: val})
                    }}
                    options={this.state.deliveries}
                    getTitle={a => a.user.name}
                />
                }
                {this.isReportFieldVisible(EcommerceReport.REPORTFIELD_ORDERTYPE) &&
                <PickerBox
                    title={'نوع سفارش'}
                    selectedValue={this.state.orderType}
                    onValueChange={val => {
                        this.setState({orderType: val})
                    }}
                    options={[1, 2, 3]}
                    getTitle={a => Ecommerce.Entity.OrderType.getDisplayName(a)}
                />
                }
                {this.isReportFieldVisible(EcommerceReport.REPORTFIELD_REGION) &&
                <PickerBox
                    title={'منطقه'}
                    selectedValue={this.state.region}
                    onValueChange={val => {
                        this.setState({region: val})
                    }}
                    options={this.state.regions}
                />
                }
                {this.isReportFieldVisible(EcommerceReport.REPORTFIELD_PAYMENTTYPE) &&
                <PickerBox
                    title={'نوع پرداخت'}
                    selectedValue={this.state.paymentType}
                    onValueChange={val => {
                        this.setState({paymentType: val})
                    }}
                    options={[{id: 34, displayName: 'آنلاین'}, {id: 35, displayName: 'نقدی'}]}
                />
                }
            </ManagePageFieldsContainer>
                <SweetButton value={'دریافت گزارش'} onButtonPress={(onEnd)=>{
                    let searchInfo={};
                    let filtered=[];
                    let orderEntity=new OrderEntity();
                    let {paymentType,region,orderType,delivery,status}=this.state;
                    // if(paymentType) orderEntity.paymenttype=paymentType;
                    // if(region) orderEntity.region={id:region};
                    // if(orderType) orderEntity.orderType=orderType;
                    // if(delivery) orderEntity.delivery={id:delivery};
                    // if(status) orderEntity.orderStatus={id:status};

                    if(paymentType) filtered.push({id:"paymenttype",value:paymentType});
                    if(region) filtered.push({id:"location_region_fid",value:region});
                    if(orderType) filtered.push({id:"ordertype",value:orderType});
                    if(delivery) filtered.push({id:"delivery__ecommerceuser_fid",value:delivery});
                    if(status) filtered.push({id:"orderstatus",value:status});

                    orderEntity.getDateIntervalReport(this.state.year,this.state.month,this.state.day,this.state.endyear,this.state.endmonth,this.state.endday,filtered,(res)=>{
                        console.log(res);
                        this.setState({isDateSelectVisible:false});
                        onEnd(true);
                        window.location.href =Constants.SiteURL+'/'+res.Data.path;
                    },()=>{ this.setState({isDateSelectVisible:false});onEnd(false)});
                }}/>

        </ManagePageContainer>;
    }
}
