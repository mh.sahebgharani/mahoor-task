// @flow
import EcommerceReport from "./EcommerceReport";

export default class EcommerceDeliveryReport extends EcommerceReport {

    isReportFieldVisible($fieldName){
        return ($fieldName===EcommerceDeliveryReport.REPORTFIELD_DELIVERY);
    }
}
