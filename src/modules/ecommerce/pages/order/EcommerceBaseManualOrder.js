// @flow
import * as React from 'react';
import { MDBCol, MDBContainer, MDBRow, MDBTable, MDBTableBody, MDBTableHead } from "mdbreact";
import '../../files/styles/ecommerce_manualorder.scss';
import { PickerBox, SweetButton, SweetModal, TextBox } from "Libs/one/sweet-react-components";
import EcommerceManualOrderController from "./EcommerceManualOrderController";
export default class EcommerceBaseManualOrder extends EcommerceManualOrderController {

    renderData(customerSearchBar, customerList, customerInfo, deliveryList, addNewModal, definedAddressesList) {
        const { orderItems, discount, receiverInfo } = this.state;
        let { user } = this.state;
        let address = {};
        if (user && user.defaultAddress)
            address = user.defaultAddress;
        let sum = this.getOrderSum();
        let sendPrice = this.getSendPriceFromSendTime(this.getSendTimeFromID(1), sum);
        return <div className={'manualorderpage'}>
            {addNewModal}
            <MDBContainer>
                <MDBRow className={'mainrow'}>
                    <MDBCol md={4} className={'manualorderpage_sidebar'}>
                        <MDBContainer>
                            <MDBRow className={'manualorderpage_groups card'}>
                                <MDBContainer>
                                    <MDBRow>
                                        {this.state.groups.map(g => {
                                            if (g.products != null && g.products.length > 0) {
                                                return <MDBCol md={4} className={'manualorderpage_group ' + (this.state.group === g ? 'selected' : '')}
                                                    onClick={() => this.setState({ group: g, products: g.products, productKinds: [] })}>
                                                    {g.displayName}
                                                </MDBCol>
                                            }
                                            return;
                                        })
                                        }

                                    </MDBRow>
                                </MDBContainer>
                            </MDBRow>
                            <MDBRow className={'manualorderpage_products card'}>
                                <MDBContainer>
                                    <MDBRow>
                                        {this.state.products.map(p => {
                                            return <MDBCol md={4} className={'manualorderpage_product ' + (this.state.product === p ? 'selected' : '')}
                                                onClick={() => {
                                                    this.setState({ productKinds: p.productKinds, product: p })
                                                }}>
                                                {p.displayName}
                                            </MDBCol>
                                        })
                                        }

                                    </MDBRow>
                                </MDBContainer>
                            </MDBRow>
                            <MDBRow className={'manualorderpage_productkinds card'}>
                                <MDBContainer>
                                    <MDBRow>
                                        {this.state.productKinds.map(p => {
                                            return <MDBCol md={4} className={'manualorderpage_productkind'} onClick={() => {
                                                p.product = this.state.product;
                                                let orderItem = { productKind: p, count: 1 };
                                                const itemsWithThisPK = orderItems.filter(oi => oi.productKind.id === p.id);
                                                if (itemsWithThisPK != null && itemsWithThisPK.length > 0)
                                                    itemsWithThisPK[0].count++;
                                                else
                                                    orderItems.push(orderItem);
                                                this.setState({ orderItems: orderItems });
                                            }}>
                                                {p.quality}
                                            </MDBCol>
                                        })
                                        }

                                    </MDBRow>
                                </MDBContainer>
                            </MDBRow>
                        </MDBContainer>
                    </MDBCol>
                    <MDBCol className={'manualorderpage_mainbar'}>
                        {customerSearchBar}
                        {customerList}
                        {customerInfo}
                        <MDBRow className={'manualorderpage_order card'}>
                            <MDBTable>
                                <MDBTableHead>
                                    <tr>
                                        <th>کد کالا</th>
                                        <th>نام کالا</th>
                                        <th>قیمت واحد</th>
                                        <th>تعداد</th>
                                        <th>قیمت کل</th>
                                        <th></th>
                                    </tr>
                                </MDBTableHead>
                                <MDBTableBody>
                                    {orderItems.map((orderItem, index) => {
                                        return <tr>
                                            <td>{orderItem.productKind.code}</td>
                                            <td>{orderItem.productKind.product.displayName + " (" + orderItem.productKind.quality + ")"}</td>
                                            <td>{orderItem.productKind.price}</td>
                                            <td><input type={'text'} value={orderItem.count} onChange={(e) => {
                                                orderItems[index].count = e.target.value;
                                                this.setState({ orderItems: orderItems })
                                            }} /></td>
                                            <td>{orderItem.productKind.price * orderItem.count}</td>
                                            <td><a onClick={()=>{
                                                orderItems.splice(index,1);
                                                this.setState({orderItems:orderItems});
                                                }
                                            }><span><i className={'fa fa-trash'}></i></span></a></td>
                                        </tr>
                                    })}
                                </MDBTableBody>
                            </MDBTable>
                        </MDBRow>
                        <MDBRow className={'manualorderpage_deliveriesandsum'}>
                            {deliveryList}
                            {definedAddressesList}
                            <MDBCol size={4} className={'manualorderpage_sum  card'}>
                                <MDBRow>
                                    <MDBCol size={12}><TextBox title={'جمع'} value={sum} /></MDBCol>
                                    <MDBCol size={12}><TextBox title={'تخفیف'} value={discount} /></MDBCol>
                                    <MDBCol size={12}><TextBox title={'هزینه ارسال'} value={sendPrice} /></MDBCol>
                                    <MDBCol size={12}><TextBox title={'پرداختی'} value={sum + sendPrice - discount} /></MDBCol>
                                    <MDBRow className={'manualorderpage_orderoperation'}>
                                        <SweetButton value={'سفارش'} onButtonPress={onEnd => {
                                            this.addOrder(orderItems, receiverInfo.address, onEnd);
                                        }} />
                                    </MDBRow>
                                </MDBRow>
                            </MDBCol>
                        </MDBRow>

                    </MDBCol>
                </MDBRow>
            </MDBContainer>
        </div>

    }
}
