// @flow
import * as React from 'react';
import {PickerBox} from "Libs/one/sweet-react-components";
import {SearchBoxContainer,SweetSearchBox} from "Libs/one/sweet-one-react";
import GroupEntity from '../../entity/GroupEntity';
export default class EcommerceGroupSearch extends SweetSearchBox {
    moduleName='ecommerce';
    tableName='group';
    constructor(props) {
        super(props);
        this.state = {
            searchParams:{},
            formData:new GroupEntity(),

			motherGroupOptions:new GroupEntity(),
        };
    }

    loadMotherGroups = () => {
        this.state.motherGroupOptions.getAll(null,null,null,null,(data)=>{
                this.setState({motherGroupOptions:data});
            },null);
    };

    componentDidMount() {
        super.componentDidMount();

        this.loadMotherGroups();
        this.loadSimpleFields();

    }
    render(){
        return <SearchBoxContainer title={'جستجوی دسته بندی ها'} onConfirm={this.getOnConfirm()}>

            <PickerBox
                title={'دسته بندی مادر'}
                selectedValue={this.state.searchParams.mothergroup_fid}
                onValueChange={this.getOnFidValueChangeListener('mothergroup_fid')}
                options={this.state.motherGroupOptions}
            />

        </SearchBoxContainer>
    }
}
