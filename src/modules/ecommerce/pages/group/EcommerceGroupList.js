// @flow
import * as React from 'react';
import GroupEntity from "../../entity/GroupEntity";
import {ListPageContainer,SweetManageListPage} from "Libs/one/sweet-one-react";
import {Link} from "react-router-dom";
import {IoIosAlbums} from "react-icons/io";
import EcommerceGroupSearch from './EcommerceGroupSearch';

export default class EcommerceGroupList extends SweetManageListPage {
    moduleName='ecommerce';
    tableName='group';
    entity = new GroupEntity();
    componentDidMount() {
        super.componentDidMount();
    }
    render() {
        return <ListPageContainer title={'لیست دسته بندی ها'}>
            <EcommerceGroupSearch onConfirm={this.searchData}/>
            {this.getAddButton()}
            {this.getTable()}
        </ListPageContainer>;
    }
    columns =
    [
        {Header: 'دسته بندی مادر',id: 'mothergroup_fid',accessor: data => data.motherGroup.displayname},
        {Header: 'نام',id: 'name',accessor: 'name'},
        {Header: 'نام نمایشی',id: 'displayname',accessor: 'displayName'},
        {Header: 'عملیات',id:'operations',accessor: data => data,Cell: props => {return this.getOperationsBox(props.value)}}

    ];

    getOperationsBox(group){
        console.log(group);
        return <div className={'operationsrow'}>
            {this.canView() && this.getViewButton(group.name)}
            {this.canEdit() && this.getEditButton(group.id)}
            {this.canDelete() && this.getDeleteButton(group.id)}
        </div>
    }
    getViewLink(name){
        return '/'+this.moduleName+'/product/bygroup/'+name;
    }
    getViewButton(name){
        return  <Link className={'viewlink'} to={this.getViewLink(name)}><IoIosAlbums/></Link>;
    }
}
