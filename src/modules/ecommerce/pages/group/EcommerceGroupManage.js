// @flow
import * as React from 'react';
import {TextBox,PickerBox,ImageSelector} from "Libs/one/sweet-react-components";
import {ManagePageFieldsContainer,ManagePageContainer,SweetManagePage} from "Libs/one/sweet-one-react";
import GroupEntity from '../../entity/GroupEntity';

export default class EcommerceGroupManage extends SweetManagePage {
    moduleName='ecommerce';
    tableName='group';
    constructor(props) {
        super(props);
        this.state = {
            canEdit:this.canEdit(),
            formData:new GroupEntity(),

			motherGroupOptions:new GroupEntity(),
        };
    }

    loadMotherGroups = () => {
        this.state.motherGroupOptions.getAll(null,null,null,null,(data)=>{
                this.setState({motherGroupOptions:data});
            },null);
    };

    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

        this.loadMotherGroups();
        this.loadSimpleFields();

    }
    render(){
        return <ManagePageContainer title={'تعریف دسته بندی'}>
        <ManagePageFieldsContainer>
            <PickerBox
                emptyItemTitle={'بدون دسته بندی مادر'}
                title={'دسته بندی مادر'}
                selectedValue={this.state.formData.motherGroup.id}
                onValueChange={this.getOnFidValueChangeListener('motherGroup')}
                options={this.state.motherGroupOptions}
            />
            <TextBox title={'نام'} id='name' value={this.state.formData.name}
                onChangeText={this.getOnTextChangeListener('name')}/>
            <TextBox title={'نام نمایشی'} id='displayName' value={this.state.formData.displayName}
                onChangeText={this.getOnTextChangeListener('displayName')}/>

                    <ImageSelector title='لوگو'
                        onConfirm={this.getOnFilePathChanged('logoImage')}
                        previewImage={this.state.formData.logoImage.url}
                        onImagePreviewLoaded={this.getOnImagePreviewLoaded('logoImage')}
                    />

        </ManagePageFieldsContainer>
        {this.getSaveAndBackButtons()}
        </ManagePageContainer>
    }
}
