// @flow
import * as React from 'react';
import {TextViewBox,ImageModal} from "Libs/one/sweet-react-components";
import {ManagePageContainer,ManagePageFieldsContainer,SweetManagePage} from "Libs/one/sweet-one-react";
import Constants from "../../../../classes/Constants";
import StockEntity from "../../entity/StockEntity";
import EcommerceStockViewController from "../../controllers/stock/EcommerceStockViewController";


export default class EcommerceStockView extends EcommerceStockViewController {
    render(){
        return <ManagePageContainer title={'تعریف انبار'}>
        <ManagePageFieldsContainer>
            <TextViewBox title={'عنوان فارسی'} value={this.state.formData.displayName}/>
            <TextViewBox title={'نام لاتین'} value={this.state.formData.name}/>

        </ManagePageFieldsContainer>
        {this.getSaveAndBackButtons()}
        </ManagePageContainer>
    }
}
