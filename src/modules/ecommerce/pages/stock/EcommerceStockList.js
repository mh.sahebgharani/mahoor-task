// @flow
import * as React from 'react';
import {ListPageContainer} from "Libs/one/sweet-one-react";
import EcommerceStockListController from "../../controllers/stock/EcommerceStockListController";
import EcommerceStockSearch from './EcommerceStockSearch';

export default class EcommerceStockList extends EcommerceStockListController {
    render() {
        return <ListPageContainer title={'لیست انبارها'}>
            <EcommerceStockSearch onConfirm={this.searchData}/>
            {this.getAddButton()}
            {this.getTable()}
        </ListPageContainer>;
    }
    columns =
    [
        {Header: 'عنوان فارسی',id: 'displayname',accessor: 'displayName'},
        {Header: 'نام لاتین',id: 'name',accessor: 'name'},
        {Header: 'عملیات',accessor: 'id',Cell: props => {return this.getOperationsBox(props.value)}}
    ];
}
