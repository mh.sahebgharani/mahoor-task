// @flow
import * as React from 'react';
import { FileSelector,SweetButton} from "Libs/one/sweet-react-components";
import {ManagePageContainer} from "Libs/one/sweet-one-react";
import {SweetAlert} from "Libs/one/sweet-react-common-tools";
import ProductkindEntity from "../../entity/ProductkindEntity";
import {MDBCol, MDBContainer, MDBRow} from "mdbreact";


export default class EcommerceProductkindExcelAdd extends React.Component{
    getOnFilePathChanged(name){
        return (path) => {
                this.setState({file:path});
        }
    }
    constructor(props) {
        super(props);
        this.state = {
            formData:new ProductkindEntity(null),
            excelPreview:[],
        };
    }

    render(){
        return <ManagePageContainer title={'بروزرسانی موجودی محصولات'}>

            {this.state.excelPreview.length == 0 &&
                <div>
            <FileSelector title='فایل اکسل'
                          onConfirm={this.getOnFilePathChanged()}
            />
            < SweetButton value={'آپلود'} onButtonPress={(onEnd)=>{
                const pkent=new ProductkindEntity(null);
                pkent.uploadExcel(this.state.file,(result)=>{
                this.setState({excelPreview:result.Data});
                onEnd(true);
            },()=>{onEnd(true)});
            }}/>
                </div>
            }
            {this.state.excelPreview.length > 0 &&
                <div>
            {this.Preview(this.state.excelPreview)}
                <SweetButton value={'اعمال تغییرات'} onButtonPress={(onEnd) => {
                const pkent = new ProductkindEntity(null);
                pkent.batchUpdate(this.state.excelPreview, (result) => {
                    // console.log(result);
                    // this.setState({excelPreview:result.Data});
                    onEnd(true);
                    SweetAlert.displaySuccessAlert('اعمال شد', 'میزان موجودی و قیمت محصولات با موفقیت بروزرسانی شد.');
                    this.props.history.push('/ecommerce/product/monitor');
                }, () => {
                    onEnd(true)
                });
            }}/>
                </div>
            }

        </ManagePageContainer>
    }
    Preview(excelPreview){
        let result=[];
        for(let i=0;i<excelPreview.length;i++){
            const rowObject=excelPreview[i];
            const row=<MDBRow>
                <MDBCol>{i+1}</MDBCol>
                <MDBCol>{rowObject.displayname+" "+rowObject.quality}</MDBCol>
                <MDBCol>{rowObject.currentprice}</MDBCol>
                <MDBCol>{rowObject.newprice}</MDBCol>
                <MDBCol>{rowObject.currentcount}</MDBCol>
                <MDBCol>{rowObject.addedcount}</MDBCol>
                <MDBCol>{rowObject.newcount}</MDBCol>
            </MDBRow>;
            result[i]=row;
        }
        return <MDBContainer>
            <MDBRow>
                <MDBCol>سطر</MDBCol>
                <MDBCol>نام</MDBCol>
                <MDBCol>قیمت فعلی</MDBCol>
                <MDBCol>قیمت جدید</MDBCol>
                <MDBCol>میزان موجود</MDBCol>
                <MDBCol>میزان افزایش</MDBCol>
                <MDBCol>میزان جدید</MDBCol>
            </MDBRow>
            {result}
        </MDBContainer>
    }
}
