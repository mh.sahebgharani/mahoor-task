// @flow
import * as React from 'react';
import {TextBox, PickerBox, ImageSelector, SweetButton, SweetModal,SweetSinglePicker,SweetMultiPicker} from "Libs/one/sweet-react-components";
import {Common} from "Libs/one/sweet-react-common-tools";
import {ManagePageContainer,ManagePageFieldsContainer} from "Libs/one/sweet-one-react";
import EcommerceProductkindManageController from "../../controllers/productkind/EcommerceProductkindManageController";


export default class EcommerceProductkindManage extends EcommerceProductkindManageController {
    render(){
        // console.log(this.props.factorReceivers);
        // console.log(this.state.formData);
        this.load();
        if(!Common.isObjectsEquivalent(this.state.lastFormData,this.state.formData)) {
            this.props.onChange(this.state.formData);
        }
        const selectedReceivers=this.state.formData.factorReceivers.map(a=>a.id);

        console.log(selectedReceivers);
        return <ManagePageContainer title={''}>
            <SweetModal centered visible={this.state.isCountSetModalOpen} onHideRequest={()=>{
                this.setState({isCountSetModalOpen:false});
            }}>
                <TextBox title={'تعداد'} id='count' value={this.state.addedCount}
                         onChangeText={(text)=>{this.setState({addedCount:text})}} keyboardType='numeric'/>
                <SweetButton value={'ثبت ورود به انبار'} onButtonPress={(onEnd)=>{
                    this.onAddCountPress(this.getID(),1*this.state.addedCount,onEnd)
                    }
                }/>
                <SweetButton value={'ثبت خروج از انبار'} onButtonPress={(onEnd)=>{this.onAddCountPress(this.getID(),-1*this.state.addedCount,onEnd)}}/>
            </SweetModal>
        <ManagePageFieldsContainer>
            <TextBox title={'کد کالا'} id='code' value={this.state.formData.code}
                onChangeText={this.getOnTextChangeListener('code')}/>
            <SweetSinglePicker
                title={'کیفیت'}
                selectedValue={this.state.formData.quality.id}
                onValueChange={this.getOnFidValueChangeListener('quality')}
                options={this.state.formData.simpleFields.quality}
            />
            <PickerBox
                title={'انبار'}
                selectedValue={this.state.formData.stock.id}
                onValueChange={this.getOnFidValueChangeListener('stock')}
                options={this.state.stockOptions}
            />
        <TextBox title={'قیمت واحد'} id='price' value={this.state.formData.price}
         onChangeText={this.getOnTextChangeListener('price')} keyboardType='numeric'/>
        {this.getID()>0 &&<TextBox readOnly={true} title={'تعداد موجودی'} id='count' value={this.state.formData.count}
         onChangeText={this.getOnTextChangeListener('count')} keyboardType='numeric' onClick={()=>{this.setState({isCountSetModalOpen:true})}}/>}

            <SweetMultiPicker
                title={'دریافت کننده فاکتور'}
                selectedValues={selectedReceivers}
                onValueChange={
                    (option,action)=>{
                        // console.log(option);
                        console.log(action);
                        this.getOnMultiFidValueChangeListener('factorReceivers')(option,action);
                    }}
                options={this.props.factorReceivers}
            />
                    <ImageSelector title='تصویر'
                        onConfirm={this.getOnFilePathChanged('photo')}
                        previewImage={this.state.formData.photo.url}
                        onImagePreviewLoaded={this.getOnImagePreviewLoaded('photo')}
                    />            <TextBox title={'توضیحات'} id='description' value={this.state.formData.description}
                onChangeText={this.getOnTextChangeListener('description')}/>


        </ManagePageFieldsContainer>
        </ManagePageContainer>
    }
}
