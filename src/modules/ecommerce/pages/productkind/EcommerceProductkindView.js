// @flow
import * as React from 'react';
import {TextViewBox,ImageModal} from "Libs/one/sweet-react-components";
import {SweetDependentManagePage,ManagePageContainer,ManagePageFieldsContainer} from "Libs/one/sweet-one-react";
import ProductkindEntity from "../../entity/ProductkindEntity";

export default class EcommerceProductkindView extends SweetDependentManagePage {
    moduleName='ecommerce';
    tableName='productkind';
    __ownerTable='product';
    constructor(props) {
        super(props);
        this.state = {
            formData:new ProductkindEntity(this.getOwnerID()),

        };
    }

    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

    }
    render(){
        return <ManagePageContainer title={'تعریف انواع مختلف کالا'}>
        <ManagePageFieldsContainer>
            <TextViewBox title={'کد کالا'} value={this.state.formData.code}/>
            <TextViewBox title={'کیفیت'} value={this.state.formData.quality.displayname}/>
            <TextViewBox title={'قیمت واحد'} value={this.state.formData.price}/>
            <TextViewBox title={'تعداد موجودی'} value={this.state.formData.count}/>

                    <ImageModal title='تصویر'
                        previewImage={this.state.formData.photo.url}
                    />            <TextViewBox title={'توضیحات'} value={this.state.formData.description}/>

        </ManagePageFieldsContainer>
        {this.getSaveAndBackButtons()}
        </ManagePageContainer>
    }
}
