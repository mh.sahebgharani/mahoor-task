// @flow
import * as React from 'react';
import {TextBox} from "Libs/one/sweet-react-components";
import {SearchBoxContainer,SweetSearchBox} from "Libs/one/sweet-one-react";
import ProductkindEntity from "../../entity/ProductkindEntity";

export default class EcommerceProductkindSearch extends SweetSearchBox {
    moduleName='ecommerce';
    tableName='productkind';
    constructor(props) {
        super(props);
        this.state = {
            searchParams:{},
            formData:new ProductkindEntity(),

        };
    }

    componentDidMount() {
        super.componentDidMount();

        this.loadSimpleFields();

    }
    render(){
        return <SearchBoxContainer title={'جستجوی انواع مختلف کالا'} onConfirm={this.getOnConfirm()}>
            <TextBox title={'کد کالا'} id='code'
                onChangeText={this.getOnTextChangeListener('code')}/>

        </SearchBoxContainer>
    }
}
