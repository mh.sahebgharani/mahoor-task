// @flow
import * as React from 'react';
import {SweetManageDepenedentListPage,ListPageContainer} from "Libs/one/sweet-one-react";
import ProductkindEntity from "../../entity/ProductkindEntity";
import EcommerceProductkindSearch from './EcommerceProductkindSearch';

export default class EcommerceProductkindList extends SweetManageDepenedentListPage {
    moduleName='ecommerce';
    tableName='productkind';
    __defaultFilters=[{id:'product_fid',value:this.getOwnerID()}]
    entity = new ProductkindEntity();
    componentDidMount() {
        super.componentDidMount();
        this.LoadData(this.state.pageSize,1,null,null);
    }
    render() {
        return <ListPageContainer title={'لیست انواع مختلف کالا'}>
            <EcommerceProductkindSearch onConfirm={this.searchData}/>
            {this.getAddButton()}
            {this.getTable()}
        </ListPageContainer>;
    }
    columns =
    [
        {Header: 'کد کالا',id: 'code',accessor: 'code'},
        {Header: 'کیفیت',id: 'quality',accessor: data => data.quality.displayname},
        {Header: 'قیمت واحد',id: 'price_num',accessor: 'price'},
        {Header: 'تعداد موجودی',id: 'count',accessor: 'count'},
        {Header: 'توضیحات',id: 'description',accessor: 'description'},
        {Header: 'عملیات',accessor: 'id',Cell: props => {return this.getOperationsBox(props.value)}}
    ];
}
