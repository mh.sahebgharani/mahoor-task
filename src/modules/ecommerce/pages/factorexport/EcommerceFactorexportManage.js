// @flow
import * as React from 'react';
import {TextBox,PickerBox,FileSelector,ImageSelector} from "Libs/one/sweet-react-components";
import {ManagePageFieldsContainer,ManagePageContainer,SweetManagePage} from "Libs/one/sweet-one-react";
import FactorexportEntity from "../../entity/FactorexportEntity";


import EcommerceFactorexportitemList from '../factorexportitem/EcommerceFactorexportitemList';
export default class EcommerceFactorexportManage extends SweetManagePage {
    moduleName='ecommerce';
    tableName='factorexport';
    constructor(props) {
        super(props);
        this.state = {
            canEdit:this.canEdit(),
            formData:new FactorexportEntity(),

        };
    }
    
    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

        this.loadSimpleFields();

    }
    render(){
        return <ManagePageContainer title={'تعریف فاکتور خروجی'}>
        <ManagePageFieldsContainer>
            <TextBox title={'نام فایل'} id='fileName' value={this.state.formData.fileName}
                onChangeText={this.getOnTextChangeListener('fileName')}/>
            <TextBox title={'عنوان'} id='title' value={this.state.formData.title}
                onChangeText={this.getOnTextChangeListener('title')}/>


        </ManagePageFieldsContainer>
        {this.getSaveAndBackButtons()}
        <EcommerceFactorexportitemList ownerID={this.getID()}/>
        </ManagePageContainer>
    }
}
