// @flow
import * as React from 'react';
import {TextViewBox,ImageModal} from "Libs/one/sweet-react-components";
import {ManagePageContainer,ManagePageFieldsContainer,SweetManagePage} from "Libs/one/sweet-one-react";
import Constants from "../../../../classes/Constants";
import FactorexportEntity from "../../entity/FactorexportEntity";

export default class EcommerceFactorexportView extends SweetManagePage {
    moduleName='ecommerce';
    tableName='factorexport';
    constructor(props) {
        super(props);
        this.state = {
            formData:new FactorexportEntity(),

        };
    }
    
    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

    }
    render(){
        return <ManagePageContainer title={'تعریف فاکتور خروجی'}>
        <ManagePageFieldsContainer>
            <TextViewBox title={'نام فایل'} value={this.state.formData.fileName}/>
            <TextViewBox title={'عنوان'} value={this.state.formData.title}/>

        </ManagePageFieldsContainer>
        {this.getSaveAndBackButtons()}
        </ManagePageContainer>
    }
}
