// @flow
import * as React from 'react';
import Constants from '../../../../classes/Constants';
import FactorexportEntity from "../../entity/FactorexportEntity";
import {ListPageContainer,SweetManageListPage} from "Libs/one/sweet-one-react";
import OrderEntity from "../../entity/OrderEntity";
import {SweetAlert} from "Libs/one/sweet-react-common-tools";
import {SweetButton} from "Libs/one/sweet-react-components";


export default class EcommerceFactorexportList extends SweetManageListPage {
    moduleName='ecommerce';
    tableName='factorexport';
    entity = new FactorexportEntity();
    componentDidMount() {
        super.componentDidMount();
        this.LoadData(this.state.pageSize,1,null,null);
    }
    render() {
        return <ListPageContainer title={'لیست فاکتورهای خروجی'}>
            <SweetButton value={'ایجاد فایل سفارشات جدید'} onButtonPress={(onEnd)=>{
                const oe=new OrderEntity();
                oe.makeExcel((result)=>{
                    onEnd(true);
                    console.log(result);
                    if(result.Data!=null && result.Data.path!=null){
                        window.location.href =global.SiteURL+result.Data.path;
                    }
                    else{
                        SweetAlert.displayWarningAlert(result.message);
                    }
                    this.LoadData(this.state.pageSize,1,null,null);

                },()=>{onEnd(true)})
            }}/>
            {this.getTable()}
        </ListPageContainer>;
    }
    columns =
    [
        {Header: 'عنوان',id: 'title',accessor: 'title'},
        {Header: 'عملیات',accessor: 'fileName',Cell: props => {return <div className={'operationsrow'}>
                <a className={'viewlink'} href={global.SiteURL+props.value}>
                    <span className={'fa fa-download'}></span>
                </a>
        </div>
        }}

    ];
}
