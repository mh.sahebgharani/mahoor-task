// @flow
import * as React from 'react';
import ProductEntity from "../../entity/ProductEntity";
import {MDBCol, MDBContainer, MDBRow} from "mdbreact";
import Product from "./Components/Product";
import '../../files/styles/product.css';
import {SweetUserListPage} from "Libs/one/sweet-one-react";

export default class EcommerceProductMonitor extends SweetUserListPage {
    moduleName='ecommerce';
    tableName='product';
    entity = new ProductEntity();
    constructor(props) {
        super(props);
        this.load();
        setInterval(()=>{this.load()},60000)
    }
    componentWillUnmount() {
        clearInterval();
    }
    load(){
        let filter=[
            {id:'removestockout',value:'1'},
            {id:'orderbycount',value:'1'},

        ];
        this.LoadData(null,null,null,filter);
    }
    render() {
        let products=[];
        for(let product of this.state.data){
            products[products.length]=<MDBCol size={6} md={3}>
                <Product name={product.displayName} productkinds={product.productKinds} product={product}/>
            </MDBCol>;
        }
        return <MDBContainer>
                <MDBRow className={'products '}>
                    {products}
                </MDBRow>
            </MDBContainer>;
    }
}
