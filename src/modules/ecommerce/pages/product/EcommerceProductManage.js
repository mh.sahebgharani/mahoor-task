// @flow
import * as React from 'react';
import {TextBox, PickerBox, TabView} from "Libs/one/sweet-react-components";
import {Common} from "Libs/one/sweet-react-common-tools";
import {ManagePageContainer, ManagePageFieldsContainer} from "Libs/one/sweet-one-react";

import EcommerceProductkindManage from "../productkind/EcommerceProductkindManage";
import ProductkindEntity from "../../entity/ProductkindEntity";
import Constants from "../../../../classes/Constants";
import EcommerceProductManageController from "../../controllers/product/EcommerceProductManageController";

export default class EcommerceProductManage extends EcommerceProductManageController {
    render() {
        let pks = this.state.formData.productKinds;
        if(pks==null || pks.length===0)
            pks=[new ProductkindEntity()];
        let pkManagers=[];
        let tabTitles=[];
        if (pks != null) {
            pkManagers=pks.map((pk,index)=>{
               return <EcommerceProductkindManage factorReceivers={this.state.factorReceiversOptions} ownerID={this.getID()} productkindid={pk.id}  productkind={pk} onChange={(newData) => {
                    let FormData = this.state.formData;
                    let Kinds = FormData.productKinds;

                    if(Kinds[index]==null || !Common.isObjectsEquivalent(Kinds[index],newData)){
                        console.log('changing pk');

                   Kinds[index] = newData;
                   FormData.productKinds=Kinds;

                   console.log(FormData);
                    this.setState({formData: FormData});
                    }
               }}/>;
            });

            tabTitles=pks.map(pk=>{
                // console.log(pk.code);
                return pk.code+"-"+pk.price;
            });

        }

        return <ManagePageContainer title={'تعریف محصول'}>
            <ManagePageFieldsContainer>
                <TextBox title={'نام لاتین'} id='name' value={this.state.formData.name}
                         onChangeText={this.getOnTextChangeListener('name')}/>
                <TextBox title={'نام فارسی محصول'} id='displayName' value={this.state.formData.displayName}
                         onChangeText={this.getOnTextChangeListener('displayName')}/>
                <TextBox title={'حداقل موجودی'} id='minCount' value={this.state.formData.minCount}
                         onChangeText={this.getOnTextChangeListener('minCount')} keyboardType='numeric'/>
                <TextBox title={'درصد تخفیف(نمایشی)'} id='discount' value={this.state.formData.discount}
                         onChangeText={this.getOnTextChangeListener('discount')} keyboardType='numeric'/>
                <PickerBox
                    title={'دسته بندی'}
                    selectedValue={this.state.formData.group.id}
                    onValueChange={this.getOnFidValueChangeListener('group')}
                    options={this.state.groupOptions}
                />
                <PickerBox
                    title={'واحد اندازه گیری'}
                    selectedValue={this.state.formData.unit.id}
                    onValueChange={this.getOnFidValueChangeListener('unit')}
                    options={this.state.formData.simpleFields.unit}
                />
                <TextBox title={'حداقل تعداد واحد قابل سفارش'} id='minOrderCount' value={this.state.formData.minOrderCount}
                         onChangeText={this.getOnTextChangeListener('minOrderCount')} keyboardType='numeric'/>
                <TextBox title={'توضیحات محصول'} id='displayName' value={this.state.formData.description}
                         onChangeText={this.getOnTextChangeListener('description')}/>
            </ManagePageFieldsContainer>
            <TabView maxTabCount={Constants.MaxProductKinds} tabTitles={tabTitles} onAddRequest={()=>{
                pks[pks.length]=new ProductkindEntity();
                let fd=this.state.formData;
                fd.productKinds=pks;
                this.setState({formData:fd,activeTab:pks.length+""});
            }} toggle={(tab)=>{this.setState({activeTab:tab})}}
                     activeItem={this.state.activeTab}>
            {pkManagers}
            </TabView>


            {this.getSaveAndBackButtons()}
        </ManagePageContainer>
    }
}
