/**
 * @flow
 */
import React from 'react';
class ProductKind extends React.Component {

    constructor(props){
        super(props);
        this.state={
            opened:false,
        }
    }
    render() {
        const productKind=this.props.productKind;
        const pkCount=productKind.count;
        const product=this.props.product;
        let className='normal';
        if(pkCount==0)
            className='empty';
        else if(pkCount<=product.minCount)
            className='low';
        return <div className={'productkind '+className}>
                <div className={'productprice'} onClick={()=>{
                    this.props.onSelect();
                    this.setState({opened:!this.state.opened});
                }}>
                    <div className={'productkindname'}>{productKind.quality}</div>
                    <div className={'productkindprice'}>{productKind.count}</div>
                </div>
            {this.state.opened &&
            <div className={'productkinddetails'}>{productKind.description}

            </div>
            }
            </div>;
    }
}

export default ProductKind;
