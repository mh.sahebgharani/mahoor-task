/**
 * @flow
 */
import React from 'react';
import ProductKind from "./ProductKind";
export default class Product extends React.Component {
    constructor(props){
        super(props);
        this.state={
            activeProductKind:0,
        }
    }
    render() {
        let productKinds=null;
        if(this.props.productkinds!=null){
            productKinds=this.props.productkinds.map((pk,i)=>{
                return <ProductKind productKind={pk} key={pk.id} product={this.props.product} onSelect={()=>{
                    this.setState({activeProductKind:i});
                }
                }/>
            });
        }
        const activePK=this.props.productkinds[this.state.activeProductKind];
        return <div className={'product'}>
            <div className={'productimage'}><img src={activePK==null?'':activePK.photo.url}/> </div>
            <div className={'producttitle'}>{this.props.name}</div>
            {productKinds}
        </div>;
    }
}

