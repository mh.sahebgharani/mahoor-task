// @flow
import * as React from 'react';
import {TextBox} from "Libs/one/sweet-react-components";
import {SearchBoxContainer,SweetSearchBox} from "Libs/one/sweet-one-react";
import ProductEntity from "../../entity/ProductEntity";

export default class EcommerceProductSearch extends SweetSearchBox {
    moduleName='ecommerce';
    tableName='product';
    constructor(props) {
        super(props);
        this.state = {
            searchParams:{},
            formData:new ProductEntity(),

        };
    }

    componentDidMount() {
        super.componentDidMount();

        this.loadSimpleFields();

    }
    render(){
        return <SearchBoxContainer title={'جستجوی محصولات'}  onConfirm={this.getOnConfirm()}>
            <TextBox title={'نام محصول'} id='displayName'
                onChangeText={this.getOnTextChangeListener('displayname')}/>
            <TextBox title={'کد محصول'} id='code'
                     onChangeText={this.getOnTextChangeListener('productkindcode')}/>

        </SearchBoxContainer>
    }
}
