// @flow
import * as React from 'react';
import {SweetManageListPage,ListPageContainer} from "Libs/one/sweet-one-react";
import {SweetAlert} from "Libs/one/sweet-react-common-tools";
import ProductEntity from "../../entity/ProductEntity";
import EcommerceProductSearch from './EcommerceProductSearch';
import {Link} from "react-router-dom";
import {FaHandPointDown,FaHandPointUp,FaEyeSlash,FaEye} from "react-icons/all";
import ProductkindEntity from "../../entity/ProductkindEntity";

export default class EcommerceProductList extends SweetManageListPage {
    moduleName='ecommerce';
    tableName='product';
    entity = new ProductEntity();
    __defaultSorts=[{id:'group',desc:false},{id:'priority',desc:true}];
    constructor(props){
        super(props);
        this.state.pageSize=30;

    }
    getGroupName(){
        return this.props.match.params.groupname;
    }
    getData(pageSize, page, finalSort, finalFilter, afterFetch,onError){
        let filter=finalFilter;
        const stockout=this.props.match.params.stockout;
        const groupName=this.getGroupName();
        if(stockout==1)
            filter[filter.length] = {id: 'removestockout', value: "1"};
        if(groupName!=null)
            filter[filter.length] = {id: 'groupname', value: groupName};

        this.entity.getAll(pageSize, page, finalSort, filter, afterFetch,onError);
    }
    render() {
        return (
            <ListPageContainer title={'لیست محصولات'}>
                <EcommerceProductSearch onConfirm={this.searchData}/>
                <div className={'topoperationsrow'}>
                    {this.canInsert() &&
                        <div><Link className={'link'}  to={this.getAddLink()}>
                            <div>
                                <i className="fa fa-plus-circle" aria-hidden="true"></i>
                                <div className={'topoperationsrowtitle'}>تعریف محصول جدید</div>
                            </div>
                        </Link></div>
                    }
                    <div>
                        <Link className={'link'}  to={'/ecommerce/productkind/updatebyexcel'}>
                            <div>
                                <i className="fa fa-file-excel-o" aria-hidden="true"></i>
                                <div className={'topoperationsrowtitle'}>بروزسانی با excel</div>
                            </div>
                        </Link>
                    </div>
                    <div>
                        <a href={'#'} className={'link'} onClick={()=>{
                            const group=this.getGroupName();
                            const onConfirmHandler=()=>{
                                ProductkindEntity.cleanStore(group,(res)=>{
                                    SweetAlert.displaySuccessAlert('انجام شد',res.message);
                                    this.LoadData(this.state.pageSize,1,null,null);
                                })
                            };
                            SweetAlert.displayYesNoAlert('توجه','با کلیک روی این دکمه موجودی تمام محصولات صفر خواهد شد. آیا مطمئن هستید؟',{text:'بله، انبار خالی شود.',onPress:onConfirmHandler,bg:'#c52d28'},{text:'خیر'},'error')

                        }}>
                            <div>
                                <i className="fa fa-bed" aria-hidden="true"></i>
                                <div className={'topoperationsrowtitle'}>خالی کردن موجودی انبار</div>
                            </div></a>
                    </div>
                    <div>
                        <a href={'#'} className={'link'} onClick={()=>{
                            ProductkindEntity.getProductsExcel((result)=>{
                                if(result.Data!=null && result.Data.path!=null){
                                    window.location.href =global.SiteURL+result.Data.path;
                                }
                                else{
                                    SweetAlert.displayWarningAlert(result.message);
                                }

                            });
                        }}>
                            <div>
                                <i className="fa fa-cloud-download" aria-hidden="true"></i>
                                <div className={'topoperationsrowtitle'}>دریافت فهرست محصولات</div>
                            </div></a>
                    </div>
                </div>
                {this.getTable()}
            </ListPageContainer>
        )
    }

    columns =
    [
        {Header: 'نام محصول',id: 'displayname',accessor: 'displayName'},
        {Header: 'دسته بندی',id: 'group_fid',accessor: data => data.group.displayname},
        {Header: 'کدها',id: 'codes',accessor: data => {
            let codes='';
            const pks=data.productKinds;
            for(let i=0;i<pks.length;i++){
                if(codes!=='')
                    codes+=',';
                codes+=pks[i].code;
            }
            return codes;
            }},

        {Header: 'جمع موجودی',id: 'totalcount',accessor: data => {
                let sum=0;
                const pks=data.productKinds;
                for(let i=0;i<pks.length;i++){
                    sum+=pks[i].count;
                }
                return sum;
            }},
        {Header:'عملیات',id: 'operations',accessor: data => {
                return data;
            },
            Cell: props => {return this.getOperationsBox(props.value)}},
    ];
    canView() {
        return false;
    }
    canDelete() {
        return false;
    }

    getOperationsBox(data){
        let id=data.id;
        const loadData=()=>{this.LoadData(this.state.pageSize,this.state.page,this.state.sorted,this.state.filtered)};
        let visible=data.isVisibleToUser;
        return (
            <div className={'operationsrow'}>
                {this.canEdit() && this.getEditButton(id)}
                {this.canEdit() &&
                    <Link className={'editlink'} onClick={()=>{
                        this.entity.changePriority(id,1,()=>{loadData()},()=>{});
                    }}><FaHandPointUp/>
                    </Link>
                }
                {this.canEdit() &&
                    <Link className={'editlink'} onClick={()=>{
                        this.entity.changePriority(id,-1,()=>{loadData()},()=>{});
                    }}><FaHandPointDown/>
                    </Link>
                }
                {this.canEdit() &&
                    <Link className={'editlink'} onClick={()=>{
                        this.entity.changeVisiblity(id,!visible,()=>{loadData()},()=>{});
                    }}>{visible?<FaEye/>:<FaEyeSlash/>}</Link>
                }
            </div>
        )
    }
}
