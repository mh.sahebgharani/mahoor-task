// @flow
import * as React from 'react';
import {TextViewBox} from "Libs/one/sweet-react-components";
import {SweetManagePage,ManagePageContainer,ManagePageFieldsContainer} from "Libs/one/sweet-one-react";
import {} from "Libs/one/sweet-react-common-tools";
import ProductEntity from "../../entity/ProductEntity";

export default class EcommerceProductView extends SweetManagePage {
    moduleName='ecommerce';
    tableName='product';
    constructor(props) {
        super(props);
        this.state = {
            formData:new ProductEntity(),

        };
    }

    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

    }
    render(){
        return <ManagePageContainer title={'تعریف محصول'}>
        <ManagePageFieldsContainer>
            <TextViewBox title={'نام محصول'} value={this.state.formData.displayName}/>
            <TextViewBox title={'دسته بندی'} value={this.state.formData.group.displayname}/>
            <TextViewBox title={'حداقل مقدار قابل سفارش'} value={this.state.formData.minOrderCount}/>

        </ManagePageFieldsContainer>
        {this.getSaveAndBackButtons()}
        </ManagePageContainer>
    }
}
