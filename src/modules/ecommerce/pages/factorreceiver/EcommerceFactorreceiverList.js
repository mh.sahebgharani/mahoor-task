// @flow
import * as React from 'react';
import {ListPageContainer} from "Libs/one/sweet-one-react";
import EcommerceFactorreceiverListController from "../../controllers/factorreceiver/EcommerceFactorreceiverListController";
import EcommerceFactorreceiverSearch from './EcommerceFactorreceiverSearch';

export default class EcommerceFactorreceiverList extends EcommerceFactorreceiverListController {
    render() {
        return <ListPageContainer title={'لیست دریافت کننده های فاکتور'}>
            <EcommerceFactorreceiverSearch onConfirm={this.searchData}/>
            {this.getAddButton()}
            {this.getTable()}
        </ListPageContainer>;
    }
    columns =
    [
        {Header: 'نام انگلیسی',id: 'name',accessor: 'name'},
        {Header: 'عنوان فارسی',id: 'displayname',accessor: 'displayName'},
        {Header: 'عملیات',accessor: 'id',Cell: props => {return this.getOperationsBox(props.value)}}
    ];
}
