// @flow
import * as React from 'react';
import {TextViewBox,ImageModal} from "Libs/one/sweet-react-components";
import {ManagePageContainer,ManagePageFieldsContainer,SweetManagePage} from "Libs/one/sweet-one-react";
import Constants from "../../../../classes/Constants";
import FactorreceiverEntity from "../../entity/FactorreceiverEntity";
import EcommerceFactorreceiverViewController from "../../controllers/factorreceiver/EcommerceFactorreceiverViewController";


export default class EcommerceFactorreceiverView extends EcommerceFactorreceiverViewController {
    render(){
        return <ManagePageContainer title={'تعریف دریافت کننده فاکتور'}>
        <ManagePageFieldsContainer>
            <TextViewBox title={'نام انگلیسی'} value={this.state.formData.name}/>
            <TextViewBox title={'عنوان فارسی'} value={this.state.formData.displayName}/>
            {/*<TextViewBox title={'کد چاپگر'} value={this.state.formData.printerCode}/>*/}
            {/*<TextViewBox title={'چاپ قیمت'} value={this.state.formData.hasPrice.displayname}/>*/}
            {/*<TextViewBox title={'چاپ اطلاعات سفارش دهنده'} value={this.state.formData.hasUserInfo.displayname}/>*/}

                    {/*<ImageModal title='تصویر لوگو'*/}
                    {/*    previewImage={this.state.formData.logo.url}*/}
                    {/*/>*/}
        </ManagePageFieldsContainer>
        {this.getSaveAndBackButtons()}
        </ManagePageContainer>
    }
}
