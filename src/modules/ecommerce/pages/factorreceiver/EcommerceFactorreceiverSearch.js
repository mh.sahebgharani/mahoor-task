// @flow
import * as React from 'react';
import {TextBox,PickerBox} from "Libs/one/sweet-react-components";
import {SearchBoxContainer,SweetSearchBox} from "Libs/one/sweet-one-react";
import FactorreceiverEntity from "../../entity/FactorreceiverEntity";

export default class EcommerceFactorreceiverSearch extends SweetSearchBox {
    moduleName='ecommerce';
    tableName='factorreceiver';
    constructor(props) {
        super(props);
        this.state = {
            searchParams:{},
            formData:new FactorreceiverEntity(),

        };
    }

    componentDidMount() {
        super.componentDidMount();

        this.loadSimpleFields();

    }
    render(){
        return <SearchBoxContainer title={'جستجوی دریافت کننده های فاکتور'} onConfirm={this.getOnConfirm()}>

         <TextBox title={'نام انگلیسی'} id='name'
                value={this.state.searchParams.name}
                onChangeText={this.getOnTextChangeListener('name')}/>

         <TextBox title={'عنوان فارسی'} id='displayName'
                value={this.state.searchParams.displayname}
                onChangeText={this.getOnTextChangeListener('displayname')}/>

        </SearchBoxContainer>
    }
}
