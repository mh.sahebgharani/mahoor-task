// @flow
import * as React from 'react';
import {TextBox,PickerBox,FileSelector,ImageSelector,TimeSelectorBox} from "Libs/one/sweet-react-components";
import {ManagePageFieldsContainer,ManagePageContainer} from "Libs/one/sweet-one-react";
import EcommerceFactorreceiverManageController from "../../controllers/factorreceiver/EcommerceFactorreceiverManageController";


export default class EcommerceFactorreceiverManage extends EcommerceFactorreceiverManageController {
    render(){
        return <ManagePageContainer title={'تعریف دریافت کننده فاکتور'}>
        <ManagePageFieldsContainer>
            <TextBox title={'نام انگلیسی'} id='name' value={this.state.formData.name}
                onChangeText={this.getOnTextChangeListener('name')}/>
            <TextBox title={'عنوان فارسی'} id='displayName' value={this.state.formData.displayName}
                onChangeText={this.getOnTextChangeListener('displayName')}/>
            {/*<TextBox title={'کد چاپگر'} id='printerCode' value={this.state.formData.printerCode}*/}
            {/*    onChangeText={this.getOnTextChangeListener('printerCode')}/>*/}
            {/*<PickerBox*/}
            {/*    title={'چاپ قیمت'}*/}
            {/*    selectedValue={this.state.formData.hasPrice}*/}
            {/*    onValueChange={this.getOnBooleanChangeListener('hasPrice')}*/}
            {/*    options={[{id:0,name:'خیر'},{id:1,name:'بله'}]}*/}
            {/*/>*/}
            {/*<PickerBox*/}
            {/*    title={'چاپ اطلاعات سفارش دهنده'}*/}
            {/*    selectedValue={this.state.formData.hasUserInfo}*/}
            {/*    onValueChange={this.getOnBooleanChangeListener('hasUserInfo')}*/}
            {/*    options={[{id:0,name:'خیر'},{id:1,name:'بله'}]}*/}
            {/*/>*/}

                    {/*<ImageSelector title='تصویر لوگو'*/}
                    {/*    onConfirm={this.getOnFilePathChanged('logo')}*/}
                    {/*    previewImage={this.state.formData.logo.url}*/}
                    {/*    onImagePreviewLoaded={this.getOnImagePreviewLoaded('logo')}*/}
                    {/*/>*/}

        </ManagePageFieldsContainer>
        {this.getSaveAndBackButtons()}
        </ManagePageContainer>
    }
}
