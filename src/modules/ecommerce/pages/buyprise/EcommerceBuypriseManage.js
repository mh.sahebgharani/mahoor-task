// @flow
import * as React from 'react';
import {TextBox,PickerBox,FileSelector,ImageSelector} from "Libs/one/sweet-react-components";
import {ManagePageFieldsContainer,ManagePageContainer,SweetManagePage} from "Libs/one/sweet-one-react";
import BuypriseEntity from "../../entity/BuypriseEntity";


export default class EcommerceBuypriseManage extends SweetManagePage {
    moduleName='ecommerce';
    tableName='buyprise';
    constructor(props) {
        super(props);
        this.state = {
            canEdit:this.canEdit(),
            formData:new BuypriseEntity(),

        };
    }
    
    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

        this.loadSimpleFields();

    }
    render(){
        return <ManagePageContainer title={'تعریف امتیاز خرید'}>
        <ManagePageFieldsContainer>
        <TextBox title={'حداقل مبلغ'} id='minPrice' value={this.state.formData.minPrice}
         onChangeText={this.getOnTextChangeListener('minPrice')} keyboardType='numeric'/>
        <TextBox title={'حداکثر مبلغ'} id='maxPrice' value={this.state.formData.maxPrice}
         onChangeText={this.getOnTextChangeListener('maxPrice')} keyboardType='numeric'/>
        <TextBox title={'مبلغ امتیاز'} id='prise' value={this.state.formData.prise}
         onChangeText={this.getOnTextChangeListener('prise')} keyboardType='numeric'/>


        </ManagePageFieldsContainer>
        {this.getSaveAndBackButtons()}
        </ManagePageContainer>
    }
}
