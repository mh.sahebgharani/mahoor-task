// @flow
import * as React from 'react';
import {TextViewBox,ImageModal} from "Libs/one/sweet-react-components";
import {ManagePageContainer,ManagePageFieldsContainer,SweetManagePage} from "Libs/one/sweet-one-react";
import Constants from "../../../../classes/Constants";
import BuypriseEntity from "../../entity/BuypriseEntity";

export default class EcommerceBuypriseView extends SweetManagePage {
    moduleName='ecommerce';
    tableName='buyprise';
    constructor(props) {
        super(props);
        this.state = {
            formData:new BuypriseEntity(),

        };
    }

    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

    }
    render(){
        return <ManagePageContainer title={'تعریف امتیاز خرید'}>
        <ManagePageFieldsContainer>
            <TextViewBox title={'حداقل مبلغ'} value={this.state.formData.minPrice}/>
            <TextViewBox title={'حداکثر مبلغ'} value={this.state.formData.maxPrice}/>
            <TextViewBox title={'مبلغ امتیاز'} value={this.state.formData.prise}/>

        </ManagePageFieldsContainer>
        {this.getSaveAndBackButtons()}
        </ManagePageContainer>
    }
}
