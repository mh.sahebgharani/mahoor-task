// @flow
import * as React from 'react';
import Constants from '../../../../classes/Constants';
import BuypriseEntity from "../../entity/BuypriseEntity";
import {ListPageContainer,SweetManageListPage} from "Libs/one/sweet-one-react";


export default class EcommerceBuypriseList extends SweetManageListPage {
    moduleName='ecommerce';
    tableName='buyprise';
    entity = new BuypriseEntity();
    componentDidMount() {
        super.componentDidMount();
        this.LoadData(this.state.pageSize,1,null,null);
    }
    render() {
        return <ListPageContainer title={'لیست امتیازات خرید'}>
            
            {this.getAddButton()}
            {this.getTable()}
        </ListPageContainer>;
    }
    columns =
    [
        {Header: 'حداقل مبلغ',id: 'minprice',accessor: 'minPrice'},
        {Header: 'حداکثر مبلغ',id: 'maxprice',accessor: 'maxPrice'},
        {Header: 'مبلغ امتیاز',id: 'prise_prc',accessor: 'prise'},
        {Header: 'عملیات',accessor: 'id',Cell: props => {return this.getOperationsBox(props.value)}}
    ];
}
