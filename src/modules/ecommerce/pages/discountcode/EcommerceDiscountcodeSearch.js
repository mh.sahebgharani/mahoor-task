// @flow
import * as React from 'react';

import {PickerBox,} from "Libs/one/sweet-react-components";
import {SearchBoxContainer,SweetSearchBox} from "Libs/one/sweet-one-react";
import DiscountcodeEntity from "../../entity/DiscountcodeEntity";
import EcommerceuserEntity from '../../entity/EcommerceuserEntity';
export default class EcommerceDiscountcodeSearch extends SweetSearchBox {
    moduleName='ecommerce';
    tableName='discountcode';
    constructor(props) {
        super(props);
        this.state = {
            searchParams:{},
            formData:new DiscountcodeEntity(),

			ecommerceUserOptions:new EcommerceuserEntity(),
        };
    }

    loadEcommerceUsers = () => {
        this.state.ecommerceUserOptions.getAll(null,null,null,null,(data)=>{
                this.setState({ecommerceUserOptions:data});
            },null);
    };

    componentDidMount() {
        super.componentDidMount();

        this.loadEcommerceUsers();
        this.loadSimpleFields();

    }
    render(){
        return <SearchBoxContainer title={'جستجوی کدهای تخفیف'} onConfirm={this.getOnConfirm()}>
            <PickerBox
                title={'کد کاربر'}
                onValueChange={this.getOnFidValueChangeListener('ecommerceuser_fid')}
                options={this.state.ecommerceUserOptions}
            />

        </SearchBoxContainer>
    }
}
