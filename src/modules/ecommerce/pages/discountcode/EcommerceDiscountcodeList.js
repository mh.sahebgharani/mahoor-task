// @flow
import * as React from 'react';
import {SweetManageDepenedentListPage,DependentListPageContainer} from "Libs/one/sweet-one-react";
import DiscountcodeEntity from "../../entity/DiscountcodeEntity";
import EcommerceDiscountcodeSearch from './EcommerceDiscountcodeSearch';

export default class EcommerceDiscountcodeList extends SweetManageDepenedentListPage {
    moduleName='ecommerce';
    tableName='discountcode';
    __defaultFilters=[{id:'ecommerceuser_fid',value:this.getOwnerID()}]
    entity = new DiscountcodeEntity();
    getOwnerID(){
        return super.getOwnerID()||-1;
    }
    componentDidMount() {
        super.componentDidMount();
        // this.LoadData(this.state.pageSize,1,null,null);
    }
    getDefaultPageSize(){
        return 8;
    }
    render() {
        return <DependentListPageContainer title={'لیست کدهای تخفیف'}>
            {this.getAddButton()}
            {this.getTable()}
        </DependentListPageContainer>;
    }
    columns =
    [
        {Header: 'عنوان',id: 'title',accessor: 'title'},
        {Header: 'مبلغ',id: 'amount_prc',accessor: 'amount'},
        {Header: 'نوع',id: 'type_fid',accessor: data => data.type.displayname},
        {Header: 'عملیات',accessor: 'id',Cell: props => {return this.getOperationsBox(props.value)}}
    ];

    getOperationsBox(id){
        return <div className={'operationsrow'}>
            {this.getEditButton(id)}
        </div>
    }
}
