// @flow
import * as React from 'react';
import {TextViewBox,} from "Libs/one/sweet-react-components";
import {SweetDependentManagePage,ManagePageContainer,ManagePageFieldsContainer} from "Libs/one/sweet-one-react";
import DiscountcodeEntity from "../../entity/DiscountcodeEntity";

export default class EcommerceDiscountcodeView extends SweetDependentManagePage {
    moduleName='ecommerce';
    tableName='discountcode';
    __ownerTable='ecommerceuser';
    constructor(props) {
        super(props);
        this.state = {
            formData:new DiscountcodeEntity(this.getOwnerID()),

        };
    }

    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

    }
    render(){
        return (
            <ManagePageContainer title={'تعریف کد تخفیف'}>
                <ManagePageFieldsContainer>
                    <TextViewBox title={'عنوان'} value={this.state.formData.title}/>
                    <TextViewBox title={'کد'} value={this.state.formData.code}/>
                    <TextViewBox title={'حداقل مبلغ سفارش'} value={this.state.formData.minOrderPrice}/>
                    <TextViewBox title={'مبلغ'} value={this.state.formData.amount}/>
                    <TextViewBox title={'درصد تخفیف'} value={this.state.formData.percent}/>
                    <TextViewBox title={'توضیحات'} value={this.state.formData.description}/>
                    <TextViewBox title={'تاریخ شروع'} value={this.state.formData.startdate}/>
                    <TextViewBox title={'تاریخ انقضا'} value={this.state.formData.enddate}/>
                    <TextViewBox title={'نوع'} value={this.state.formData.type.displayname}/>
                    <TextViewBox title={'تعداد قابل استفاده'} value={this.state.formData.count}/>
                    <TextViewBox title={'کد کاربر'} value={this.state.formData.ecommerceUser.displayname}/>

                </ManagePageFieldsContainer>
                {this.getSaveAndBackButtons()}
            </ManagePageContainer>
        )
    }
}
