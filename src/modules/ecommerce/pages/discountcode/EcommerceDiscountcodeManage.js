// @flow
import * as React from 'react';
import {TextBox,PickerBox,DateSelectorBox} from "Libs/one/sweet-react-components";
// import basic from '../../../../PersianDatePicker/styles/basic.css';
import {
    SweetDependentManagePage,
    ManagePageContainer,
    ManagePageFieldsContainer,
    SweetFetcher
} from "Libs/one/sweet-one-react";
import DiscountcodeEntity from "../../entity/DiscountcodeEntity";
import {} from "Libs/one/sweet-one-react";
import {} from "Libs/one/sweet-react-common-tools";
import AsyncSelect from "react-select/async";


export default class EcommerceDiscountcodeManage extends SweetDependentManagePage {
    moduleName='ecommerce';
    tableName='discountcode';
    __ownerTable='ecommerceuser';
    constructor(props) {
        super(props);
        this.state = {
            canEdit:this.canEdit(),
            formData:new DiscountcodeEntity(this.getOwnerID()),

        };
    }

    componentDidMount() {
        super.componentDidMount();
        if(this.getOwnerID()>0)
            this.state.formData.get(this.getOwnerID(), () => {this.notifyFormDataChanged();});
        // if(this.getID()>0)
        //     this.state.data;
        console.log("this.state.formData",this.state.formData)
        console.log("**",this.state.data)
        this.setState({dates:true});
        this.loadSimpleFields();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
    }


    render() {
        return (
            <ManagePageContainer title={'تعریف کد تخفیف'}>
                <ManagePageFieldsContainer>
                    <DateSelectorBox title={'تاریخ شروع(اولین روز)'} id='startdate' value={this.state.formData.startdate}
                                     onChange={this.getOnTextChangeListener('startdate')}/>
                    <DateSelectorBox title={'تاریخ انقضا(یک روز بعد از آخرین روز)'} id='enddate' value={this.state.formData.enddate}
                                     onChange={this.getOnTextChangeListener('enddate')}/>
                    <TextBox title={'عنوان'} id='title' value={this.state.formData.title}
                             onChangeText={this.getOnTextChangeListener('title')}/>
                    <TextBox title={'کد تخفیف'} placeholder={'کد تخفیف به صورت ترکیبی از حروف و اعداد انگلیسی'} id='code' value={this.state.formData.code}
                             onChangeText={this.getOnTextChangeListener('code')}/>
                    <TextBox title={'حداقل مبلغ سفارش'} placeholder={'حداقل مبلغ سفارش به ریال'} id='minOrderPrice' value={this.state.formData.minOrderPrice}
                             onChangeText={this.getOnTextChangeListener('minOrderPrice')} keyboardType='numeric'/>
                    <TextBox title={'امتیاز تخفیف'} id='amount' value={this.state.formData.amount}
                             onChangeText={this.getOnTextChangeListener('amount')} keyboardType='numeric' placeholder={'حداکثر امتیاز(ضریب مبلغ) تخفیف'}/>


                    <AsyncSelect
                        cacheOptions
                        // https://api.ghazakade-kilidar.ir/api/ecommerce/ecommerceuser?name=${inputValue}
                        //https://api.ghazakade-kilidar.ir/api/ecommerce/ecommerceuser?__pagesize=30&__startrow=0&name=${inputValue}
                        loadOptions={(inputValue,callback) => {
                            // if(!inputValue) return;
                            new SweetFetcher().Fetch(`https://api.ghazakade-kilidar.ir/api/ecommerce/ecommerceuser?__pagesize=30&__startrow=0&name=${inputValue}`, SweetFetcher.METHOD_GET,null,
                                data => {
                                    // this.state.formData.get(this.state.data());
                                    // console.log("this.state.data",this.state.data());
                                    // console.log("saddasdsaads",data.map(item => (item.user.name)));
                                    callback(data.Data?.map((item)=>({
                                        value :item.user.name,
                                        label : item.user.name
                                    })))
                                });
                        }}
                        defaultOptions
                        // onInputChange={this.handleInputChange}
                        onChangeText={this.getOnTextChangeListener("list")}
                    />

                    <TextBox title={'درصد تخفیف'} placeholder={'برای در نظر نگرفتن ۱۰۰ وارد کنید.'} id='percent' value={this.state.formData.percent}
                             onChangeText={this.getOnTextChangeListener('percent')} keyboardType='numeric'/>
                    <div colweight={2} className={'informhelp'}>
                        <p>در محاسبه میزان تخفیف حداقل مبلغ به دست آمده بین امتیاز تخفیف و درصد تخفیف از جمع کل سفارش محاسبه می شود.
                        </p>
                        <p>
                            به طور مثال اگر درصد تخفیف ۱۰ درصد، حداکثر امتیاز ۲۰۰۰ و مبلغ هر امتیاز(قابل تنظیم در منوی تنظیمات) ۱۰۰ریال باشد و مبلغ سفارش ۱ میلیون ریال باشد مبلغ تخفیف حداقل بین ۲۰۰ هزار ریال و ۱۰۰ هزار ریال یا همان ۱۰۰ هزار ریال محاسبه می شود.
                        </p><p>
                        درصورتی که می خواهید فقط حداکثر امتیاز برای کد تخفیف محاسبه شود، درصد تخفیف را ۱۰۰ وارد کنید.
                    </p>
                    </div>
                    {/*<PickerBox*/}
                    {/*    title={'نوع'}*/}
                    {/*    selectedValue={this.state.formData.type.id}*/}
                    {/*    onValueChange={this.getOnFidValueChangeListener('type')}*/}
                    {/*    options={this.state.formData.simpleFields.discounttype}*/}
                    {/*/>*/}
                    {/*<TextBox title={'حداکثر تعداد قابل استفاده'} id='count' value={this.state.formData.count}*/}
                    {/* onChangeText={this.getOnTextChangeListener('count')} keyboardType='numeric'*/}
                    {/*/>*/}
                    {/*<TextBox title={'توضیحات'} id='description' value={this.state.formData.description}*/}
                    {/*         onChangeText={this.getOnTextChangeListener('description')}*/}
                    {/*/>*/}


                </ManagePageFieldsContainer>
                {this.getSaveAndBackButtons()}
            </ManagePageContainer>
        )
    }
    returnBack() {
        if(this.getOwnerID()>0)
            super.returnBack();
        else
            this.props.history.push('/'+this.moduleName+'/'+this.tableName);
    }
}
