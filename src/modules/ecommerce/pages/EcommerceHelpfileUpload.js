// @flow
import * as React from 'react';
import { FileSelector,SweetButton} from "Libs/one/sweet-react-components";
import {ManagePageContainer} from "Libs/one/sweet-one-react";
import {SweetAlert} from "Libs/one/sweet-react-common-tools";
import HelpfileEntity from "../entity/HelpfileEntity";
import {MDBCol, MDBContainer, MDBRow} from "mdbreact";


export default class EcommerceHelpfileUpload extends React.Component{
    getOnFilePathChanged(name){
        return (path) => {
                this.setState({file:path});
        }
    }
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render(){
        return <ManagePageContainer title={'بروزرسانی فایل راهنما'}>
                <div>
            <FileSelector title='فایل راهنما'
                          onConfirm={this.getOnFilePathChanged()}
            />
            < SweetButton value={'آپلود'} onButtonPress={(onEnd)=>{
                const pkent=new HelpfileEntity(null);
                pkent.uploadFile(this.state.file,(result)=>{
                SweetAlert.displaySuccessAlert('آپلود شد','فایل با موفقیت آپلود شد')
                onEnd(true);
            },()=>{onEnd(true)});
            }}/>
                </div>


        </ManagePageContainer>
    }
}
