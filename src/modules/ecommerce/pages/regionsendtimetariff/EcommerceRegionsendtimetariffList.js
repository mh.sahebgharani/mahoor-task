// @flow
import * as React from 'react';
import RegionsendtimetariffEntity from "../../entity/RegionsendtimetariffEntity";
import {DependentListPageContainer,SweetManageDepenedentListPage} from "Libs/one/sweet-one-react";

export default class EcommerceRegionsendtimetariffList extends SweetManageDepenedentListPage {
    moduleName='ecommerce';
    tableName='regionsendtimetariff';__defaultFilters=[{id:'sendtime_fid',value:this.getOwnerID()}]
    entity = new RegionsendtimetariffEntity(this.getOwnerID());
    componentDidMount() {
        super.componentDidMount();
    }
    render() {
        return(
            <DependentListPageContainer title={'لیست تعرفه های ارسال به محدوده جغرافیایی'}>
                {this.getAddButton()}
                {this.getTable()}
            </DependentListPageContainer>
        )
    }
    columns =
    [
        // {Header: 'زمان ارسال',id: 'sendtime_fid',accessor: data => data.sendTime.displayname},
        {Header: 'محدوده جغرافیایی',id: 'location_region_fid',accessor: data => data.region.displayname},
        // {Header: 'حداقل مبلغ سفارش برای ارسال رایگان',id: 'minfreesendprice',accessor: 'minFreeSendPrice'},
        // {Header: 'اضافه مبلغ برای هر 100 متر',id: 'pricefordistance_prc',accessor: 'pricefordistance'},
        {Header: 'مبلغ اولیه',id: 'pricestart_prc',accessor: 'priceStart'},
        {Header: 'عملیات',accessor: 'id',Cell: props => {return this.getOperationsBox(props.value)}}
    ];
}
