// @flow
import * as React from 'react';
import {TextBox,PickerBox,FileSelector,ImageSelector,TimeSelectorBox} from "Libs/one/sweet-react-components";
import {ManagePageFieldsContainer,ManagePageContainer,SweetDependentManagePage} from "Libs/one/sweet-one-react";
import RegionsendtimetariffEntity from "../../entity/RegionsendtimetariffEntity";
import RegionEntity from '../../../location/entity/RegionEntity';

export default class EcommerceRegionsendtimetariffManage extends SweetDependentManagePage {
    moduleName='ecommerce';
    tableName='regionsendtimetariff';
    __ownerTable='sendtime';
    constructor(props) {
        super(props);
        this.state = {
            canEdit:this.canEdit(),
            formData:new RegionsendtimetariffEntity(this.getOwnerID()),

			regionOptions:new RegionEntity(),
        };
    }

    loadRegions = () => {
        this.state.regionOptions.getAll(null,null,null,null,(data)=>{
                this.setState({regionOptions:data});
            },null);
    };

    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

        this.loadRegions();
        this.loadSimpleFields();

    }
    render(){
        return <ManagePageContainer title={'تعریف تعرفه ارسال به محدوده جغرافیایی'}>
        <ManagePageFieldsContainer>
            <PickerBox
                title={'محدوده جغرافیایی'}
                selectedValue={this.state.formData.region.id}
                onValueChange={this.getOnFidValueChangeListener('region')}
                options={this.state.regionOptions}
            />
        <TextBox title={'حداقل مبلغ سفارش برای ارسال رایگان'} id='minFreeSendPrice' value={this.state.formData.minFreeSendPrice}
         onChangeText={this.getOnTextChangeListener('minFreeSendPrice')} keyboardType='numeric'/>
        {/*<TextBox title={'اضافه مبلغ برای هر 100 متر'} id='pricefordistance' value={this.state.formData.pricefordistance}*/}
        {/* onChangeText={this.getOnTextChangeListener('pricefordistance')} keyboardType='numeric'/>*/}
        <TextBox title={'مبلغ اولیه'} id='priceStart' value={this.state.formData.priceStart}
         onChangeText={this.getOnTextChangeListener('priceStart')} keyboardType='numeric'/>


        </ManagePageFieldsContainer>
        {this.getSaveAndBackButtons()}
        </ManagePageContainer>
    }
}
