// @flow
import * as React from 'react';
import {TextViewBox,ImageModal} from "Libs/one/sweet-react-components";
import {ManagePageContainer,ManagePageFieldsContainer,SweetDependentManagePage} from "Libs/one/sweet-one-react";
import Constants from "../../../../classes/Constants";
import RegionsendtimetariffEntity from "../../entity/RegionsendtimetariffEntity";

export default class EcommerceRegionsendtimetariffView extends SweetDependentManagePage {
    moduleName='ecommerce';
    tableName='regionsendtimetariff';
    __ownerTable='sendtime';
    constructor(props) {
        super(props);
        this.state = {
            formData:new RegionsendtimetariffEntity(this.getOwnerID()),

        };
    }

    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

    }
    render(){
        return <ManagePageContainer title={'تعریف تعرفه ارسال به محدوده جغرافیایی'}>
        <ManagePageFieldsContainer>
            <TextViewBox title={'زمان ارسال'} value={this.state.formData.sendTime.displayname}/>
            <TextViewBox title={'محدوده جغرافیایی'} value={this.state.formData.region.displayname}/>
            <TextViewBox title={'حداقل مبلغ سفارش برای ارسال رایگان'} value={this.state.formData.minFreeSendPrice}/>
            <TextViewBox title={'اضافه مبلغ برای هر 100 متر'} value={this.state.formData.pricefordistance}/>
            <TextViewBox title={'مبلغ اولیه'} value={this.state.formData.priceStart}/>

        </ManagePageFieldsContainer>
        {this.getSaveAndBackButtons()}
        </ManagePageContainer>
    }
}
