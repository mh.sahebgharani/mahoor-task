// @flow
import * as React from 'react';
import Constants from '../../../../classes/Constants';
import FactorexportitemEntity from "../../entity/FactorexportitemEntity";
import {DependentListPageContainer,SweetManageDepenedentListPage} from "Libs/one/sweet-one-react";


export default class EcommerceFactorexportitemList extends SweetManageDepenedentListPage {
    moduleName='ecommerce';
    tableName='factorexportitem';__defaultFilters=[{id:'factorexport_fid',value:this.getOwnerID()}]
    entity = new FactorexportitemEntity();
    componentDidMount() {
        super.componentDidMount();
        this.LoadData(this.state.pageSize,1,null,null);
    }
    render() {
        return <DependentListPageContainer title={'لیست آیتم های فاکتور خروجی'}>

            {this.getAddButton()}
            {this.getTable()}
        </DependentListPageContainer>;
    }
    columns =
    [
        {Header: 'ردیف سفارش',id: 'orderproductkind_fid',accessor: data => data.orderProductKind.displayname},
        {Header: 'فاکتور خروجی',id: 'factorexport_fid',accessor: data => data.factorExport.displayname},
        {Header: 'عملیات',accessor: 'id',Cell: props => {return this.getOperationsBox(props.value)}}
    ];
}
