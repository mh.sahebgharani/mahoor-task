// @flow
import * as React from 'react';
import {TextViewBox,ImageModal} from "Libs/one/sweet-react-components";
import {ManagePageContainer,ManagePageFieldsContainer,SweetDependentManagePage} from "Libs/one/sweet-one-react";
import Constants from "../../../../classes/Constants";
import FactorexportitemEntity from "../../entity/FactorexportitemEntity";

export default class EcommerceFactorexportitemView extends SweetDependentManagePage {
    moduleName='ecommerce';
    tableName='factorexportitem';
    __ownerTable='factorexport';
    constructor(props) {
        super(props);
        this.state = {
            formData:new FactorexportitemEntity(this.getOwnerID()),

        };
    }
    
    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

    }
    render(){
        return <ManagePageContainer title={'تعریف آیتم فاکتور خروجی'}>
        <ManagePageFieldsContainer>
            <TextViewBox title={'ردیف سفارش'} value={this.state.formData.orderProductKind.displayname}/>
            <TextViewBox title={'فاکتور خروجی'} value={this.state.formData.factorExport.displayname}/>

        </ManagePageFieldsContainer>
        {this.getSaveAndBackButtons()}
        </ManagePageContainer>
    }
}
