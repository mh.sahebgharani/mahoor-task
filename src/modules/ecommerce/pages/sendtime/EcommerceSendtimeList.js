// @flow
import * as React from 'react';
import Constants from '../../../../classes/Constants';
import SendtimeEntity from "../../entity/SendtimeEntity";
import {ListPageContainer,SweetManageListPage} from "Libs/one/sweet-one-react";


export default class EcommerceSendtimeList extends SweetManageListPage {
    moduleName='ecommerce';
    tableName='sendtime';
    entity = new SendtimeEntity();
    componentDidMount() {
        super.componentDidMount();
        this.LoadData(this.state.pageSize,1,null,null);
    }
    render() {
        return <ListPageContainer title={'لیست بازه های زمانی ارسال'}>

            {this.getAddButton()}
            {this.getTable()}
        </ListPageContainer>;
    }
    columns =
    [
        {Header: 'نام نمایشی',id: 'displayname',accessor: 'displayName'},
        {Header: 'هزینه ارسال',id: 'sendprice',accessor: 'sendPrice'},
        {Header: 'عملیات',accessor: 'id',Cell: props => {return this.getOperationsBox(props.value)}}
    ];
    canView(){
        return false;
    }
    canDelete(){
        return false;
    }
}
