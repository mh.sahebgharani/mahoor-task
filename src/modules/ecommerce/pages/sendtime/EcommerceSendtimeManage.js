// @flow
import * as React from 'react';
import {TextBox,PickerBox,FileSelector,ImageSelector,TimeSelectorBox} from "Libs/one/sweet-react-components";
import {ManagePageFieldsContainer,ManagePageContainer,SweetManagePage} from "Libs/one/sweet-one-react";
import SendtimeEntity from "../../entity/SendtimeEntity";
import EcommerceRegionsendtimetariffList from "../regionsendtimetariff/EcommerceRegionsendtimetariffList";
import Constants from "../../../../classes/Constants";


export default class EcommerceSendtimeManage extends SweetManagePage {
    moduleName='ecommerce';
    tableName='sendtime';
    constructor(props) {
        super(props);
        this.state = {
            canEdit:this.canEdit(),
            formData:new SendtimeEntity(),

        };
    }

    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

        this.loadSimpleFields();

    }
    render(){
        return (
            <ManagePageContainer title={'تعریف بازه زمانی ارسال'}>
                <ManagePageFieldsContainer>
                    <TextBox title={'نام نمایشی'} id='displayName' value={this.state.formData.displayName}
                             onChangeText={this.getOnTextChangeListener('displayName')}/>
                    <TimeSelectorBox title={'ساعت شروع'} id='startTime' value={this.state.formData.startTime}
                                     onChange={this.getOnTextChangeListener('startTime')}/>
                    <TimeSelectorBox title={'ساعت پایان'} id='endTime' value={this.state.formData.endTime}
                                     onChange={this.getOnTextChangeListener('endTime')}/>
                    <TextBox title={'توضیحات'} id='description' value={this.state.formData.description}
                             onChangeText={this.getOnTextChangeListener('description')}/>
                    <TextBox title={'اولویت نمایش'} id='priority' value={this.state.formData.priority}
                             onChangeText={this.getOnTextChangeListener('priority')} keyboardType='numeric'/>
                    <TextBox title={'هزینه ارسال'} id='sendPrice' value={this.state.formData.sendPrice}
                             onChangeText={this.getOnTextChangeListener('sendPrice')} keyboardType='numeric'/>
                    <TextBox title={'حداقل مبلغ برای ارسال رایگان'} id='minFreeSendPrice' value={this.state.formData.minFreeSendPrice}
                             onChangeText={this.getOnTextChangeListener('minFreeSendPrice')} keyboardType='numeric'/>
                    <PickerBox
                        title={'فعال'}
                        selectedValue={this.state.formData.isEnabled}
                        onValueChange={this.getOnBooleanChangeListener('isEnabled')}
                        options={[{id:0,name:'خیر'},{id:1,name:'بله'}]}
                    />
                    <PickerBox
                        title={'ارسال قبل از زمانبندی'}
                        selectedValue={this.state.formData.canSendBeforeTime}
                        onValueChange={this.getOnBooleanChangeListener('canSendBeforeTime')}
                        options={[{id:0,name:'خیر'},{id:1,name:'بله'}]}
                    />
                    <TextBox title={'پیام ارسال قبل از زمانبندی'} id='beforeTimeMessage' value={this.state.formData.beforeTimeMessage}
                             onChangeText={this.getOnTextChangeListener('beforeTimeMessage')}/>

                    <PickerBox
                        title={'ارسال پس از زمانبندی'}
                        selectedValue={this.state.formData.canSendAfterTime}
                        onValueChange={this.getOnBooleanChangeListener('canSendAfterTime')}
                        options={[{id:0,name:'خیر'},{id:1,name:'بله'}]}
                    />
                    <TextBox title={'پیام ارسال پس از زمانبندی'} id='afterTimeMessage' value={this.state.formData.afterTimeMessage}
                             onChangeText={this.getOnTextChangeListener('afterTimeMessage')}/>

                    <PickerBox
                        title={'نمایش پیام راهنما'}
                        selectedValue={this.state.formData.displayCautionMessage}
                        onValueChange={this.getOnBooleanChangeListener('displayCautionMessage')}
                        options={[{id:0,name:'خیر'},{id:1,name:'بله'}]}
                    />

                    <TextBox title={'پیام راهنما'} id='cautionMessage' value={this.state.formData.cautionMessage}
                             onChangeText={this.getOnTextChangeListener('cautionMessage')}/>

                    {false && <div>
                        <ImageSelector title='لوگو'
                                       onConfirm={this.getOnFilePathChanged('logo')}
                                       previewImage={this.state.formData.logo.url}
                                       onImagePreviewLoaded={this.getOnImagePreviewLoaded('logo')}
                        /> < TextBox title={'رنگ زمینه'} id='backgroundColor' value={this.state.formData.backgroundColor}
                                     onChangeText={this.getOnTextChangeListener('backgroundColor')}/>
                        <TextBox title={'رنگ متن'} id='textColor' value={this.state.formData.textColor}
                                 onChangeText={this.getOnTextChangeListener('textColor')}/>
                        <TextBox title={'رنگ توضیحات'} id='descriptionColor' value={this.state.formData.descriptionColor}
                                 onChangeText={this.getOnTextChangeListener('descriptionColor')}/>
                    </div>
                    }
                    {/*{this.getID() > 0 && Constants.DisplayRegionSendTimeTariffs &&*/}
                        <EcommerceRegionsendtimetariffList colweight={2} ownerID={this.getID()}/>
                    {/*}*/}

                </ManagePageFieldsContainer>
                {this.getSaveAndBackButtons()}
            </ManagePageContainer>
        )
    }
}
