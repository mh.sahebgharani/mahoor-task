// @flow
import * as React from 'react';
import {SweetManageListPage,ListPageContainer} from "Libs/one/sweet-one-react";
import EcommerceuserEntity from "../../entity/EcommerceuserEntity";
import EcommerceDiscountcodeSearch from "../discountcode/EcommerceDiscountcodeSearch";
import EcommerceEcommerceuserSearch from "./EcommerceEcommerceuserSearch";


export default class EcommerceDeliveriesList extends SweetManageListPage {
    moduleName='ecommerce';
    tableName='ecommerceuser';
    entity = new EcommerceuserEntity();
    constructor(props){
        super(props);
        this.state.pageSize=30;
    }
    componentDidMount() {
        super.componentDidMount();
        // this.LoadData(this.state.pageSize,1,null,null);
    }
    getData(pageSize, page, finalSort, finalFilter, afterFetch,onError){
        this.entity.getDeliveries(pageSize, page, finalSort, finalFilter, afterFetch,onError);
    }
    render() {
        console.log(this.state.data);
        return <ListPageContainer title={'فهرست ماموران تحویل'}>
            <EcommerceEcommerceuserSearch onConfirm={this.searchData}/>
            {this.getTable()}
        </ListPageContainer>;
    }
    columns =
    [
        {Header: 'کد کاربری',id: 'code',accessor: data => data.id},
        {Header: 'نام',id: 'name',accessor: data => data.user.name},
        {Header: 'تلفن',id: 'phone',accessor: data => data.user.phone},
        // {Header: 'تاریخ ثبت نام',id: 'updated_at',accessor: data => data.created_at},
        {Header: 'عملیات',accessor: 'id',Cell: props => {return this.getOperationsBox(props.value)}}
    ];
    canView() {
        return false;
    }

    getEditLink(id){
        return '/'+this.moduleName+"/delivery/"+'management/'+id;
    }
}
