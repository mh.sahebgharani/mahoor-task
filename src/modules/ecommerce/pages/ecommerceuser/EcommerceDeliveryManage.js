// @flow
import * as React from 'react';
import {SweetManagePage, ManagePageContainer, ManagePageFieldsContainer, ListPageContainer} from "Libs/one/sweet-one-react";
import EcommerceuserEntity from "../../entity/EcommerceuserEntity";
import {Link} from "react-router-dom";
import {SweetAlert} from "Libs/one/sweet-react-common-tools";

import EcommerceDiscountcodeList from '../discountcode/EcommerceDiscountcodeList';
import {SweetButton, TextViewBox} from "Libs/one/sweet-react-components";
import {MDBCol, MDBContainer, MDBRow} from "mdbreact";
export default class EcommerceDeliveryManage extends SweetManagePage {
    moduleName='ecommerce';
    tableName='ecommerceuser';
    constructor(props) {
        super(props);
        this.state = {
            canEdit:this.canEdit(),
            formData:new EcommerceuserEntity(),

        };

        this.loadData();
        this.loadSimpleFields();
    }

    componentDidMount() {
        super.componentDidMount();

    }
    loadData(){
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

    }
    isDelivery(roles){
        for(let i=0;i<roles.length;i++){
            if(roles[i].trim().toLowerCase()=='delivery')
                return true;
        }
        return false;
    }
    render(){
        return <ManagePageContainer title={'اطلاعات راننده'}>
            {this.state.formData.user.id != null &&
            <div>
                <ManagePageFieldsContainer>
                    <TextViewBox title={'نام'} value={this.state.formData.user.name}/>
                    <TextViewBox title={'تلفن'} value={this.state.formData.user.phone}/>
                    {/*<TextViewBox title={'تعداد سفارش تحویل داده شده'} value={this.state.formData.deliveredOrders}/>*/}
                    {/*<TextViewBox title={'جمع پرداختی'} value={this.state.formData.deliveredOrdersSum/10+' تومان'}/>*/}
                </ManagePageFieldsContainer>
                <MDBContainer className={'smallOperations'}>
                    <MDBRow>
                        <MDBCol>
                            <div>
                                <Link to={'/ecommerce/order/delivery/' + this.getID()+'/3'}>
                                    سفارشات آماده ارسال
                                </Link>
                            </div>
                            <div>
                                <Link to={'/ecommerce/order/delivery/' + this.getID()+'/4'}>
                                    سفارشات تحویل داده شده
                                </Link>
                            </div>
                            <div>
                                <Link to={'/ecommerce/order/delivery/' + this.getID()+'/6'}>
                                    سفارشات لغو شده
                                </Link>
                            </div>
                                <div>
                                    <a href={'#'} onClick={() => {
                                        this.state.formData.retractDelivery(this.getID(), (res) => {
                                            SweetAlert.displaySuccessAlert('پیام', res.message);
                                            this.loadData();
                                        })
                                    }}>
                                        تبدیل به کاربر عادی
                                    </a>
                                </div>
                        </MDBCol>
                    </MDBRow>
                </MDBContainer>

                <SweetButton value={'برگشت'} onButtonPress={()=>{this.returnBack()}}/>
            </div>
            }
        </ManagePageContainer>
    }
    returnBack(){
        if(this.props!=null)
            this.props.history.push('/'+this.moduleName+'/delivery');
        else
            console.log('this.props is null');
    }
}
