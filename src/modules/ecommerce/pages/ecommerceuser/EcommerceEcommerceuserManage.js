// @flow
import * as React from 'react';
import {SweetManagePage, ManagePageContainer, ManagePageFieldsContainer, ListPageContainer} from "Libs/one/sweet-one-react";
import EcommerceuserEntity from "../../entity/EcommerceuserEntity";
import {Link} from "react-router-dom";
import {SweetAlert} from "Libs/one/sweet-react-common-tools";


import EcommerceDiscountcodeList from '../discountcode/EcommerceDiscountcodeList';
import {SweetButton, TextViewBox} from "Libs/one/sweet-react-components";
import {MDBCol, MDBContainer, MDBRow} from "mdbreact";
export default class EcommerceEcommerceuserManage extends SweetManagePage {
    moduleName='ecommerce';
    tableName='ecommerceuser';
    constructor(props) {
        super(props);
        this.state = {
            canEdit:this.canEdit(),
            formData:new EcommerceuserEntity(),

        };

        this.loadData();
        this.loadSimpleFields();
    }

    componentDidMount() {
        super.componentDidMount();

    }
    loadData(){
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

    }
    isDelivery(roles){
        for(let i=0;i<roles.length;i++){
            if(roles[i].trim().toLowerCase()=='delivery')
                return true;
        }
        return false;
    }
    render(){
        return <ManagePageContainer title={'تعریف کاربر فروشگاه اینترنتی'}>
            {this.state.formData.user.id != null &&
            <div>
                <ManagePageFieldsContainer>
                    <TextViewBox title={'نام'} value={this.state.formData.user.name}/>
                    <TextViewBox title={'تلفن'} value={this.state.formData.user.phone}/>
                    <TextViewBox title={'تعداد سفارش تحویل داده شده'} value={this.state.formData.deliveredOrders}/>
                    <TextViewBox title={'جمع پرداختی'} value={this.state.formData.deliveredOrdersSum/10+' تومان'}/>
                </ManagePageFieldsContainer>
                <MDBContainer className={'bigOperations'}>
                    <MDBRow>
                        <MDBCol>
                            <div>
                                <Link to={'/ecommerce/order/all/' + this.state.formData.user.id+'/0'}>
                                   <span className={'fa fa-shopping-cart'}/>
                                    سفارشات
                                </Link>
                            </div>

                            <div>
                                <Link to={'/finance/transaction/user/' + this.state.formData.user.id}>
                                    <span className={'fa fa-dollar'}/>
                                    گردش حساب
                                </Link>
                            </div>
                            <div>
                                <Link to={'/location/place/' + this.state.formData.user.id}>
                                    <span className={'fa fa-map-marker'}/>
                                    آدرس ها
                                </Link>
                            </div>

                            <div>
                                <Link to={'/messaging/message/' + this.state.formData.user.id}>
                                    <span className={'fa fa-commenting'}/>
                                    پیام ها
                                </Link>
                            </div>

                            {
                                !this.isDelivery(this.state.formData.roles) &&
                                <div>
                                    <a href={'#'} onClick={() => {
                                        this.state.formData.setDelivery(this.getID(), (res) => {
                                            SweetAlert.displaySuccessAlert('پیام', res.message);
                                            this.loadData();
                                        })
                                    }}>
                                        <span className={'fa fa-car'}/>
                                        تبدیل به پیک
                                    </a>
                                </div>
                            }

                            {
                                this.isDelivery(this.state.formData.roles) &&
                                <div>
                                    <a href={'#'} onClick={() => {
                                        this.state.formData.retractDelivery(this.getID(), (res) => {
                                            SweetAlert.displaySuccessAlert('پیام', res.message);
                                            this.loadData();
                                        })
                                    }}>
                                        <span className={'fa fa-users'}/>
                                        تبدیل به کاربر عادی
                                    </a>
                                </div>
                            }

                        </MDBCol>
                    </MDBRow>
                </MDBContainer>
                <EcommerceDiscountcodeList ownerID={this.getID()}/>
                <SweetButton value={'برگشت'} onButtonPress={()=>{this.returnBack()}}/>
            </div>
            }
        </ManagePageContainer>
    }
}
