// @flow
import * as React from 'react';

import {PickerBox, TextBox,} from "Libs/one/sweet-react-components";
import {SearchBoxContainer,SweetSearchBox} from "Libs/one/sweet-one-react";
import DiscountcodeEntity from "../../entity/DiscountcodeEntity";
import EcommerceuserEntity from '../../entity/EcommerceuserEntity';
export default class EcommerceEcommerceuserSearch extends SweetSearchBox {
    moduleName='ecommerce';
    tableName='ecommerceuser';
    constructor(props) {
        super(props);
        this.state = {
            searchParams:{},
            formData:new EcommerceuserEntity(),
        };
    }
    componentDidMount() {
        super.componentDidMount();
    }
    render(){
        return <SearchBoxContainer title={'جستجوی کاربران'} onConfirm={this.getOnConfirm()}>
            <TextBox
                title={'نام'}
                onChangeText={this.getOnValueChangeListener('name')}
                value={this.state.searchParams.name}
            />
            <TextBox
                title={'تلفن'}
                keyboardType={'numeric'}
                onChangeText={this.getOnValueChangeListener('phone')}
                value={this.state.searchParams.phone}
            />

        </SearchBoxContainer>
    }
}
