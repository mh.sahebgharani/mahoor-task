// @flow
import {SweetEntity,AccessManager} from 'Libs/one/sweet-one-react';

export default class RegionsendtimetariffEntity extends SweetEntity {
    __moduleName='ecommerce';
    __tableName='regionsendtimetariff';

    sendTime={};
    region={};
    minFreeSendPrice='';
    pricefordistance=0;
    priceStart='';
    fields={id:{field:'id'},sendTime:{field:'sendtime_fid',type:'fid'},region:{field:'location_region_fid',type:'fid'},minFreeSendPrice:{field:'minfreesendprice'},pricefordistance:{field:'pricefordistance_prc'},priceStart:{field:'pricestart_prc'}};
    constructor(ownerID){
        super();
        this.sendTime={id:ownerID};
    }
    get(id,afterFetch){
        this._get(this.getBaseURL()+'/'+id,this.getServiceName(),AccessManager.VIEW,afterFetch);
    }
    getAll(pageSize,page,sorted,filtered,afterFetch,onError){
        const filterAndSortString=this.getFilterString(pageSize,page,sorted,filtered);
        const url=this.getBaseURL()+filterAndSortString;
        this._getAll(url,this.getServiceName(),AccessManager.LIST,afterFetch,onError);
    }
    save(id,afterFetchListener,onError){
        const formData=this.getFormRequest();
        this._save(this.getServiceName(),this.getBaseURL(),id,formData,afterFetchListener,onError);
    }
    delete(id,afterFetchListener,onError){
        this._delete(this.getServiceName(),this.getBaseURL()+"/"+id,afterFetchListener,onError);
    }
}
