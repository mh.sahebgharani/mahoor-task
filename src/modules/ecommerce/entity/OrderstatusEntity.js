// @flow
import {SweetEntity,AccessManager} from 'Libs/one/sweet-one-react';

export default class OrderstatusEntity extends SweetEntity {
    __moduleName='ecommerce';
    __tableName='orderstatus';

    name='';
    displayname='';
    isFinished='';
    isSucceed='';
    isUserChangable='';
    priority='';
    fields={id:{field:'id'},name:{field:'name'},displayname:{field:'displayname'},isFinished:{field:'is_finished'},isSucceed:{field:'is_succeed'},isUserChangable:{field:'is_userchangable'},priority:{field:'priority'}};
    get(id,afterFetch){
        this._get(this.getBaseURL()+'/'+id,this.getServiceName(),AccessManager.VIEW,afterFetch);
    }
    getAll(pageSize,page,sorted,filtered,afterFetch,onError){
        const filterAndSortString=this.getFilterString(pageSize,page,sorted,filtered);
        const url=this.getBaseURL()+filterAndSortString;
        this._getAll(url,this.getServiceName(),AccessManager.LIST,afterFetch,onError);
    }
    save(id,afterFetchListener,onError){
        const formData=this.getFormRequest();
        this._save(this.getServiceName(),this.getBaseURL(),id,formData,afterFetchListener,onError);
    }
    delete(id,afterFetchListener,onError){
        this._delete(this.getServiceName(),this.getBaseURL()+"/"+id,afterFetchListener,onError);
    }
}
