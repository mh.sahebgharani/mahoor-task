// @flow
import {AccessManager} from 'Libs/one/sweet-one-react';
import {Ecommerce} from 'Libs/one/sweet-one-app-react';
export default class OrderEntity extends Ecommerce.Entity.OrderEntity {
    getAll(pageSize,page,sorted,filtered,afterFetch,onError){
        const filterAndSortString=this.getFilterString(pageSize,page,sorted,filtered);
        const url=this.getBaseURL()+filterAndSortString;
        this._getAll(url,this.getServiceName(),AccessManager.LIST,afterFetch,onError);
    }
    save(id,afterFetchListener,onError){
        const formData=this.getFormRequest();
        this._save(this.getServiceName(),this.getBaseURL(),id,formData,afterFetchListener,onError);
    }
}
