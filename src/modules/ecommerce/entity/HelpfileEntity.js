// @flow
import {SweetEntity,AccessManager,SweetFetcher} from 'Libs/one/sweet-one-react';

export default class HelpfileEntity extends SweetEntity {
    __moduleName='ecommerce';
    __tableName='helpfile';
    uploadFile(file,afterFetchListener,onError){
        const data = new FormData();
        data.append('file', file);
        let method=SweetFetcher.METHOD_PUT;
        let action=AccessManager.EDIT;
        new SweetFetcher().Fetch('/ecommerce/helpfile/upload',method,data,
            res => {
                afterFetchListener(res);
            },onError,
            'helpfile',action,
            null);

    }
}
