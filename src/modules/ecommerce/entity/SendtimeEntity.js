// @flow
import {SweetEntity,AccessManager} from 'Libs/one/sweet-one-react';
import RegionsendtimetariffEntity from "./RegionsendtimetariffEntity";

export default class SendtimeEntity extends SweetEntity {
    __moduleName='ecommerce';
    __tableName='sendtime';

    displayName='';
    startTime='';
    endTime='';
    description='';
    priority='';
    sendPrice='';
    minFreeSendPrice='';
    isEnabled='';
    canSendBeforeTime='';
    canSendAfterTime='';
    beforeTimeMessage='';
    afterTimeMessage='';
    displayCautionMessage='';
    cautionMessage='';
    logo={};
    backgroundColor='';
    textColor='';
    descriptionColor='';
    tariff={};
    fields={
        id:{field:'id'},
        displayName:{field:'displayname'},
        startTime:{field:'starttime'},
        endTime:{field:'endtime'},
        description:{field:'description'},
        priority:{field:'priority'},
        sendPrice:{field:'sendprice'},
        minFreeSendPrice:{field:'minfreesendprice'},
        isEnabled:{field:'is_enabled'},
        canSendBeforeTime:{field:'cansendbeforetime'},
        canSendAfterTime:{field:'cansendaftertime'},
        afterTimeMessage:{field:'aftertimemessage'},
        beforeTimeMessage:{field:'beforetimemessage'},
        displayCautionMessage:{field:'displaycautionmessage'},
        cautionMessage:{field:'cautionmessage'},
        logo:{field:'logo_igu',type:'file'},
        backgroundColor:{field:'backgroundcolor'},
        textColor:{field:'textcolor'},
        descriptionColor:{field:'descriptioncolor'},
        tariff:{field:'tariff',type:'entity',object:new RegionsendtimetariffEntity(-1)}};
    get(id,afterFetch){
        this._get(this.getBaseURL()+'/'+id,this.getServiceName(),AccessManager.VIEW,afterFetch);
    }
    getAll(pageSize,page,sorted,filtered,afterFetch,onError){
        const filterAndSortString=this.getFilterString(pageSize,page,sorted,filtered);
        const url=this.getBaseURL()+filterAndSortString;
        this._getAll(url,this.getServiceName(),AccessManager.LIST,afterFetch,onError);
    }
    save(id,afterFetchListener,onError){
        const formData=this.getFormRequest();
        this._save(this.getServiceName(),this.getBaseURL(),id,formData,afterFetchListener,onError);
    }
    delete(id,afterFetchListener,onError){
        this._delete(this.getServiceName(),this.getBaseURL()+"/"+id,afterFetchListener,onError);
    }
}
