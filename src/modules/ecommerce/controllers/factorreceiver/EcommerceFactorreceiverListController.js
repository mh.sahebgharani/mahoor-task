// @flow
import * as React from 'react';
import Constants from '../../../../classes/Constants';
import FactorreceiverEntity from "../../entity/FactorreceiverEntity";
import {SweetManageListPage} from "Libs/one/sweet-one-react";

export default class EcommerceFactorreceiverListController extends SweetManageListPage {
    moduleName='ecommerce';
    tableName='factorreceiver';
    entity = new FactorreceiverEntity();
    componentDidMount() {
        super.componentDidMount();
    }
}
