// @flow
import * as React from 'react';
import {TextViewBox,ImageModal} from "Libs/one/sweet-react-components";
import {ManagePageContainer,ManagePageFieldsContainer,SweetManagePage} from "Libs/one/sweet-one-react";
import Constants from "../../../../classes/Constants";
import FactorreceiverEntity from "../../entity/FactorreceiverEntity";


export default class EcommerceFactorreceiverView extends SweetManagePage {
    moduleName='ecommerce';
    tableName='factorreceiver';
    constructor(props) {
        super(props);
        this.state = {
            formData:new FactorreceiverEntity(),

        };
    }
    
    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

    }
}
