// @flow
import * as React from 'react';
import {SweetManagePage} from "Libs/one/sweet-one-react";
import FactorreceiverEntity from "../../entity/FactorreceiverEntity";


export default class EcommerceFactorreceiverManageController extends SweetManagePage {
    moduleName='ecommerce';
    tableName='factorreceiver';
    constructor(props) {
        super(props);
        this.state = {
            canEdit:this.canEdit(),
            formData:new FactorreceiverEntity(),

        };
    }
    
    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

        this.loadSimpleFields();

    }
}
