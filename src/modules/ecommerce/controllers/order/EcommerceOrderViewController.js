// @flow
import * as React from 'react';
import {SweetManagePage} from "Libs/one/sweet-one-react";
import OrderEntity from "../../entity/OrderEntity";
import {Link} from "react-router-dom";
import EcommerceuserEntity from "../../entity/EcommerceuserEntity";
import {SweetAlert} from "Libs/one/sweet-react-common-tools";
export default class EcommerceOrderViewController extends SweetManagePage {
    moduleName='ecommerce';
    tableName='order';
    constructor(props) {
        super(props);
        this.state = {
            formData:new OrderEntity(),
            isStatusChangeModalOpen:false,
            deliveryOptions:new EcommerceuserEntity(),
        };
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

        this.loadDeliveries();
        this.loadSimpleFields();
    }
    setOrderStatus=(order,status,onEnd)=>{
        OrderEntity.setStatus(order.id,status,()=>{
            SweetAlert.displaySuccessAlert('وضعیت سفارش با موفقیت ثبت گردید');

            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();onEnd(true);});
            },()=>{onEnd(false)});
    };
    reject=(order,onEnd)=>{
        SweetAlert.displayYesNoAlert('لغو سفارش','آیا می خواهید سفارش را لغو کنید؟'
            ,{text:'بله، سفارش لغو شود',bg:'#c02012',onPress:()=>{this.setOrderStatus(order,7,onEnd)}},{text:'خیر',onPress:()=>{onEnd(false)}},'error');
    };
    approve=(order,onEnd)=>{
        SweetAlert.displayYesNoAlert('ثبت تحویل سفارش','آیا مطمئن هستید که سفارش به مشتری تحویل داده شده است؟'
            ,{text:'بله، سفارش بسته شود.',bg:'#21cb3f',onPress:()=>{this.setOrderStatus(order,4,onEnd)}},{text:'خیر',onPress:()=>{onEnd(false)}},'error');
    };
    userID(){
        return this.props.match.params.userid||-1;
    }
    returnBack() {
        // this.props.history.push('/' + this.moduleName + '/' + this.tableName+'/'+this.userID());
        this.props.history.goBack();
    }

    loadDeliveries = () => {
        this.state.deliveryOptions.getAllDeliveries((data)=>{
            // console.log(data);
            this.setState({deliveryOptions:data});
        },null);
    };
    componentDidMount() {
        super.componentDidMount();
    }
    getIconLink(title,iconClassName,link,isLocalLink){
            return <div className={'stockfactorbutton'}>
                <div>
                    {!isLocalLink &&
                    <a href={link}>
                        <i
                            className={iconClassName + " stockfactorbuttonicon"}
                            aria-hidden="true"></i>
                    </a>
                    }
                    {isLocalLink &&
                    <Link to={link}>
                        <i
                            className={iconClassName + " stockfactorbuttonicon"}
                            aria-hidden="true"></i>
                    </Link>
                    }
                </div>
                {title !== null &&
                <span className={'stockfactortitle'}>{title}</span>
                }

            </div>;
    }
    getReceiverFactorLinks(pks,orderId){
        let receivers={};
        pks.forEach(pk=>{
            const pkReceivers=pk.factorReceivers;
            pkReceivers.forEach(pkReceiver => {
                const receiverID = pkReceiver.id;
                receivers[receiverID] = pkReceiver;
            });
        });
        const receiverIds=Object.keys(receivers);
        return receiverIds.map(receiverId=>{
            const receiver=receivers[receiverId];
            return this.getIconLink('فاکتور'+' '+receiver.displayName,
                "fa fa-print",global.SiteURL+'/storage/Ecommerce/receiverfactors/'+receiverId+'/receiverfactor'+'-'+orderId+'-'+receiverId+'.pdf'
            ,false)
        });
    }
}
