// @flow
import * as React from 'react';
import {SweetDependentManagePage} from "Libs/one/sweet-one-react";
import ProductkindEntity from "../../entity/ProductkindEntity";
import StockEntity from "../../entity/StockEntity";


export default class EcommerceProductkindManageController extends SweetDependentManagePage {
    moduleName='ecommerce';
    tableName='productkind';
    __ownerTable='product';
    constructor(props) {
        super(props);
        this.state = {
            canEdit:true,
            formData:new ProductkindEntity(this.getOwnerID()),
            lastFormData:new ProductkindEntity(this.getOwnerID()),
            loaded:false,
            stockOptions: new StockEntity(),
        };
    }

    componentDidMount() {
        super.componentDidMount();
        this.loadSimpleFields();
        this.loadStocks();

    }
    loadStocks = () => {
        this.state.stockOptions.getAll(null, null, null, null, (data) => {
            this.setState({stockOptions: data});
        }, null);
    };
    load(){
        if(this.getID()>0 && !this.state.loaded){
            this.state.formData.get(this.getID(), () => {
                this.setState({loaded:true});
                this.notifyFormDataChanged();
            });
        }
    }
    getID() {
        console.log(this.props.productkind.id);
        return this.props.productkind.id;
    }

    onAddCountPress=(id,count,onEnd)=>{
        this.state.formData.addCount(id,count,(res)=>{
            this.state.formData.get(id, () => {this.notifyFormDataChanged();});
            this.setState({isCountSetModalOpen:false});
            onEnd(true);
        },()=>{ this.setState({isCountSetModalOpen:false});onEnd(false)});
    };
    getOwnerID() {
        return this.props.ownerID;
    }
    returnBack() {
        this.props.returnBack();
    }
}
