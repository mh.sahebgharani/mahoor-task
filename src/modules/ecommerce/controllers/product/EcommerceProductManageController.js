// @flow
import * as React from 'react';
import {SweetManagePage} from "Libs/one/sweet-one-react";
import ProductEntity from "../../entity/ProductEntity";
import GroupEntity from '../../entity/GroupEntity';
import FactorreceiverEntity from "../../entity/FactorreceiverEntity";

export default class EcommerceProductManageController extends SweetManagePage {
    moduleName = 'ecommerce';
    tableName = 'product';

    constructor(props) {
        super(props);
        this.state = {
            canEdit: this.canEdit(),
            formData: new ProductEntity(),
            productKinds: [],
            groupOptions: [],
            factorReceiversOptions: [],
            activeTab:"1",
        };
    }

    loadGroups = () => {
        new GroupEntity().getAll(null, null, null, null, (data) => {
            this.setState({groupOptions: data});
        }, null);
    };
    loadFactorReceivers = () => {
        new FactorreceiverEntity().getAll(null, null, null, null, (data) => {
            this.setState({factorReceiversOptions: data});
        }, null);
    };

    componentDidMount() {
        super.componentDidMount();
        if (this.getID() > 0)
            this.state.formData.get(this.getID(), () => {
                this.notifyFormDataChanged();
            });

        this.loadGroups();
        this.loadSimpleFields();
        this.loadFactorReceivers();

    }
    onSave = (onEnd) => {
        const OnEnd=(succeed)=>{
            onEnd(succeed);
            if(succeed)
                this.returnBack();
        };
        console.log(this.state.formData);
        this.state.formData.save(this.getID(),res=>{
            if (res.Data != null) {
                OnEnd(true);
            } else
                OnEnd(false);
        },()=>{
            OnEnd(false);
        });
    };
    savePK(productID,index,OnEnd){

        if(index<this.state.productKinds.length) {
            let thePK = this.state.productKinds[index];
            thePK.product.id = productID;
            thePK.save(this.getID(), res => {
                this.savePK(productID, index + 1,OnEnd);
            }, () => {
                OnEnd(false);
            });
        }
        else
            OnEnd(true);
    }
}
