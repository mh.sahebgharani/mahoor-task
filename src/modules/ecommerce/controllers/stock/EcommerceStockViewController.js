// @flow
import * as React from 'react';
import {TextViewBox,ImageModal} from "Libs/one/sweet-react-components";
import {ManagePageContainer,ManagePageFieldsContainer,SweetManagePage} from "Libs/one/sweet-one-react";
import Constants from "../../../../classes/Constants";
import StockEntity from "../../entity/StockEntity";


export default class EcommerceStockView extends SweetManagePage {
    moduleName='ecommerce';
    tableName='stock';
    constructor(props) {
        super(props);
        this.state = {
            formData:new StockEntity(),

        };
    }

    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

    }
}
