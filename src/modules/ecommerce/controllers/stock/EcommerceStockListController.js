// @flow
import * as React from 'react';
import Constants from '../../../../classes/Constants';
import StockEntity from "../../entity/StockEntity";
import {SweetManageListPage} from "Libs/one/sweet-one-react";

export default class EcommerceStockListController extends SweetManageListPage {
    moduleName='ecommerce';
    tableName='stock';
    entity = new StockEntity();
    componentDidMount() {
        super.componentDidMount();
    }
}
