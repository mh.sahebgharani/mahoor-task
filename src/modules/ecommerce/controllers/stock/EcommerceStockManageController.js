// @flow
import * as React from 'react';
import {SweetManagePage} from "Libs/one/sweet-one-react";
import StockEntity from "../../entity/StockEntity";


export default class EcommerceStockManageController extends SweetManagePage {
    moduleName='ecommerce';
    tableName='stock';
    constructor(props) {
        super(props);
        this.state = {
            canEdit:this.canEdit(),
            formData:new StockEntity(),

        };
    }
    
    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

        this.loadSimpleFields();

    }
}
