
import EcommerceBuypriseList from '../pages/buyprise/EcommerceBuypriseList';
import EcommerceBuypriseManage from '../pages/buyprise/EcommerceBuypriseManage';
import EcommerceBuypriseView from '../pages/buyprise/EcommerceBuypriseView';
let EcommerceBuypriseRoutes=[];
EcommerceBuypriseRoutes.push({ path: '/ecommerce/buyprise',exact:true, name: 'لیست امتیازات خرید',component:EcommerceBuypriseList});
EcommerceBuypriseRoutes.push({ path: '/ecommerce/buyprise/management/:id',exact:false, name: 'ویرایش امتیاز خرید',component:EcommerceBuypriseManage});
EcommerceBuypriseRoutes.push({ path: '/ecommerce/buyprise/management',exact:false, name: 'تعریف امتیاز خرید',component:EcommerceBuypriseManage});
EcommerceBuypriseRoutes.push({ path: '/ecommerce/buyprise/view/:id',exact:false, name: 'امتیاز خرید',component:EcommerceBuypriseView});
export default EcommerceBuypriseRoutes;
