
import EcommerceGroupList from '../pages/group/EcommerceGroupList';
import EcommerceGroupManage from '../pages/group/EcommerceGroupManage';
let EcommerceGroupRoutes=[];
EcommerceGroupRoutes.push({ path: '/ecommerce/group',exact:true, name: 'لیست دسته بندی ها',component:EcommerceGroupList});
EcommerceGroupRoutes.push({ path: '/ecommerce/group/management/:id',exact:false, name: 'ویرایش دسته بندی',component:EcommerceGroupManage});
EcommerceGroupRoutes.push({ path: '/ecommerce/group/management',exact:false, name: 'تعریف دسته بندی',component:EcommerceGroupManage});
export default EcommerceGroupRoutes;
