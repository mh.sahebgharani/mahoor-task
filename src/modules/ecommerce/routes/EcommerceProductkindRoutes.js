
import EcommerceProductkindList from '../pages/productkind/EcommerceProductkindList';
import EcommerceProductkindManage from '../pages/productkind/EcommerceProductkindManage';
import EcommerceProductkindView from '../pages/productkind/EcommerceProductkindView';
import EcommerceProductkindExcelAdd from "../pages/productkind/EcommerceProductkindExcelAdd";
let EcommerceProductkindRoutes=[];
EcommerceProductkindRoutes.push({ path: '/ecommerce/productkind/management/:ownerid/:id',exact:false, name: 'ویرایش انواع مختلف کالا',component:EcommerceProductkindManage});
EcommerceProductkindRoutes.push({ path: '/ecommerce/productkind/management/:ownerid',exact:false, name: 'تعریف انواع مختلف کالا',component:EcommerceProductkindManage});EcommerceProductkindRoutes.push({ path: '/ecommerce/productkind/view/:ownerid/:id',exact:false, name: 'انواع مختلف کالا',component:EcommerceProductkindView});
EcommerceProductkindRoutes.push({ path: '/ecommerce/productkind/management/:ownerid',exact:false, name: 'تعریف انواع مختلف کالا',component:EcommerceProductkindManage});EcommerceProductkindRoutes.push({ path: '/ecommerce/productkind/updatebyexcel',exact:false, name: 'بروزرسانی موجودی محصولات',component:EcommerceProductkindExcelAdd});
export default EcommerceProductkindRoutes;
