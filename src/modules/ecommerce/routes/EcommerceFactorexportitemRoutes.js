
import EcommerceFactorexportitemView from '../pages/factorexportitem/EcommerceFactorexportitemView';
let EcommerceFactorexportitemRoutes=[];
EcommerceFactorexportitemRoutes.push({ path: '/ecommerce/factorexportitem/view/:ownerid/:id',exact:false, name: 'آیتم فاکتور خروجی',component:EcommerceFactorexportitemView});
export default EcommerceFactorexportitemRoutes;
