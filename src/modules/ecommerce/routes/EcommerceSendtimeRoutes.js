
import EcommerceSendtimeList from '../pages/sendtime/EcommerceSendtimeList';
import EcommerceSendtimeManage from '../pages/sendtime/EcommerceSendtimeManage';
let EcommerceSendtimeRoutes=[];
EcommerceSendtimeRoutes.push({ path: '/ecommerce/sendtime',exact:true, name: 'لیست بازه های زمانی ارسال',component:EcommerceSendtimeList});
EcommerceSendtimeRoutes.push({ path: '/ecommerce/sendtime/management/:id',exact:false, name: 'ویرایش بازه زمانی ارسال',component:EcommerceSendtimeManage});
EcommerceSendtimeRoutes.push({ path: '/ecommerce/sendtime/management',exact:false, name: 'تعریف بازه زمانی ارسال',component:EcommerceSendtimeManage});
export default EcommerceSendtimeRoutes;
