
import EcommerceFactorreceiverList from '../pages/factorreceiver/EcommerceFactorreceiverList';
import EcommerceFactorreceiverManage from '../pages/factorreceiver/EcommerceFactorreceiverManage';
import EcommerceFactorreceiverView from '../pages/factorreceiver/EcommerceFactorreceiverView';
let EcommerceFactorreceiverRoutes=[];
EcommerceFactorreceiverRoutes.push({ path: '/ecommerce/factorreceiver',exact:true, name: 'لیست دریافت کننده های فاکتور',component:EcommerceFactorreceiverList});
EcommerceFactorreceiverRoutes.push({ path: '/ecommerce/factorreceiver/management/:id',exact:false, name: 'ویرایش دریافت کننده فاکتور',component:EcommerceFactorreceiverManage});
EcommerceFactorreceiverRoutes.push({ path: '/ecommerce/factorreceiver/management',exact:false, name: 'تعریف دریافت کننده فاکتور',component:EcommerceFactorreceiverManage});
EcommerceFactorreceiverRoutes.push({ path: '/ecommerce/factorreceiver/view/:id',exact:false, name: 'دریافت کننده فاکتور',component:EcommerceFactorreceiverView});
export default EcommerceFactorreceiverRoutes;
