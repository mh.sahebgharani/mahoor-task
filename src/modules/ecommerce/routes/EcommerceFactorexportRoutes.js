
import EcommerceFactorexportList from '../pages/factorexport/EcommerceFactorexportList';
import EcommerceFactorexportManage from '../pages/factorexport/EcommerceFactorexportManage';
import EcommerceFactorexportView from '../pages/factorexport/EcommerceFactorexportView';
let EcommerceFactorexportRoutes=[];
EcommerceFactorexportRoutes.push({ path: '/ecommerce/factorexport',exact:true, name: 'لیست فاکتورهای خروجی',component:EcommerceFactorexportList});
EcommerceFactorexportRoutes.push({ path: '/ecommerce/factorexport/management/:id',exact:false, name: 'ویرایش فاکتور خروجی',component:EcommerceFactorexportManage});
EcommerceFactorexportRoutes.push({ path: '/ecommerce/factorexport/management',exact:false, name: 'تعریف فاکتور خروجی',component:EcommerceFactorexportManage});EcommerceFactorexportRoutes.push({ path: '/ecommerce/factorexport/view/:id',exact:false, name: 'فاکتور خروجی',component:EcommerceFactorexportView});
export default EcommerceFactorexportRoutes;
