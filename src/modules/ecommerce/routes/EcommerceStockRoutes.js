
import EcommerceStockList from '../pages/stock/EcommerceStockList';
import EcommerceStockManage from '../pages/stock/EcommerceStockManage';
import EcommerceStockView from '../pages/stock/EcommerceStockView';
let EcommerceStockRoutes=[];
EcommerceStockRoutes.push({ path: '/ecommerce/stock',exact:true, name: 'لیست انبارها',component:EcommerceStockList});
EcommerceStockRoutes.push({ path: '/ecommerce/stock/management/:id',exact:false, name: 'ویرایش انبار',component:EcommerceStockManage});
EcommerceStockRoutes.push({ path: '/ecommerce/stock/management',exact:false, name: 'تعریف انبار',component:EcommerceStockManage});
EcommerceStockRoutes.push({ path: '/ecommerce/stock/view/:id',exact:false, name: 'انبار',component:EcommerceStockView});
export default EcommerceStockRoutes;
