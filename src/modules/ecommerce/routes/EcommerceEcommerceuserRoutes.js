
import EcommerceEcommerceuserList from '../pages/ecommerceuser/EcommerceEcommerceuserList';
import EcommerceEcommerceuserManage from '../pages/ecommerceuser/EcommerceEcommerceuserManage';
import EcommerceDeliveriesList from "../pages/ecommerceuser/EcommerceDeliveriesList";
import EcommerceDeliveryManage from "../pages/ecommerceuser/EcommerceDeliveryManage";
let EcommerceEcommerceuserRoutes=[];
EcommerceEcommerceuserRoutes.push({ path: '/ecommerce/ecommerceuser',exact:true, name: 'لیست کاربران فروشگاه اینترنتی',component:EcommerceEcommerceuserList});
EcommerceEcommerceuserRoutes.push({ path: '/ecommerce/delivery',exact:true, name: 'فهرست ماموران تحویل',component:EcommerceDeliveriesList});
EcommerceEcommerceuserRoutes.push({ path: '/ecommerce/delivery/management/:id',exact:false, name: 'ویرایش کاربر فروشگاه اینترنتی',component:EcommerceDeliveryManage});
EcommerceEcommerceuserRoutes.push({ path: '/ecommerce/ecommerceuser/management/:id',exact:false, name: 'ویرایش کاربر فروشگاه اینترنتی',component:EcommerceEcommerceuserManage});
EcommerceEcommerceuserRoutes.push({ path: '/ecommerce/ecommerceuser/management',exact:false, name: 'تعریف کاربر فروشگاه اینترنتی',component:EcommerceEcommerceuserManage});
export default EcommerceEcommerceuserRoutes;
