
import EcommerceProductList from '../pages/product/EcommerceProductList';
import EcommerceProductManage from '../pages/product/EcommerceProductManage';
import EcommerceProductMonitor from "../pages/product/EcommerceProductMonitor";
let EcommerceProductRoutes=[];
EcommerceProductRoutes.push({ path: '/ecommerce/product/bygroup/:groupname',exact:true, name: 'لیست محصولات دسته بندی',component:EcommerceProductList});
EcommerceProductRoutes.push({ path: '/ecommerce/product/stockstate/:stockout',exact:true, name: 'لیست محصولات',component:EcommerceProductList});
EcommerceProductRoutes.push({ path: '/ecommerce/product',exact:true, name: 'لیست محصولات',component:EcommerceProductList});
EcommerceProductRoutes.push({ path: '/ecommerce/product/monitor',exact:true, name: 'دیدبان محصولات',component:EcommerceProductMonitor});
EcommerceProductRoutes.push({ path: '/ecommerce/product/management/:id',exact:false, name: 'ویرایش محصول',component:EcommerceProductManage});
EcommerceProductRoutes.push({ path: '/ecommerce/product/management',exact:false, name: 'تعریف محصول',component:EcommerceProductManage});
export default EcommerceProductRoutes;
