
import EcommerceRegionsendtimetariffList from '../pages/regionsendtimetariff/EcommerceRegionsendtimetariffList';
import EcommerceRegionsendtimetariffManage from '../pages/regionsendtimetariff/EcommerceRegionsendtimetariffManage';
import EcommerceRegionsendtimetariffView from '../pages/regionsendtimetariff/EcommerceRegionsendtimetariffView';
let EcommerceRegionsendtimetariffRoutes=[];
EcommerceRegionsendtimetariffRoutes.push({ path: '/ecommerce/regionsendtimetariff/management/:ownerid/:id',exact:false, name: 'ویرایش تعرفه ارسال به محدوده جغرافیایی',component:EcommerceRegionsendtimetariffManage});
EcommerceRegionsendtimetariffRoutes.push({ path: '/ecommerce/regionsendtimetariff/management/:ownerid',exact:false, name: 'تعریف تعرفه ارسال به محدوده جغرافیایی',component:EcommerceRegionsendtimetariffManage});
EcommerceRegionsendtimetariffRoutes.push({ path: '/ecommerce/regionsendtimetariff/view/:ownerid/:id',exact:false, name: 'تعرفه ارسال به محدوده جغرافیایی',component:EcommerceRegionsendtimetariffView});
export default EcommerceRegionsendtimetariffRoutes;
