
import EcommerceDiscountcodeList from '../pages/discountcode/EcommerceDiscountcodeList';
import EcommerceDiscountcodeManage from '../pages/discountcode/EcommerceDiscountcodeManage';
import EcommerceDiscountcodeView from '../pages/discountcode/EcommerceDiscountcodeView';
let EcommerceDiscountcodeRoutes=[];
EcommerceDiscountcodeRoutes.push({ path: '/ecommerce/discountcode/management/:ownerid/:id',exact:false, name: 'ویرایش کد تخفیف',component:EcommerceDiscountcodeManage});
EcommerceDiscountcodeRoutes.push({ path: '/ecommerce/discountcode/management/:ownerid',exact:false, name: 'تعریف کد تخفیف',component:EcommerceDiscountcodeManage});
EcommerceDiscountcodeRoutes.push({ path: '/ecommerce/discountcode/view/:ownerid/:id',exact:false, name: 'کد تخفیف',component:EcommerceDiscountcodeView});
EcommerceDiscountcodeRoutes.push({ path: '/ecommerce/discountcode',exact:true, name: 'فهرست کدهای تخفیف',component:EcommerceDiscountcodeList});
export default EcommerceDiscountcodeRoutes;
