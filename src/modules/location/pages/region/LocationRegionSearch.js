// @flow
import * as React from 'react';
import {TextBox,PickerBox} from "Libs/one/sweet-react-components";
import {SearchBoxContainer,SweetSearchBox} from "Libs/one/sweet-one-react";
import RegionEntity from "../../entity/RegionEntity";
import AreaEntity from '../../entity/AreaEntity';
export default class LocationRegionSearch extends SweetSearchBox {
    moduleName='location';
    tableName='region';
    constructor(props) {
        super(props);
        this.state = {
            searchParams:{},
            formData:new RegionEntity(),

			areaOptions:new AreaEntity(),
        };
    }

    loadAreas = () => {
        this.state.areaOptions.getAll(null,null,null,null,(data)=>{
                this.setState({areaOptions:data});
            },null);
    };

    componentDidMount() {
        super.componentDidMount();

        this.loadAreas();
        this.loadSimpleFields();

    }
    render(){
        return <SearchBoxContainer title={'جستجوی محدوده ها'} onConfirm={this.getOnConfirm()}>
            <PickerBox
                title={'منطقه شهری'}
                selectedValue={this.state.searchParams.area_fid}
                onValueChange={this.getOnFidValueChangeListener('area_fid')}
                options={this.state.areaOptions}
            />

         <TextBox title={'نام فارسی'} id='displayName'
                value={this.state.searchParams.displayname}
                onChangeText={this.getOnTextChangeListener('displayname')}/>
            <PickerBox
                title={'فعال'}
                selectedValue={this.state.searchParams.is_enabled}
                onValueChange={this.getOnFidValueChangeListener('is_enabled')}
                options={[{id:0,name:'خیر'},{id:1,name:'بله'}]}
            />

        </SearchBoxContainer>
    }
}
