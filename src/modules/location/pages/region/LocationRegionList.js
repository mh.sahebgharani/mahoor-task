// @flow
import * as React from 'react';
import Constants from '../../../../classes/Constants';
import RegionEntity from "../../entity/RegionEntity";
import {ListPageContainer,SweetManageListPage} from "Libs/one/sweet-one-react";
import LocationRegionSearch from './LocationRegionSearch';

export default class LocationRegionList extends SweetManageListPage {
    moduleName='location';
    tableName='region';
    entity = new RegionEntity();
    componentDidMount() {
        super.componentDidMount();
    }
    render() {
        return <ListPageContainer title={'لیست محدوده ها'}>
            <LocationRegionSearch onConfirm={this.searchData}/>
            {this.getAddButton()}
            {this.getTable()}
        </ListPageContainer>;
    }
    columns =
    [
        // {Header: 'منطقه شهری',id: 'area_fid',accessor: data => data.area.displayname},
        {Header: 'نام لاتین',id: 'name',accessor: 'name'},
        {Header: 'نام فارسی',id: 'displayname',accessor: 'displayName'},
        // {Header: 'فعال',id: 'is_enabled',accessor: data => data.isEnabled.displayname},
        {Header: 'عملیات',accessor: 'id',Cell: props => {return this.getOperationsBox(props.value)}}
    ];
}
