// @flow
import * as React from 'react';
import {TextViewBox,ImageModal} from "Libs/one/sweet-react-components";
import {ManagePageContainer,ManagePageFieldsContainer,SweetManagePage} from "Libs/one/sweet-one-react";
import Constants from "../../../../classes/Constants";
import RegionEntity from "../../entity/RegionEntity";

export default class LocationRegionView extends SweetManagePage {
    moduleName='location';
    tableName='region';
    constructor(props) {
        super(props);
        this.state = {
            formData:new RegionEntity(),

        };
    }
    
    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

    }
    render(){
        return (
            <ManagePageContainer title={'تعریف محدوده'}>
                <ManagePageFieldsContainer>
                    <TextViewBox title={'منطقه شهری'} value={this.state.formData.area.displayname}/>
                    <TextViewBox title={'نام لاتین'} value={this.state.formData.name}/>
                    <TextViewBox title={'نام فارسی'} value={this.state.formData.displayName}/>
                    <TextViewBox title={'فعال'} value={this.state.formData.isEnabled.displayname}/>

                </ManagePageFieldsContainer>
                {this.getSaveAndBackButtons()}
            </ManagePageContainer>
        )
    }
}
