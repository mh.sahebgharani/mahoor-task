// @flow
import * as React from 'react';
import {TextBox,PickerBox,FileSelector,ImageSelector,TimeSelectorBox} from "Libs/one/sweet-react-components";
import {ManagePageFieldsContainer,ManagePageContainer,SweetManagePage} from "Libs/one/sweet-one-react";
import RegionEntity from "../../entity/RegionEntity";
import AreaEntity from '../../entity/AreaEntity';
import Mapir from "mapir-react-component";
import PointEntity from "../../entity/PointEntity";
export default class LocationRegionManage extends SweetManagePage {
    moduleName='location';
    tableName='region';
    constructor(props) {
        super(props);
        this.state = {
            canEdit:this.canEdit(),
            formData:new RegionEntity(),
            lat: 35.687243,
            lon: 51.374946,

			areaOptions:new AreaEntity(),
			points:[]
        };
        this.reverseFunction = this.reverseFunction.bind(this);
    }

    loadAreas = () => {
        this.state.areaOptions.getAll(null,null,null,null,(data)=>{
                this.setState({areaOptions:data});
            },null);
    };
    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

        this.loadAreas();
        this.loadSimpleFields();
        this.Map = Mapir.setToken({
            transformRequest: (url)=> {
                return {
                    url: url,
                    headers: {
                        'x-api-key': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImI2MzE3NDc0NmE2MTBhNjI2NTZhNDBmN2YzYjM3MThkYzVlNjc3MGJhNzQ1ODkyZTc4OTIyNDc4Y2JlOGQ5YzZjNjZhZmQzNTcyN2Y4MmEwIn0.eyJhdWQiOiIxMTI1NSIsImp0aSI6ImI2MzE3NDc0NmE2MTBhNjI2NTZhNDBmN2YzYjM3MThkYzVlNjc3MGJhNzQ1ODkyZTc4OTIyNDc4Y2JlOGQ5YzZjNjZhZmQzNTcyN2Y4MmEwIiwiaWF0IjoxNjAzMjE1MDU3LCJuYmYiOjE2MDMyMTUwNTcsImV4cCI6MTYwNTcyMDY1Nywic3ViIjoiIiwic2NvcGVzIjpbImJhc2ljIl19.UDRC5Bui4qoXWL2hGLmOqal7-XsD0bNn3c7mqlfwvxzDgOVCQ8LGFhyX1Tle6wHNlrRj-7UQfvaaFoBarQ9_uFPifJiTWMtiAX6IArSf1X9hGR5b0v5xJ4SL5jPPuDDOMeWBzhavwnjYKmMkarGdW98scXDhzSOVsk6aED9nTCK8H8HFFj8BamBKUR70RE5gVd5MlxmSPxEFFAoqLpOOLG8akR2dVg6OCzJvA404rH_1ntpP4X6Jkc8pHnq23omc6Yy5Ak7PkguN1LHbw4WboAmpazyDS-Ta5gV5V_TT25cv3sewR-UwhYWhuD5U-nMLnuFkvkLMOvOQ9yT4Bi2bog', //Mapir api key
                        'Mapir-SDK': 'reactjs'
                    }
                }
            }
        });

    }
    reverseFunction(map, e) {
        // console.log(e);
        const array = this.state.formData.points;
        let thePoint=new PointEntity(this.getID());
        thePoint.latitude=e.lngLat.lat;
        thePoint.longitude=e.lngLat.lng;
        array.push(thePoint);
        let data=this.state.formData;
        data.points=array;
        this.setState({ formData: data, lat: e.lngLat.lat,lon: e.lngLat.lng });

    }
    Map = Mapir.setToken({
        transformRequest: (url)=> {
            return {
                url: url,
                headers: {
                    'x-api-key': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImI2MzE3NDc0NmE2MTBhNjI2NTZhNDBmN2YzYjM3MThkYzVlNjc3MGJhNzQ1ODkyZTc4OTIyNDc4Y2JlOGQ5YzZjNjZhZmQzNTcyN2Y4MmEwIn0.eyJhdWQiOiIxMTI1NSIsImp0aSI6ImI2MzE3NDc0NmE2MTBhNjI2NTZhNDBmN2YzYjM3MThkYzVlNjc3MGJhNzQ1ODkyZTc4OTIyNDc4Y2JlOGQ5YzZjNjZhZmQzNTcyN2Y4MmEwIiwiaWF0IjoxNjAzMjE1MDU3LCJuYmYiOjE2MDMyMTUwNTcsImV4cCI6MTYwNTcyMDY1Nywic3ViIjoiIiwic2NvcGVzIjpbImJhc2ljIl19.UDRC5Bui4qoXWL2hGLmOqal7-XsD0bNn3c7mqlfwvxzDgOVCQ8LGFhyX1Tle6wHNlrRj-7UQfvaaFoBarQ9_uFPifJiTWMtiAX6IArSf1X9hGR5b0v5xJ4SL5jPPuDDOMeWBzhavwnjYKmMkarGdW98scXDhzSOVsk6aED9nTCK8H8HFFj8BamBKUR70RE5gVd5MlxmSPxEFFAoqLpOOLG8akR2dVg6OCzJvA404rH_1ntpP4X6Jkc8pHnq23omc6Yy5Ak7PkguN1LHbw4WboAmpazyDS-Ta5gV5V_TT25cv3sewR-UwhYWhuD5U-nMLnuFkvkLMOvOQ9yT4Bi2bog', //Mapir api key
                    'Mapir-SDK': 'reactjs'
                }
            }
        }
    });
    render(){


        const dataPoints=this.state.formData.points;
        const dataPointsCount=dataPoints.length;
        const pointMarkers=dataPoints.map((p,index)=>{

           return <Mapir.Marker
               onClick={()=>{
                   // console.log(p);
                   // console.log(index);
                   let data=this.state.formData;
                   const array =data.points;
                   // console.log(data.points);
                   array.splice(index,1);
                   data.points=array;
                   // console.log(data.points);
                   this.setState({ formData: data });

               }
               }
               coordinates={[p.longitude*1, p.latitude*1]}
               anchor="bottom">
           </Mapir.Marker>;
        });
        const points=dataPoints.map(p=>{
            return [p.longitude*1, p.latitude*1];
        });
        let centerPoint=[51.40294, 35.776266];
        let avgLong=0;
        let avgLat=0;
        for(let i=0;i<dataPointsCount;i++){
            avgLat+=dataPoints[i].latitude;
            avgLong+=dataPoints[i].longitude;
        }
        if(avgLat!==0 || avgLong!==0){
            centerPoint=[avgLong/dataPointsCount, avgLat/dataPointsCount];
        }
        const polygon= {
            "type": "FeatureCollection",
            "features": [
                {
                    "type": "Feature",
                    "properties": {},
                    "geometry": {
                        "type": "Polygon",
                        "coordinates": [points],
                    }
                }
            ]
        };
        let polygonPaint = {
            'fill-color': "#ff0000",
            'fill-opacity': 0.3
        };
        return (
            <ManagePageContainer title={'تعریف محدوده'}>
                <ManagePageFieldsContainer>
                    {/*<PickerBox*/}
                    {/*    title={'منطقه شهری'}*/}
                    {/*    selectedValue={this.state.formData.area.id}*/}
                    {/*    onValueChange={this.getOnFidValueChangeListener('area')}*/}
                    {/*    options={this.state.areaOptions}*/}
                    {/*/>*/}
                    <TextBox title={'نام لاتین'} id='name' value={this.state.formData.name}
                             onChangeText={this.getOnTextChangeListener('name')}/>
                    <TextBox title={'نام فارسی'} id='displayName' value={this.state.formData.displayName}
                             onChangeText={this.getOnTextChangeListener('displayName')}/>
                    {/*<PickerBox*/}
                    {/*    title={'فعال'}*/}
                    {/*    selectedValue={this.state.formData.isEnabled}*/}
                    {/*    onValueChange={this.getOnBooleanChangeListener('isEnabled')}*/}
                    {/*    options={[{id:0,name:'خیر'},{id:1,name:'بله'}]}*/}
                    {/*/>*/}

                    <Mapir
                        colweight={2}
                        Map={this.Map}
                        containerStyle={{width:'100%',height:'400px',borderRadius:'10px'}}
                        center={centerPoint}
                        onClick={this.reverseFunction}
                        userLocation
                        zoom={[11]}
                    >
                        {/*<Mapir.ZoomControl/>*/}
                        <Mapir.GeoJSONLayer
                            data={polygon}
                            fillPaint={polygonPaint}
                            linePaint={{ 'line-color': 'green', 'line-width': 5 }}
                            // fillPaint={{ 'fill-color': '#088','fill-opacity':0.8,'background-color': '#eeeeee','background-opacity': 0.6, 'line-width': 5 }}
                        />
                        {pointMarkers}
                    </Mapir>
                </ManagePageFieldsContainer>
                {this.getSaveAndBackButtons()}

            </ManagePageContainer>
        )
    }
}
