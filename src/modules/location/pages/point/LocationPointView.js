// @flow
import * as React from 'react';
import {TextViewBox,ImageModal} from "Libs/one/sweet-react-components";
import {ManagePageContainer,ManagePageFieldsContainer,SweetDependentManagePage} from "Libs/one/sweet-one-react";
import Constants from "../../../../classes/Constants";
import PointEntity from "../../entity/PointEntity";

export default class LocationPointView extends SweetDependentManagePage {
    moduleName='location';
    tableName='point';
    __ownerTable='region';
    constructor(props) {
        super(props);
        this.state = {
            formData:new PointEntity(this.getOwnerID()),

        };
    }

    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

    }
    render(){
        return <ManagePageContainer title={'تعریف نقطه جغرافیایی'}>
        <ManagePageFieldsContainer>
            <TextViewBox title={'عرض جغرافیایی'} value={this.state.formData.latitude}/>
            <TextViewBox title={'طول جغرافیایی'} value={this.state.formData.longitude}/>
            <TextViewBox title={'محدوده جغرافیایی'} value={this.state.formData.regionID.displayname}/>

        </ManagePageFieldsContainer>
        {this.getSaveAndBackButtons()}
        </ManagePageContainer>
    }
}
