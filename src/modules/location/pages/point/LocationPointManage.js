// @flow
import * as React from 'react';
import {TextBox,PickerBox,FileSelector,ImageSelector,TimeSelectorBox} from "Libs/one/sweet-react-components";
import {ManagePageFieldsContainer,ManagePageContainer,SweetDependentManagePage} from "Libs/one/sweet-one-react";
import PointEntity from "../../entity/PointEntity";


export default class LocationPointManage extends SweetDependentManagePage {
    moduleName='location';
    tableName='point';
    __ownerTable='region';
    constructor(props) {
        super(props);
        this.state = {
            canEdit:this.canEdit(),
            formData:new PointEntity(this.getOwnerID()),

        };
    }

    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

        this.loadSimpleFields();

    }
    render(){
        return <ManagePageContainer title={'تعریف نقطه جغرافیایی'}>
        <ManagePageFieldsContainer>
        <TextBox title={'عرض جغرافیایی'} id='latitude' value={this.state.formData.latitude}
         onChangeText={this.getOnTextChangeListener('latitude')} keyboardType='numeric'/>
        <TextBox title={'طول جغرافیایی'} id='longitude' value={this.state.formData.longitude}
         onChangeText={this.getOnTextChangeListener('longitude')} keyboardType='numeric'/>


        </ManagePageFieldsContainer>
        {this.getSaveAndBackButtons()}
        </ManagePageContainer>
    }
}
