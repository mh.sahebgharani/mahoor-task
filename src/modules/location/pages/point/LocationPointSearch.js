// @flow
import * as React from 'react';
import {TextBox,PickerBox} from "Libs/one/sweet-react-components";
import {SearchBoxContainer,SweetSearchBox} from "Libs/one/sweet-one-react";
import PointEntity from "../../entity/PointEntity";
import RegionEntity from '../../entity/RegionEntity';
export default class LocationPointSearch extends SweetSearchBox {
    moduleName='location';
    tableName='point';
    constructor(props) {
        super(props);
        this.state = {
            searchParams:{},
            formData:new PointEntity(),

			regionIDOptions:new RegionEntity(),
        };
    }

    loadRegionIDs = () => {
        this.state.regionIDOptions.getAll(null,null,null,null,(data)=>{
                this.setState({regionIDOptions:data});
            },null);
    };

    componentDidMount() {
        super.componentDidMount();

        this.loadRegionIDs();
        this.loadSimpleFields();

    }
    render(){
        return <SearchBoxContainer title={'جستجوی نقاط جغرافیایی'} onConfirm={this.getOnConfirm()}>
            <PickerBox
                title={'محدوده جغرافیایی'}
                selectedValue={this.state.searchParams.region_fid}
                onValueChange={this.getOnFidValueChangeListener('region_fid')}
                options={this.state.regionIDOptions}
            />

        </SearchBoxContainer>
    }
}
