// @flow
import * as React from 'react';
import Constants from '../../../../classes/Constants';
import PointEntity from "../../entity/PointEntity";
import {DependentListPageContainer,SweetManageDepenedentListPage} from "Libs/one/sweet-one-react";
import LocationPointSearch from './LocationPointSearch';

export default class LocationPointList extends SweetManageDepenedentListPage {
    moduleName='location';
    tableName='point';__defaultFilters=[{id:'region_fid',value:this.getOwnerID()}]
    entity = new PointEntity();
    componentDidMount() {
        super.componentDidMount();
    }
    render() {
        return <DependentListPageContainer title={'لیست نقاط جغرافیایی'}>
            <LocationPointSearch onConfirm={this.searchData}/>
            {this.getAddButton()}
            {this.getTable()}
        </DependentListPageContainer>;
    }
    columns =
    [
        {Header: 'عرض جغرافیایی',id: 'latitude',accessor: 'latitude'},
        {Header: 'طول جغرافیایی',id: 'longitude',accessor: 'longitude'},
        {Header: 'عملیات',accessor: 'id',Cell: props => {return this.getOperationsBox(props.value)}}
    ];
}
