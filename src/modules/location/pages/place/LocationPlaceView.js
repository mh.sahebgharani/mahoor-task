// @flow
import * as React from 'react';
import {TextViewBox} from "Libs/one/sweet-react-components";
import {SweetManagePage,ManagePageContainer,ManagePageFieldsContainer} from "Libs/one/sweet-one-react";
import PlaceEntity from "../../entity/PlaceEntity";

import {Map, Marker, TileLayer} from "react-leaflet";
import {createRef} from "react";
import 'leaflet/dist/leaflet.css';
import icon from 'leaflet/dist/images/marker-icon.png';
import iconShadow from 'leaflet/dist/images/marker-shadow.png';
import L from 'leaflet';

let DefaultIcon = L.icon({
    iconUrl: icon,
    shadowUrl: iconShadow
});

L.Marker.prototype.options.icon = DefaultIcon;
export default class LocationPlaceView extends SweetManagePage {
    moduleName='location';
    tableName='place';
    constructor(props) {
        super(props);
        this.state = {
            formData:new PlaceEntity(),

        };
    }

    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

    }
    refmarker = createRef<Marker>();
    updatePosition = () => {
        const marker = this.refmarker.current;
        if (marker != null) {
            let oldFormData=this.state.formData;
            oldFormData.latitude=marker.leafletElement.getLatLng().lat;
            oldFormData.longitude=marker.leafletElement.getLatLng().lng;
            this.setState({formData:oldFormData})
        }
    };
    render(){
        const position = [this.state.formData.latitude, this.state.formData.longitude];
        const map = (
            <Map center={position} zoom={13} >
                <TileLayer
                    attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />
                <Marker
                    draggable={false}
                    position={position}
                    ref={this.refmarker}>
                </Marker>
            </Map>
        );
        return <ManagePageContainer title={'تعریف محل'}>
        <ManagePageFieldsContainer>
            <TextViewBox title={'آدرس'} value={this.state.formData.address}/>
            <TextViewBox title={'تلفن تماس'} value={this.state.formData.tel}/>
            <TextViewBox title={'توضیحات'}  value={this.state.formData.description}/>
            <div>
                <a className={'fonticonlink'} href={'https://maps.google.com/?q='+this.state.formData.latitude+','+this.state.formData.longitude}><i
                    className="fa fa-map" aria-hidden="true"></i>
                </a>
            </div>
            <div colweight={2}>
                {map}
            </div>
        </ManagePageFieldsContainer>
        {this.getSaveAndBackButtons()}
        </ManagePageContainer>
    }
    returnBack() {
        this.props.history.push('/' + this.moduleName + '/' + this.tableName+'/'+this.userID());
    }
    userID(){
        return this.props.match.params.userid||-1;
    }
}
