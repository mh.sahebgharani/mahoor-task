// @flow
import * as React from 'react';

import {SweetManageListPage,ListPageContainer} from "Libs/one/sweet-one-react";
import PlaceEntity from "../../entity/PlaceEntity";
import {SweetButton} from "Libs/one/sweet-react-components";

export default class LocationPlaceList extends SweetManageListPage {
    moduleName='location';
    tableName='place';
    entity = new PlaceEntity();
    __defaultFilters=[{id:'user_fid',value:this.userID()}];
    componentDidMount() {
        super.componentDidMount();
        this.LoadData(this.state.pageSize,1,null,null);
    }
    userID(){
        return this.props.match.params.userid||-1;
    }
    returnBack() {
        this.props.history.goBack();
    }
    render() {
        return <ListPageContainer title={'لیست آدرس ها'}>
            {this.getTable()}
            <SweetButton value={'برگشت'} onButtonPress={(onEnd)=>{
                this.returnBack();
                onEnd(true);
            }}/>
        </ListPageContainer>;
    }

    getOperationsBox(id){
        return <div className={'operationsrow'}>
            {this.getViewButton(id)}
        </div>
    }

    getViewLink(id){
        return '/'+this.moduleName+'/'+this.tableName+'/view/'+this.userID()+'/'+id;
    }
    columns =
    [
        {Header: 'آدرس',id: 'address',accessor: 'address'},
        {Header: 'تلفن تماس',id: 'tel',accessor: 'tel'},
        {Header: 'عملیات',accessor: 'id',Cell: props => {return this.getOperationsBox(props.value)}}
    ];
}
