// @flow
import * as React from 'react';
import {TextBox} from "Libs/one/sweet-react-components";
import {ManagePageFieldsContainer,ManagePageContainer,SweetManagePage} from "Libs/one/sweet-one-react";
import PlaceEntity from "../../entity/PlaceEntity";
import {Map, Marker, TileLayer} from "react-leaflet";
import {createRef} from "react";


export default class LocationPlaceManage extends SweetManagePage {
    moduleName='location';
    tableName='place';
    constructor(props) {
        super(props);
        this.state = {
            canEdit:this.canEdit(),
            formData:new PlaceEntity(),

        };
    }

    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

        this.loadSimpleFields();

    }
    refmarker = createRef<Marker>();
    updatePosition = () => {
        const marker = this.refmarker.current;
        if (marker != null) {
            let oldFormData=this.state.formData;
            oldFormData.latitude=marker.leafletElement.getLatLng().lat;
            oldFormData.longitude=marker.leafletElement.getLatLng().lng;
            this.setState({formData:oldFormData})
        }
    };
    render(){
        const position = [this.state.formData.latitude, this.state.formData.longitude];
        const map = (
            <Map center={position} zoom={13} >
                <TileLayer
                    attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />
                <Marker
                    draggable={true}
                    onDragend={this.updatePosition}
                    position={position}
                    ref={this.refmarker}>
                </Marker>
            </Map>
        );
        return <ManagePageContainer title={'تعریف محل'}>
        <ManagePageFieldsContainer>
            <TextBox title={'تلفن تماس'} id='tel' value={this.state.formData.tel}
                     onChangeText={this.getOnTextChangeListener('tel')}/>
            <TextBox title={'نام مکان'} id='title' value={this.state.formData.title}
                     onChangeText={this.getOnTextChangeListener('title')}/>
            <TextBox colweight={2} title={'آدرس'} id='address' value={this.state.formData.address}
                onChangeText={this.getOnTextChangeListener('address')}/>
            <TextBox colweight={2}  title={'توضیحات'} id='description' value={this.state.formData.description}
                onChangeText={this.getOnTextChangeListener('description')}/>

            <div colweight={2} className={'selector-map'}>
                {map}</div>

        </ManagePageFieldsContainer>
        {this.getSaveAndBackButtons()}
        </ManagePageContainer>
    }
}
