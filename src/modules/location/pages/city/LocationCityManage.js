// @flow
import * as React from 'react';

import {TextBox} from "Libs/one/sweet-react-components";
import {ManagePageFieldsContainer,ManagePageContainer,SweetDependentManagePage} from "Libs/one/sweet-one-react";
import CityEntity from "../../entity/CityEntity";


import LocationAreaList from '../area/LocationAreaList';
export default class LocationCityManage extends SweetDependentManagePage {
    moduleName='location';
    tableName='city';
    __ownerTable='province';
    constructor(props) {
        super(props);
        this.state = {
            canEdit:this.canEdit(),
            formData:new CityEntity(this.getOwnerID()),

        };
    }

    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

        this.loadSimpleFields();

    }
    render(){
        return <ManagePageContainer title={'تعریف شهر'}>
        <ManagePageFieldsContainer>
            <TextBox title={'نام'} id='name' value={this.state.formData.name}
                onChangeText={this.getOnTextChangeListener('name')}/>
            <TextBox title={'عنوان فارسی'} id='title' value={this.state.formData.title}
                onChangeText={this.getOnTextChangeListener('title')}/>


        </ManagePageFieldsContainer>
        {this.getSaveAndBackButtons()}
        <LocationAreaList ownerID={this.getID()}/>
        </ManagePageContainer>
    }
}
