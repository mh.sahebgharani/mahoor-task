// @flow
import * as React from 'react';
import '../../../../scss/datepicker.scss';

import {TextViewBox} from "Libs/one/sweet-react-components";
import {SweetDependentManagePage,ManagePageContainer,ManagePageFieldsContainer} from "Libs/one/sweet-one-react";
import CityEntity from "../../entity/CityEntity";

export default class LocationCityView extends SweetDependentManagePage {
    moduleName='location';
    tableName='city';
    __ownerTable='province';
    constructor(props) {
        super(props);
        this.state = {
            formData:new CityEntity(this.getOwnerID()),

        };
    }

    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

    }
    render(){
        return <ManagePageContainer title={'تعریف شهر'}>
        <ManagePageFieldsContainer>
            <TextViewBox title={'عنوان فارسی'} value={this.state.formData.title}/>
            <TextViewBox title={'استان'} value={this.state.formData.province.displayname}/>

        </ManagePageFieldsContainer>
        {this.getSaveAndBackButtons()}
        </ManagePageContainer>
    }
}
