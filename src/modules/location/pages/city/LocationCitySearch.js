// @flow
import * as React from 'react';

import {TextBox} from "Libs/one/sweet-react-components";
import {SearchBoxContainer,SweetSearchBox} from "Libs/one/sweet-one-react";
import CityEntity from "../../entity/CityEntity";

export default class LocationCitySearch extends SweetSearchBox {
    moduleName='location';
    tableName='city';
    constructor(props) {
        super(props);
        this.state = {
            searchParams:{},
            formData:new CityEntity(),

        };
    }

    componentDidMount() {
        super.componentDidMount();

        this.loadSimpleFields();

    }
    render(){
        return <SearchBoxContainer title={'جستجوی شهرها'} onConfirm={this.getOnConfirm()}>
            <TextBox title={'عنوان فارسی'} id='title'
                onChangeText={this.getOnTextChangeListener('title')}/>

        </SearchBoxContainer>
    }
}
