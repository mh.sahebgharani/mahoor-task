// @flow
import * as React from 'react';
import {SweetManageDepenedentListPage,ListPageContainer} from "Libs/one/sweet-one-react";
import CityEntity from "../../entity/CityEntity";
import LocationCitySearch from './LocationCitySearch';

export default class LocationCityList extends SweetManageDepenedentListPage {
    moduleName='location';
    tableName='city';__defaultFilters=[{id:'province_fid',value:this.getOwnerID()}]
    entity = new CityEntity();
    componentDidMount() {
        super.componentDidMount();
        this.LoadData(this.state.pageSize,1,null,null);
    }
    render() {
        return <ListPageContainer title={'لیست شهرها'}>
            <LocationCitySearch onConfirm={this.searchData}/>
            {this.getAddButton()}
            {this.getTable()}
        </ListPageContainer>;
    }
    columns =
    [
        {Header: 'عنوان فارسی',id: 'title',accessor: 'title'},
        {Header: 'عملیات',accessor: 'id',Cell: props => {return this.getOperationsBox(props.value)}}
    ];
}
