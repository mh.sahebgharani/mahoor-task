// @flow
import * as React from 'react';

import {SweetManageListPage,ListPageContainer} from "Libs/one/sweet-one-react";
import ProvinceEntity from "../../entity/ProvinceEntity";
import LocationProvinceSearch from './LocationProvinceSearch';

export default class LocationProvinceList extends SweetManageListPage {
    moduleName='location';
    tableName='province';
    entity = new ProvinceEntity();
    componentDidMount() {
        super.componentDidMount();
        this.LoadData(this.state.pageSize,1,null,null);
    }
    render() {
        return <ListPageContainer title={'لیست استان ها'}>
            <LocationProvinceSearch onConfirm={this.searchData}/>
            {this.getAddButton()}
            {this.getTable()}
        </ListPageContainer>;
    }
    columns =
    [
        {Header: 'عنوان فارسی',id: 'title',accessor: 'title'},
        {Header: 'عملیات',accessor: 'id',Cell: props => {return this.getOperationsBox(props.value)}}
    ];
}
