// @flow
import * as React from 'react';
import {TextBox} from "Libs/one/sweet-react-components";
import {SearchBoxContainer,SweetSearchBox} from "Libs/one/sweet-one-react";
import ProvinceEntity from "../../entity/ProvinceEntity";

export default class LocationProvinceSearch extends SweetSearchBox {
    moduleName='location';
    tableName='province';
    constructor(props) {
        super(props);
        this.state = {
            searchParams:{},
            formData:new ProvinceEntity(),

        };
    }

    componentDidMount() {
        super.componentDidMount();

        this.loadSimpleFields();

    }
    render(){
        return <SearchBoxContainer title={'جستجوی استان ها'} onConfirm={this.getOnConfirm()}>
            <TextBox title={'عنوان فارسی'} id='title'
                onChangeText={this.getOnTextChangeListener('title')}/>

        </SearchBoxContainer>
    }
}
