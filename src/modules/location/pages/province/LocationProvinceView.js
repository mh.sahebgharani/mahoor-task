// @flow
import * as React from 'react';
import {TextViewBox} from "Libs/one/sweet-react-components";
import {SweetManagePage,ManagePageContainer,ManagePageFieldsContainer} from "Libs/one/sweet-one-react";
import ProvinceEntity from "../../entity/ProvinceEntity";

export default class LocationProvinceView extends SweetManagePage {
    moduleName='location';
    tableName='province';
    constructor(props) {
        super(props);
        this.state = {
            formData:new ProvinceEntity(),

        };
    }

    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

    }
    render(){
        return <ManagePageContainer title={'تعریف استان'}>
        <ManagePageFieldsContainer>
            <TextViewBox title={'عنوان فارسی'} value={this.state.formData.title}/>

        </ManagePageFieldsContainer>
        {this.getSaveAndBackButtons()}
        </ManagePageContainer>
    }
}
