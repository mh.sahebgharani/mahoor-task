// @flow
import * as React from 'react';

import {TextBox} from "Libs/one/sweet-react-components";
import {ManagePageFieldsContainer,ManagePageContainer,SweetManagePage} from "Libs/one/sweet-one-react";
import ProvinceEntity from "../../entity/ProvinceEntity";


import LocationCityList from '../city/LocationCityList';
export default class LocationProvinceManage extends SweetManagePage {
    moduleName='location';
    tableName='province';
    constructor(props) {
        super(props);
        this.state = {
            canEdit:this.canEdit(),
            formData:new ProvinceEntity(),

        };
    }

    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

        this.loadSimpleFields();

    }
    render(){
        return <ManagePageContainer title={'تعریف استان'}>
        <ManagePageFieldsContainer>
            <TextBox title={'نام انگلیسی'} id='name' value={this.state.formData.name}
                onChangeText={this.getOnTextChangeListener('name')}/>
            <TextBox title={'عنوان فارسی'} id='title' value={this.state.formData.title}
                onChangeText={this.getOnTextChangeListener('title')}/>


        </ManagePageFieldsContainer>
        {this.getSaveAndBackButtons()}
        <LocationCityList ownerID={this.getID()}/>
        </ManagePageContainer>
    }
}
