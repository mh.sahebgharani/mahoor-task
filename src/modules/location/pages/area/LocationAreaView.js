// @flow
import * as React from 'react';
import {TextViewBox} from "Libs/one/sweet-react-components";
import {SweetDependentManagePage,ManagePageContainer,ManagePageFieldsContainer} from "Libs/one/sweet-one-react";
import AreaEntity from "../../entity/AreaEntity";

export default class LocationAreaView extends SweetDependentManagePage {
    moduleName='location';
    tableName='area';
    __ownerTable='city';
    constructor(props) {
        super(props);
        this.state = {
            formData:new AreaEntity(this.getOwnerID()),

        };
    }

    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

    }
    render(){
        return <ManagePageContainer title={'تعریف منطقه شهری'}>
        <ManagePageFieldsContainer>
            <TextViewBox title={'نام'} value={this.state.formData.name}/>
            <TextViewBox title={'عنوان فارسی'} value={this.state.formData.title}/>
            <TextViewBox title={'شهر'} value={this.state.formData.city.displayname}/>

        </ManagePageFieldsContainer>
        {this.getSaveAndBackButtons()}
        </ManagePageContainer>
    }
}
