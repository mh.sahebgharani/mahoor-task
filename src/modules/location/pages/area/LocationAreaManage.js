// @flow
import * as React from 'react';
import {TextBox} from "Libs/one/sweet-react-components";
import {ManagePageFieldsContainer,ManagePageContainer,SweetDependentManagePage} from "Libs/one/sweet-one-react";
import AreaEntity from "../../entity/AreaEntity";


export default class LocationAreaManage extends SweetDependentManagePage {
    moduleName='location';
    tableName='area';
    __ownerTable='city';
    constructor(props) {
        super(props);
        this.state = {
            canEdit:this.canEdit(),
            formData:new AreaEntity(this.getOwnerID()),

        };
    }

    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

        this.loadSimpleFields();

    }
    render(){
        return <ManagePageContainer title={'تعریف منطقه شهری'}>
        <ManagePageFieldsContainer>
            <TextBox title={'نام'} id='name' value={this.state.formData.name}
                onChangeText={this.getOnTextChangeListener('name')}/>
            <TextBox title={'عنوان فارسی'} id='title' value={this.state.formData.title}
                onChangeText={this.getOnTextChangeListener('title')}/>


        </ManagePageFieldsContainer>
        {this.getSaveAndBackButtons()}
        </ManagePageContainer>
    }
}
