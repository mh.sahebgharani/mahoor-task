// @flow
import * as React from 'react';
import {TextBox} from "Libs/one/sweet-react-components";
import {SearchBoxContainer,SweetSearchBox} from "Libs/one/sweet-one-react";
import AreaEntity from "../../entity/AreaEntity";

export default class LocationAreaSearch extends SweetSearchBox {
    moduleName='location';
    tableName='area';
    constructor(props) {
        super(props);
        this.state = {
            searchParams:{},
            formData:new AreaEntity(),

        };
    }

    componentDidMount() {
        super.componentDidMount();

        this.loadSimpleFields();

    }
    render(){
        return <SearchBoxContainer title={'جستجوی مناطق شهری'} onConfirm={this.getOnConfirm()}>
            <TextBox title={'عنوان فارسی'} id='title'
                onChangeText={this.getOnTextChangeListener('title')}/>

        </SearchBoxContainer>
    }
}
