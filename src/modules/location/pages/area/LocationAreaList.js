// @flow
import * as React from 'react';
import {SweetManageDepenedentListPage,ListPageContainer} from "Libs/one/sweet-one-react";
import AreaEntity from "../../entity/AreaEntity";
import LocationAreaSearch from './LocationAreaSearch';

export default class LocationAreaList extends SweetManageDepenedentListPage {
    moduleName='location';
    tableName='area';__defaultFilters=[{id:'city',value:this.getOwnerID()}]
    entity = new AreaEntity();
    componentDidMount() {
        super.componentDidMount();
        this.LoadData(this.state.pageSize,1,null,null);
    }
    render() {
        return <ListPageContainer title={'لیست مناطق شهری'}>
            <LocationAreaSearch onConfirm={this.searchData}/>
            {this.getAddButton()}
            {this.getTable()}
        </ListPageContainer>;
    }
    columns =
    [
        {Header: 'عنوان فارسی',id: 'title',accessor: 'title'},
        {Header: 'عملیات',accessor: 'id',Cell: props => {return this.getOperationsBox(props.value)}}
    ];
}
