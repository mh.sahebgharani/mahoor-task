// @flow
import {AccessManager} from "Libs/one/sweet-one-react";
import {Location} from 'Libs/one/sweet-one-app-react';
export default class PlaceEntity extends Location.Entity.PlaceEntity {
    getAll(pageSize,page,sorted,filtered,afterFetch,onError){
        const filterAndSortString=this.getFilterString(pageSize,page,sorted,filtered);
        const url=this.getBaseURL()+filterAndSortString;
        this._getAll(url,this.getServiceName(),AccessManager.LIST,afterFetch,onError);
    }
}
