// @flow
import {SweetEntity,AccessManager} from 'Libs/one/sweet-one-react';
import PointEntity from "./PointEntity";

export default class RegionEntity extends SweetEntity {
    __moduleName='location';
    __tableName='region';

    area={id:1};
    name='';
    displayName='';
    color='';
    isEnabled=1;
    points=[];
    fields={id:{field:'id'},
        area:{field:'area_fid',type:'fid'}
        ,name:{field:'name'},
        displayName:{field:'displayname'},
        color:{field:'color'},
        isEnabled:{field:'is_enabled'},
        points:{field:'points',type:'entityarray',object:new PointEntity(-1)}
    };
    get(id,afterFetch){
        this._get(this.getBaseURL()+'/'+id,this.getServiceName(),AccessManager.VIEW,afterFetch);
    }
    getAll(pageSize,page,sorted,filtered,afterFetch,onError){
        const filterAndSortString=this.getFilterString(pageSize,page,sorted,filtered);
        const url=this.getBaseURL()+filterAndSortString;
        this._getAll(url,this.getServiceName(),AccessManager.LIST,afterFetch,onError);
    }
    save(id,afterFetchListener,onError){
        const formData=this.getFormRequest();
        this._save(this.getServiceName(),this.getBaseURL(),id,formData,afterFetchListener,onError);
    }
    delete(id,afterFetchListener,onError){
        this._delete(this.getServiceName(),this.getBaseURL()+"/"+id,afterFetchListener,onError);
    }
}
