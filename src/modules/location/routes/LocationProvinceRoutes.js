
import LocationProvinceList from '../pages/province/LocationProvinceList';
import LocationProvinceManage from '../pages/province/LocationProvinceManage';
import LocationProvinceView from '../pages/province/LocationProvinceView';
let LocationProvinceRoutes=[];
LocationProvinceRoutes.push({ path: '/location/province',exact:true, name: 'لیست استان ها',component:LocationProvinceList});
LocationProvinceRoutes.push({ path: '/location/province/management/:id',exact:false, name: 'ویرایش استان',component:LocationProvinceManage});
LocationProvinceRoutes.push({ path: '/location/province/management',exact:false, name: 'تعریف استان',component:LocationProvinceManage});LocationProvinceRoutes.push({ path: '/location/province/view/:id',exact:false, name: 'استان',component:LocationProvinceView});
export default LocationProvinceRoutes;
