
import LocationAreaRoutes from './LocationAreaRoutes';
import LocationCityRoutes from "./LocationCityRoutes";
import LocationPlaceRoutes from "./LocationPlaceRoutes";
import LocationProvinceRoutes from "./LocationProvinceRoutes";
import LocationPointRoutes from "./LocationPointRoutes";
import LocationRegionRoutes from "./LocationRegionRoutes";
let LocationRoutes=[];
LocationRoutes = LocationRoutes.concat(LocationAreaRoutes);
LocationRoutes = LocationRoutes.concat(LocationCityRoutes);
LocationRoutes = LocationRoutes.concat(LocationPlaceRoutes);
LocationRoutes = LocationRoutes.concat(LocationProvinceRoutes);
LocationRoutes = LocationRoutes.concat(LocationPointRoutes);
LocationRoutes = LocationRoutes.concat(LocationRegionRoutes);
export default LocationRoutes;
