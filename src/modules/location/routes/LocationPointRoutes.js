
import LocationPointList from '../pages/point/LocationPointList';
import LocationPointManage from '../pages/point/LocationPointManage';
import LocationPointView from '../pages/point/LocationPointView';
let LocationPointRoutes=[];
LocationPointRoutes.push({ path: '/location/point/management/:ownerid/:id',exact:false, name: 'ویرایش نقطه جغرافیایی',component:LocationPointManage});
LocationPointRoutes.push({ path: '/location/point/management/:ownerid',exact:false, name: 'تعریف نقطه جغرافیایی',component:LocationPointManage});
LocationPointRoutes.push({ path: '/location/point/view/:ownerid/:id',exact:false, name: 'نقطه جغرافیایی',component:LocationPointView});
export default LocationPointRoutes;
