
import LocationCityList from '../pages/city/LocationCityList';
import LocationCityManage from '../pages/city/LocationCityManage';
import LocationCityView from '../pages/city/LocationCityView';
let LocationCityRoutes=[];
LocationCityRoutes.push({ path: '/location/city/management/:ownerid/:id',exact:false, name: 'ویرایش شهر',component:LocationCityManage});
LocationCityRoutes.push({ path: '/location/city/management/:ownerid',exact:false, name: 'تعریف شهر',component:LocationCityManage});LocationCityRoutes.push({ path: '/location/city/view/:ownerid/:id',exact:false, name: 'شهر',component:LocationCityView});
export default LocationCityRoutes;
