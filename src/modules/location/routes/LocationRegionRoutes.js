
import LocationRegionList from '../pages/region/LocationRegionList';
import LocationRegionManage from '../pages/region/LocationRegionManage';
import LocationRegionView from '../pages/region/LocationRegionView';
let LocationRegionRoutes=[];
LocationRegionRoutes.push({ path: '/location/region',exact:true, name: 'لیست محدوده ها',component:LocationRegionList});
LocationRegionRoutes.push({ path: '/location/region/management/:id',exact:false, name: 'ویرایش محدوده',component:LocationRegionManage});
LocationRegionRoutes.push({ path: '/location/region/management',exact:false, name: 'تعریف محدوده',component:LocationRegionManage});
LocationRegionRoutes.push({ path: '/location/region/view/:id',exact:false, name: 'محدوده',component:LocationRegionView});
export default LocationRegionRoutes;
