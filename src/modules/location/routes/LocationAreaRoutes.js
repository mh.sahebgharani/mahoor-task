
import LocationAreaList from '../pages/area/LocationAreaList';
import LocationAreaManage from '../pages/area/LocationAreaManage';
import LocationAreaView from '../pages/area/LocationAreaView';
let LocationAreaRoutes=[];
LocationAreaRoutes.push({ path: '/location/area/management/:ownerid/:id',exact:false, name: 'ویرایش منطقه شهری',component:LocationAreaManage});
LocationAreaRoutes.push({ path: '/location/area/management/:ownerid',exact:false, name: 'تعریف منطقه شهری',component:LocationAreaManage});LocationAreaRoutes.push({ path: '/location/area/view/:ownerid/:id',exact:false, name: 'منطقه شهری',component:LocationAreaView});
export default LocationAreaRoutes;
