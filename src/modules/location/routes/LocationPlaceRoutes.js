
import LocationPlaceList from '../pages/place/LocationPlaceList';
import LocationPlaceManage from '../pages/place/LocationPlaceManage';
import LocationPlaceView from '../pages/place/LocationPlaceView';
let LocationPlaceRoutes=[];
LocationPlaceRoutes.push({ path: '/location/place/:userid',exact:true, name: 'لیست آدرس ها',component:LocationPlaceList});
LocationPlaceRoutes.push({ path: '/location/place/view/:userid/:id',exact:false, name: 'آدرس',component:LocationPlaceView});
export default LocationPlaceRoutes;
