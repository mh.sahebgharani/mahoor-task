// @flow
import * as React from 'react';
import Constants from '../../../../classes/Constants';
import TransactionEntity from "../../entity/TransactionEntity";
import {ListPageContainer,SweetManageListPage} from "Libs/one/sweet-one-react";
import FinanceTransactionSearch from './FinanceTransactionSearch';

export default class FinanceTransactionList extends SweetManageListPage {
    moduleName='finance';
    tableName='transaction';

    entity = new TransactionEntity();
    componentDidMount() {
        super.componentDidMount();
    }
    render() {
        return <ListPageContainer title={'لیست تراکنش های مالی'}>
            <FinanceTransactionSearch onConfirm={this.searchData}/>
            {this.getAddButton()}
            {this.getTable()}
        </ListPageContainer>;
    }
    columns =
    [
        {Header: 'مبلغ به ریال',id: 'amount_prc',accessor: 'amount'},
        {Header: 'کد تراکنش',id: 'transactionid',accessor: 'transactionID'},
        {Header: 'وضعیت تراکنش',id: 'status',accessor: data => data.status.displayname},
        {Header: 'توضیحات',id: 'description_te',accessor: 'description'},
        {Header: 'عملیات',accessor: 'id',Cell: props => {return this.getOperationsBox(props.value)}}
    ];
}
