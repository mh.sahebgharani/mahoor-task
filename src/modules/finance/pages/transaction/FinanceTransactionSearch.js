// @flow
import * as React from 'react';
import {TextBox,PickerBox} from "Libs/one/sweet-react-components";
import {SearchBoxContainer,SweetSearchBox} from "Libs/one/sweet-one-react";
import TransactionEntity from "../../entity/TransactionEntity";

export default class FinanceTransactionSearch extends SweetSearchBox {
    moduleName='finance';
    tableName='transaction';
    constructor(props) {
        super(props);
        this.state = {
            searchParams:{},
            formData:new TransactionEntity(),

        };
    }

    componentDidMount() {
        super.componentDidMount();

        this.loadSimpleFields();

    }
    render(){
        return <SearchBoxContainer title={'جستجوی تراکنش های مالی'} onConfirm={this.getOnConfirm()}>

         <TextBox title={'کد تراکنش'} id='transactionID'
                value={this.state.searchParams.transactionid}
                onChangeText={this.getOnTextChangeListener('transactionid')}/>

        </SearchBoxContainer>
    }
}
