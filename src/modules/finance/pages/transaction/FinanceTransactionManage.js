// @flow
import * as React from 'react';
import {TextBox,PickerBox,FileSelector,ImageSelector,TimeSelectorBox} from "Libs/one/sweet-react-components";
import {ManagePageFieldsContainer,ManagePageContainer,SweetManagePage} from "Libs/one/sweet-one-react";
import TransactionEntity from "../../entity/TransactionEntity";


export default class FinanceTransactionManage extends SweetManagePage {
    moduleName='finance';
    tableName='transaction';
    constructor(props) {
        super(props);
        const fd=new TransactionEntity();
        fd.userID={id:this.getUserID()};
        fd.status={id:38};
        fd.transactionID="0";
        this.state = {
            canEdit:this.canEdit(),
            formData:fd,

        };
    }

    getUserID(){
        return this.props.match.params.userID;
    }
    componentDidMount() {
        super.componentDidMount();

    }
    returnBack(){
        if(this.props!=null)
            this.props.history.goBack();
    }
    render(){
        return <ManagePageContainer title={'ثبت تراکنش مالی جدید'}>
        <ManagePageFieldsContainer>
        <TextBox title={'مبلغ به ریال'} id='amount' value={this.state.formData.amount}
         onChangeText={this.getOnTextChangeListener('amount')} keyboardType='numeric'/>
            <TextBox title={'توضیحات'} id='description' value={this.state.formData.description}
                onChangeText={this.getOnTextChangeListener('description')}/>


        </ManagePageFieldsContainer>
        {this.getSaveAndBackButtons()}
        </ManagePageContainer>
    }
}
