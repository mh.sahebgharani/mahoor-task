// @flow
import * as React from 'react';
import {TextViewBox,ImageModal} from "Libs/one/sweet-react-components";
import {ManagePageContainer,ManagePageFieldsContainer,SweetManagePage} from "Libs/one/sweet-one-react";
import Constants from "../../../../classes/Constants";
import TransactionEntity from "../../entity/TransactionEntity";

export default class FinanceTransactionView extends SweetManagePage {
    moduleName='finance';
    tableName='transaction';
    constructor(props) {
        super(props);
        this.state = {
            formData:new TransactionEntity(),

        };
    }
    
    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

    }
    render(){
        return <ManagePageContainer title={'تعریف تراکنش مالی'}>
        <ManagePageFieldsContainer>
            <TextViewBox title={'مبلغ به ریال'} value={this.state.formData.amount}/>
            <TextViewBox title={'کد تراکنش'} value={this.state.formData.transactionID}/>
            <TextViewBox title={'وضعیت تراکنش'} value={this.state.formData.status.displayname}/>
            <TextViewBox title={'توضیحات'} value={this.state.formData.description}/>

        </ManagePageFieldsContainer>
        {this.getSaveAndBackButtons()}
        </ManagePageContainer>
    }
}
