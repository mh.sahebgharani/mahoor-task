// @flow
import * as React from 'react';
import TransactionEntity from "../../entity/TransactionEntity";
import {ListPageContainer,SweetManageListPage} from "Libs/one/sweet-one-react";
import FinanceTransactionSearch from './FinanceTransactionSearch';
import {IoMdAddCircle} from "react-icons/all";
import {Link} from "react-router-dom";

export default class FinanceUserTransactionList extends SweetManageListPage {
    moduleName='finance';
    tableName='transaction';

    entity = new TransactionEntity();
    componentDidMount() {
        super.componentDidMount();
    }
    getData(pageSize, page, finalSort, finalFilter, afterFetch,onError){
        this.entity.getAllByUser(this.getUserID(),pageSize, page, finalSort, finalFilter, afterFetch,onError);
    }
    getUserID(){
        return this.props.match.params.userID;
    }
    render() {
        return <ListPageContainer title={'لیست تراکنش های مالی'}>
            <FinanceTransactionSearch onConfirm={this.searchData}/>
            {this.getAddButton()}
            {this.getTable()}
        </ListPageContainer>;
    }
    getAddLink(){
        return '/'+this.moduleName+'/'+this.tableName+'/management/'+this.getUserID();
    }
    getAddButton(){
        return <div className={'topoperationsrow'}>{this.canInsert() && <Link to={this.getAddLink()} className={'addlink'}><IoMdAddCircle/></Link>}</div>
    }
    columns =
    [
        {Header: 'مبلغ به ریال',id: 'amount_prc',accessor: 'amount'},
        {Header: 'کد تراکنش',id: 'transactionid',accessor: 'transactionID'},
        {Header: 'وضعیت تراکنش',id: 'status',accessor: data => data.status.displayname},
        {Header: 'توضیحات',id: 'description_te',accessor: 'description'},
    ];
}
