// @flow
import {SweetEntity,AccessManager} from 'Libs/one/sweet-one-react';

export default class TransactionEntity extends SweetEntity {
    __moduleName='finance';
    __tableName='transaction';

    amount='';
    transactionID='';
    status={};
    userID={};
    description='';
    fields={id:{field:'id'},amount:{field:'amount_prc'},transactionID:{field:'transactionid'},status:{field:'status',type:'simplefield',simplefieldname:'transactionstatus'},userID:{field:'user_fid',type:'fid'},description:{field:'description_te'}};
    get(id,afterFetch){
        this._get(this.getBaseURL()+'/'+id,this.getServiceName(),AccessManager.VIEW,afterFetch);
    }
    getAll(pageSize,page,sorted,filtered,afterFetch,onError){
        const filterAndSortString=this.getFilterString(pageSize,page,sorted,filtered);
        const url=this.getBaseURL()+filterAndSortString;
        this._getAll(url,this.getServiceName(),AccessManager.LIST,afterFetch,onError);
    }
    getAllByUser(userID,pageSize,page,sorted,filtered,afterFetch,onError){
        const filterAndSortString=this.getFilterString(pageSize,page,sorted,filtered);
        const url=this.getBaseURL()+'/usertransaction/'+userID+filterAndSortString;
        this._getAll(url,this.getServiceName(),AccessManager.LIST,afterFetch,onError);
    }
    save(id,afterFetchListener,onError){
        const formData=this.getFormRequest();
        this._save(this.getServiceName(),this.getBaseURL(),id,formData,afterFetchListener,onError);
    }
    delete(id,afterFetchListener,onError){
        this._delete(this.getServiceName(),this.getBaseURL()+"/"+id,afterFetchListener,onError);
    }
}
