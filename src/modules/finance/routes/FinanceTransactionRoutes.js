
import FinanceTransactionList from '../pages/transaction/FinanceTransactionList';
import FinanceTransactionManage from '../pages/transaction/FinanceTransactionManage';
import FinanceTransactionView from '../pages/transaction/FinanceTransactionView';
import FinanceUserTransactionList from "../pages/transaction/FinanceUserTransactionList";
let FinanceTransactionRoutes=[];
FinanceTransactionRoutes.push({ path: '/finance/transaction',exact:true, name: 'لیست تراکنش های مالی',component:FinanceTransactionList});
FinanceTransactionRoutes.push({ path: '/finance/transaction/user/:userID',exact:true, name: 'لیست تراکنش های مالی',component:FinanceUserTransactionList});
FinanceTransactionRoutes.push({ path: '/finance/transaction/management/:userID',exact:false, name: 'ثبت تراکنش مالی جدید',component:FinanceTransactionManage});
FinanceTransactionRoutes.push({ path: '/finance/transaction/view/:id',exact:false, name: 'تراکنش مالی',component:FinanceTransactionView});
export default FinanceTransactionRoutes;
