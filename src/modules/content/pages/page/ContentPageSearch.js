// @flow
import * as React from 'react';
import {TextBox,PickerBox} from "Libs/one/sweet-react-components";
import {SearchBoxContainer,SweetSearchBox} from "Libs/one/sweet-one-react";
import PageEntity from "../../entity/PageEntity";

export default class ContentPageSearch extends SweetSearchBox {
    moduleName='content';
    tableName='page';
    constructor(props) {
        super(props);
        this.state = {
            searchParams:{},
            formData:new PageEntity(),

        };
    }

    componentDidMount() {
        super.componentDidMount();

        this.loadSimpleFields();

    }
    render(){
        return <SearchBoxContainer title={'جستجوی برگه ها'} onConfirm={this.getOnConfirm()}>

         <TextBox title={'نام'} id='name'
                value={this.state.searchParams.name}
                onChangeText={this.getOnTextChangeListener('name')}/>

         <TextBox title={'عنوان صفحه'} id='title'
                value={this.state.searchParams.title}
                onChangeText={this.getOnTextChangeListener('title')}/>

         <TextBox title={'کلمات کلیدی'} id='keywords'
                value={this.state.searchParams.keywords}
                onChangeText={this.getOnTextChangeListener('keywords')}/>

        </SearchBoxContainer>
    }
}
