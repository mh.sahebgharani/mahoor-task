// @flow
import * as React from 'react';
import {ListPageContainer} from "Libs/one/sweet-one-react";
import ContentPageListController from "../../controllers/page/ContentPageListController";
import ContentPageSearch from './ContentPageSearch';

export default class ContentPageList extends ContentPageListController {
    render() {
        return <ListPageContainer title={'لیست برگه ها'}>
            <ContentPageSearch onConfirm={this.searchData}/>
            {this.getAddButton()}
            {this.getTable()}
        </ListPageContainer>;
    }
    columns =
    [
        {Header: 'نام',id: 'name',accessor: 'name'},
        {Header: 'عنوان صفحه',id: 'title',accessor: 'title'},
        {Header: 'وضعیت انتشار',id: 'is_published',accessor: data => data.isPublished.displayname},
        {Header: 'عملیات',accessor: 'id',Cell: props => {return this.getOperationsBox(props.value)}}
    ];
}
