// @flow
import * as React from 'react';
import {TextViewBox,ImageModal} from "Libs/one/sweet-react-components";
import {ManagePageContainer,ManagePageFieldsContainer,SweetManagePage} from "Libs/one/sweet-one-react";
import Constants from "../../../../classes/Constants";
import PageEntity from "../../entity/PageEntity";
import ContentPageViewController from "../../controllers/page/ContentPageViewController";


export default class ContentPageView extends ContentPageViewController {
    render(){
        return <ManagePageContainer title={'تعریف برگه'}>
        <ManagePageFieldsContainer>
            <TextViewBox title={'نام'} value={this.state.formData.name}/>
            <TextViewBox title={'عنوان صفحه'} value={this.state.formData.title}/>
            <TextViewBox title={'محتوای صفحه'} value={this.state.formData.content}/>
            <TextViewBox title={'توصیف صفحه(برای سئو)'} value={this.state.formData.description}/>
            <TextViewBox title={'کلمات کلیدی'} value={this.state.formData.keywords}/>
            <TextViewBox title={'وضعیت انتشار'} value={this.state.formData.isPublished.displayname}/>

        </ManagePageFieldsContainer>
        {this.getSaveAndBackButtons()}
        </ManagePageContainer>
    }
}
