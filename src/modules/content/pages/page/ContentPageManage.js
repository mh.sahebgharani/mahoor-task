// @flow
import * as React from 'react';
import {TextBox,PickerBox,FileSelector,ImageSelector,TimeSelectorBox} from "Libs/one/sweet-react-components";
import {ManagePageFieldsContainer,ManagePageContainer} from "Libs/one/sweet-one-react";
import ContentPageManageController from "../../controllers/page/ContentPageManageController";


export default class ContentPageManage extends ContentPageManageController {
    render(){
        return <ManagePageContainer title={'تعریف برگه'}>
        <ManagePageFieldsContainer>
            <TextBox title={'نام'} id='name' value={this.state.formData.name}
                onChangeText={this.getOnTextChangeListener('name')}/>
            <TextBox title={'عنوان صفحه'} id='title' value={this.state.formData.title}
                onChangeText={this.getOnTextChangeListener('title')}/>

            <TextBox title={'توصیف صفحه(برای سئو)'} id='description' value={this.state.formData.description}
                onChangeText={this.getOnTextChangeListener('description')}/>
            <TextBox title={'کلمات کلیدی'} id='keywords' value={this.state.formData.keywords}
                onChangeText={this.getOnTextChangeListener('keywords')}/>
            <PickerBox
                title={'وضعیت انتشار'}
                selectedValue={this.state.formData.isPublished}
                onValueChange={this.getOnBooleanChangeListener('isPublished')}
                options={[{id:0,name:'خیر'},{id:1,name:'بله'}]}
            />
            <TextBox multiline={true} colweight={2} title={'محتوای صفحه'} id='content' value={this.state.formData.content}
                     onChangeText={this.getOnTextChangeListener('content')}/>


        </ManagePageFieldsContainer>
        {this.getSaveAndBackButtons()}
        </ManagePageContainer>
    }
}
