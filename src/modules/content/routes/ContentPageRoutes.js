
import ContentPageList from '../pages/page/ContentPageList';
import ContentPageManage from '../pages/page/ContentPageManage';
import ContentPageView from '../pages/page/ContentPageView';
let ContentPageRoutes=[];
ContentPageRoutes.push({ path: '/content/page',exact:true, name: 'لیست برگه ها',component:ContentPageList});
ContentPageRoutes.push({ path: '/content/page/management/:id',exact:false, name: 'ویرایش برگه',component:ContentPageManage});
ContentPageRoutes.push({ path: '/content/page/management',exact:false, name: 'تعریف برگه',component:ContentPageManage});
ContentPageRoutes.push({ path: '/content/page/view/:id',exact:false, name: 'برگه',component:ContentPageView});
export default ContentPageRoutes;
