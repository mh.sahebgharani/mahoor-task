// @flow
import * as React from 'react';
import {SweetManagePage} from "Libs/one/sweet-one-react";
import PageEntity from "../../entity/PageEntity";


export default class ContentPageManageController extends SweetManagePage {
    moduleName='content';
    tableName='page';
    constructor(props) {
        super(props);
        this.state = {
            canEdit:this.canEdit(),
            formData:new PageEntity(),

        };
    }
    
    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

        this.loadSimpleFields();

    }
}
