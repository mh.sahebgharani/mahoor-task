// @flow
import * as React from 'react';
import {TextViewBox,ImageModal} from "Libs/one/sweet-react-components";
import {ManagePageContainer,ManagePageFieldsContainer,SweetManagePage} from "Libs/one/sweet-one-react";
import Constants from "../../../../classes/Constants";
import PageEntity from "../../entity/PageEntity";
import ContentPageViewController from "../../controllers/page/ContentPageViewController";


export default class ContentPageView extends SweetManagePage {
    moduleName='content';
    tableName='page';
    constructor(props) {
        super(props);
        this.state = {
            formData:new PageEntity(),

        };
    }

    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

    }
}
