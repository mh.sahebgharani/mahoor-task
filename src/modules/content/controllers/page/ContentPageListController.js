// @flow
import * as React from 'react';
import Constants from '../../../../classes/Constants';
import PageEntity from "../../entity/PageEntity";
import {SweetManageListPage} from "Libs/one/sweet-one-react";

export default class ContentPageListController extends SweetManageListPage {
    moduleName='content';
    tableName='page';
    entity = new PageEntity();
    componentDidMount() {
        super.componentDidMount();
    }
}
