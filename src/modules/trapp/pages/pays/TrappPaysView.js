// @flow
import * as React from 'react';
import {TextViewBox,ImageModal} from "Libs/one/sweet-react-components";
import {ManagePageContainer,ManagePageFieldsContainer,SweetManagePage} from "Libs/one/sweet-one-react";
import Constants from "../../../../classes/Constants";
import PaysEntity from "../../entity/PaysEntity";
import TrappPaysViewController from "../../controllers/pays/TrappPaysViewController";


export default class TrappPaysView extends TrappPaysViewController {
    render(){
        return <ManagePageContainer title={'تعریف پرداخت'}>
        <ManagePageFieldsContainer>
            <TextViewBox title={'نوع پرداخت'} value={this.state.formData.paymentMethod.displayname}/>
            <TextViewBox title={'کاربر'} value={this.state.formData.user.displayname}/>
            <TextViewBox title={'ویلا'} value={this.state.formData.villa.displayname}/>
            <TextViewBox title={'وضعیت'} value={this.state.formData.status.displayname}/>
            <TextViewBox title={'ایمیل'} value={this.state.formData.email}/>
            <TextViewBox title={'موبایل'} value={this.state.formData.mobile}/>
            <TextViewBox title={'نام'} value={this.state.formData.name}/>
            <TextViewBox title={'کد درگاه'} value={this.state.formData.authority}/>
            <TextViewBox title={'مبلغ'} value={this.state.formData.amount}/>

        </ManagePageFieldsContainer>
        {this.getSaveAndBackButtons()}
        </ManagePageContainer>
    }
}
