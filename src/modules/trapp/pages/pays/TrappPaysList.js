// @flow
import * as React from 'react';
import {ListPageContainer} from "Libs/one/sweet-one-react";
import TrappPaysListController from "../../controllers/pays/TrappPaysListController";
import TrappPaysSearch from './TrappPaysSearch';

export default class TrappPaysList extends TrappPaysListController {
    render() {
        return <ListPageContainer title={'لیست پرداخت ها'}>
            <TrappPaysSearch onConfirm={this.searchData}/>
            {this.getTable()}
        </ListPageContainer>;
    }
    // __defaultFilters=[{id:'status',value:'2'}];
    __defaultSorts=[{id:'id',desc:'1'}];
    canView() {
        return false;
    }
    canDelete() {
        return false;
    }
    canEdit() {
        return false;
    }

    columns =
    [
        // {Header: 'نوع پرداخت',id: 'payment_method',accessor: data => data.paymentMethod.displayname},
        {Header: 'موبایل',id: 'mobile',accessor: 'mobile'},
        {Header: 'نام',id: 'name',accessor: 'name'},
        {Header: 'کد درگاه',id: 'authority',accessor: 'authority'},
        {Header: 'مبلغ',id: 'amount',accessor: 'amount'},
        {Header: 'عملیات',accessor: 'id',Cell: props => {return this.getOperationsBox(props.value)}}
    ];
}
