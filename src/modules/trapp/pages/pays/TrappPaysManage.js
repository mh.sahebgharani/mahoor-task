// @flow
import * as React from 'react';
import {TextBox,PickerBox,FileSelector,ImageSelector,TimeSelectorBox} from "Libs/one/sweet-react-components";
import {ManagePageFieldsContainer,ManagePageContainer} from "Libs/one/sweet-one-react";
import TrappPaysManageController from "../../controllers/pays/TrappPaysManageController";


export default class TrappPaysManage extends TrappPaysManageController {
    render(){
        return <ManagePageContainer title={'تعریف پرداخت'}>
        <ManagePageFieldsContainer>


        </ManagePageFieldsContainer>
        {this.getSaveAndBackButtons()}
        </ManagePageContainer>
    }
}
