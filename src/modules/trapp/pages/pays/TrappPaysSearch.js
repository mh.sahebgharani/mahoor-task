// @flow
import * as React from 'react';
import {TextBox,PickerBox} from "Libs/one/sweet-react-components";
import {SearchBoxContainer,SweetSearchBox} from "Libs/one/sweet-one-react";
import PaysEntity from "../../entity/PaysEntity";
import UsersEntity from '../../entity/UsersEntity';import VillaEntity from '../../entity/VillaEntity';
export default class TrappPaysSearch extends SweetSearchBox {
    moduleName='trapp';
    tableName='pays';
    constructor(props) {
        super(props);
        this.state = {
            searchParams:{},
            formData:new PaysEntity(),

			userOptions:new UsersEntity(),
			villaOptions:new VillaEntity(),
        };
    }

    loadUsers = () => {
        this.state.userOptions.getAll(null,null,null,null,(data)=>{
                this.setState({userOptions:data});
            },null);
    };

    loadVillas = () => {
        this.state.villaOptions.getAll(null,null,null,null,(data)=>{
                this.setState({villaOptions:data});
            },null);
    };

    componentDidMount() {
        super.componentDidMount();

        this.loadUsers();
        this.loadVillas();
        this.loadSimpleFields();

    }
    render(){
        return <SearchBoxContainer title={'جستجوی پرداخت ها'} onConfirm={this.getOnConfirm()}>

            {/*<PickerBox*/}
            {/*    title={'نوع پرداخت'}*/}
            {/*    selectedValue={this.state.formData.searchParams.payment_method}*/}
            {/*    onValueChange={this.getOnFidValueChangeListener('payment_method')}*/}
            {/*    options={this.state.formData.simpleFields.trapp_villa_status}*/}
            {/*/>*/}

            {/*<PickerBox*/}
            {/*    title={'کاربر'}*/}
            {/*    selectedValue={this.state.searchParams.user_id}*/}
            {/*    onValueChange={this.getOnFidValueChangeListener('user_id')}*/}
            {/*    options={this.state.userOptions}*/}
            {/*/>*/}

            {/*<PickerBox*/}
            {/*    title={'ویلا'}*/}
            {/*    selectedValue={this.state.searchParams.villa_id}*/}
            {/*    onValueChange={this.getOnFidValueChangeListener('villa_id')}*/}
            {/*    options={this.state.villaOptions}*/}
            {/*/>*/}

         <TextBox title={'موبایل'} id='mobile'
                value={this.state.searchParams.mobile}
                onChangeText={this.getOnTextChangeListener('mobile')}/>

         <TextBox title={'کد رهگیری'} id='refid'
                value={this.state.searchParams.refid}
                onChangeText={this.getOnTextChangeListener('refid')}/>

         <TextBox title={'نام'} id='name'
                value={this.state.searchParams.name}
                onChangeText={this.getOnTextChangeListener('name')}/>

         <TextBox title={'کد درگاه'} id='authority'
                value={this.state.searchParams.authority}
                onChangeText={this.getOnTextChangeListener('authority')}/>

         <TextBox title={'مبلغ'} id='amount'
                value={this.state.searchParams.amount}
                onChangeText={this.getOnTextChangeListener('amount')}/>

        </SearchBoxContainer>
    }
}
