// @flow
import * as React from 'react';
import {ListPageContainer} from "Libs/one/sweet-one-react";
import TrappVillaListController from "../../controllers/villa/TrappVillaListController";
import TrappVillaSearch from './TrappVillaSearch';
import TrappVillaList from "./TrappVillaList";
import {IoMdEye} from "react-icons/all";

export default class TrappAcceptedVillaList extends TrappVillaList {
    __defaultFilters=[{id:'status',value:'2'}];
    __defaultSorts=[{id:'id',desc:'1'}];
    render() {
        return <ListPageContainer title={'لیست ویلاهای منتظر تایید'}>
            <TrappVillaSearch onConfirm={this.searchData}/>
            {this.getTable()}
        </ListPageContainer>;
    }

    canView() {
        return true;
    }

    getViewButton(id){
        return  <a className={'viewlink'} href={this.getViewLink(id)} target={"_blank"}><IoMdEye/></a>;
    }
    columns =
    [
        {Header: 'عنوان',id: 'title',accessor: 'title'},
        {Header: 'عملیات',accessor: 'id',Cell: props => {return this.getOperationsBox(props.value)}}
    ];
}
