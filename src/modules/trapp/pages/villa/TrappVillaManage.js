// @flow
import * as React from 'react';
import {TextBox,PickerBox,FileSelector,ImageSelector,TimeSelectorBox} from "Libs/one/sweet-react-components";
import {ManagePageFieldsContainer,ManagePageContainer} from "Libs/one/sweet-one-react";
import TrappVillaManageController from "../../controllers/villa/TrappVillaManageController";
import VillaEntity from '../../entity/VillaEntity';

export default class TrappVillaManage extends TrappVillaManageController {
    render(){
        // console.log(this.state.formData.status);
        return <ManagePageContainer title={'تعریف ویلا'}>
        <ManagePageFieldsContainer>
        <TextBox title={'قیمت معمولی به ازای هر شب'} id='normalCost' value={this.state.formData.normalCost}
         onChangeText={this.getOnTextChangeListener('normalCost')} keyboardType='numeric'/>
            <PickerBox
                title={'ضدعفونی شده'}
                selectedValue={this.state.formData.disinfected}
                onValueChange={this.getOnBooleanChangeListener('disinfected')}
                options={[{id:0,name:'خیر'},{id:1,name:'بله'}]}
            />
            <PickerBox
                title={'وضعیت'}
                selectedValue={this.state.formData.status.id}
                onValueChange={this.getOnFidValueChangeListener('status')}
                options={[{id:0,name:'منتظر تایید'},{id:2,name:'تایید شده'},{id:1,name:'رد شده'}]}
            />
        {/*<TextBox title={'امتیاز'} id='score' value={this.state.formData.score}*/}
        {/* onChangeText={this.getOnTextChangeListener('score')} keyboardType='numeric'/>*/}

                    {/*<ImageSelector title='تصویر سربرگ'*/}
                    {/*    onConfirm={this.getOnFilePathChanged('mainImg')}*/}
                    {/*    previewImage={this.state.formData.mainImg.url}*/}
                    {/*    onImagePreviewLoaded={this.getOnImagePreviewLoaded('mainImg')}*/}
                    {/*/>*/}
            {/*        <PickerBox*/}
            {/*    title={'شناسه کاربر'}*/}
            {/*    selectedValue={this.state.formData.userId.id}*/}
            {/*    onValueChange={this.getOnFidValueChangeListener('userId')}*/}
            {/*    options={this.state.userIdOptions}*/}
            {/*/>*/}
            <TextBox multiline={true} colweight={2} title={'آدرس'} id='address' value={this.state.formData.address}
                onChangeText={this.getOnTextChangeListener('address')}/>
            <TextBox title={'کد پستی'} id='postalCode' value={this.state.formData.postalCode}
                onChangeText={this.getOnTextChangeListener('postalCode')}/>
            <TextBox title={'روستا'} id='village' value={this.state.formData.village}
                onChangeText={this.getOnTextChangeListener('village')}/>
            <TextBox title={'شهر'} id='city' value={this.state.formData.city}
                onChangeText={this.getOnTextChangeListener('city')}/>
            <TextBox title={'منطقه'} id='state' value={this.state.formData.state}
                onChangeText={this.getOnTextChangeListener('state')}/>
            <TextBox title={'داستان'} id='story' value={this.state.formData.story}
                onChangeText={this.getOnTextChangeListener('story')}/>
        <TextBox title={'شماره تماس'} id='phoneNumber' value={this.state.formData.phoneNumber}
         onChangeText={this.getOnTextChangeListener('phoneNumber')} keyboardType='numeric'/>
            <TextBox title={'نوع'} id='type' value={this.state.formData.type}
                onChangeText={this.getOnTextChangeListener('type')}/>
            <TextBox title={'عنوان'} id='title' value={this.state.formData.title}
                onChangeText={this.getOnTextChangeListener('title')}/>


        </ManagePageFieldsContainer>
        {this.getSaveAndBackButtons()}
        </ManagePageContainer>
    }
}
