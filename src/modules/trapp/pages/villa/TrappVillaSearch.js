// @flow
import * as React from 'react';
import {TextBox,PickerBox} from "Libs/one/sweet-react-components";
import {SearchBoxContainer,SweetSearchBox} from "Libs/one/sweet-one-react";
import VillaEntity from "../../entity/VillaEntity";

export default class TrappVillaSearch extends SweetSearchBox {
    moduleName='trapp';
    tableName='villa';
    constructor(props) {
        super(props);
        this.state = {
            searchParams:{},
            formData:new VillaEntity(),

        };
    }

    componentDidMount() {
        super.componentDidMount();

        this.loadSimpleFields();

    }
    render(){
        return <SearchBoxContainer title={'جستجوی ویلاها'} onConfirm={this.getOnConfirm()}>

         {/*<TextBox title={'قیمت معمولی به ازای هر شب'} id='normalCost'*/}
         {/*       value={this.state.searchParams.normal_cost}*/}
         {/*       onChangeText={this.getOnTextChangeListener('normal_cost')}/>*/}

            {/*<PickerBox*/}
            {/*    title={'ضدعفونی شده'}*/}
            {/*    selectedValue={this.state.searchParams.disinfected}*/}
            {/*    onValueChange={this.getOnFidValueChangeListener('disinfected')}*/}
            {/*    options={[{id:0,name:'خیر'},{id:1,name:'بله'}]}*/}
            {/*/>*/}

         {/*<TextBox title={'امتیاز'} id='score'*/}
         {/*       value={this.state.searchParams.score}*/}
         {/*       onChangeText={this.getOnTextChangeListener('score')}/>*/}

         <TextBox title={'روستا'} id='village'
                value={this.state.searchParams.village}
                onChangeText={this.getOnTextChangeListener('village')}/>

         <TextBox title={'شهر'} id='city'
                value={this.state.searchParams.city}
                onChangeText={this.getOnTextChangeListener('city')}/>

         <TextBox title={'نوع'} id='type'
                value={this.state.searchParams.type}
                onChangeText={this.getOnTextChangeListener('type')}/>

        </SearchBoxContainer>
    }
}
