// @flow
import * as React from 'react';
import {ListPageContainer} from "Libs/one/sweet-one-react";
import TrappVillaListController from "../../controllers/villa/TrappVillaListController";
import TrappVillaSearch from './TrappVillaSearch';
import {FaEdit, IoMdAddCircle, IoMdEye, MdDeleteForever} from "react-icons/all";

export default class TrappVillaList extends TrappVillaListController {
    render() {
        return <ListPageContainer title={'لیست ویلاها'}>
            <TrappVillaSearch onConfirm={this.searchData}/>
            {this.getTable()}
        </ListPageContainer>;
    }

    columns =
    [
        {Header: 'عنوان',id: 'title',accessor: 'title'},
        {Header: 'عملیات',accessor: 'id',Cell: props => {return this.getOperationsBox(props.value)}}
    ];
}
