// @flow
import * as React from 'react';
import {TextViewBox,ImageModal} from "Libs/one/sweet-react-components";
import {ManagePageContainer,ManagePageFieldsContainer,SweetManagePage} from "Libs/one/sweet-one-react";
import Constants from "../../../../classes/Constants";
import VillaEntity from "../../entity/VillaEntity";
import TrappVillaViewController from "../../controllers/villa/TrappVillaViewController";


export default class TrappVillaView extends TrappVillaViewController {
    render(){
        return <ManagePageContainer title={'تعریف ویلا'}>
        <ManagePageFieldsContainer>
            <TextViewBox title={'قیمت معمولی به ازای هر شب'} value={this.state.formData.normalCost}/>
            <TextViewBox title={'ضدعفونی شده'} value={this.state.formData.disinfected.displayname}/>
            <TextViewBox title={'وضعیت'} value={this.state.formData.status.displayname}/>
            <TextViewBox title={'امتیاز'} value={this.state.formData.score}/>

                    <ImageModal title='تصویر سربرگ'
                        previewImage={this.state.formData.mainImg.url}
                    />            <TextViewBox title={'تعداد بازدیدها'} value={this.state.formData.visitCount}/>
            <TextViewBox title={'شناسه کاربر'} value={this.state.formData.userId.displayname}/>
            <TextViewBox title={'عرض جغرافیایی'} value={this.state.formData.lat}/>
            <TextViewBox title={'طول جغرافیایی'} value={this.state.formData.long}/>
            <TextViewBox title={'آدرس'} value={this.state.formData.address}/>
            <TextViewBox title={'کد پستی'} value={this.state.formData.postalCode}/>
            <TextViewBox title={'روستا'} value={this.state.formData.village}/>
            <TextViewBox title={'شهر'} value={this.state.formData.city}/>
            <TextViewBox title={'منطقه'} value={this.state.formData.state}/>
            <TextViewBox title={'داستان'} value={this.state.formData.story}/>
            <TextViewBox title={'شماره تماس'} value={this.state.formData.phoneNumber}/>
            <TextViewBox title={'نوع'} value={this.state.formData.type}/>
            <TextViewBox title={'عنوان'} value={this.state.formData.title}/>

        </ManagePageFieldsContainer>
        {this.getSaveAndBackButtons()}
        </ManagePageContainer>
    }
}
