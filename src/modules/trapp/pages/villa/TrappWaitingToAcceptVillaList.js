// @flow
import * as React from 'react';
import {ListPageContainer} from "Libs/one/sweet-one-react";
import TrappVillaListController from "../../controllers/villa/TrappVillaListController";
import TrappVillaSearch from './TrappVillaSearch';
import TrappVillaList from "./TrappVillaList";

export default class TrappWaitingToAcceptVillaList extends TrappVillaList {
    __defaultFilters=[{id:'status',value:'0'}];
    __defaultSorts=[{id:'id',desc:'1'}];
    render() {
        return <ListPageContainer title={'لیست ویلاهای منتظر تایید'}>
            <TrappVillaSearch onConfirm={this.searchData}/>
            {this.getTable()}
        </ListPageContainer>;
    }
    columns =
    [
        {Header: 'عنوان',id: 'title',accessor: 'title'},
        {Header: 'عملیات',accessor: 'id',Cell: props => {return this.getOperationsBox(props.value)}}
    ];
}
