// @flow
import * as React from 'react';
import {ListPageContainer} from "Libs/one/sweet-one-react";
import TrappVilla_reservationsListController from "../../controllers/villa_reservations/TrappVilla_reservationsListController";
import TrappVilla_reservationsSearch from './TrappVilla_reservationsSearch';

export default class TrappVilla_reservationsList extends TrappVilla_reservationsListController {
    render() {
        return <ListPageContainer title={'لیست رزروهای ویلا'}>
            <TrappVilla_reservationsSearch onConfirm={this.searchData}/>
            {this.getAddButton()}
            {this.getTable()}
        </ListPageContainer>;
    }
    columns =
    [
        {Header: 'وضعیت',id: 'status',accessor: data => data.status.displayname},
        {Header: 'تاریخ پایان',id: 'end_date',accessor: 'endDate'},
        {Header: 'تاریخ شروع',id: 'start_date',accessor: 'startDate'},
        {Header: 'مبلغ نهایی',id: 'final_cost',accessor: 'finalCost'},
        {Header: 'کاربر',id: 'user_id',accessor: data => data.user.displayname},
        {Header: 'ویلا',id: 'villa_id',accessor: data => data.villa.displayname},
        {Header: 'عملیات',accessor: 'id',Cell: props => {return this.getOperationsBox(props.value)}}
    ];
}
