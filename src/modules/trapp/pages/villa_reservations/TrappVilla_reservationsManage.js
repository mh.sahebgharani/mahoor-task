// @flow
import * as React from 'react';
import {TextBox,PickerBox,FileSelector,ImageSelector,TimeSelectorBox} from "Libs/one/sweet-react-components";
import {ManagePageFieldsContainer,ManagePageContainer} from "Libs/one/sweet-one-react";
import TrappVilla_reservationsManageController from "../../controllers/villa_reservations/TrappVilla_reservationsManageController";


export default class TrappVilla_reservationsManage extends TrappVilla_reservationsManageController {
    render(){
        return <ManagePageContainer title={'تعریف رزرو ویلا'}>
        <ManagePageFieldsContainer>


        </ManagePageFieldsContainer>
        {this.getSaveAndBackButtons()}
        </ManagePageContainer>
    }
}
