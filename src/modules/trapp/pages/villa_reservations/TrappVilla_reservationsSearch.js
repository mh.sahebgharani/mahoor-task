// @flow
import * as React from 'react';
import {TextBox,PickerBox} from "Libs/one/sweet-react-components";
import {SearchBoxContainer,SweetSearchBox} from "Libs/one/sweet-one-react";
import Villa_reservationsEntity from "../../entity/Villa_reservationsEntity";
import UsersEntity from '../../entity/UsersEntity';import VillaEntity from '../../entity/VillaEntity';
export default class TrappVilla_reservationsSearch extends SweetSearchBox {
    moduleName='trapp';
    tableName='villa_reservations';
    constructor(props) {
        super(props);
        this.state = {
            searchParams:{},
            formData:new Villa_reservationsEntity(),

			userOptions:new UsersEntity(),
			villaOptions:new VillaEntity(),
        };
    }

    loadUsers = () => {
        this.state.userOptions.getAll(null,null,null,null,(data)=>{
                this.setState({userOptions:data});
            },null);
    };

    loadVillas = () => {
        this.state.villaOptions.getAll(null,null,null,null,(data)=>{
                this.setState({villaOptions:data});
            },null);
    };

    componentDidMount() {
        super.componentDidMount();

        this.loadUsers();
        this.loadVillas();
        this.loadSimpleFields();

    }
    render(){
        return <SearchBoxContainer title={'جستجوی رزروهای ویلا'} onConfirm={this.getOnConfirm()}>

            <PickerBox
                title={'وضعیت'}
                selectedValue={this.state.formData.searchParams.status}
                onValueChange={this.getOnFidValueChangeListener('status')}
                options={this.state.formData.simpleFields.trapp_villa_status}
            />

            <PickerBox
                title={'کاربر'}
                selectedValue={this.state.searchParams.user_id}
                onValueChange={this.getOnFidValueChangeListener('user_id')}
                options={this.state.userOptions}
            />

            <PickerBox
                title={'ویلا'}
                selectedValue={this.state.searchParams.villa_id}
                onValueChange={this.getOnFidValueChangeListener('villa_id')}
                options={this.state.villaOptions}
            />

        </SearchBoxContainer>
    }
}
