// @flow
import * as React from 'react';
import {TextViewBox,ImageModal} from "Libs/one/sweet-react-components";
import {ManagePageContainer,ManagePageFieldsContainer,SweetManagePage} from "Libs/one/sweet-one-react";
import Constants from "../../../../classes/Constants";
import Villa_reservationsEntity from "../../entity/Villa_reservationsEntity";
import TrappVilla_reservationsViewController from "../../controllers/villa_reservations/TrappVilla_reservationsViewController";


export default class TrappVilla_reservationsView extends TrappVilla_reservationsViewController {
    render(){
        return <ManagePageContainer title={'تعریف رزرو ویلا'}>
        <ManagePageFieldsContainer>
            <TextViewBox title={'وضعیت'} value={this.state.formData.status.displayname}/>
            <TextViewBox title={'تاریخ پایان'} value={this.state.formData.endDate}/>
            <TextViewBox title={'تاریخ شروع'} value={this.state.formData.startDate}/>
            <TextViewBox title={'مبلغ نهایی'} value={this.state.formData.finalCost}/>
            <TextViewBox title={'تعداد مهمان'} value={this.state.formData.passengersNumber}/>
            <TextViewBox title={'تاریخ رزرو'} value={this.state.formData.reserveDate}/>
            <TextViewBox title={'کاربر'} value={this.state.formData.user.displayname}/>
            <TextViewBox title={'ویلا'} value={this.state.formData.villa.displayname}/>

        </ManagePageFieldsContainer>
        {this.getSaveAndBackButtons()}
        </ManagePageContainer>
    }
}
