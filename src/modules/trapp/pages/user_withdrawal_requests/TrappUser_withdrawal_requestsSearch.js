// @flow
import * as React from 'react';
import {TextBox,PickerBox} from "Libs/one/sweet-react-components";
import {SearchBoxContainer,SweetSearchBox} from "Libs/one/sweet-one-react";
import User_withdrawal_requestsEntity from "../../entity/User_withdrawal_requestsEntity";
import UsersEntity from '../../entity/UsersEntity';
export default class TrappUser_withdrawal_requestsSearch extends SweetSearchBox {
    moduleName='trapp';
    tableName='user_withdrawal_requests';
    constructor(props) {
        super(props);
        this.state = {
            searchParams:{},
            formData:new User_withdrawal_requestsEntity(),

			userOptions:new UsersEntity(),
        };
    }
    
    loadUsers = () => {
        this.state.userOptions.getAll(null,null,null,null,(data)=>{
                this.setState({userOptions:data});
            },null);
    };
                
    componentDidMount() {
        super.componentDidMount();

        this.loadUsers();
        this.loadSimpleFields();

    }
    render(){
        return <SearchBoxContainer title={'جستجوی درخواست های واریز وجه'} onConfirm={this.getOnConfirm()}>

         <TextBox title={'مبلغ'} id='cost'
                value={this.state.searchParams.requested_amount}
                onChangeText={this.getOnTextChangeListener('requested_amount')}/>

            <PickerBox
                title={'کاربر'}
                selectedValue={this.state.searchParams.user_id}
                onValueChange={this.getOnFidValueChangeListener('user_id')}
                options={this.state.userOptions}
            />

        </SearchBoxContainer>
    }
}
