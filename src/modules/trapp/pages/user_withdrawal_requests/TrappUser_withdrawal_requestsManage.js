// @flow
import * as React from 'react';
import {TextBox,PickerBox,FileSelector,ImageSelector,TimeSelectorBox} from "Libs/one/sweet-react-components";
import {ManagePageFieldsContainer,ManagePageContainer} from "Libs/one/sweet-one-react";
import TrappUser_withdrawal_requestsManageController from "../../controllers/user_withdrawal_requests/TrappUser_withdrawal_requestsManageController";


export default class TrappUser_withdrawal_requestsManage extends TrappUser_withdrawal_requestsManageController {
    render(){
        return <ManagePageContainer title={'تعریف درخواست واریز وجه'}>
        <ManagePageFieldsContainer>


        </ManagePageFieldsContainer>
        {this.getSaveAndBackButtons()}
        </ManagePageContainer>
    }
}
