// @flow
import * as React from 'react';
import {ListPageContainer} from "Libs/one/sweet-one-react";
import TrappUser_withdrawal_requestsListController from "../../controllers/user_withdrawal_requests/TrappUser_withdrawal_requestsListController";
import TrappUser_withdrawal_requestsSearch from './TrappUser_withdrawal_requestsSearch';

export default class TrappUser_withdrawal_requestsList extends TrappUser_withdrawal_requestsListController {
    render() {
        return <ListPageContainer title={'لیست درخواست های واریز وجه'}>
            <TrappUser_withdrawal_requestsSearch onConfirm={this.searchData}/>
            {this.getTable()}
        </ListPageContainer>;
    }
    // __defaultFilters=[{id:'status',value:'2'}];
    __defaultSorts=[{id:'id',desc:'1'}];
    canView() {
        return false;
    }
    canDelete() {
        return false;
    }
    canEdit() {
        return false;
    }
    columns =
    [
        {Header: 'مبلغ',id: 'requested_amount',accessor: 'cost'},
        {Header: 'کاربر',id: 'user_id',accessor: data => {
            return data.user.fullname
            }
            },
        {Header: 'عملیات',accessor: 'id',Cell: props => {return this.getOperationsBox(props.value)}}
    ];
}
