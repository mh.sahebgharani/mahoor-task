// @flow
import * as React from 'react';
import {TextViewBox,ImageModal} from "Libs/one/sweet-react-components";
import {ManagePageContainer,ManagePageFieldsContainer,SweetManagePage} from "Libs/one/sweet-one-react";
import Constants from "../../../../classes/Constants";
import User_withdrawal_requestsEntity from "../../entity/User_withdrawal_requestsEntity";
import TrappUser_withdrawal_requestsViewController from "../../controllers/user_withdrawal_requests/TrappUser_withdrawal_requestsViewController";


export default class TrappUser_withdrawal_requestsView extends TrappUser_withdrawal_requestsViewController {
    render(){
        return <ManagePageContainer title={'تعریف درخواست واریز وجه'}>
        <ManagePageFieldsContainer>
            <TextViewBox title={'مبلغ'} value={this.state.formData.cost}/>
            <TextViewBox title={'کاربر'} value={this.state.formData.user.displayname}/>

        </ManagePageFieldsContainer>
        {this.getSaveAndBackButtons()}
        </ManagePageContainer>
    }
}
