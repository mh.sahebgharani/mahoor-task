// @flow
import * as React from 'react';
import {TextBox,PickerBox,FileSelector,ImageSelector,TimeSelectorBox} from "Libs/one/sweet-react-components";
import {ManagePageFieldsContainer,ManagePageContainer} from "Libs/one/sweet-one-react";
import TrappUser_reservationsManageController from "../../controllers/user_reservations/TrappUser_reservationsManageController";


export default class TrappUser_reservationsManage extends TrappUser_reservationsManageController {
    render(){
        return <ManagePageContainer title={'تعریف رزرو کاربر'}>
        <ManagePageFieldsContainer>


        </ManagePageFieldsContainer>
        {this.getSaveAndBackButtons()}
        </ManagePageContainer>
    }
}
