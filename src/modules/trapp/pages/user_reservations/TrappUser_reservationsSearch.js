// @flow
import * as React from 'react';
import {TextBox,PickerBox} from "Libs/one/sweet-react-components";
import {SearchBoxContainer,SweetSearchBox} from "Libs/one/sweet-one-react";
import User_reservationsEntity from "../../entity/User_reservationsEntity";
import VillaEntity from '../../entity/VillaEntity';import UsersEntity from '../../entity/UsersEntity';
export default class TrappUser_reservationsSearch extends SweetSearchBox {
    moduleName='trapp';
    tableName='user_reservations';
    constructor(props) {
        super(props);
        this.state = {
            searchParams:{},
            formData:new User_reservationsEntity(),

			villaOptions:new VillaEntity(),
			userOptions:new UsersEntity(),
        };
    }

    loadVillas = () => {
        this.state.villaOptions.getAll(null,null,null,null,(data)=>{
                this.setState({villaOptions:data});
            },null);
    };

    loadUsers = () => {
        this.state.userOptions.getAll(null,null,null,null,(data)=>{
                this.setState({userOptions:data});
            },null);
    };

    componentDidMount() {
        super.componentDidMount();

        this.loadVillas();
        this.loadUsers();
        this.loadSimpleFields();

    }
    render(){
        return <SearchBoxContainer title={'جستجوی رزروهای کاربر'} onConfirm={this.getOnConfirm()}>

            <PickerBox
                title={'پیامک نظر سنجی'}
                selectedValue={this.state.searchParams.sendpollsms}
                onValueChange={this.getOnFidValueChangeListener('sendpollsms')}
                options={[{id:0,name:'خیر'},{id:1,name:'بله'}]}
            />

            <PickerBox
                title={'ویلا'}
                selectedValue={this.state.searchParams.villa_id}
                onValueChange={this.getOnFidValueChangeListener('villa_id')}
                options={this.state.villaOptions}
            />

            <PickerBox
                title={'وضعیت پرداخت'}
                selectedValue={this.state.formData.searchParams.pay_status}
                onValueChange={this.getOnFidValueChangeListener('pay_status')}
                options={this.state.formData.simpleFields.trapp_villa_status}
            />

         <TextBox title={'مبلغ نهایی'} id='cost'
                value={this.state.searchParams.cost}
                onChangeText={this.getOnTextChangeListener('cost')}/>

         <TextBox title={'شهر'} id='city'
                value={this.state.searchParams.city}
                onChangeText={this.getOnTextChangeListener('city')}/>

            <PickerBox
                title={'وضعیت'}
                selectedValue={this.state.formData.searchParams.state}
                onValueChange={this.getOnFidValueChangeListener('state')}
                options={this.state.formData.simpleFields.trapp_villa_status}
            />

            <PickerBox
                title={'کاربر'}
                selectedValue={this.state.searchParams.user_id}
                onValueChange={this.getOnFidValueChangeListener('user_id')}
                options={this.state.userOptions}
            />

         <TextBox title={'عنوان ویلا'} id='villaTitle'
                value={this.state.searchParams.villa_title}
                onChangeText={this.getOnTextChangeListener('villa_title')}/>

        </SearchBoxContainer>
    }
}
