// @flow
import * as React from 'react';
import {TextViewBox,ImageModal} from "Libs/one/sweet-react-components";
import {ManagePageContainer,ManagePageFieldsContainer,SweetManagePage} from "Libs/one/sweet-one-react";
import Constants from "../../../../classes/Constants";
import User_reservationsEntity from "../../entity/User_reservationsEntity";
import TrappUser_reservationsViewController from "../../controllers/user_reservations/TrappUser_reservationsViewController";


export default class TrappUser_reservationsView extends TrappUser_reservationsViewController {
    render(){
        return <ManagePageContainer title={'تعریف رزرو کاربر'}>
        <ManagePageFieldsContainer>
            <TextViewBox title={'طول اقامت'} value={this.state.formData.lengthStay}/>
            <TextViewBox title={'تعداد اضافه'} value={this.state.formData.extraPeople}/>
            <TextViewBox title={'تعداد مهمان'} value={this.state.formData.passengersNumber}/>
            <TextViewBox title={'ویلا'} value={this.state.formData.villa.displayname}/>
            <TextViewBox title={'درصد برگشت'} value={this.state.formData.returnPercentage}/>
            <TextViewBox title={'وضعیت پرداخت'} value={this.state.formData.payStatus.displayname}/>
            <TextViewBox title={'هزینه امکانات'} value={this.state.formData.facilitiesCost}/>
            <TextViewBox title={'مبلغ اضافه'} value={this.state.formData.extraCost}/>
            <TextViewBox title={'تاریخ پایان'} value={this.state.formData.exitDate}/>
            <TextViewBox title={'تاریخ شروع'} value={this.state.formData.entryDate}/>
            <TextViewBox title={'مبلغ نهایی'} value={this.state.formData.cost}/>
            <TextViewBox title={'شهر'} value={this.state.formData.city}/>
            <TextViewBox title={'وضعیت'} value={this.state.formData.state.displayname}/>
            <TextViewBox title={'کاربر'} value={this.state.formData.user.displayname}/>
            <TextViewBox title={'عنوان ویلا'} value={this.state.formData.villaTitle}/>

        </ManagePageFieldsContainer>
        {this.getSaveAndBackButtons()}
        </ManagePageContainer>
    }
}
