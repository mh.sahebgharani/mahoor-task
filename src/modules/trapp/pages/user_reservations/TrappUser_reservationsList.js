// @flow
import * as React from 'react';
import {ListPageContainer} from "Libs/one/sweet-one-react";
import TrappUser_reservationsListController from "../../controllers/user_reservations/TrappUser_reservationsListController";
import TrappUser_reservationsSearch from './TrappUser_reservationsSearch';

export default class TrappUser_reservationsList extends TrappUser_reservationsListController {
    render() {
        return <ListPageContainer title={'لیست رزروهای کاربر'}>
            <TrappUser_reservationsSearch onConfirm={this.searchData}/>
            {this.getAddButton()}
            {this.getTable()}
        </ListPageContainer>;
    }
    columns =
    [
        {Header: 'تاریخ پایان',id: 'exit_date',accessor: 'exitDate'},
        {Header: 'تاریخ شروع',id: 'entry_date',accessor: 'entryDate'},
        {Header: 'مبلغ نهایی',id: 'cost',accessor: 'cost'},
        {Header: 'شهر',id: 'city',accessor: 'city'},
        {Header: 'کاربر',id: 'user_id',accessor: data => data.user.displayname},
        {Header: 'عنوان ویلا',id: 'villa_title',accessor: 'villaTitle'},
        {Header: 'عملیات',accessor: 'id',Cell: props => {return this.getOperationsBox(props.value)}}
    ];
}
