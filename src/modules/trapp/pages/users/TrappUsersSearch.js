// @flow
import * as React from 'react';
import {TextBox,PickerBox} from "Libs/one/sweet-react-components";
import {SearchBoxContainer,SweetSearchBox} from "Libs/one/sweet-one-react";
import UsersEntity from "../../entity/UsersEntity";

export default class TrappUsersSearch extends SweetSearchBox {
    moduleName='trapp';
    tableName='users';
    constructor(props) {
        super(props);
        this.state = {
            searchParams:{},
            formData:new UsersEntity(),

        };
    }
    
    componentDidMount() {
        super.componentDidMount();

        this.loadSimpleFields();

    }
    render(){
        return <SearchBoxContainer title={'جستجوی کاربران ترپ'} onConfirm={this.getOnConfirm()}>

         <TextBox title={'شماره شبا'} id='shaba_number'
                value={this.state.searchParams.shaba_number}
                onChangeText={this.getOnTextChangeListener('shaba_number')}/>

         <TextBox title={'شماره کارت'} id='card_number'
                value={this.state.searchParams.card_number}
                onChangeText={this.getOnTextChangeListener('card_number')}/>

         <TextBox title={'زبان خارجی'} id='foreignLanguage'
                value={this.state.searchParams.foreign_language}
                onChangeText={this.getOnTextChangeListener('foreign_language')}/>

         <TextBox title={'تحصیلات'} id='education'
                value={this.state.searchParams.education}
                onChangeText={this.getOnTextChangeListener('education')}/>

         <TextBox title={'شغل'} id='job'
                value={this.state.searchParams.job}
                onChangeText={this.getOnTextChangeListener('job')}/>

         <TextBox title={'کد ملی'} id='nationalCode'
                value={this.state.searchParams.national_code}
                onChangeText={this.getOnTextChangeListener('national_code')}/>

         <TextBox title={'ایمیل'} id='email'
                value={this.state.searchParams.email}
                onChangeText={this.getOnTextChangeListener('email')}/>

         <TextBox title={'تلفن'} id='phoneNumber'
                value={this.state.searchParams.phone_number}
                onChangeText={this.getOnTextChangeListener('phone_number')}/>

         <TextBox title={'کد ترپ'} id='trappID'
                value={this.state.searchParams.trapp_id}
                onChangeText={this.getOnTextChangeListener('trapp_id')}/>

         <TextBox title={'نام'} id='fullName'
                value={this.state.searchParams.fullname}
                onChangeText={this.getOnTextChangeListener('fullname')}/>

        </SearchBoxContainer>
    }
}
