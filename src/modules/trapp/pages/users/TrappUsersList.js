// @flow
import * as React from 'react';
import {ListPageContainer} from "Libs/one/sweet-one-react";
import TrappUsersListController from "../../controllers/users/TrappUsersListController";
import TrappUsersSearch from './TrappUsersSearch';

export default class TrappUsersList extends TrappUsersListController {
    canDelete() {
        return false;
    }canEdit() {
        return false;
    }

    render() {
        return <ListPageContainer title={'لیست کاربران ترپ'}>
            <TrappUsersSearch onConfirm={this.searchData}/>
            {/*{this.getAddButton()}*/}
            {this.getTable()}
            {this.getTable()}
        </ListPageContainer>;
    }
    columns =
    [
        {Header: 'کد ملی',id: 'national_code',accessor: 'nationalCode'},
        {Header: 'تلفن',id: 'phone_number',accessor: 'phoneNumber'},
        {Header: 'کد ترپ',id: 'trapp_id',accessor: 'trappID'},
        {Header: 'نام',id: 'fullname',accessor: 'fullName'},
        {Header: 'عملیات',accessor: 'id',Cell: props => {return this.getOperationsBox(props.value)}}
    ];
}
