// @flow
import * as React from 'react';
import {TextBox,PickerBox,FileSelector,ImageSelector,TimeSelectorBox} from "Libs/one/sweet-react-components";
import {ManagePageFieldsContainer,ManagePageContainer} from "Libs/one/sweet-one-react";
import TrappUsersManageController from "../../controllers/users/TrappUsersManageController";


export default class TrappUsersManage extends TrappUsersManageController {
    render(){
        return <ManagePageContainer title={'تعریف کاربر ترپ'}>
        <ManagePageFieldsContainer>
            <TextBox title={'زبان خارجی'} id='foreignLanguage' value={this.state.formData.foreignLanguage}
                onChangeText={this.getOnTextChangeListener('foreignLanguage')}/>
            <TextBox title={'تحصیلات'} id='education' value={this.state.formData.education}
                onChangeText={this.getOnTextChangeListener('education')}/>
            <TextBox title={'شغل'} id='job' value={this.state.formData.job}
                onChangeText={this.getOnTextChangeListener('job')}/>
        <TextBox title={'کد ترپ'} id='trappID' value={this.state.formData.trappID}
         onChangeText={this.getOnTextChangeListener('trappID')} keyboardType='numeric'/>
            <TextBox title={'نام'} id='fullName' value={this.state.formData.fullName}
                onChangeText={this.getOnTextChangeListener('fullName')}/>


        </ManagePageFieldsContainer>
        {this.getSaveAndBackButtons()}
        </ManagePageContainer>
    }
}
