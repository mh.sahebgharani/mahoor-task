// @flow
import * as React from 'react';
import {TextViewBox,ImageModal} from "Libs/one/sweet-react-components";
import {ManagePageContainer,ManagePageFieldsContainer,SweetManagePage} from "Libs/one/sweet-one-react";
import Constants from "../../../../classes/Constants";
import UsersEntity from "../../entity/UsersEntity";
import TrappUsersViewController from "../../controllers/users/TrappUsersViewController";


export default class TrappUsersView extends TrappUsersViewController {
    render(){
        return <ManagePageContainer title={'تعریف کاربر ترپ'}>
        <ManagePageFieldsContainer>
            <TextViewBox title={'شماره شبا'} value={this.state.formData.shaba_number}/>
            <TextViewBox title={'شماره کارت'} value={this.state.formData.card_number}/>
            <TextViewBox title={'زبان خارجی'} value={this.state.formData.foreignLanguage}/>
            <TextViewBox title={'تحصیلات'} value={this.state.formData.education}/>
            <TextViewBox title={'شغل'} value={this.state.formData.job}/>
            <TextViewBox title={'کد ملی'} value={this.state.formData.nationalCode}/>
            <TextViewBox title={'ایمیل'} value={this.state.formData.email}/>
            <TextViewBox title={'تلفن'} value={this.state.formData.phoneNumber}/>
            <TextViewBox title={'کد ترپ'} value={this.state.formData.trappID}/>
            <TextViewBox title={'نام'} value={this.state.formData.fullName}/>

        </ManagePageFieldsContainer>
        {this.getSaveAndBackButtons()}
        </ManagePageContainer>
    }
}
