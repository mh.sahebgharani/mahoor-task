
import TrappPaysList from '../pages/pays/TrappPaysList';
import TrappPaysManage from '../pages/pays/TrappPaysManage';
import TrappPaysView from '../pages/pays/TrappPaysView';
let TrappPaysRoutes=[];
TrappPaysRoutes.push({ path: '/trapp/pays',exact:true, name: 'لیست پرداخت ها',component:TrappPaysList});
TrappPaysRoutes.push({ path: '/trapp/pays/management/:id',exact:false, name: 'ویرایش پرداخت',component:TrappPaysManage});
TrappPaysRoutes.push({ path: '/trapp/pays/management',exact:false, name: 'تعریف پرداخت',component:TrappPaysManage});
TrappPaysRoutes.push({ path: '/trapp/pays/view/:id',exact:false, name: 'پرداخت',component:TrappPaysView});
export default TrappPaysRoutes;
