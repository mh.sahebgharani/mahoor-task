
import TrappUser_withdrawal_requestsList from '../pages/user_withdrawal_requests/TrappUser_withdrawal_requestsList';
import TrappUser_withdrawal_requestsManage from '../pages/user_withdrawal_requests/TrappUser_withdrawal_requestsManage';
import TrappUser_withdrawal_requestsView from '../pages/user_withdrawal_requests/TrappUser_withdrawal_requestsView';
let TrappUser_withdrawal_requestsRoutes=[];
TrappUser_withdrawal_requestsRoutes.push({ path: '/trapp/user_withdrawal_requests',exact:true, name: 'لیست درخواست های واریز وجه',component:TrappUser_withdrawal_requestsList});
TrappUser_withdrawal_requestsRoutes.push({ path: '/trapp/user_withdrawal_requests/management/:id',exact:false, name: 'ویرایش درخواست واریز وجه',component:TrappUser_withdrawal_requestsManage});
TrappUser_withdrawal_requestsRoutes.push({ path: '/trapp/user_withdrawal_requests/management',exact:false, name: 'تعریف درخواست واریز وجه',component:TrappUser_withdrawal_requestsManage});
TrappUser_withdrawal_requestsRoutes.push({ path: '/trapp/user_withdrawal_requests/view/:id',exact:false, name: 'درخواست واریز وجه',component:TrappUser_withdrawal_requestsView});
export default TrappUser_withdrawal_requestsRoutes;
