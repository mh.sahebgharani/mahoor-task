
import TrappUser_reservationsList from '../pages/user_reservations/TrappUser_reservationsList';
import TrappUser_reservationsManage from '../pages/user_reservations/TrappUser_reservationsManage';
import TrappUser_reservationsView from '../pages/user_reservations/TrappUser_reservationsView';
let TrappUser_reservationsRoutes=[];
TrappUser_reservationsRoutes.push({ path: '/trapp/user_reservations',exact:true, name: 'لیست رزروهای کاربر',component:TrappUser_reservationsList});
TrappUser_reservationsRoutes.push({ path: '/trapp/user_reservations/management/:id',exact:false, name: 'ویرایش رزرو کاربر',component:TrappUser_reservationsManage});
TrappUser_reservationsRoutes.push({ path: '/trapp/user_reservations/management',exact:false, name: 'تعریف رزرو کاربر',component:TrappUser_reservationsManage});
TrappUser_reservationsRoutes.push({ path: '/trapp/user_reservations/view/:id',exact:false, name: 'رزرو کاربر',component:TrappUser_reservationsView});
export default TrappUser_reservationsRoutes;
