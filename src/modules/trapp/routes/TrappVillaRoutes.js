
import TrappVillaList from '../pages/villa/TrappVillaList';
import TrappVillaManage from '../pages/villa/TrappVillaManage';
import TrappVillaView from '../pages/villa/TrappVillaView';
import TrappWaitingToAcceptVillaList from "../pages/villa/TrappWaitingToAcceptVillaList";
import TrappAcceptedVillaList from "../pages/villa/TrappAcceptedVillaList";
let TrappVillaRoutes=[];
TrappVillaRoutes.push({ path: '/trapp/villa',exact:true, name: 'لیست ویلاها',component:TrappVillaList});
TrappVillaRoutes.push({ path: '/trapp/villa/waiting',exact:true, name: 'لیست ویلاهای منتظر تایید',component:TrappWaitingToAcceptVillaList});
TrappVillaRoutes.push({ path: '/trapp/villa/accepted',exact:true, name: 'لیست تایید شده',component:TrappAcceptedVillaList});
TrappVillaRoutes.push({ path: '/trapp/villa/management/:id',exact:false, name: 'ویرایش ویلا',component:TrappVillaManage});
TrappVillaRoutes.push({ path: '/trapp/villa/management',exact:false, name: 'تعریف ویلا',component:TrappVillaManage});
TrappVillaRoutes.push({ path: '/trapp/villa/view/:id',exact:false, name: 'ویلا',component:TrappVillaView});
export default TrappVillaRoutes;
