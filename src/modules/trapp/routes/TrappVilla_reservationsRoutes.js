
import TrappVilla_reservationsList from '../pages/villa_reservations/TrappVilla_reservationsList';
import TrappVilla_reservationsManage from '../pages/villa_reservations/TrappVilla_reservationsManage';
import TrappVilla_reservationsView from '../pages/villa_reservations/TrappVilla_reservationsView';
let TrappVilla_reservationsRoutes=[];
TrappVilla_reservationsRoutes.push({ path: '/trapp/villa_reservations',exact:true, name: 'لیست رزروهای ویلا',component:TrappVilla_reservationsList});
TrappVilla_reservationsRoutes.push({ path: '/trapp/villa_reservations/management/:id',exact:false, name: 'ویرایش رزرو ویلا',component:TrappVilla_reservationsManage});
TrappVilla_reservationsRoutes.push({ path: '/trapp/villa_reservations/management',exact:false, name: 'تعریف رزرو ویلا',component:TrappVilla_reservationsManage});
TrappVilla_reservationsRoutes.push({ path: '/trapp/villa_reservations/view/:id',exact:false, name: 'رزرو ویلا',component:TrappVilla_reservationsView});
export default TrappVilla_reservationsRoutes;
