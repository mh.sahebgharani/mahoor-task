
import TrappUsersList from '../pages/users/TrappUsersList';
import TrappUsersManage from '../pages/users/TrappUsersManage';
import TrappUsersView from '../pages/users/TrappUsersView';
let TrappUsersRoutes=[];
TrappUsersRoutes.push({ path: '/trapp/users',exact:true, name: 'لیست کاربران ترپ',component:TrappUsersList});
TrappUsersRoutes.push({ path: '/trapp/users/management/:id',exact:false, name: 'ویرایش کاربر ترپ',component:TrappUsersManage});
TrappUsersRoutes.push({ path: '/trapp/users/management',exact:false, name: 'تعریف کاربر ترپ',component:TrappUsersManage});
TrappUsersRoutes.push({ path: '/trapp/users/view/:id',exact:false, name: 'کاربر ترپ',component:TrappUsersView});
export default TrappUsersRoutes;
