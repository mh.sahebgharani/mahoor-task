// @flow
import * as React from 'react';
import {TextViewBox,ImageModal} from "Libs/one/sweet-react-components";
import {ManagePageContainer,ManagePageFieldsContainer,SweetManagePage} from "Libs/one/sweet-one-react";
import Constants from "../../../../classes/Constants";
import PaysEntity from "../../entity/PaysEntity";


export default class TrappPaysView extends SweetManagePage {
    moduleName='trapp';
    tableName='pays';
    constructor(props) {
        super(props);
        this.state = {
            formData:new PaysEntity(),

        };
    }
    
    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

    }
}
