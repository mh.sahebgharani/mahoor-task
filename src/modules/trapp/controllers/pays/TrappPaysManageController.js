// @flow
import * as React from 'react';
import {SweetManagePage} from "Libs/one/sweet-one-react";
import PaysEntity from "../../entity/PaysEntity";


export default class TrappPaysManageController extends SweetManagePage {
    moduleName='trapp';
    tableName='pays';
    constructor(props) {
        super(props);
        this.state = {
            canEdit:this.canEdit(),
            formData:new PaysEntity(),

        };
    }

    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

        this.loadSimpleFields();

    }
}
