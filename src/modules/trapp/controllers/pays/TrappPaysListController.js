// @flow
import * as React from 'react';
import Constants from '../../../../classes/Constants';
import PaysEntity from "../../entity/PaysEntity";
import {SweetManageListPage} from "Libs/one/sweet-one-react";

export default class TrappPaysListController extends SweetManageListPage {
    moduleName='trapp';
    tableName='pays';
    entity = new PaysEntity();
    componentDidMount() {
        super.componentDidMount();
    }
}
