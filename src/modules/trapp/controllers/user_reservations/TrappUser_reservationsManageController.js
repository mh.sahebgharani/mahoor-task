// @flow
import * as React from 'react';
import {SweetManagePage} from "Libs/one/sweet-one-react";
import User_reservationsEntity from "../../entity/User_reservationsEntity";


export default class TrappUser_reservationsManageController extends SweetManagePage {
    moduleName='trapp';
    tableName='user_reservations';
    constructor(props) {
        super(props);
        this.state = {
            canEdit:this.canEdit(),
            formData:new User_reservationsEntity(),

        };
    }
    
    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

        this.loadSimpleFields();

    }
}
