// @flow
import * as React from 'react';
import Constants from '../../../../classes/Constants';
import User_reservationsEntity from "../../entity/User_reservationsEntity";
import {SweetManageListPage} from "Libs/one/sweet-one-react";

export default class TrappUser_reservationsListController extends SweetManageListPage {
    moduleName='trapp';
    tableName='user_reservations';
    entity = new User_reservationsEntity();
    componentDidMount() {
        super.componentDidMount();
    }
}
