// @flow
import * as React from 'react';
import {TextViewBox,ImageModal} from "Libs/one/sweet-react-components";
import {ManagePageContainer,ManagePageFieldsContainer,SweetManagePage} from "Libs/one/sweet-one-react";
import Constants from "../../../../classes/Constants";
import User_withdrawal_requestsEntity from "../../entity/User_withdrawal_requestsEntity";


export default class TrappUser_withdrawal_requestsView extends SweetManagePage {
    moduleName='trapp';
    tableName='user_withdrawal_requests';
    constructor(props) {
        super(props);
        this.state = {
            formData:new User_withdrawal_requestsEntity(),

        };
    }

    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

    }
}
