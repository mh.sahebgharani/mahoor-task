// @flow
import * as React from 'react';
import {SweetManagePage} from "Libs/one/sweet-one-react";
import User_withdrawal_requestsEntity from "../../entity/User_withdrawal_requestsEntity";


export default class TrappUser_withdrawal_requestsManageController extends SweetManagePage {
    moduleName='trapp';
    tableName='user_withdrawal_requests';
    constructor(props) {
        super(props);
        this.state = {
            canEdit:this.canEdit(),
            formData:new User_withdrawal_requestsEntity(),

        };
    }

    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

        this.loadSimpleFields();

    }
}
