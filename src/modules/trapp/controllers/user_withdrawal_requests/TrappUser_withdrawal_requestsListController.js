// @flow
import * as React from 'react';
import Constants from '../../../../classes/Constants';
import User_withdrawal_requestsEntity from "../../entity/User_withdrawal_requestsEntity";
import {SweetManageListPage} from "Libs/one/sweet-one-react";

export default class TrappUser_withdrawal_requestsListController extends SweetManageListPage {
    moduleName='trapp';
    tableName='user_withdrawal_requests';
    entity = new User_withdrawal_requestsEntity();
    componentDidMount() {
        super.componentDidMount();
    }
}
