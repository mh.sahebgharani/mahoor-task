// @flow
import * as React from 'react';
import {TextViewBox,ImageModal} from "Libs/one/sweet-react-components";
import {ManagePageContainer,ManagePageFieldsContainer,SweetManagePage} from "Libs/one/sweet-one-react";
import Constants from "../../../../classes/Constants";
import UsersEntity from "../../entity/UsersEntity";


export default class TrappUsersView extends SweetManagePage {
    moduleName='trapp';
    tableName='users';
    constructor(props) {
        super(props);
        this.state = {
            formData:new UsersEntity(),

        };
    }
    
    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

    }
}
