// @flow
import * as React from 'react';
import {SweetManagePage} from "Libs/one/sweet-one-react";
import UsersEntity from "../../entity/UsersEntity";


export default class TrappUsersManageController extends SweetManagePage {
    moduleName='trapp';
    tableName='users';
    constructor(props) {
        super(props);
        this.state = {
            canEdit:this.canEdit(),
            formData:new UsersEntity(),

        };
    }

    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

        this.loadSimpleFields();

    }
}
