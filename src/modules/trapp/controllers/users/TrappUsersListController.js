// @flow
import * as React from 'react';
import Constants from '../../../../classes/Constants';
import UsersEntity from "../../entity/UsersEntity";
import {SweetManageListPage} from "Libs/one/sweet-one-react";

export default class TrappUsersListController extends SweetManageListPage {
    moduleName='trapp';
    tableName='users';
    entity = new UsersEntity();
    componentDidMount() {
        super.componentDidMount();
    }
}
