// @flow
import * as React from 'react';
import {SweetManagePage} from "Libs/one/sweet-one-react";
import VillaEntity from "../../entity/VillaEntity";

export default class TrappVillaManageController extends SweetManagePage {
    moduleName='trapp';
    tableName='villa';
    constructor(props) {
        super(props);
        this.state = {
            canEdit:this.canEdit(),
            formData:new VillaEntity(),
        };
    }

    loadUserIds = () => {
        this.state.userIdOptions.getAll(null,null,null,null,(data)=>{
                this.setState({userIdOptions:data});
            },null);
    };

    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

        this.loadSimpleFields();

    }
}
