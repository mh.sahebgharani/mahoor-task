// @flow
import * as React from 'react';
import Constants from '../../../../classes/Constants';
import VillaEntity from "../../entity/VillaEntity";
import {SweetManageListPage} from "Libs/one/sweet-one-react";

export default class TrappVillaListController extends SweetManageListPage {
    moduleName='trapp';
    tableName='villa';
    entity = new VillaEntity();
    getViewLink(id) {
        return "https://mahoorapps.ir/displayPage/"+id;
    }

    canDelete() {
        return false;
    }
    canView() {
        return false;
    }

    componentDidMount() {
        super.componentDidMount();
    }
}
