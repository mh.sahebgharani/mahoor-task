// @flow
import * as React from 'react';
import {TextViewBox,ImageModal} from "Libs/one/sweet-react-components";
import {ManagePageContainer,ManagePageFieldsContainer,SweetManagePage} from "Libs/one/sweet-one-react";
import Constants from "../../../../classes/Constants";
import Villa_reservationsEntity from "../../entity/Villa_reservationsEntity";


export default class TrappVilla_reservationsView extends SweetManagePage {
    moduleName='trapp';
    tableName='villa_reservations';
    constructor(props) {
        super(props);
        this.state = {
            formData:new Villa_reservationsEntity(),

        };
    }

    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

    }
}
