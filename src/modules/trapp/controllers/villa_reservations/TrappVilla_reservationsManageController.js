// @flow
import * as React from 'react';
import {SweetManagePage} from "Libs/one/sweet-one-react";
import Villa_reservationsEntity from "../../entity/Villa_reservationsEntity";


export default class TrappVilla_reservationsManageController extends SweetManagePage {
    moduleName='trapp';
    tableName='villa_reservations';
    constructor(props) {
        super(props);
        this.state = {
            canEdit:this.canEdit(),
            formData:new Villa_reservationsEntity(),

        };
    }
    
    componentDidMount() {
        super.componentDidMount();
        if(this.getID()>0)
            this.state.formData.get(this.getID(), () => {this.notifyFormDataChanged();});

        this.loadSimpleFields();

    }
}
