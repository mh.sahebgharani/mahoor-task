// @flow
import * as React from 'react';
import Constants from '../../../../classes/Constants';
import Villa_reservationsEntity from "../../entity/Villa_reservationsEntity";
import {SweetManageListPage} from "Libs/one/sweet-one-react";

export default class TrappVilla_reservationsListController extends SweetManageListPage {
    moduleName='trapp';
    tableName='villa_reservations';
    entity = new Villa_reservationsEntity();
    componentDidMount() {
        super.componentDidMount();
    }
}
