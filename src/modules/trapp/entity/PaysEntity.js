// @flow
import {SweetEntity,AccessManager} from 'Libs/one/sweet-one-react';

export default class PaysEntity extends SweetEntity {
    __moduleName='trapp';
    __tableName='pays';

    paymentMethod={};
    user={};
    villa={};
    status={};
    email='';
    mobile='';
    refid='';
    name='';
    userReservation={};
    villaReservation={};
    authority='';
    amount='';
    fields={id:{field:'id'},paymentMethod:{field:'payment_method',type:'simplefield',simplefieldname:'trapp_villa_status'},user:{field:'user_id',type:'fid'},villa:{field:'villa_id',type:'fid'},status:{field:'status',type:'simplefield',simplefieldname:'trapp_villa_status'},email:{field:'email'},mobile:{field:'mobile'},refid:{field:'refid'},name:{field:'name'},userReservation:{field:'user_reservation_id',type:'fid'},villaReservation:{field:'villa_reservation_id',type:'fid'},authority:{field:'authority'},amount:{field:'amount'}};
    get(id,afterFetch){
        this._get(this.getBaseURL()+'/'+id,this.getServiceName(),AccessManager.VIEW,afterFetch);
    }
    getAll(pageSize,page,sorted,filtered,afterFetch,onError){
        const filterAndSortString=this.getFilterString(pageSize,page,sorted,filtered);
        const url=this.getBaseURL()+filterAndSortString;
        this._getAll(url,this.getServiceName(),AccessManager.LIST,afterFetch,onError);
    }
    save(id,afterFetchListener,onError){
        const formData=this.getFormRequest();
        this._save(this.getServiceName(),this.getBaseURL(),id,formData,afterFetchListener,onError);
    }
    delete(id,afterFetchListener,onError){
        this._delete(this.getServiceName(),this.getBaseURL()+"/"+id,afterFetchListener,onError);
    }
}
