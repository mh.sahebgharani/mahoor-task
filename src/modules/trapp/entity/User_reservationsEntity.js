// @flow
import {SweetEntity,AccessManager} from 'Libs/one/sweet-one-react';

export default class User_reservationsEntity extends SweetEntity {
    __moduleName='trapp';
    __tableName='user_reservations';

    SendPollSMS='';
    lengthStay='';
    extraPeople='';
    passengersNumber='';
    villa={};
    returnPercentage='';
    payStatus={};
    facilitiesCost='';
    extraCost='';
    exitDate='';
    entryDate='';
    cost='';
    city='';
    state={};
    user={};
    villaTitle='';
    fields={id:{field:'id'},SendPollSMS:{field:'sendpollsms'},lengthStay:{field:'length_stay'},extraPeople:{field:'extra_people'},passengersNumber:{field:'passengers_number'},villa:{field:'villa_id',type:'fid'},returnPercentage:{field:'return_percentage'},payStatus:{field:'pay_status',type:'simplefield',simplefieldname:'trapp_villa_status'},facilitiesCost:{field:'facilities_cost'},extraCost:{field:'extra_cost'},exitDate:{field:'exit_date'},entryDate:{field:'entry_date'},cost:{field:'cost'},city:{field:'city'},state:{field:'state',type:'simplefield',simplefieldname:'trapp_villa_status'},user:{field:'user_id',type:'fid'},villaTitle:{field:'villa_title'}};
    get(id,afterFetch){
        this._get(this.getBaseURL()+'/'+id,this.getServiceName(),AccessManager.VIEW,afterFetch);
    }
    getAll(pageSize,page,sorted,filtered,afterFetch,onError){
        const filterAndSortString=this.getFilterString(pageSize,page,sorted,filtered);
        const url=this.getBaseURL()+filterAndSortString;
        this._getAll(url,this.getServiceName(),AccessManager.LIST,afterFetch,onError);
    }
    save(id,afterFetchListener,onError){
        const formData=this.getFormRequest();
        this._save(this.getServiceName(),this.getBaseURL(),id,formData,afterFetchListener,onError);
    }
    delete(id,afterFetchListener,onError){
        this._delete(this.getServiceName(),this.getBaseURL()+"/"+id,afterFetchListener,onError);
    }
}
