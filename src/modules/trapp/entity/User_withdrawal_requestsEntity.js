// @flow
import {SweetEntity,AccessManager} from 'Libs/one/sweet-one-react';

export default class User_withdrawal_requestsEntity extends SweetEntity {
    __moduleName='trapp';
    __tableName='user_withdrawal_requests';

    cost='';
    user={};
    fields={id:{field:'id'},cost:{field:'requested_amount'},user:{field:'user_id',type:'fid'}};
    get(id,afterFetch){
        this._get(this.getBaseURL()+'/'+id,this.getServiceName(),AccessManager.VIEW,afterFetch);
    }
    getAll(pageSize,page,sorted,filtered,afterFetch,onError){
        const filterAndSortString=this.getFilterString(pageSize,page,sorted,filtered);
        const url=this.getBaseURL()+filterAndSortString;
        this._getAll(url,this.getServiceName(),AccessManager.LIST,afterFetch,onError);
    }
    save(id,afterFetchListener,onError){
        const formData=this.getFormRequest();
        this._save(this.getServiceName(),this.getBaseURL(),id,formData,afterFetchListener,onError);
    }
    delete(id,afterFetchListener,onError){
        this._delete(this.getServiceName(),this.getBaseURL()+"/"+id,afterFetchListener,onError);
    }
}
