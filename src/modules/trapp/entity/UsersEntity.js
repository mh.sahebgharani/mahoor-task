// @flow
import {SweetEntity,AccessManager} from 'Libs/one/sweet-one-react';

export default class UsersEntity extends SweetEntity {
    __moduleName='trapp';
    __tableName='users';

    avatar={};
    shaba_number='';
    card_number='';
    foreignLanguage='';
    education='';
    job='';
    nationalCode='';
    email='';
    phoneNumber='';
    trappID='';
    fullName='';
    fields={id:{field:'id'},avatar:{field:'avatar',type:'file'},shaba_number:{field:'shaba_number'},card_number:{field:'card_number'},foreignLanguage:{field:'foreign_language'},education:{field:'education'},job:{field:'job'},nationalCode:{field:'national_code'},email:{field:'email'},phoneNumber:{field:'phone_number'},trappID:{field:'trapp_id'},fullName:{field:'fullname'}};
    get(id,afterFetch){
        this._get(this.getBaseURL()+'/'+id,this.getServiceName(),AccessManager.VIEW,afterFetch);
    }
    getAll(pageSize,page,sorted,filtered,afterFetch,onError){
        const filterAndSortString=this.getFilterString(pageSize,page,sorted,filtered);
        const url=this.getBaseURL()+filterAndSortString;
        this._getAll(url,this.getServiceName(),AccessManager.LIST,afterFetch,onError);
    }
    save(id,afterFetchListener,onError){
        const formData=this.getFormRequest();
        this._save(this.getServiceName(),this.getBaseURL(),id,formData,afterFetchListener,onError);
    }
    delete(id,afterFetchListener,onError){
        this._delete(this.getServiceName(),this.getBaseURL()+"/"+id,afterFetchListener,onError);
    }
}
