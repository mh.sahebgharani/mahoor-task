// @flow
import {SweetEntity,AccessManager} from 'Libs/one/sweet-one-react';

export default class VillaEntity extends SweetEntity {
    __moduleName='trapp';
    __tableName='villa';

    normalCost='';
    disinfected='';
    status={};
    score='';
    mainImg={};
    visitCount='';
    userId={};
    lat='';
    long='';
    address='';
    postalCode='';
    village='';
    city='';
    state='';
    story='';
    phoneNumber='';
    type='';
    title='';
    fields={id:{field:'id'},normalCost:{field:'normal_cost'},disinfected:{field:'disinfected'},status:{field:'status',type:'simplefield',simplefieldname:'trapp_villa_status'},score:{field:'score'},mainImg:{field:'main_img',type:'file'},visitCount:{field:'visit_count'},userId:{field:'user_id',type:'fid'},lat:{field:'lat'},long:{field:'long'},address:{field:'address'},postalCode:{field:'postal_code'},village:{field:'village'},city:{field:'city'},state:{field:'state'},story:{field:'story'},phoneNumber:{field:'phone_number'},type:{field:'type'},title:{field:'title'}};
    get(id,afterFetch){
        this._get(this.getBaseURL()+'/'+id,this.getServiceName(),AccessManager.VIEW,afterFetch);
    }
    getAll(pageSize,page,sorted,filtered,afterFetch,onError){
        const filterAndSortString=this.getFilterString(pageSize,page,sorted,filtered);
        const url=this.getBaseURL()+filterAndSortString;
        this._getAll(url,this.getServiceName(),AccessManager.LIST,afterFetch,onError);
    }
    save(id,afterFetchListener,onError){
        const formData=this.getFormRequest();
        this._save(this.getServiceName(),this.getBaseURL(),id,formData,afterFetchListener,onError);
    }
    delete(id,afterFetchListener,onError){
        this._delete(this.getServiceName(),this.getBaseURL()+"/"+id,afterFetchListener,onError);
    }
}
