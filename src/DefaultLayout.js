import React from 'react';
import EcommerceProductList from "./modules/ecommerce/pages/product/EcommerceProductList";
import Basket from "./modules/ecommerce/Classes/Basket";
import {withCookies, Cookies} from 'react-cookie';
import {instanceOf} from "prop-types";
import SweetRouter from "../src/Libs/classes/SweetRouter";
import {Link, Route, Switch} from "react-router-dom";
import EcommerceBasket from "./modules/ecommerce/pages/product/EcommerceBasket";
import EcommercePreorder from "./modules/ecommerce/pages/product/EcommercePreorder";
import LocationPlaceManage from "./modules/location/pages/place/LocationPlaceManage";
import User from "./modules/users/Classes/User";
import UserMenu from "./modules/users/pages/user/UserMenu";
import LocationPlaceList from "./modules/location/pages/place/LocationPlaceList";
import EcommercePay from "./modules/ecommerce/pages/product/EcommercePay";
import EcommerceOrderList from "./modules/ecommerce/pages/order/EcommerceOrderList";
import EcommerceSearch from "./modules/ecommerce/pages/product/EcommerceSearch";
import EcommerceDeliveryOrderList from "./modules/ecommerce/pages/order/EcommerceDeliveryOrderList";
import EcommerceOrderpollManage from "./modules/ecommerce/pages/orderpoll/EcommerceOrderpollManage";
import MessagingMessageList from "./modules/messaging/pages/message/MessagingMessageList";
import Contactus from "./modules/common/pages/Contactus";
import Aboutus from "./modules/common/pages/Aboutus";
import Footer from "./Footer";

class DefaultLayout extends React.Component{
    static propTypes = {
        cookies: instanceOf(Cookies).isRequired
    };
    constructor(props){
        super(props);
        this.state={
            basket:Basket.all(true,props.cookies),
        };
        User.checkloginAndRedirect(this.props.history);
    }
    render() {
        const plist=(props)=><EcommerceProductList {...props} />;
        const basket=(props)=><EcommerceBasket {...props} />;
        return (
                    <div className="App">
                        <div className={'header'}><img src={require('./Files/img/vp/logo.png')}/> </div>
                        <div className={'pagecontent'}>
                            <Switch>
                            <Route exact path={SweetRouter.getLink('')} component={EcommerceSearch}/>
                            <Route exact path={SweetRouter.getLink('products/:group')} component={plist}/>
                            <Route exact path={SweetRouter.getLink('basket')} component={basket}/>
                            <Route exact path={SweetRouter.getLink('preorder')} component={EcommercePreorder}/>
                            <Route exact path={SweetRouter.getLink('ecommerce/orders')} component={EcommerceOrderList}/>
                            <Route exact path={SweetRouter.getLink('ecommerce/deliveryorders/:status')} component={EcommerceDeliveryOrderList}/>
                            <Route exact path={SweetRouter.getLink('pay')} component={EcommercePay}/>
                            <Route exact path={SweetRouter.getLink('contactus')} component={Contactus}/>
                            <Route exact path={SweetRouter.getLink('aboutus')} component={Aboutus}/>
                            <Route exact path={SweetRouter.getLink('usermenu')} component={UserMenu}/>
                            <Route exact path={SweetRouter.getLink('messages')} component={MessagingMessageList}/>
                            <Route exact path={SweetRouter.getLink('location/place/management')} component={LocationPlaceManage}/>
                            <Route exact path={SweetRouter.getLink('ecommerce/poll/:orderid')} component={EcommerceOrderpollManage}/>
                            <Route exact path={SweetRouter.getLink('location/place/management/:id')} component={LocationPlaceManage}/>
                            <Route exact path={SweetRouter.getLink('location/place')} component={LocationPlaceList}/>
                            </Switch>
                        </div>
                        <Footer/>
                    </div>
        );
    }
}
export default withCookies(DefaultLayout);
