import AccessManager from "./classes/AccessManager";
let FinalItems=[];
let Items=[
    {
        name: 'ورود به سامانه',
        icon: 'fa fa-sign-in',
        badge: {
            variant: 'info',
        },
        url: '/login',
        access:!AccessManager.UserIsLoggedIn()
    },

    {
        name: 'اطلاعات پایه',
        icon: 'fa fa-database',
        badge: {
            variant: 'info',
        },
        access:AccessManager.UserIsLoggedIn(),
        children:[
            {
                name: 'انواع خدمت',
                url: '/crm/servicetype',
                icon: 'fa fa-cog',
                badge: {
                    variant: 'info',
                },
                access:AccessManager.UserIsLoggedIn(),
            },
            {
                name: 'وضعیت های خدمت',
                url: '/crm/servicestatus',
                icon: 'fa fa-cog',
                badge: {
                    variant: 'info',
                },
                access:AccessManager.UserIsLoggedIn(),
            },
            {
                name: 'انواع پرداخت',
                url: '/crm/paymenttype',
                icon: 'fa fa-cog',
                badge: {
                    variant: 'info',
                },
                access:AccessManager.UserIsLoggedIn(),
            },
            {
                name: 'ارزشهای خدمت',
                url: '/crm/servicevalue',
                icon: 'fa fa-cog',
                badge: {
                    variant: 'info',
                },
                access:AccessManager.UserIsLoggedIn(),
            },
            {
                name: 'اولویت های رسیدگی',
                url: '/crm/handlingpriority',
                icon: 'fa fa-cog',
                badge: {
                    variant: 'info',
                },
                access:AccessManager.UserIsLoggedIn(),
            },
            {
                name: 'انواع درخواست',
                url: '/crm/servicerequesttype',
                icon: 'fa fa-cog',
                badge: {
                    variant: 'info',
                },
                access:AccessManager.UserIsLoggedIn(),
            },
        ]},
    {
        name: 'امور اجرایی',
        icon: 'fa fa-bolt',
        badge: {
            variant: 'info',
        },
        access:AccessManager.UserIsLoggedIn(),
        children:[
            {
                name: 'شرکت ها',
                url: '/crm/company',
                icon: 'fa fa-cog',
                badge: {
                    variant: 'info',
                },
                access:AccessManager.UserIsLoggedIn(),
            },
            {
                name: 'مشتریان',
                url: '/crm/customer',
                icon: 'fa fa-cog',
                badge: {
                    variant: 'info',
                },
                access:AccessManager.UserIsLoggedIn(),
            },
            {
                name: 'پروژه ها',
                url: '/crm/project',
                icon: 'fa fa-cog',
                badge: {
                    variant: 'info',
                },
                access:AccessManager.UserIsLoggedIn(),
            },
            ]},

    {
        name: 'مدیریت',
        icon: 'fa fa-cogs',
        badge: {
            variant: 'info',
        },
        access:AccessManager.UserIsLoggedIn(),
        children:[

            {
                name: 'تنظیمات',
                url: '/common/parameter',
                icon: 'fa fa-cog',
                badge: {
                    variant: 'info',
                },
                access:AccessManager.UserIsLoggedIn(),
            },
        ]},
    {
        name: 'تغییر رمز',
        url: '/changepass',
        icon: 'fa fa-lock',
        badge: {
            variant: 'info',
        },
        access:AccessManager.UserIsLoggedIn(),

    },
    {
        name: 'خروج',
        url: '/login/signout',
        icon: 'fa fa-sign-out',
        badge: {
            variant: 'info',
        },
        access:true

    }
];
for(let i=0;i<Items.length;i++)
{
    if(Items[i].access==true)
        FinalItems.push(Items[i]);
}
export default {
    items: FinalItems
};
