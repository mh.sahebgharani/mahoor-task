import NavVp from './_navvp';
import NavArya from './_navarya';
import NavCrm from './_navcrm';
import NavTrapp from './_navtrapp';
import NavRap from './_navrap';
import Constants from "./classes/Constants";
let menu=null;
if(Constants.menuName==='arya')
    menu=NavArya;
else if(Constants.menuName==='vp')
    menu=NavVp;
else if(Constants.menuName==='crm')
    menu=NavCrm;
else if(Constants.menuName==='trapp')
    menu=NavTrapp;
else if(Constants.menuName==='rap')
    menu=NavRap;
export default menu;
