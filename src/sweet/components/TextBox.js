/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React from 'react';
import {Editor} from "@tinymce/tinymce-react/lib/es2015/main/ts";
import Constants from "../../classes/Constants";
import SweetFetcher from "../../classes/sweet-fetcher";
import AccessManager from "../../classes/AccessManager";

export default class TextBox extends React.Component {
    render() {
        let inputType='text';
        if(this.props.keyboardType!=null)
        {
            if(this.props.keyboardType.trim().toLowerCase()==='numeric')
                inputType='number';
        }
        return (
            <div className='form-group'>
                <label htmlFor={this.props.id}>{this.props.title}</label>
                {(this.props.multiline == null || this.props.multiline == false) &&
                < input
                    className='form-control'
                    id={this.props.id}
                    type={inputType}
                    readOnly={this.props.readOnly}
                    group
                    validate
                    value={this.props.value}
                    onChange={(event) =>{this.props.onChangeText(event.target.value)}} {...this.props}/>
                }
                {this.props.multiline != null && this.props.multiline &&
                <Editor
                    className='form-control'
                    id={this.props.id}
                    readOnly={this.props.readOnly}
                    group
                    initialValue={this.props.value}
                    value={this.props.value}
                    apiKey='efk2g300fptq9ram563eyvv8lod8xvofyvdhbzvjtdl7qiwj'
                    init={{
                        height: 500,
                        menubar: 'edit view insert format tools tc',
                        plugins: [
                            'advlist autolink lists link image charmap print preview anchor',
                            'searchreplace visualblocks code fullscreen',
                            'insertdatetime media table paste code help wordcount'
                        ],
                        toolbar:
                            'undo redo | formatselect | bold italic backcolor | image |\
                            alignleft aligncenter alignright alignjustify | \
                            bullist numlist outdent indent | removeformat | help',
                        image_uploadtab:true,
                        images_upload_handler: function (blobInfo, success, failure) {
                            const data = new FormData();
                            data.append('file', blobInfo.blob());
                            new SweetFetcher().Fetch('/files/upload',SweetFetcher.METHOD_POST,data,(data)=>{
                                success(Constants.SiteURL+'/'+data.Data.location);
                            },()=>{},'files.upload',AccessManager.INSERT,null);
                        }
                    }}
                    onEditorChange={this.props.onChangeText}
                />
                }
            </div>
           );
    }
}

