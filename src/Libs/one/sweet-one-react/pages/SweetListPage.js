import AccessManager from "../classes/AccessManager";
import * as React from "react";
import {Common} from 'Libs/one/sweet-react-common-tools';

export default class SweetListPage extends React.Component {
    moduleName=null;
    tableName=null;
    getDefaultPageSize(){
        if(this.props.DefaultPageSize)
            return this.props.DefaultPageSize;
        return global.DefaultPageSize;
    }
    canView(){
        return AccessManager.UserCan(this.moduleName, this.tableName, AccessManager.VIEW);
    }
    canInsert(){
        return  AccessManager.UserCan(this.moduleName, this.tableName, AccessManager.INSERT);
    }
    canEdit(){
        return  AccessManager.UserCan(this.moduleName, this.tableName, AccessManager.EDIT);
    }
    canDelete(){
        return  AccessManager.UserCan(this.moduleName, this.tableName, AccessManager.DELETE);
    }
    searchData=(searchParams)=>
    {
        this.LoadData(this.state.pageSize,1,null,Common.ObjectToIdValueArray(searchParams));
    };
    getPageCount(RecordCount,PageSize){
        return Math.ceil(RecordCount/PageSize);
    }
    __defaultFilters=[];
    __defaultSorts=[];

    getFilterString(filtered){
            // console.log(filtered);
            let finalFilter=[];
            if(filtered!=null && filtered.length>0)
                finalFilter=finalFilter.concat(filtered);
            // console.log(finalFilter);
            if(this.__defaultFilters!=null && this.__defaultFilters.length>0)
                finalFilter=finalFilter.concat(this.__defaultFilters);
            return finalFilter;
    }
    getSortString(sorted){
        let finalSort=[];
        if(sorted!=null && sorted.length>0)
            finalSort=finalSort.concat(sorted);
        if(this.__defaultSorts!=null && this.__defaultSorts.length>0)
            finalSort=finalSort.concat(this.__defaultSorts);
        return finalSort;
    }
    LoadData(pageSize,page,sorted,filtered) {
        this.setState({loading:true,filtered:filtered,sorted:sorted});
        let finalFilter=this.getFilterString(filtered);
        let finalSort=this.getSortString(sorted);
        if(pageSize==null)
            pageSize=this.getDefaultPageSize();
        if(page==null)
            page=this.state.page||1;
        this.getData(pageSize, page, finalSort, finalFilter, (data,RecordCount) => {
            let Pages = this.getPageCount(RecordCount, pageSize);
            this.setState({data: this.normalizeAllListItems(data), pages: Pages,page:page,pageSize:pageSize,loading:false})
        },()=>{this.setState({loading:false,data:[]})});
    }
    getData(pageSize, page, finalSort, finalFilter, afterFetch,onError){
        this.entity.getAll(pageSize, page, finalSort, finalFilter, afterFetch,onError);
    }
    normalizeAllListItems(data){
        for(let i=0;i<data.length;i++)
            data[i]=Common.convertNullKeysToEmpty(data[i]);
        return data;
    }
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            pages:1,
            page:0,
            loading:false,
            pageSize:this.getDefaultPageSize(),
        };
    };
    componentDidMount() {
    }
}
