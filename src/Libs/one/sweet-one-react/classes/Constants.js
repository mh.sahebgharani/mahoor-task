// @flow

import * as React from "react";

class Constants {
    static SERVERMODE_LARAVEL=1;
    static SERVERMODE_ASP=2;
    static DELETE_METHOD_MODE_NORMAL=0;
    static DELETE_METHOD_MODE_PUT=1;
}

export default Constants;
