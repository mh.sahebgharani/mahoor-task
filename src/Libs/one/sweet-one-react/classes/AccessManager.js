// @flow
import * as React from 'react';
import Cookies from 'universal-cookie';
import Constants from "./Constants";

class AccessManager{
    static LIST='list';
    static VIEW='view';
    static EDIT='edit';
    static INSERT='insert';
    static DELETE='delete';
    static UserCan(ModuleName,TableName,Action)
    {
        if(global.ServerMode===Constants.SERVERMODE_LARAVEL) {

            ModuleName=ModuleName.toLowerCase();
            TableName=TableName.toLowerCase();
            Action=Action.toLowerCase();
            let ActionString=ModuleName+'.'+TableName + "."+Action;
            let access=localStorage.getItem('access.'+ActionString);
            // console.log(access);
            return access!=null;
        }
        else
        {
            if(Action===AccessManager.LIST)
                Action=AccessManager.VIEW;
            let access=localStorage.getItem('access');
            console.log(access);
            let ActionString=TableName + "."+Action;
            let hasAccess=(access.hasOwnProperty(ActionString) && access[ActionString].toString()==="1");
            return hasAccess;
        }

    }
    static getUserRoles()
    {
        let roles= localStorage.getItem('userroles');
        return roles==null?[]:roles;
    }
    static UserIsLoggedIn()
    {
        let sessionKey= localStorage.getItem('sessionkey');
        if(sessionKey==null || sessionKey=="")
            return false;
        return true;
    }
    static getUserDisplayName()
    {
        let userdisplayname= localStorage.getItem('userdisplayname');
        if(userdisplayname==null)
            userdisplayname="کاربر مهمان";
        return userdisplayname;
    }
    static getUserProfilePhoto()
    {
        return localStorage.getItem('userprofilephoto');
    }
    static getUserID()
    {
        return localStorage.getItem('userid');
    }
    static getUserLoginTime()
    {
        let userlogintime= localStorage.getItem('userlogintime');
        if(userlogintime==null)
            userlogintime="";
        return userlogintime;
    }
}

export default AccessManager;
