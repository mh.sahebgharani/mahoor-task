// @flow
import Constants from "./Constants";
import {Common,SweetAlert} from 'Libs/one/sweet-react-common-tools';
import axios from 'axios';

class SweetFetcher {
    static METHOD_GET='get';
    static METHOD_POST='post';
    static METHOD_PUT='put';
    static METHOD_DELETE='delete';
    MaxRetriesOnErrorCount=2;
    TimeOut=30000;
    onError=(errorCode,response)=>{
        if(errorCode===401) {
            if (response.hasOwnProperty('data') && response.data.message != null) {
                SweetAlert.displayErrorAlert("خطای اطلاعات کاربری", response.data.message);
            } else {
                SweetAlert.displayErrorAlert("خطای اطلاعات کاربری", 'اطلاعات کاربری صحیح نمی باشد');
            }
        }
    };
    constructor(){
        if(global.RequestTimeOut!=null && global.RequestTimeOut>0)
            this.TimeOut=global.RequestTimeOut;
        if(global.onFetchError!=null)
            this.onError=global.onFetchError;
    }
    Fetch(URL,InitialMethod,PostingData,AfterFetchFunction,OnErrorFunction,ServiceName,ActionName,history,isSilent,retries){
        let Method=InitialMethod.toString().trim().toLowerCase();
        if(retries==null)
            retries=0;
        let PostData=null;
        if(PostingData==null)
            PostingData=new FormData();

        let retryRequest=()=>{
            this.Fetch(URL,InitialMethod,PostingData,AfterFetchFunction,OnErrorFunction,ServiceName,ActionName,history,isSilent,retries+1);
        };
        let retryRequestIfNeeded=()=>{
            if(retries<this.MaxRetriesOnErrorCount){
                retryRequest();
                return true;
            }
            return false;
        };

        let runAfterFetchFunction=(data)=>{
            try{
                AfterFetchFunction(data);
            }
            catch (error) {
                if(global.Debugging)
                    console.log(error);
                if (OnErrorFunction != null)
                    OnErrorFunction(null);
            }
        };
        if(global.ServerMode===Constants.SERVERMODE_LARAVEL)
            {
                if(Method==="put")
                {
                    PostingData.append('_method', 'put');
                    Method=SweetFetcher.METHOD_POST;
                }
                PostData=PostingData;
            }
            else if(global.ServerMode===Constants.SERVERMODE_ASP)
            {
                PostData=new URLSearchParams(PostingData);
            }

        let Fetched=null;
        let Prefix='';
        if(global.ServerMode===Constants.SERVERMODE_LARAVEL)
            Prefix='Bearer ';
        let ax=axios.create({
            baseURL: global.SiteURL+"/api",
            timeout: this.TimeOut,
            headers: {
                Accept: 'application/json',
                Authorization: Prefix+localStorage.getItem('sessionkey'),
                'Content-Type': 'application/x-www-form-urlencoded',
            },
            mode: 'cors',
            crossDomain:true,
        });
        if(Method===SweetFetcher.METHOD_GET)
        {
            Fetched=ax.get(URL);
        }
        else if(Method===SweetFetcher.METHOD_POST)
        {
            Fetched=ax.post(URL,PostData);
        }
        else if(Method===SweetFetcher.METHOD_PUT)
        {
            Fetched=ax.put(URL,PostData);
        }
        else if(Method===SweetFetcher.METHOD_DELETE)
        {
            Fetched=ax.delete(URL);
        }
        else{
            Fetched=ax.get(URL);
        }
        const Fetcher=this;
        Fetched.then(response => {
                try {
                    console.log("URL:"+URL);
                    console.log(response);
                }catch (e) {

                }

                    let data=response.data;
                    if(data!=null)
                    {
                        console.log(data);
                        if(Array.isArray(data.Data))
                        {
                            for(let i=0;i<data.Data.length;i++)
                            {
                                data.Data[i]=Common.convertObjectPropertiesToLowerCase(data.Data[i]);
                            }
                        }
                        else if(data.Data!=null)
                        {
                            data.Data=Common.convertObjectPropertiesToLowerCase(data.Data);
                        }
                        runAfterFetchFunction(data);
                    }

            }).catch(function (error) {

                if(OnErrorFunction!=null)
                    OnErrorFunction(error);
                if(error.response!=null)
                {
                    const response=error.response;
                    const status=response.status.toString().trim();
                    if (status!=='200' && status!=='201'  && status!=='202' && status!=='203') {

                        console.log(status);
                        if(status==="403"){
                            if(!isSilent) {
                                if (response.hasOwnProperty('data') && response.data.message != null) {
                                    SweetAlert.displayErrorAlert("خطای دسترسی", response.data.message);
                                } else {
                                    SweetAlert.displayErrorAlert("خطای دسترسی", 'شما دسترسی لازم برای اجرای این کار را ندارید');
                                }
                            }
                        }
                        if(status==="401")
                        {
                            if(!isSilent) {
                                Fetcher.onError(401,response);
                            }
                        }
                        if(status==="405" && !isSilent)
                            SweetAlert.displayAccessDeniedAlert();
                        if(status==="429" && !isSilent)
                            SweetAlert.displayErrorAlert("خطای محافظت امنیتی",'تعداد درخواست های شما بیش از حد مجاز است و به دلایل امنیتی دسترسی شما تا چند دقیقه بعد مسدود شد. لطفا چند دقیقه دیگر مراجعه نمایید');
                        if (status === "422")
                        {
                            let displayDefaultMessage=true;
                            if(response.hasOwnProperty('data')) {
                                let data = response.data;
                                if(data.hasOwnProperty('errors') && data.errors!=null)
                                {
                                    displayDefaultMessage=false;
                                    let message='';
                                    // SweetConsole.log(data.errors);
                                    Object.keys(data.errors).forEach(function(key, index) {
                                        let item=data.errors[key];
                                        Object.keys(item).forEach(function(key, index) {
                                            let itemMessage=item[key];
                                            // message=message+"<p>"+itemMessage+"</p>";
                                            message="<p>"+itemMessage+"</p>";
                                        });
                                    });
                                    if(!isSilent)
                                        SweetAlert.displayWarningAlert("خطای اطلاعات ورودی",message);

                                }
                                else if (data.hasOwnProperty('message'))
                                {
                                    displayDefaultMessage=false;
                                    if(data.message!=='' && !isSilent)
                                        SweetAlert.displayWarningAlert("خطای اطلاعات ورودی",data.message);

                                }
                            }
                            if(displayDefaultMessage && !isSilent)
                                SweetAlert.displayWarningAlert("خطای اطلاعات ورودی", 'لطفا اطلاعات را به صورت صحیح وارد کنید');

                        }//if 422
                        else if (status === "500")
                        {
                            let displayDefaultMessage=true;
                            console.log(response);
                            let data = response.data;
                            if (global.Debugging && data.hasOwnProperty('message'))
                            {
                                displayDefaultMessage=false;
                                if(data.message!=='' && !isSilent)
                                    SweetAlert.displayErrorAlert("خطای سرور",data.message);
                            }
                            if(displayDefaultMessage && !isSilent)
                                SweetAlert.displayErrorAlert("خطای سرور", 'خطایی در سمت سرور رخ داد، لطفا این مشکل را به پشتیبانی اطلاع دهید.');
                        }//if 500
                        else if (status=== "400")
                        {
                            if(!retryRequestIfNeeded()){
                                SweetAlert.displayWarningAlert("خطای موقتی سرور", 'لطفا دوباره تلاش کنید.');
                            }

                        }
                    }//if not status 200
                    console.log(error.response);
                }//if error has response

                else {

                    if(!retryRequestIfNeeded()){
                        const showNetError=()=>{
                            SweetAlert.displayYesNoAlert('خطا', 'متاسفانه مشکلی در اتصال به سرور به وجود آمد، لطفا اتصال اینترنت خود را بررسی کنید و درخواست خود را دوباره تکرار کنید',{text:'تلاش مجدد',onPress:retryRequest},{text:'بستن'});
                        };
                        if(global.Debugging) {
                            if (error.toString().toLowerCase().includes("network error")) {
                                showNetError();
                            }
                            else if (error.toString().toLowerCase().includes("timeout of")) {
                                showNetError();
                            }
                            else {
                                SweetAlert.displaySimpleAlert('خطا', error.toString());

                            }
                        }
                        else{
                            if (error.toString().toLowerCase().includes("network error")) {
                                showNetError();
                            } else {
                                SweetAlert.displaySimpleAlert("خطا", 'با عرض پوزش، خطایی در اجرای درخواست شما به وجود آمد. لطفا چند دقیقه دیگر مراجعه نمایید و در صورت عدم حل مشکل با ما در تماس باشید. ');
                            }
                        }

                    }
                }//if error has no response
            console.log(error);
        });
    }
}

export default SweetFetcher;
