// @flow
import { SweetEntity, AccessManager } from 'Libs/one/sweet-one-react';

export default class PlaceEntity extends SweetEntity {
    __moduleName = 'location';
    __tableName = 'place';

    title = '';
    ownerName = '';
    address = '';
    tel = '';
    latitude = 35.708646;
    longitude = 51.397524;
    user = {};
    area_fid = {};
    visits = '';
    description = '';
    visible = '';
    addressType = '';
    region = {};
    fields =
        {
            id: { field: 'id' },
            title: { field: 'title' },
            ownerName: { field: 'ownername' },
            address: { field: 'address' },
            tel: { field: 'tel' },
            latitude: { field: 'latitude' },
            longitude: { field: 'longitude' },
            user: { field: 'user_fid', type: 'fid' },
            area_fid: { field: 'area_fid', type: 'fid' },
            region: { field: 'region_fid', type: 'fid' },
            visits: { field: 'visits_num' },
            addressType: { field: 'address_type' },
            description: { field: 'description' },
            visible: { field: 'is_visible' }
        };
    get(id, afterFetch) {
        this._get(this.getBaseURL() + '/' + id, this.getServiceName(), AccessManager.VIEW, afterFetch);
    }
    save(id, afterFetchListener, onError) {
        const formData = this.getFormRequest();
        this._save(this.getServiceName(), this.getBaseURL(), id, formData, afterFetchListener, onError);
    }
    delete(id, afterFetchListener, onError) {
        this._delete(this.getServiceName(), this.getBaseURL() + "/" + id, afterFetchListener, onError);
    }
}
