import {SweetFetcher} from 'Libs/one/sweet-one-react';

export default class AppVersionMan {
    static getAppLastVersion(AfterFetch,OnError){
        new SweetFetcher().Fetch('/common/appinfo',SweetFetcher.METHOD_GET,null,AfterFetch,OnError,'load','appv',null);
    }
}
