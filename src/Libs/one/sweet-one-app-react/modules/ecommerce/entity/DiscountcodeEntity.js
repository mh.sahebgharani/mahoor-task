// @flow
import {SweetEntity, AccessManager, SweetFetcher} from 'Libs/one/sweet-one-react';

export default class DiscountcodeEntity extends SweetEntity {
    __moduleName='ecommerce';
    __tableName='discountcode';

    title='';
    code='';
    minOrderPrice='';
    amount='';
    percent='';
    description='';
    startdate='';
    enddate='';
    type={};
    count='';
    ecommerceUser={};
    scoreToRials='';
    fields={id:{field:'id'}
    ,title:{field:'title'}
    ,code:{field:'code'}
    ,minOrderPrice:{field:'minorderprice'}
    ,amount:{field:'amount_prc'}
    ,percent:{field:'percent_num'}
    ,scoreToRials:{field:'scoretorials'}
    ,description:{field:'description'}
    ,startdate:{field:'startdate_date'}
    ,enddate:{field:'enddate_date'}
    ,type:{field:'type_fid',type:'simplefield',simplefieldname:'discounttype'}
    ,count:{field:'count_num'}
    ,ecommerceUser:{field:'ecommerceuser_fid',type:'fid'}};
    constructor(ownerID){
        super();
        this.ecommerceUser={id:ownerID};
    }
    get(id,afterFetch){
        this._get(this.getBaseURL()+'/'+id,this.getServiceName(),AccessManager.VIEW,afterFetch);
    }
    findDiscountCode(code,orderPurePrice,afterFetch,onError){
        const url=this.getBaseURL()+'/findDiscountCode/'+code+'?orderpureprice='+orderPurePrice;
        new SweetFetcher().Fetch(url, SweetFetcher.METHOD_GET,null,
            data => {
                afterFetch(data.Data);
            },
            onError,this.getServiceName(),AccessManager.VIEW,
            null);
    }
    getAll(pageSize,page,sorted,filtered,afterFetch,onError){
        const filterAndSortString=this.getFilterString(pageSize,page,sorted,filtered);
        const url=this.getBaseURL()+filterAndSortString;
        this._getAll(url,this.getServiceName(),AccessManager.LIST,afterFetch,onError);
    }
    save(id,afterFetchListener,onError){
        const formData=this.getFormRequest();
        this._save(this.getServiceName(),this.getBaseURL(),id,formData,afterFetchListener,onError);
    }
    delete(id,afterFetchListener,onError){
        this._delete(this.getServiceName(),this.getBaseURL()+"/delete/"+id,afterFetchListener,onError);
    }
}
