import ProductEntity from "./ProductEntity";
import ProductkindEntity from "./ProductkindEntity";
import GroupEntity from "./GroupEntity";
import FactorreceiverEntity from "./FactorreceiverEntity";
import EcommerceuserEntity from "./EcommerceuserEntity";
import OrderpollEntity from "./OrderpollEntity";
import AtplaceorderEntity from "./AtplaceorderEntity";
import OrderEntity from "./OrderEntity";
import DiscountcodeEntity from "./DiscountcodeEntity";
import OrderType from "./OrderType";
import DeliveryType from "./DeliveryType";
export default {ProductEntity,
    GroupEntity,
    ProductkindEntity,
    FactorreceiverEntity,
    OrderEntity,
    AtplaceorderEntity,
    EcommerceuserEntity,
    OrderpollEntity,
    DiscountcodeEntity,
    OrderType,
    DeliveryType
};
