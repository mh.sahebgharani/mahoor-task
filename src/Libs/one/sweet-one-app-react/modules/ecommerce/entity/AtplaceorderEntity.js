// @flow
import {SweetEntity} from 'Libs/one/sweet-one-react';

export default class AtpalceorderEntity extends SweetEntity {
    __moduleName='ecommerce';
    __tableName='atplaceorder';
    personCount='';
    internalPlace='';
    startDateTime='';
    durationMinutes='';
    orderStatus='';
    updatedAt='';
    createdAt='';
    fields={
        id:{field:'id'},
        internalPlace:{field:'internalplace_fid'},
        personCount:{field:'personcount_num'},
        startDateTime:{field:'startdatetime'},
        durationMinutes:{field:'durationminutes_num'},
        orderStatus:{field:'atplaceorderstatus_fid'},
        updatedAt:{field:'updated_at'},
        createdAt:{field:'created_at'},

    };
}
