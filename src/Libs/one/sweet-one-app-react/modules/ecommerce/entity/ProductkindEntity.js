// @flow
import {SweetEntity,AccessManager,SweetFetcher} from 'Libs/one/sweet-one-react';
import FactorreceiverEntity from "./FactorreceiverEntity";

export default class ProductkindEntity extends SweetEntity {
    __moduleName='ecommerce';
    __tableName='productkind';

    code='';
    quality={};
    stock={};
    price='';
    count='';
    photo={};
    description='';
    product={};
    factorReceivers=[];
    fields={
        id:{field:'id'},
        code:{field:'code'},
        quality:{field:'quality',type:'simplefield',simplefieldname:'quality'}
        ,price:{field:'price_num'},
        count:{field:'count'},
        photo:{field:'photo_igu',type:'file'},
        description:{field:'description'},
        product:{field:'product_fid',type:'fid'},
        stock:{field:'stock',type:'fid'},
        factorReceivers:{field:'factorreceivers',type:'entityarray',object:new FactorreceiverEntity()}
    };
    constructor(ownerID){
        super();
        this.product={id:ownerID};
    }
    get(id,afterFetch){
        this._get(this.getBaseURL()+'/'+id,this.getServiceName(),AccessManager.VIEW,afterFetch);
    }
    getAll(pageSize,page,sorted,filtered,afterFetch,onError){
        const filterAndSortString=this.getFilterString(pageSize,page,sorted,filtered);
        const url=this.getBaseURL()+filterAndSortString;
        this._getAll(url,this.getServiceName(),AccessManager.LIST,afterFetch,onError);
    }
    save(id,afterFetchListener,onError){
        const formData=this.getFormRequest();
        this._save(this.getServiceName(),this.getBaseURL(),id,formData,afterFetchListener,onError);
    }
    addCount(id,count,afterFetchListener,onError){
        const data = new FormData();
        data.append('count',count);
        let method=SweetFetcher.METHOD_PUT;
        let action=AccessManager.EDIT;
        new SweetFetcher().Fetch('/ecommerce/productkind/addcount/'+id,method,data,
            res => {
                afterFetchListener(res);
            },onError,
            'addcount',action,
            null);
    }
    uploadExcel(file,afterFetchListener,onError){
        const data = new FormData();
        data.append('file', file);
        let method=SweetFetcher.METHOD_POST;
        let action=AccessManager.INSERT;
        new SweetFetcher().Fetch('/ecommerce/productkind/excelread',method,data,
            res => {
                afterFetchListener(res);
            },onError,
            'excelread',action,
            null);

    }
    batchUpdate(updateinfo,afterFetchListener,onError){
        const data = new FormData();
        data.append('rows', JSON.stringify(updateinfo));
        let method=SweetFetcher.METHOD_POST;
        let action=AccessManager.INSERT;
        new SweetFetcher().Fetch('/ecommerce/productkind/batchupdate',method,data,
            res => {
                afterFetchListener(res);
            },onError,
            'batchupdate',action,
            null);

    }
    static cleanStore(groupName,afterFetchListener,onError){
        const data = new FormData();
        let method=SweetFetcher.METHOD_POST;
        let action=AccessManager.INSERT;
        let url='/ecommerce/productkind/clearstore';
        if(groupName!=null)
            url=url+'/'+groupName;
        new SweetFetcher().Fetch(url,method,data,
            res => {
                afterFetchListener(res);
            },onError,
            'cleanstore',action,
            null);

    }

    static getProductsExcel(afterFetchListener,onError){
        const data = new FormData();
        let method=SweetFetcher.METHOD_POST;
        let action=AccessManager.INSERT;
        new SweetFetcher().Fetch('/ecommerce/productkind/getexcel',method,data,
            res => {
                afterFetchListener(res);
            },onError,
            'excel',action,
            null);

    }
    delete(id,afterFetchListener,onError){
        this._delete(this.getServiceName(),this.getBaseURL()+"/delete/"+id,afterFetchListener,onError);
    }
}
