// @flow
import {SweetEntity, AccessManager, SweetFetcher} from 'Libs/one/sweet-one-react';
import DiscountcodeEntity from "./DiscountcodeEntity";
import ProductkindEntity from "./ProductkindEntity";
import EcommerceuserEntity from "./EcommerceuserEntity";
import OrderpollEntity from "./OrderpollEntity";
import PlaceEntity from "../../location/entity/PlaceEntity";
import OrderType from "./OrderType";
import AtplaceorderEntity from "./AtplaceorderEntity";
import DeliveryType from "./DeliveryType";

export default class OrderEntity extends SweetEntity {
    __moduleName='ecommerce';
    __tableName='order';

    toPayPrice='';
    name='';
    mobile='';
    sendPrice='';
    paymenttype={};
    sendTime={};
    orderStatus={};
    transaction={};
    cancelTransaction={};
    discount='';
    userrate='';
    usercomment='';
    description='';
    address={};
    manualAddress={};
    productkinds=[];
    discountcodes=[];
    orderPoll={};
    delivery={};
    region={};
    orderType=OrderType.Online;
    deliveryType=DeliveryType.ByDelivery;
    atPlaceOrder={};
    updatedAt='';
    createdAt='';
    fields={
        id:{field:'id'},
        toPayPrice:{field:'topayprice'},
        name:{field:'name'},
        mobile:{field:'mobile'},
        sendPrice:{field:'sendprice'},
        paymenttype:{field:'paymenttype',type:'simplefield',simplefieldname:'paymenttype'},
        sendTime:{field:'sendtime',type:'fid'},
        orderStatus:{field:'orderstatus',type:'fid'},
        transaction:{field:'transaction_fid',type:'fid'},
        cancelTransaction:{field:'canceltransaction_fid',type:'fid'},
        discount:{field:'discount_num'},
        userrate:{field:'userrate_num'},
        usercomment:{field:'usercomment'},
        description:{field:'description'},
        orderType:{field:'ordertype_enum'},
        deliveryType:{field:'deliverytype_enum'},
        address:{field:'location_place_fid',type:'fid'},
        region:{field:'location_region_fid',type:'fid'},
        manualAddress:{field:'manualAddress',type:'entity',object:new PlaceEntity()},
        productkinds:{field:'productkinds',type:'entityarray',object:new ProductkindEntity()},
        orderPoll:{field:'orderpoll',type:'entity',object:new OrderpollEntity()},
        discountcodes:{field:'discountcodes',type:'entityarray',object:new DiscountcodeEntity()},
        updatedAt:{field:'updated_at'},
        createdAt:{field:'created_at'},
        pdfPath:{field:'pdfpath'},
        atPlaceOrder:{field:'atplaceorder',type:'entity',object:new AtplaceorderEntity()},
        delivery:{field:'delivery__ecommerceuser_fid',type:'fid',object:new EcommerceuserEntity()},

    };
    get(id,afterFetch){
        this._get(this.getBaseURL()+'/'+id,this.getServiceName(),AccessManager.VIEW,afterFetch);
    }
    getAllByDate(type,pageSize,page,sorted,filtered,afterFetch,onError){
        const filterAndSortString=this.getFilterString(pageSize,page,sorted,filtered);
        const url='ecommerce/myorders/'+type+filterAndSortString;
        this._getAll(url,this.getServiceName(),AccessManager.LIST,afterFetch,onError);
    }
    getAll(pageSize,page,sorted,filtered,afterFetch,onError){
        return;//Implement in Each App
    }
    getDeliveryOrders(pageSize,page,sorted,filtered,afterFetch,onError){
        const filterAndSortString=this.getFilterString(pageSize,page,sorted,filtered);
        const url='ecommerce/deliveryorders'+filterAndSortString;
        this._getAll(url,this.getServiceName(),AccessManager.LIST,afterFetch,onError);
    }
    getAllPDF(pageSize,page,sorted,filtered,afterFetch,onError){
        const filterAndSortString=this.getFilterString(pageSize,page,sorted,filtered);
        const url='ecommerce/orderpdf'+filterAndSortString;

        let method=SweetFetcher.METHOD_GET;
        let action=AccessManager.VIEW;
        new SweetFetcher().Fetch(url,method,null,
            res => {
                afterFetch(res.Data);
            },onError,
            'getorderpdf',action,
            null);
    }
    save(id,afterFetchListener,onError){
        return;//Implement in Each App
    }
    setDelivery(id,afterFetchListener,onError){
        const formData=this.getFormRequest();
        this._save(this.getServiceName(),this.getBaseURL()+'/setdelivery',id,formData,afterFetchListener,onError);
    }
    makeExcel(afterFetchListener,onError){
        let method=SweetFetcher.METHOD_POST;
        let action=AccessManager.INSERT;
        new SweetFetcher().Fetch('/ecommerce/order/makeexcel',method,null,
            res => {
                afterFetchListener(res);
            },onError,
            'makeexcel',action,
            null);
    }
    static setStatus(id,newStatus,afterFetchListener,onError){
        let formData=new FormData;
        formData.append('id',id);
        formData.append('orderstatus',newStatus);
        new SweetFetcher().Fetch("/ecommerce/order/setstatus/"+id,SweetFetcher.METHOD_PUT,formData,
            res => {
                afterFetchListener(res);
            },onError,
            'ecommerce.order.setstatus',AccessManager.EDIT,
            null);
    }
    delete(id,afterFetchListener,onError){
        this._delete(this.getServiceName(),this.getBaseURL()+"/"+id,afterFetchListener,onError);
    }
    static getDayReport(year,month,day,afterFetchListener,onError){
        let method=SweetFetcher.METHOD_POST;
        let action=AccessManager.INSERT;
        new SweetFetcher().Fetch('/ecommerce/order/dayreport/'+year+'/'+month+'/'+day,method,null,
            res => {
                afterFetchListener(res);
            },onError,
            'dayreport',action,
            null);

    }
    getDateIntervalReport(startyear,startmonth,startday,endyear,endmonth,endday,filtered,afterFetchListener,onError){
        let method=SweetFetcher.METHOD_POST;
        let action=AccessManager.INSERT;
        const filterAndSortString=this.getFilterString(null,null,null,filtered);

        new SweetFetcher().Fetch('/ecommerce/order/interval-report/'+startyear+'/'+startmonth+'/'+startday+'/'+endyear+'/'+endmonth+'/'+endday+filterAndSortString,method,null,
            res => {
                afterFetchListener(res);
            },onError,
            'interval-report',action,
            null);

    }
    saveManualOrder(id,afterFetchListener,onError){
        const formData=this.getFormRequest();
        this._save(this.getServiceName(),this.getBaseURL()+"/manual",id,formData,afterFetchListener,onError);
    }
    getServicingStatus(afterFetchListener,onError){
        new SweetFetcher().Fetch('/ecommerce/getServicingStatus',SweetFetcher.METHOD_GET,null
            ,afterFetchListener,onError);
    }

    saveAtPlaceOrder(id,afterFetchListener,onError){
        const formData=this.getFormRequest();
        this._save(this.getServiceName(),this.getBaseURL()+"/atplace",id,formData,afterFetchListener,onError);
    }
    callPager(id,afterFetchListener,onError){
        new SweetFetcher().Fetch('/ecommerce/order/callpager/'+id,SweetFetcher.METHOD_POST,null,afterFetchListener
        ,onError);
    }
    saveDeliverAtPlaceOrder(id,afterFetchListener,onError){
        const formData=this.getFormRequest();
        this._save(this.getServiceName(),this.getBaseURL()+"/deliveratplace",id,formData,afterFetchListener,onError);
    }
}
