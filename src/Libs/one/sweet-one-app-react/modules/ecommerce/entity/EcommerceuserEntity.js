// @flow
import { SweetEntity, AccessManager, SweetFetcher } from 'Libs/one/sweet-one-react';
import PlaceEntity from '../../location/entity/PlaceEntity';

export default class EcommerceuserEntity extends SweetEntity {
    __moduleName = 'ecommerce';
    __tableName = 'ecommerceuser';

    friendUser = {};
    user = {};
    defaultAddress = {};
    roles = [];
    deliveredOrders = '';
    deliveredOrdersSum = '';
    code = '';
    created_at = '';
    fields = {
        id: { field: 'id' },
        friendUser: { field: 'frienduser_fid', type: 'fid' },
        user: { field: 'user', type: 'fid' },
        defaultAddress: { field: 'defaultaddress', type: 'entity', object: new PlaceEntity() },
        roles: { field: 'roles' },
        deliveredOrders: { field: 'deliveredorders' },
        deliveredOrdersSum: { field: 'deliveredorderssum', },
        code: { field: 'code', },
        created_at: { field: 'created_at' }
    };
    get(id, afterFetch) {
        this._get(this.getBaseURL() + '/' + id, this.getServiceName(), AccessManager.VIEW, afterFetch);
    }
    getAll(pageSize, page, sorted, filtered, afterFetch, onError) {
        // console.log(filtered);
        const filterAndSortString = this.getFilterString(pageSize, page, sorted, filtered);
        const url = this.getBaseURL() + filterAndSortString;
        this._getAll(url, this.getServiceName(), AccessManager.LIST, afterFetch, onError);
    }
    getDeliveries(pageSize, page, sorted, filtered, afterFetch, onError) {
        // console.log(filtered);
        const filterAndSortString = this.getFilterString(pageSize, page, sorted, filtered);
        const url = this.getBaseURL() + '/deliveries' + filterAndSortString;
        this._getAll(url, this.getServiceName(), AccessManager.LIST, afterFetch, onError);
    }
    getAllDeliveries(afterFetch, onError) {
        // console.log(filtered);
        const filterAndSortString = this.getFilterString(null, null, null, null);
        const url = this.getBaseURL() + '/deliveries' + filterAndSortString;
        this._getAll(url, this.getServiceName(), AccessManager.LIST, afterFetch, onError);
    }
    save(id, afterFetchListener, onError) {
        const formData = this.getFormRequest();
        this._save(this.getServiceName(), this.getBaseURL(), id, formData, afterFetchListener, onError);
    }
    saveManual(id, afterFetchListener, onError) {
        const formData = this.getFormRequest();
        if(id>0)
            this._save(this.getServiceName(), this.getBaseURL() + "/manualedit", id, formData, afterFetchListener, onError);
        else
            this._save(this.getServiceName(), this.getBaseURL() + "/manualadd", id, formData, afterFetchListener, onError);
    }
    setDelivery(id, afterFetch, onError) {
        new SweetFetcher().Fetch('/ecommerce/ecommerceuser/setdelivery/' + id, SweetFetcher.METHOD_PUT, null, afterFetch, onError);
    }
    retractDelivery(id, afterFetch, onError) {
        new SweetFetcher().Fetch('/ecommerce/ecommerceuser/retractdelivery/' + id, SweetFetcher.METHOD_PUT, null, afterFetch, onError);
    }
    deliveries(afterFetch, onError) {
        new SweetFetcher().Fetch('/ecommerce/ecommerceuser/deliveries', SweetFetcher.METHOD_GET, null, afterFetch, onError);
    }
    delete(id, afterFetchListener, onError) {
        this._delete(this.getServiceName(), this.getBaseURL() + "/delete/" + id, afterFetchListener, onError);
    }
}
