// @flow
import {SweetEntity,AccessManager} from 'Libs/one/sweet-one-react';

export default class OrderType {
    static Online=1;
    static Delivery=2;
    static Manual=3;
    static Unknown=0;
    static getDisplayName(Type){
        switch (Type) {
            case this.Online:
                return 'آنلاین';
            case this.Delivery:
                return 'پیک';
            case this.Manual:
                return 'حضوری';
            case this.Unknown:
                return 'نامشخص';
        }
    }
};
