// @flow
import {SweetEntity,AccessManager} from 'Libs/one/sweet-one-react';

export default class OrderpollEntity extends SweetEntity {
    __moduleName='ecommerce';
    __tableName='orderpoll';

    order={};
    overallRate='';
    deliveryRate='';
    comment='';
    fields={id:{field:'id'},order:{field:'order_fid',type:'fid'},overallRate:{field:'overallrate_num'},deliveryRate:{field:'deliveryrate_num'},comment:{field:'comment'}};
    getByOrder(orderID,afterFetch){
        this._get(this.getBaseURL()+'/'+orderID,this.getServiceName(),AccessManager.VIEW,afterFetch);
    }

    saveByOrder(orderID,afterFetchListener,onError){
        const formData=this.getFormRequest();
        formData.append('order_fid',orderID);
        this._save(this.getServiceName(),this.getBaseURL(),undefined,formData,afterFetchListener,onError);
    }
    get(id,afterFetch){
        this._get(this.getBaseURL()+'/'+id,this.getServiceName(),AccessManager.VIEW,afterFetch);
    }
    getAll(pageSize,page,sorted,filtered,afterFetch,onError){
        const filterAndSortString=this.getFilterString(pageSize,page,sorted,filtered);
        const url=this.getBaseURL()+filterAndSortString;
        this._getAll(url,this.getServiceName(),AccessManager.LIST,afterFetch,onError);
    }
    save(id,afterFetchListener,onError){
        const formData=this.getFormRequest();
        this._save(this.getServiceName(),this.getBaseURL(),id,formData,afterFetchListener,onError);
    }
    delete(id,afterFetchListener,onError){
        this._delete(this.getServiceName(),this.getBaseURL()+"/"+id,afterFetchListener,onError);
    }
}
