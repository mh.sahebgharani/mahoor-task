
/**
 * @flow
 */
export default class DeliveryType {
    static ByDelivery=1;
    static UseAtPlace=2;
        static GetAtPlace=3;
        static Unknown=0;

    static getDisplayName(Type){
        switch (Type) {
            case this.ByDelivery:
                return 'توسط پیک';
            case this.UseAtPlace:
                return 'سرو در محل';
            case this.GetAtPlace:
                return 'دریافت در محل';
            case this.Unknown:
                return 'نامشخص';
        }
    }
}

