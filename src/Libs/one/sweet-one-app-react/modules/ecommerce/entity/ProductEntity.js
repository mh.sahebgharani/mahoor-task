// @flow
import {SweetEntity,AccessManager,SweetFetcher} from 'Libs/one/sweet-one-react';
import ProductkindEntity from "./ProductkindEntity";
import {Common} from 'Libs/one/sweet-react-common-tools';
import GroupEntity from "./GroupEntity";

export default class ProductEntity extends SweetEntity {
    __moduleName='ecommerce';
    __tableName='product';

    name='';
    displayName='';
    minCount='';
    group={};
    unit={};
    minOrderCount='';
    priority='';
    isVisibleToUser='';
    description='';
    discount='';
    likes='';
    liked='0';
    productKinds=[];
    fields={
        id:{field:'id'},
        name:{field:'name'},
        displayName:{field:'displayname'},
        minCount:{field:'mincount_num'},
        description:{field:'description_te'},
        priority:{field:'priority_num'},
        isVisibleToUser:{field:'is_visibletouser'},
        group:{field:'group_fid',type:'fid'},
        unit:{field:'unit_fid',type:'simplefield',simplefieldname:'unit'},
        minOrderCount:{field:'minordercount_num'},
        discount:{field:'discount_num'},
        likes:{field:'likes_num'},
        liked:{field:'liked'},
        productKinds:{field:'productkinds',type:'entityarray',object:new ProductkindEntity()}
    };
    get(id,afterFetch){
        this._get(this.getBaseURL()+'/'+id,this.getServiceName(),AccessManager.VIEW,afterFetch);
    }
    getAll(pageSize,page,sorted,filtered,afterFetch,onError){
        const filterAndSortString=this.getFilterString(pageSize,page,sorted,filtered);
        const url=this.getBaseURL()+filterAndSortString;
        this._getAll(url,this.getServiceName(),AccessManager.LIST,afterFetch,onError);
    }
    save(id,afterFetchListener,onError){
        const formData=this.getFormRequest();
        this._save(this.getServiceName(),this.getBaseURL(),id,formData,afterFetchListener,onError);
    }
    changePriority(id,step,afterFetchListener,onError){
        const url=this.getBaseURL()+'/changepriority/'+id+'?step='+step;
        new SweetFetcher().Fetch(url,SweetFetcher.METHOD_PUT,null,
            res => {
                afterFetchListener(res);
            },onError,
            this.getServiceName(),AccessManager.EDIT,
            null);
    }
    changeVisiblity(id,visible,afterFetchListener,onError){
        const url=this.getBaseURL()+'/changevisiblity/'+id+'?visible='+(visible?'1':'0');
        new SweetFetcher().Fetch(url,SweetFetcher.METHOD_PUT,null,
            res => {
                afterFetchListener(res);
            },onError,
            this.getServiceName(),AccessManager.EDIT,
            null);
    }
    SetLike(id,isLiking,afterFetchListener,onError){
        const url=this.getBaseURL()+'/like/'+id+'/'+(isLiking?'1':'0');
        new SweetFetcher().Fetch(url,SweetFetcher.METHOD_PUT,null,
            res => {
                afterFetchListener(res);
            },onError,
            this.getServiceName(),AccessManager.EDIT,
            null);
    }
    delete(id,afterFetchListener,onError){
        this._delete(this.getServiceName(),this.getBaseURL()+"/delete/"+id,afterFetchListener,onError);
    }
    getAllByGroups(pageSize,page,sorted,filtered,afterFetch,onError){
        const filterAndSortString=this.getFilterString(pageSize,page,sorted,filtered);
        const url=this.getBaseURL()+'/bygroup'+filterAndSortString;
        new SweetFetcher().Fetch(url, SweetFetcher.METHOD_GET, null,
            data => {
                console.log(data);
                let dataObjectArray=[];
                for(let i=0;i<data.Data.length;i++) {
                    data.Data[i] = Common.convertNullKeysToEmpty(data.Data[i]);

                    let products=data.Data[i]['products'];
                    let productObjects=[];
                    for(let j=0;j<products.length;j++) {
                        productObjects[j] = new this.constructor();
                        productObjects[j].loadFromRetrievedObject(products[j]);
                    }
                    dataObjectArray[i]=new GroupEntity();
                    dataObjectArray[i].loadFromRetrievedObject(data.Data[i].group);
                    dataObjectArray[i].products=productObjects;
                    // console.log(data.Data[i]);

                }
                afterFetch(dataObjectArray,data.RecordCount);
            },
            onError,this.getServiceName(),AccessManager.LIST,null);
    }
}
