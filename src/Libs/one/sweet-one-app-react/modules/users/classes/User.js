// @flow
import {SweetFetcher} from 'Libs/one/sweet-one-react';
import {Common} from 'Libs/one/sweet-react-common-tools';

export default class User {
    DefaultRole = "";
    AppName = "";

    sendVerifySMS(mobile, afterFetchListener, onError) {
        const data = new FormData();
        data.append('phone', mobile);
        data.append('appName', this.AppName);
        data.append('role', this.DefaultRole);
        new SweetFetcher().Fetch('/ecommerce/ecommerceuser/sendcode', SweetFetcher.METHOD_POST, data, data => {
            afterFetchListener(data);
        }, (error) => {
            if (onError != null)
                onError(error);
        }, 'users', 'sendverificationcode', null);
    }

    save(name, profilePhoto, birthDate, afterFetchListener, onError) {
        const data = new FormData();
        data.append('name', name);
        data.append('photo_igu', profilePhoto);
        data.append('birth_date', birthDate);
        new SweetFetcher().Fetch('/ecommerce/ecommerceuser/save', SweetFetcher.METHOD_PUT, data, data => {
            this.setName(name);
            this.setBirthDate(birthDate);
            afterFetchListener(data);
        }, (error) => {
            if (onError != null)
                onError(error);
        }, 'users', 'save', null);
    }

    verifyCodeAndLogin(name, mobile, friendMobile, code, isnew, afterFetchListener, onError) {
        const data = new FormData();
        data.append('phone', mobile);
        data.append('friendphone', friendMobile);
        data.append('name', name);
        data.append('code', code);
        data.append('isnew', isnew);
        data.append('appName', this.AppName);
        data.append('role', this.DefaultRole);
        new SweetFetcher().Fetch('/ecommerce/ecommerceuser/verifycode', SweetFetcher.METHOD_POST, data, data => {
            const dt = data.Data;
            this.login(dt.userid, dt.ecommerceuserid, dt.sessionkey, dt.displayname, dt.photo, dt?.birth_date, dt.phone, dt.roles, dt.access, afterFetchListener);
        }, (error) => {
            onError()
        }, 'users', 'loginbyphone', null);
    }

    checkloginAndRedirect(history, afterLoginPath) {
        if(!this.isLoggedIn())
            history.push('/register/' + afterLoginPath);

    }
    isLoggedIn() {
        const skey = localStorage.getItem('sessionkey');
        return !(skey == null || skey.length <= 0)


    }

    getUserID() {
        return localStorage.getItem('userid');
    }

    getEcommerceUserID() {
        return localStorage.getItem('ecommerceuserid');
    }

    getName() {
        return localStorage.getItem('userdisplayname');
    }

    setName(name) {
        localStorage.setItem('userdisplayname', name);

    }

    setBirthDate(date) {
        localStorage.setItem('userbirthdate', date);

    }

    getProfilePhoto() {
        return localStorage.getItem('userprofilephoto');
    }

    setProfilePhoto(name) {
        localStorage.setItem('userprofilephoto', name);

    }

    getMobile() {
        return localStorage.getItem('userphone');
    }

    getBirthDate() {
        return localStorage.getItem('userbirthdate');
    }

    getBirthDateObject() {
        const dateString = this.getBirthDate();
        if (!dateString && !dateString.trim())
            return "";
        const dateArr = dateString.split("-");
        if (dateArr.length !== 3)
            return "";
        return {year: dateArr[0] * 1, month: dateArr[1] * 1, day: dateArr[2] * 1};
    }

    login(userid, ecommerceuserid, token, name, userprofilephoto, userBirthDate, phone, roles, access, afterDoneListener) {
        localStorage.setItem('sessionkey', token);
        localStorage.setItem('userdisplayname', name);
        localStorage.setItem('userprofilephoto', userprofilephoto);
        localStorage.setItem('userbirthdate', userBirthDate);
        localStorage.setItem('userphone', phone);
        localStorage.setItem('userid', userid);
        localStorage.setItem('userroles', roles);
        localStorage.setItem('ecommerceuserid', ecommerceuserid);

        const theAccess = Common.convertObjectPropertiesToLowerCase(access);
        let key, keys = Object.keys(theAccess);
        let n = keys.length;
        while (n--) {
            key = keys[n];
            localStorage.setItem('access.' + theAccess[key].name, true);
        }
        afterDoneListener();
    }

    logout(afterDoneListener) {
        localStorage.removeItem('sessionkey');
        localStorage.removeItem('userdisplayname');
        localStorage.removeItem('userprofilephoto');
        localStorage.removeItem('userphone');
        localStorage.removeItem('userid');
        localStorage.removeItem('userroles');
        localStorage.removeItem('ecommerceuserid');
        localStorage.removeItem('userid');
        afterDoneListener();
    }
}
