// @flow
import * as React from 'react';
import {SweetManagePage} from 'Libs/one/sweet-one-react';
import PageEntity from "../../entity/PageEntity";


export default class ContentPageViewController extends SweetManagePage {
    moduleName='content';
    tableName='page';
    constructor(props) {
        super(props);
        this.state = {
            formData:new PageEntity(),

        };
    }
    componentDidMount() {
        super.componentDidMount();
        if(this.getID()!=null)
            this.state.formData.getByName(this.getID(), () => {this.notifyFormDataChanged();});

    }
}
