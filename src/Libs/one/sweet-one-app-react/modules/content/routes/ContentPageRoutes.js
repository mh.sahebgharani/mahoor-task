
import ContentPageList from '../pages/page/ContentPageList';
import ContentPageManage from '../pages/page/ContentPageManage';
import ContentPageView from '../pages/page/ContentPageView';
let ContentPageRoutes=[];
ContentPageRoutes.push({ path: '/content/page/:id',exact:false, name: 'برگه',component:ContentPageView});
export default ContentPageRoutes;
