/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React from 'react';
import ModernPickerBox from "./ModernPickerBox";

export default class SweetMultiPicker extends ModernPickerBox {

    isMultiSelect=true;
    onChange(val,act){
        this.props.onValueChange(val,act);
    }

    getValue(Options){
        const selectedValues=this.props.selectedValues;
        let SelectedOptions=[];
        Options.forEach(option=>{
           let isSelected=false;
            selectedValues.forEach(selectedValue=>{
                if(option.value===selectedValue)
                    isSelected=true;
            });
            if(isSelected)
                SelectedOptions.push(option);
        });

        return SelectedOptions;
    }
}

