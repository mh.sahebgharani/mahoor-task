/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React from 'react';
import LoadingAnimation from "../LoadingAnimation";
export default class ActivityIndicator extends React.Component {
    render() {

        return (
            <div {...this.props} className={'activityindicator'}>
                {this.props.waiting && <LoadingAnimation/> }
            </div>);
    }
}

