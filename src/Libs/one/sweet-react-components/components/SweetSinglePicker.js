/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React from 'react';
import ModernPickerBox from "./ModernPickerBox";
export default class SweetSinglePicker extends ModernPickerBox {

    isMultiSelect=false;
    onChange(val){
        if(val==null)
            this.props.onValueChange(val);
        else
            this.props.onValueChange(val.value);
    }
    getValue(Options){
        const selected=Options.filter(a=>a.value==this.props.selectedValue);
        return selected[0];
    }
}

