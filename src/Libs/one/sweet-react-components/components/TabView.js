/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
import React from 'react';
import { MDBContainer, MDBNav, MDBNavItem, MDBNavLink, MDBTabContent, MDBTabPane} from "mdbreact";
export default class TabView extends React.Component {


    toggle = tab => e => {
        if (this.props.activeItem !== tab) {
            this.props.toggle(tab);
        }
    };
  render() {
      const propsChildren=this.props.children;
      let tabs = [];
      if(propsChildren!=null) {
          const childCount = propsChildren.length;
          for (let i = 0; i < childCount; i++) {
              const theChild = propsChildren[i];
              if (theChild && theChild.props != null) {
                  tabs[tabs.length] = <MDBTabPane tabId={(tabs.length+1)+""} role="tabpanel">{theChild}</MDBTabPane>;
              }
          }
      }
      let tabTitles=this.props.tabTitles.map((title,index)=>{
          return <MDBNavItem>
              <div className={'tab-title'} onClick={this.toggle((index+1)+"")} role="tab" >
                  {title}
              </div>
          </MDBNavItem>
      });
      let maxTabs=this.props.maxTabCount||100;
    return (<MDBContainer>
        <MDBNav className="nav-tabs mt-5">
            {tabTitles}
            {(tabTitles.length) < maxTabs &&
            <div className={'tab-title'} onClick={this.props.onAddRequest} role="tab">
                <span className={'fa fa-plus-circle'}/>
            </div>
            }
        </MDBNav>
        <MDBTabContent activeItem={this.props.activeItem+""} >
            {tabs}
        </MDBTabContent>
    </MDBContainer>);
  }
}

