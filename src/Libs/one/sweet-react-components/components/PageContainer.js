/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import ActivityIndicator from "./nativeAdapter/ActivityIndicator";
export default class PageContainer extends Component {
    render() {
        console.log(this.props.isEmpty);
        return (
            <div className={'pagecontainer'}>
                {this.props.isLoading &&
                <div className={'pagecontainer_loadingbox'}>
                    {global.loadingImage == null &&
                    <ActivityIndicator animating={this.props.isLoading} size="large"
                                       color="#000000" waiting={true}/>
                    }
                    {global.loadingImage != null && this.props.isLoading &&
                        <img src={global.loadingImage} className={'page_loading_image'}/>
                    }
                </div>
                }

                {!this.props.isLoading && this.props.isEmpty &&
                <div className={'pagecontainer_empty_container'}>
                    {this.props.renderEmpty != null &&
                        this.props.renderEmpty()
                    }
                    {this.props.renderEmpty == null &&
                    <div className={'pagecontainer_empty_text'}>{this.props.emptyText != null ? this.props.emptyText : 'هیچ محتوایی برای نمایش وجود ندارد'} </div>
                    }
                </div>
                }
                {!this.props.isLoading && !this.props.isEmpty &&
                    this.props.children
                }
            </div>
        );
    }
}
