/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
import 'react-modern-calendar-datepicker/lib/DatePicker.css';
import React, { Component } from 'react';
import Calendar from 'react-modern-calendar-datepicker';
import {MDBModal} from "mdbreact";
import View from "./nativeAdapter/View";

export default class SweetDatePickerModal extends Component{

    render() {
        return (
            <MDBModal isOpen={this.props.visible} transparent={true} animationType={'fade'} toggle={this.props.onRequestClose}>
                <View style={{width:'100%',height:'100%',justifyContent:'center',alignItems:'center'}}>
                <View >
                    <Calendar
                        onChange={this.props.onDateChange}
                        shouldHighlightWeekends
                        locale="fa" // add this
                    />

                </View>
                </View>
            </MDBModal>);
    }
}

SweetDatePickerModal.defaultProps = {
};
