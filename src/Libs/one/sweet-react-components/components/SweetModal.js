/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {MDBModal} from "mdbreact";
import TouchableHighlight from "./nativeAdapter/TouchableHighlight";
import View from "./nativeAdapter/View";
export default class SweetModal extends Component{
    render() {
        return (
            <MDBModal centered transparent={true} animationType={'fade'} isOpen={this.props.visible} {...this.props} toggle={this.props.onHideRequest}>
                            <View className={'sweet-react-components-sweet-modal'} >
                            {this.props.children}
                        </View>
            </MDBModal>);
    }
}
