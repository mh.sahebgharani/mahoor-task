/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React from 'react';
import DatePicker from "react-modern-calendar-datepicker";
import moment from 'moment-jalaali';
export default class DateSelectorBox extends React.Component {
    constructor(props){
        super(props);
    }
    stringToDateObject=(str)=>{
        let dateObject=null;
        if(str!=null && str.trim().length>0) {
            const dateMoment = moment(str, 'jYYYY/jM/jD');
            dateObject={day:dateMoment.format('jD')*1,month:dateMoment.format('jM')*1,year:dateMoment.format('jYYYY')*1}
        }
        // console.log(dateObject);
        return dateObject;
    };
    render() {
        const renderCustomInput = ({ ref }) =>{
            return <input
                ref={ref} // necessary
                placeholder="انتخاب تاریخ"
                {...this.props}
                onClick={()=>{this.setState({displayModal:true})}}
                className='form-control date'
                id={this.props.id}
                readOnly={true}

                group
                validate
                value={this.props.value}
            />};
        console.log(this.stringToDateObject(this.props.value));
        return (
            <div className='form-group dateselectorbox'>
                <label htmlFor={this.props.id}>{this.props.title}</label>
                <DatePicker
                    value={this.stringToDateObject(this.props.value)}
                    onChange={(dateObject)=>{
                        let dateStr1=dateObject.year+'/'+this.addO(dateObject.month)+'/'+this.addO(dateObject.day);
                        console.log(dateStr1);
                        // const dateMoment = moment(dateStr1, 'jYYYY/jM/jD');
                        this.props.onChange(dateStr1);
                    }}
                    renderInput={renderCustomInput} // render a custom input
                    shouldHighlightWeekends
                    locale="fa" // add this
                />
            </div>
        );
    }
    addO(num){
        if((num+'').length===1)
            return "0"+num;
        return num+"";
    }
}

