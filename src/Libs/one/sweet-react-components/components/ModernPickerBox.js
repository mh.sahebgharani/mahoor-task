/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React from 'react';
import Select from 'react-select'
import PickerBox from "./PickerBox";

export default class ModernPickerBox extends PickerBox {

    isMultiSelect=false;
    init(){
        super.init();
    };

    getItem=(item,itemTitle,itemValue,itemLogo)=>{
       return  { value: itemValue, label: itemTitle };
    };
    onChange(val){
        return -1;
    }
    getValue(){
        return {value:-1};
    }
    render() {
        let OptionViews=this.getItemViews();
        const isSearchable=this.props.isSearchable||true;
        const isRtl=this.props.isRtl||true;
        const placeHolder=this.props.placeHolder||"انتخاب کنید...";
        return (
            <div className='form-group'>
                <label htmlFor={this.props.name}>{this.props.title}</label>
                <Select
                    id={this.props.name}
                    name={this.props.name}
                    disabled={this.props.readOnly}
                    options={OptionViews}
                    isSearchable={isSearchable}
                    placeholder={placeHolder}
                    isRtl={isRtl}
                    isMulti={this.isMultiSelect}
                    value={this.getValue(OptionViews)}
                    onChange={(val,act)=>{
                        this.onChange(val,act)}}>
                </Select>
            </div>);
    }
}

