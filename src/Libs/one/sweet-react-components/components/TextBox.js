/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React from 'react';
import { Editor } from '@tinymce/tinymce-react';
import SweetButton from './sweet-button';
export default class TextBox extends React.Component {
    render() {
        let inputType='text';
        let pattern=undefined;
        if(this.props.keyboardType!=null)
        {
            if(this.props.keyboardType.trim().toLowerCase()==='numeric'){
                inputType='number';
                pattern="[0-9]*";
            }
        }
        if(this.props.type==='password')
            inputType='password';
        const className=this.props.className||'';
        const buttonClassName=this.props.buttonTitle!=null?'withbutton':'';
        const simpleTextBox=<input
            id={this.props.id}
            type={inputType}
            readOnly={this.props.readOnly}
            pattern={pattern}
            group
            validate
            value={this.props.value}
            onChange={(event) =>{
                if(this.props.onChangeText!=null)
                    this.props.onChangeText(event.target.value);
            }}
            {...this.props}
            className={'form-control '+className}/>;
        const buttonedTextBox=<div className="md-form input-group mt-0 mb-3">
            <div className="input-group-prepend">
                <SweetButton className="inputbutton" value={this.props.buttonTitle} onButtonPress={this.props.onButtonPress}/>
            </div>
            {simpleTextBox}
        </div>;
        return (
            <div className={'form-group '+buttonClassName}>
                <label htmlFor={this.props.id}>{this.props.title}</label>

                {(this.props.multiline == null || this.props.multiline == false) && this.props.buttonTitle!=null &&
                    buttonedTextBox
                }
                {(this.props.multiline == null || this.props.multiline == false) && this.props.buttonTitle==null &&
                    simpleTextBox
                }
                {this.props.multiline != null && this.props.multiline && (this.props.simpleMode!=null && this.props.simpleMode) &&
                <textarea
                    className='form-control'
                    id={this.props.id}
                    readOnly={this.props.readOnly}
                    group
                    value={this.props.value}
                    onChange={(event) =>{
                        if(this.props.onChangeText!=null)
                            this.props.onChangeText(event.target.value);
                    }} {...this.props}
                />
                }
                {this.props.multiline != null && this.props.multiline && (this.props.simpleMode==null || !this.props.simpleMode) &&
                <Editor
                    apiKey={'efk2g300fptq9ram563eyvv8lod8xvofyvdhbzvjtdl7qiwj'}
                    className='form-control'
                    id={this.props.id}
                    readOnly={this.props.readOnly}
                    group
                    config={this.editorConfiguration}
                    initialValue={'در حال بارگذاری...'}
                    value={this.props.value}
                    onEditorChange={(content, editor)=>{
                        if(this.props.onChangeText!=null)
                            this.props.onChangeText(content);
                    }}
                />
                }
                </div>
        );
    }
}

