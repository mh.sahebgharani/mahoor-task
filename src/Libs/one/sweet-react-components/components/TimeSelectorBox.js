/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React from 'react';
import SweetModal from "./SweetModal";
import TimeKeeper from "react-timekeeper";
import {MDBModal} from "mdbreact";

export default class TimeSelectorBox extends React.Component {
    constructor(props){
        super(props);
        this.state={
            displayModal:false,
        }
    }
    render() {
        let timeText="00:00";
        let time=this.props.value||"0";
        time=time%1440;
        if(time!=='0'){
            let hour= Math.floor(time/60)%24+"";
            let min=time%60+"";
            if(hour.length===1)
                hour="0"+hour;
            if(min.length===1)
                min="0"+min;
            timeText=hour+":"+min;
        }
        console.log(timeText);
        return (
            <div className='form-group timeselectorbox'>
                <MDBModal centered transparent={true} animationType={'fade'} isOpen={this.state.displayModal}
                          toggle={()=>
                    this.setState({displayModal:false})
                }>
                    <TimeKeeper
                        time={timeText}
                        forceCoarseMinutes={true}
                        hour24Mode={true}
                        switchToMinuteOnHourSelect={true}
                        doneButton={(newTime) => (
                            <a><div
                                className={'timeselectbutton'}
                                onClick={() => {
                                    this.setState({displayModal:false});
                                    this.props.onChange(newTime.hour*60+newTime.minute);
                                }}
                            >
                                انتخاب
                            </div></a>
                        )}
                    />
                </MDBModal>
                <label htmlFor={this.props.id}>{this.props.title}</label>
                <input
                    {...this.props}
                    onClick={()=>{this.setState({displayModal:true})}}
                    className='form-control time'
                    id={this.props.id}
                    readOnly={true}
                    group
                    validate
                    value={timeText}
                     />
            </div>
        );
    }
}

