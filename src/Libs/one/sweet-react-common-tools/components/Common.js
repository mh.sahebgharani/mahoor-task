// @flow
import * as React from 'react';
import Cookies from 'universal-cookie';
class Common{

    static convertObjectPropertiesToLowerCase(obj)
    {
        let key, keys = Object.keys(obj);
        let n = keys.length;
        let newobj={};
        while (n--) {
            key = keys[n];
            newobj[key.toLowerCase()] = obj[key];
        }
        return newobj;
    }
    static ObjectToIdValueArray(theObject)
    {
        let key, keys = Object.keys(theObject);
        let n = keys.length;
        let resultArray=[];
        while (n--) {
            key = keys[n];
            resultArray[n]={id:key.toLowerCase(),value:theObject[key]};
        }
        return resultArray;
    }
    static convertNullKeysToEmpty(obj)
    {
        let key, keys = Object.keys(obj);
        let n = keys.length;
        let newobj={};
        while (n--) {
            key = keys[n];
            let content=obj[key];
            if(content==null || content==="null" || ((typeof(content)==='string' || typeof(content)==='number') && content.length>0 && content.trim()==='null'))
                newobj[key] = '';
            else
                newobj[key] = content;
        }
        return newobj;
    }
    static getObjectCopy(obj)
    {
        let key, keys = Object.keys(obj);
        let n = keys.length;
        let newobj={};
        while (n--) {
            key = keys[n];
            newobj[key]=obj[key];
        }
        return newobj;
    }
    static clearAllCookies()
    {
        let cookies=new Cookies();
        let AllCookies=cookies.getAll();
        let keys = Object.keys(AllCookies);
        for(let key of keys)
            cookies.remove(key);
    }


    static getIdValueArrayFromSortArray(Sorted)
    {
        let IdValueArray=[];
        for(let i=0;Sorted!=null && i<Sorted.length;i++)
            IdValueArray.push({id:Sorted[i].id+'__sort',value:Sorted[i].desc?'1':'0'});
        return IdValueArray;
    }
    static getStringValue(Variable)
    {
        if(Variable===false)
            return "0";
        else if(Variable===true)
            return "1";
        else
            return Variable;
    }
    static isObjectsEquivalent(a, b) {
        // Create arrays of property names
        let aProps = Object.getOwnPropertyNames(a);
        let bProps = Object.getOwnPropertyNames(b);

        // If number of properties is different,
        // objects are not equivalent
        if (aProps.length != bProps.length) {
            return false;
        }

        for (let i = 0; i < aProps.length; i++) {
            let propName = aProps[i];

            // If values of same property are not equal,
            // objects are not equivalent
            if (a[propName] !== b[propName]) {
                return false;
            }
        }

        // If we made it this far, objects
        // are considered equivalent
        return true;
    }
    static StringReplaceAll(str,find,replace){
        let lastStr='';
        while(lastStr!==str){
            lastStr=str;
            str=str.replace(find,replace);
        }
        return str;
    }
}

export default Common;
