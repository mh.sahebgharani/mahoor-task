// @flow
import * as React from 'react';
class SweetDate{

    static weekDayToString(dayNum)
    {
        let dayNumber=dayNum*1;
        dayNumber=(dayNumber+2)%7;
        switch (dayNumber) {
            case 1:
                return 'شنبه';
            case 2:
                return 'یکشنبه';
            case 3:
                return 'دوشنبه';
            case 4:
                return 'سه شنبه';
            case 5:
                return 'چهارشنبه';
            case 6:
                return 'پنجشنبه';
            case 0:
                return 'جمعه';
        }
    }
}

export default SweetDate;
