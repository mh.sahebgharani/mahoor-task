import AccessManager from "./classes/AccessManager";
let FinalItems=[];
let Items=[
    {
        name: 'ورود به سامانه',
        icon: 'fa fa-sign-in',
        badge: {
            variant: 'info',
        },
        url: '/login',
        access:!AccessManager.UserIsLoggedIn()
    },

    {
        name: 'اطلاعات پایه',
        icon: 'fa fa-database',
        badge: {
            variant: 'info',
        },
        access:AccessManager.UserIsLoggedIn(),
        children:[
            {
                name: 'دسته بندی ها',
                url: '/ecommerce/group',
                icon: 'fa fa-th-large',
                badge: {
                    variant: 'info',
                },
                access:AccessManager.UserIsLoggedIn(),
            },

            {
                name: 'قوانین امتیازدهی ماهانه',
                url: '/ecommerce/buyprise',
                icon: 'fa fa-dollar',
                badge: {
                    variant: 'info',
                },
                access:AccessManager.UserIsLoggedIn(),

            },
            {
                name: 'زمان های ارسال',
                url: '/ecommerce/sendtime',
                icon: 'fa fa-clock-o',
                badge: {
                    variant: 'info',
                },
                access:AccessManager.UserIsLoggedIn(),

            },
            {
                name: 'درجه بندی ها',
                url: '/common/fieldvalue/quality',
                icon: 'fa fa-asterisk',
                badge: {
                    variant: 'info',
                },
                access:AccessManager.UserIsLoggedIn(),

            },
        ]},
    {
        name: 'امور اجرایی',
        icon: 'fa fa-bolt',
        badge: {
            variant: 'info',
        },
        access:AccessManager.UserIsLoggedIn(),
        children:[
            {
                name: 'دیده بان محصولات',
                url: '/ecommerce/product/monitor',
                icon: 'fa fa-heartbeat',
                badge: {
                    variant: 'info',
                },
                access:AccessManager.UserIsLoggedIn(),

            },
            {
                name: 'سفارشات',
                icon: 'fa fa-shopping-cart',
                badge: {
                    variant: 'info',
                },
                access:AccessManager.UserIsLoggedIn(),
                children:[
                    {
                    name: 'سفارشات جدید',
                    url: '/ecommerce/order/new/0/2',
                    icon: 'fa fa-shopping-cart',
                    badge: {
                        variant: 'info',
                    },
                    access:AccessManager.UserIsLoggedIn(),

                },
                    {
                        name: 'سفارشات ارسال شده',
                        url: '/ecommerce/order/sent/0/3',
                        icon: 'fa fa-shopping-cart',
                        badge: {
                            variant: 'info',
                        },
                        access:AccessManager.UserIsLoggedIn(),

                    },
                    {
                        name: 'سفارشات تحویل شده',
                        url: '/ecommerce/order/delivered/0/4',
                        icon: 'fa fa-shopping-cart',
                        badge: {
                            variant: 'info',
                        },
                        access:AccessManager.UserIsLoggedIn(),

                    },
                    {
                        name: 'تمام سفارشات',
                        url: '/ecommerce/order/all/0/0',
                        icon: 'fa fa-shopping-cart',
                        badge: {
                            variant: 'info',
                        },
                        access:AccessManager.UserIsLoggedIn(),

                    },
                ],
            },

            {
                name: 'مشتریان',
                url: '/ecommerce/ecommerceuser',
                icon: 'fa fa-users',
                badge: {
                    variant: 'info',
                },
                access:AccessManager.UserIsLoggedIn(),

            },
            {
                name: 'رانندگان',
                url: '/ecommerce/delivery',
                icon: 'fa fa-car',
                badge: {
                    variant: 'info',
                },
                access:AccessManager.UserIsLoggedIn(),

            },
            {
                name: 'مرکز پیام',
                url: '/common/systemmessage',
                icon: 'fa fa-bullhorn',
                badge: {
                    variant: 'info',
                },
                access:AccessManager.UserIsLoggedIn(),

            }]},

    {
        name: 'مدیریت',
        icon: 'fa fa-cogs',
        badge: {
            variant: 'info',
        },
        access:AccessManager.UserIsLoggedIn(),
        children:[
            {
                name: 'محصولات',
                icon: 'fa fa-leaf',
                badge: {
                    variant: 'info',
                },
                access:AccessManager.UserIsLoggedIn(),
                children:[
                    {
                        name: 'محصولات موجود',
                        url: '/ecommerce/product/stockstate/1',
                        icon: 'fa fa-leaf',
                        badge: {
                            variant: 'info',
                        },
                        access:AccessManager.UserIsLoggedIn(),

                    },
                    {
                        name: 'تمام محصولات',
                        url: '/ecommerce/product',
                        icon: 'fa fa-leaf',
                        badge: {
                            variant: 'info',
                        },
                        access:AccessManager.UserIsLoggedIn(),

                    },
                ]},
            {
                name: 'گزارشات',
                icon: 'fa fa-cog',
                badge: {
                    variant: 'info',
                },
                access:AccessManager.UserIsLoggedIn(),
                children:[
                    {
                        name: 'گزارشات رانندگان',
                        url: '/ecommerce/report/delivery',
                        icon: 'fa fa-leaf',
                        badge: {
                            variant: 'info',
                        },
                        access:AccessManager.UserIsLoggedIn(),

                    },
                    {
                        name: 'گزارشات انواع پرداخت',
                        url: '/ecommerce/report/paymenttype',
                        icon: 'fa fa-leaf',
                        badge: {
                            variant: 'info',
                        },
                        access:AccessManager.UserIsLoggedIn(),

                    },
                    {
                        name: 'گزارشات انواع سفارش',
                        url: '/ecommerce/report/ordertype',
                        icon: 'fa fa-leaf',
                        badge: {
                            variant: 'info',
                        },
                        access:AccessManager.UserIsLoggedIn(),

                    },
                    {
                        name: 'گزارشات منطقه ای',
                        url: '/ecommerce/report/region',
                        icon: 'fa fa-leaf',
                        badge: {
                            variant: 'info',
                        },
                        access:AccessManager.UserIsLoggedIn(),

                    },
                    {
                        name: 'گزارشات مشتریان',
                        url: '/ecommerce/ecommerceuser',
                        icon: 'fa fa-leaf',
                        badge: {
                            variant: 'info',
                        },
                        access:AccessManager.UserIsLoggedIn(),
                    },
                    {
                        name: 'گزارشگیری پیشرفته',
                        url: '/ecommerce/report/full',
                        icon: 'fa fa-leaf',
                        badge: {
                            variant: 'info',
                        },
                        access:AccessManager.UserIsLoggedIn(),
                    },
                ],
            },
            {
                name: 'کدهای تخفیف',
                url: '/ecommerce/discountcode',
                icon: 'fa fa-usd',
                badge: {
                    variant: 'info',
                },
                access:AccessManager.UserIsLoggedIn(),
            },{
                name: 'راهنمای خرید',
                url: '/content/page/management/1',
                icon: 'fa fa-question-circle-o',
                badge: {
                    variant: 'info',
                },
                access:AccessManager.UserIsLoggedIn(),
            },
            {
                name: 'تنظیمات',
                url: '/common/parameter',
                icon: 'fa fa-cog',
                badge: {
                    variant: 'info',
                },
                access:AccessManager.UserIsLoggedIn(),
            },
        ]},
    {
        name: 'تغییر رمز',
        url: '/changepass',
        icon: 'fa fa-lock',
        badge: {
            variant: 'info',
        },
        access:AccessManager.UserIsLoggedIn(),

    },
    {
        name: 'خروج',
        url: '/login/signout',
        icon: 'fa fa-sign-out',
        badge: {
            variant: 'info',
        },
        access:true

    }
];
for(let i=0;i<Items.length;i++)
{
    if(Items[i].access==true)
        FinalItems.push(Items[i]);
}
export default {
    items: FinalItems
};
