// @flow

import * as React from "react";

class Constants {
    static SERVERMODE_LARAVEL=1;
    static SERVERMODE_ASP=2;
    static DELETE_METHOD_MODE_NORMAL=0;
    static DELETE_METHOD_MODE_PUT=1;
    static DefaultPageSize=10;
    static Debugging=true;
    static DeleteMethodMode=Constants.DELETE_METHOD_MODE_NORMAL;
    static MaxProductKinds=3;
    static ServerMode=Constants.SERVERMODE_LARAVEL;
    static RequestTimeOut=60000;
    static onFetchError=null;
    static Cluster='ap2';
    // static SiteURL="http://77.104.83.68:813";
    // static SiteURL="http://laravel.test";
    // static SiteURL="http://vitamin-park.com/api/public";
    // static SiteURL="http://192.168.43.2";
    // static SiteURL="http://192.168.1.101";
    // static SiteURL="http://arya.test";
/*
    static menuName="arya";
    static SiteURL="http://api.aryiaei.com";
    static LoginLogo=require('../assets/img/loginlogoarya.png');
    static DisplayRegionSendTimeTariffs=true;
    static PusherAppKey='2bd8ed2478cff707c231';//Aryiaei
    /*/
    /*
    */
      static menuName="arya";
      static SiteURL="http://api.aryiaei.com";
      static LoginLogo=require('../assets/img/loginlogoarya.png');
      static DisplayRegionSendTimeTariffs=true;
      static PusherAppKey='2bd8ed2478cff707c231';//Aryiaei
         // *** bblounge *** //
        // static menuName="rap";
        // static SiteURL="https://api.bblounge.ir";
        // static LoginLogo=require('../assets/img/bblanj.png');
        // static DisplayRegionSendTimeTariffs=false;
        // static PusherAppKey='bbd70887a14d5cfba321';//VitaminPark
        //


        // // *** Kilidar *** //
        // static menuName="rap";
        // static SiteURL="https://api.ghazakade-kilidar.ir";
        // static LoginLogo=require('../assets/img/kilider.png');
        // static DisplayRegionSendTimeTariffs=false;
        // static PusherAppKey='2b638f67b0185bf08910';//VitaminPark
        // static Cluster='ap2';
        //
        // /*

        // // *** trapp *** //
        // static menuName="trapp";
        // static SiteURL="http://panelapi.trapp.ir";
        // static LoginLogo=require('../assets/img/trapp_logo.png');
        // static DisplayRegionSendTimeTariffs=false;
        // static PusherAppKey='bbd70887a14d5cfba321';//VitaminPark
        // /*
        // */
/*
    static menuName="vp";
    static SiteURL="https://api.vitamin-park.com";
    static LoginLogo=require('../assets/img/loginlogo.png');
    static DisplayRegionSendTimeTariffs=false;
    static PusherAppKey='bbd70887a14d5cfba321';//VitaminPark
    /*
    */
/*
    static menuName="crm";
    static SiteURL="http://api.1tx.ir";
    static LoginLogo=require('../assets/img/loginlogosweet.png');
    static DisplayRegionSendTimeTariffs=false;
    static PusherAppKey='2bd8ed2478cff707c231';
    /*
    // static SiteURL="http://localhost";
    // static MaxProductKinds=1;
    // static SiteURL="http://yaraplus.sweetsoft.ir";
    // static SiteURL="http://contact.sweetsoft.ir";
    // static SiteURL="https://trapp.sweetsoft.ir";
    // static SiteURL="http://mytrapp.ir";
    // static ServerMode=Constants.SERVERMODE_ASP;
*/
}

export default Constants;
