@servers(['local' => '127.0.0.1','server1' => 'root@194.5.175.91 -p 3031'])

@story('deploy')
build
clean old dist
copy new build
@endstory

@task('build', ['on' => 'local'])
npm run build
@endtask

@task('clean old dist', ['on' => 'server1'])
cd /var/www/ghazakade-kilidar/panel
rm -rf ./*
@endtask

@task('copy new build', ['on' => 'local'])
rsync -av -e "ssh -p 3031" ../build/ root@194.5.175.91:/var/www/ghazakade-kilidar/panel
@endtask

@task('fast-test', ['on' => 'server1'])
pwd
ls
@endtask
